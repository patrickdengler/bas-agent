﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace POC
{
    class Program
    {
        
        static void Main(string[] args)
        {

            Console.WriteLine("Started.");
            AWSPayloadService deviceToAWS = new AWSPayloadService();
            SensorComm sensorComm = new SensorComm(deviceToAWS);
            while (Statics.CONTINUE)
            {

                List<SensorData> results = sensorComm.Poll();
                if (results.Count > 0)
                {
                    deviceToAWS.Push(results);
                }
                Thread.Sleep(1000);

            }
           


        }
    }
}
