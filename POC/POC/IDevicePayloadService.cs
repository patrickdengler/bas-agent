﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POC
{
    public interface IDevicePayloadService
    {
        void Push(List<SensorData> results);
        
    }
}
