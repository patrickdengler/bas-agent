﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using M2Mqtt;
using M2Mqtt.Messages;
using M2Mqtt.Net;
using M2Mqtt.Utility;
using M2Mqtt.Internal;
using System.Threading;
using System.IO;


namespace POC
{
    public class AWSPayloadService : IDevicePayloadService
    {
        public static string AWS_IOT_MQTT_HOST = "a1le9r6mlwsepu-ats.iot.us-east-1.amazonaws.com"; ///< Customer specific MQTT HOST. The same will be used for Thing Shadow
        public static int AWS_IOT_MQTT_PORT = 8883;// 443; ///< default port for MQTT/S
        public static string AWS_IOT_MQTT_CLIENT_ID = "patrickd_pi"; ///< MQTT client ID should be unique for every device
        public static string AWS_IOT_MY_THING_NAME = "patrickd_pi"; ///< Thing Name of the Shadow this device is associated with
        public static string AWS_IOT_ROOT_CA_FILENAME = @"./Certs/root-CA.crt"; ///< Root CA file name
        
        public static string AWS_IOT_CERTIFICATE_FILENAME = @"./Certs/dotnet_devicecertificate.pfx"; ///< device signed certificate file name openssl pkcs12 -export -in dotnet-devicecertificate.pem -inkey dotnet-private.pem.key -out dotnet_devicecertificate.pfx -certfile root-CA.crt
        public static string AWS_IOT_PRIVATE_KEY_FILENAME = @"./Certs/dotnet-private.pem.key"; ///< Device private key filename

        X509Certificate CaCert;
        X509Certificate2  ClientCert;
        MqttClient IotClient;
        public AWSPayloadService()
        {

            if (!File.Exists(AWS_IOT_ROOT_CA_FILENAME))
            {
                Console.WriteLine("Cannot find file " + AWS_IOT_ROOT_CA_FILENAME);
            }
            if (!File.Exists(AWS_IOT_CERTIFICATE_FILENAME))
            {
                Console.WriteLine("Cannot find file " + AWS_IOT_CERTIFICATE_FILENAME);
            }
            CaCert = X509Certificate.CreateFromCertFile(AWS_IOT_ROOT_CA_FILENAME );
            ClientCert = new X509Certificate2(AWS_IOT_CERTIFICATE_FILENAME,"Password1");// @"C:\Iotdevices\dotnetdevice\dotnet_devicecertificate.pfx", "password1");

            IotClient = new MqttClient(AWS_IOT_MQTT_HOST, AWS_IOT_MQTT_PORT, true, CaCert, ClientCert, MqttSslProtocols.TLSv1_2);

            IotClient.Connect(AWS_IOT_MQTT_CLIENT_ID);
            Console.WriteLine("Connected to AWS IOT");

        }


        public void Push(List<SensorData> data)
        {
            Console.WriteLine("AWS IOT Dotnet core message publiser starting");
            string Topic = "Hello/World";

            foreach (SensorData message in data)
            {
                // string rawData = JsonConvert.SerializeObject(data); JSON Not holding
                string rawData = message.Building + "," + message.DeviceId + "," + message.RawData;
                IotClient.Publish(Topic, Encoding.UTF8.GetBytes(rawData));
                Console.WriteLine("Message published");
            }

        }
    }
}
