﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POC
{
    public class SensorComm
    {
        IDevicePayloadService devicePayloadService;

        public SensorComm(IDevicePayloadService _devicePayloadService)
        {
            devicePayloadService =  _devicePayloadService;
        }
        

    public List<SensorData> Poll()
        {
            List<SensorData> sensorData = new List<SensorData>();
            sensorData.Add(new SensorData()
            {
                Building = "Building1",
                DeviceId = "Device1",
                RawData = 100,
            });
            return sensorData;

        }
        
}
    public class SensorData
    {
        public string Building { get; set; }
        public string DeviceId { get; set; }
        public double RawData { get; set; }
    }
    
}
