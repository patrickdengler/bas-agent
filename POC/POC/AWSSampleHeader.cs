﻿using System;
using System.Collections.Generic;
using System.Text;

namespace POC
{
    enum IoT_Error_t
    {
        NETWORK_PHYSICAL_LAYER_CONNECTED = 6, NETWORK_MANUALLY_DISCONNECTED = 5, NETWORK_ATTEMPTING_RECONNECT = 4, NETWORK_RECONNECTED = 3,
        MQTT_NOTHING_TO_READ = 2, MQTT_CONNACK_CONNECTION_ACCEPTED = 1, SUCCESS = 0, FAILURE = -1,
        NULL_VALUE_ERROR = -2, TCP_CONNECTION_ERROR = -3, SSL_CONNECTION_ERROR = -4, TCP_SETUP_ERROR = -5,
        NETWORK_SSL_CONNECT_TIMEOUT_ERROR = -6, NETWORK_SSL_WRITE_ERROR = -7, NETWORK_SSL_INIT_ERROR = -8, NETWORK_SSL_CERT_ERROR = -9,
        NETWORK_SSL_WRITE_TIMEOUT_ERROR = -10, NETWORK_SSL_READ_TIMEOUT_ERROR = -11, NETWORK_SSL_READ_ERROR = -12, NETWORK_DISCONNECTED_ERROR = -13,
        NETWORK_RECONNECT_TIMED_OUT_ERROR = -14, NETWORK_ALREADY_CONNECTED_ERROR = -15, NETWORK_MBEDTLS_ERR_CTR_DRBG_ENTROPY_SOURCE_FAILED = -16, NETWORK_SSL_UNKNOWN_ERROR = -17,
        NETWORK_PHYSICAL_LAYER_DISCONNECTED = -18, NETWORK_X509_ROOT_CRT_PARSE_ERROR = -19, NETWORK_X509_DEVICE_CRT_PARSE_ERROR = -20, NETWORK_PK_PRIVATE_KEY_PARSE_ERROR = -21,
        NETWORK_ERR_NET_SOCKET_FAILED = -22, NETWORK_ERR_NET_UNKNOWN_HOST = -23, NETWORK_ERR_NET_CONNECT_FAILED = -24, NETWORK_SSL_NOTHING_TO_READ = -25,
        MQTT_CONNECTION_ERROR = -26, MQTT_CONNECT_TIMEOUT_ERROR = -27, MQTT_REQUEST_TIMEOUT_ERROR = -28, MQTT_UNEXPECTED_CLIENT_STATE_ERROR = -29,
        MQTT_CLIENT_NOT_IDLE_ERROR = -30, MQTT_RX_MESSAGE_PACKET_TYPE_INVALID_ERROR = -31, MQTT_RX_BUFFER_TOO_SHORT_ERROR = -32, MQTT_TX_BUFFER_TOO_SHORT_ERROR = -33,
        MQTT_MAX_SUBSCRIPTIONS_REACHED_ERROR = -34, MQTT_DECODE_REMAINING_LENGTH_ERROR = -35, MQTT_CONNACK_UNKNOWN_ERROR = -36, MQTT_CONNACK_UNACCEPTABLE_PROTOCOL_VERSION_ERROR = -37,
        MQTT_CONNACK_IDENTIFIER_REJECTED_ERROR = -38, MQTT_CONNACK_SERVER_UNAVAILABLE_ERROR = -39, MQTT_CONNACK_BAD_USERDATA_ERROR = -40, MQTT_CONNACK_NOT_AUTHORIZED_ERROR = -41,
        JSON_PARSE_ERROR = -42, SHADOW_WAIT_FOR_PUBLISH = -43, SHADOW_JSON_BUFFER_TRUNCATED = -44, SHADOW_JSON_ERROR = -45,
        MUTEX_INIT_ERROR = -46, MUTEX_LOCK_ERROR = -47, MUTEX_UNLOCK_ERROR = -48, MUTEX_DESTROY_ERROR = -49
    }

    public static class AWSSampleHeader
    {
     
        // Get from console
        // =================================================
      
        // =================================================

        // MQTT PubSub
        public static int AWS_IOT_MQTT_TX_BUF_LEN = 512; ///< Any time a message is sent out through the MQTT layer. The message is copied into this buffer anytime a publish is done. This will also be used in the case of Thing Shadow
        public static int AWS_IOT_MQTT_RX_BUF_LEN = 512; ///< Any message that comes into the device should be less than this buffer size. If a received message is bigger than this buffer size the message will be dropped.
        public static int AWS_IOT_MQTT_NUM_SUBSCRIBE_HANDLERS = 5; ///< Maximum number of topic filters the MQTT client can handle at any given time. This should be increased appropriately when using Thing Shadow

        // Thing Shadow specific configs
        public static int SHADOW_MAX_SIZE_OF_RX_BUFFER = (AWS_IOT_MQTT_RX_BUF_LEN + 1); ///< Maximum size of the SHADOW buffer to store the received Shadow message, including terminating NULL byte.
        public static int MAX_SIZE_OF_UNIQUE_CLIENT_ID_BYTES = 80;  ///< Maximum size of the Unique Client Id. For More info on the Client Id refer \ref response "Acknowledgments"
        public static int MAX_SIZE_CLIENT_ID_WITH_SEQUENCE = MAX_SIZE_OF_UNIQUE_CLIENT_ID_BYTES + 10; ///< This is size of the extra sequence number that will be appended to the Unique client Id
        public static int MAX_SIZE_CLIENT_TOKEN_CLIENT_SEQUENCE = MAX_SIZE_CLIENT_ID_WITH_SEQUENCE + 20; ///< This is size of the the total clientToken key and value pair in the JSON
        public static int MAX_ACKS_TO_COMEIN_AT_ANY_GIVEN_TIME = 10; ///< At Any given time we will wait for this many responses. This will correlate to the rate at which the shadow actions are requested
        public static int MAX_THINGNAME_HANDLED_AT_ANY_GIVEN_TIME = 10; ///< We could perform shadow action on any thing Name and this is maximum Thing Names we can act on at any given time
        public static int MAX_JSON_TOKEN_EXPECTED = 120; ///< These are the max tokens that is expected to be in the Shadow JSON document. Include the metadata that gets published
        public static int MAX_SHADOW_TOPIC_LENGTH_WITHOUT_THINGNAME = 60; ///< All shadow actions have to be published or subscribed to a topic which is of the format $aws/things/{thingName}/shadow/update/accepted. This refers to the size of the topic without the Thing Name
        public static int MAX_SIZE_OF_THING_NAME = 20; ///< The Thing Name should not be bigger than this value. Modify this if the Thing Name needs to be bigger
        public static int MAX_SHADOW_TOPIC_LENGTH_BYTES = MAX_SHADOW_TOPIC_LENGTH_WITHOUT_THINGNAME + MAX_SIZE_OF_THING_NAME; ///< This size includes the length of topic with Thing Name

        // Auto Reconnect specific config
        public static int AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL = 1000; ///< Minimum time before the First reconnect attempt is made as part of the exponential back-off algorithm
        public static int AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL = 128000; ///< Maximum time interval after which exponential back-off will stop attempting to reconnect.

        public static bool DISABLE_METRICS = false; ///< Disable the collection of metrics by setting this to true


    }
}
