﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;
using System.Collections.Concurrent;

namespace K2A.Core.Cache
{
    //Usage:
    //MemCache.SetCachedData("test1", "val1", 2);
    //MemCache.GetCachedData<string>("test3")
    public static class MemCache
    {
        private static ConcurrentDictionary<string, object> cacheLocks = new ConcurrentDictionary<string, object>();

        public static T GetCachedData<T>(string cacheKey) where T : class
        {
            //Returns null if the string does not exist, prevents a race condition where the cache invalidates between the contains check and the retreival.
            T cachedData = MemoryCache.Default.Get(cacheKey, null) as T;

            if (cachedData != null)
            {
                return cachedData;
            }

            object cacheLock = GetCacheLock(cacheKey);

            lock (cacheLock)
            {
                //Check to see if anyone wrote to the cache while we where waiting our turn to write the new value.
                return MemoryCache.Default.Get(cacheKey, null) as T;
            }
        }

        public static void SetCachedData(string cacheKey, object cacheValue, int cacheTimePolicyMinutes, bool useSliding = false)
        {
            object cacheLock = GetCacheLock(cacheKey);

            lock (cacheLock)
            {
                CacheItemPolicy cip = new CacheItemPolicy(){AbsoluteExpiration = new DateTimeOffset(DateTime.Now.AddMinutes(cacheTimePolicyMinutes))};

                if (useSliding)
                    cip = new CacheItemPolicy(){ SlidingExpiration = new TimeSpan(0, cacheTimePolicyMinutes, 0)};

                MemoryCache.Default.Set(cacheKey, cacheValue, cip);

            }
        }

        public static object GetCacheLock(string cacheKey)
        {
            object cacheLock;

            lock (cacheLocks)
            {
                if (cacheLocks.ContainsKey(cacheKey))
                    cacheLock = cacheLocks[cacheKey];
                else
                {
                    cacheLock = new object();
                    cacheLocks[cacheKey] = cacheLock;
                }
            }
            return cacheLock;
        }
    }
}
