﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K2A.Core.Helpers
{
    public class Strings
    {
        public static string GenerateUID(bool removeHyphens = false)
        {
            string key = Guid.NewGuid().ToString();

            if (removeHyphens) key = key.Replace("-", "");

            return key;
        }
        public static string GenerateUID(int length = -1)
        {
            string key = Guid.NewGuid().ToString().Replace("-", "");

            if (length < 0) return key;

            while (key.Length < length)
            {
                key = key + Guid.NewGuid().ToString().Replace("-", "");
            }

            key = key.Substring(0, length);

            return key;
        }
    }
}
