﻿namespace K2A.Core
{
    public class App
    {
        public static string StartupPath
        {
            get { return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + System.IO.Path.DirectorySeparatorChar; }
        }

        private static bool monoChecked = false;
        private static bool isRunningMono = false;

        public static bool IsRunningOnMono()
        {
            if (!monoChecked)
            {
                isRunningMono = (System.Type.GetType("Mono.Runtime") != null);
                monoChecked = true;
            }
            return isRunningMono;
        }
 
    }
}