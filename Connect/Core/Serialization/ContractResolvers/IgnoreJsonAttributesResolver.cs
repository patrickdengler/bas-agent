﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K2A.Core.Serialization.ContractResolvers
{
    public class IgnoreJsonAttributesResolver : DefaultContractResolver
    {
        private bool overrideIgnore = true;
        private bool overrideConverter = true;
        private bool overridePropertyName = true;

        public IgnoreJsonAttributesResolver()
        {

        }

        public IgnoreJsonAttributesResolver(bool ignoreAttributeIgnore, bool ignoreAttributeConvert, bool ignoreAttributePropertName)
        {
            overrideConverter = ignoreAttributeConvert;
            overrideIgnore = ignoreAttributeIgnore;
            overridePropertyName = ignoreAttributePropertName;
        }

        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            IList<JsonProperty> props = base.CreateProperties(type, memberSerialization);
            foreach (var prop in props)
            {
                if (overrideIgnore) prop.Ignored = false;   // Ignore [JsonIgnore]
                if (overrideConverter) prop.Converter = null;  // Ignore [JsonConverter]
                if (overridePropertyName) prop.PropertyName = prop.UnderlyingName;  // restore original property name


            }
            return props;
        }
    }
}
