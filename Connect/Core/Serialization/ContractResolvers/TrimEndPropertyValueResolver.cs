﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace K2A.Core.Serialization.ContractResolvers
{
    public class TrimEndPropertyValueResolver : DefaultContractResolver
    {

        public TrimEndPropertyValueResolver()
        {
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty result = base.CreateProperty(member, memberSerialization);

            PropertyInfo property = member as PropertyInfo;

            if (property.PropertyType == typeof(string))
            {
                result.ValueProvider = new StringValueProvider(property);
            }

            return result;
        }
    }

    public class StringValueProvider : IValueProvider
    {
        private PropertyInfo _targetProperty;

        public StringValueProvider(PropertyInfo targetProperty)
        {
            _targetProperty = targetProperty;
        }

        // SetValue gets called by Json.Net during deserialization.
        // The value parameter has the original value read from the JSON;
        // target is the object on which to set the value.
        public void SetValue(object target, object value)
        {
            if (value is string && value != null) value = ((string)value).TrimEnd();
            _targetProperty.SetValue(target, value);
        }

        // GetValue is called by Json.Net during serialization.
        // The target parameter has the object from which to read the value;
        // the return value is what gets written to the JSON
        public object GetValue(object target)
        {
            object value = _targetProperty.GetValue(target);
            return value == null ? null : ((string)value).TrimEnd();
        }
    }
}
