﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace K2A.Core.Timers
{
    /// <summary>
    /// Used to time processes.  Timer starts on creation
    /// </summary>
    public class SimpleProcessTimer : Stopwatch
    {
        public new int ElapsedMilliseconds
        {
            get
            {
                return (int)base.ElapsedMilliseconds;
            }
        }

        public SimpleProcessTimer()
        {
            Start();
        }
        public new void Reset()
        {
           Restart();
        }
        public void WriteDebugAndReset(string taskName = "")
        {
            System.Diagnostics.Debug.Print("{0} - Elapsed Time: {1}", taskName, ElapsedMilliseconds);
            Reset();
            Start();
        }
        public void Stop(string taskName = "")
        {
            if (!string.IsNullOrEmpty(taskName)) System.Diagnostics.Debug.Print("{0} - Elapsed Time: {1}", taskName, ElapsedMilliseconds);
            base.Stop();
        }
        public void Clear()
        {
            base.Reset();
        }
    }
}
