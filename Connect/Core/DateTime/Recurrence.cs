﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace K2A.Core.DateAndTime
{
    public class Recurrence
    {
        public RecurrenceType TypeofRecurrence { get; set; } = RecurrenceType.Weekly;
        public List<int> DaysOfWeek { get; set; } = new List<int>();
        public int Interval { get; set; } = 1;
        public DateTime Time { get; set; }

        public int DayOfMonth { get; set; } = 1;
        public MonthlyWeekSelector MonthlyWeek { get; set; } = MonthlyWeekSelector.First;
        public int MonthlyDayOfWeek { get; set; } = 1;
        public bool MonthlyUseNumericalDay { get; set; } = true;

        public DateTime GetNextOccurrence(DateTime dt)
        {
            if (dt.TimeOfDay > Time.TimeOfDay) return GetNextOccurrence(new DateTime(dt.Year, dt.Month, dt.Day).AddDays(1)); //If time has passed, it can't occur today

            if (TypeofRecurrence == RecurrenceType.Weekly)
            {
                int weekOfYear = dt.WeekOfYear();
                if ((weekOfYear % Interval) == 0)
                {
                    var thisWeek = (from d in DaysOfWeek where d >= (int)dt.DayOfWeek select d).ToList();
                    if (thisWeek != null && thisWeek.Count > 0)
                    {
                        return new DateTime(dt.Year, dt.Month, dt.Day, Time.Hour, Time.Minute, Time.Second).AddDays(thisWeek.Min() - (int)dt.DayOfWeek);
                    }
                    else
                    {
                        return new DateTime(dt.Year, dt.Month, dt.Day, Time.Hour, Time.Minute, Time.Second).AddDays((7 - (int)dt.DayOfWeek) + DaysOfWeek.Min());
                    }
                }
                else
                {
                    DateTime newDt = new DateTime(dt.Year, dt.Month, dt.Day).AddDays(7 - (int)dt.DayOfWeek);
                    return GetNextOccurrence(newDt);
                }
            }
            else if (TypeofRecurrence == RecurrenceType.Monthly)
            {
                if ((dt.Month % Interval) != 0)
                {
                    var nextMonth = dt.AddMonths(1);
                    return GetNextOccurrence(new DateTime(nextMonth.Year, nextMonth.Month, 1));
                }

                if (MonthlyUseNumericalDay)
                {
                    if (dt.Day > DayOfMonth)
                    {
                        var nextMonth = dt.AddMonths(1);
                        return new DateTime(nextMonth.Year, nextMonth.Month, DayOfMonth, Time.Hour, Time.Minute, Time.Second);
                    }
                    else
                    {
                        return new DateTime(dt.Year, dt.Month, DayOfMonth, Time.Hour, Time.Minute, Time.Second);
                    }
                }
                else
                {
                    var nextRelative = GetRelativeMonthly(dt);
                    if (nextRelative < dt)
                        return GetNextOccurrence(new DateTime(dt.AddMonths(1).Year, dt.AddMonths(1).Month, 1));
                    else
                        return nextRelative;
                }
            }
            return new DateTime();
        }
        public Recurrence ()
        {
            DaysOfWeek = new List<int>();
        }

        private DateTime GetRelativeMonthly(DateTime dt)
        {
            switch (MonthlyWeek)
            {
                case MonthlyWeekSelector.First:
                    return GetFirst(MonthlyDayOfWeek, dt);
                case MonthlyWeekSelector.Second:
                    return GetFirst(MonthlyDayOfWeek, dt).AddDays(7);
                case MonthlyWeekSelector.Third:
                    return GetFirst(MonthlyDayOfWeek, dt).AddDays(14);
                case MonthlyWeekSelector.Fourth:
                    return GetFirst(MonthlyDayOfWeek, dt).AddDays(21);
                case MonthlyWeekSelector.Last:
                    return GetLast(MonthlyDayOfWeek, dt);
            }
            return new DateTime(); //not possible
        }
        private DateTime GetFirst(int dayOfWeek, DateTime dt)
        {
            var first = new DateTime(dt.Year, dt.Month, 1);

            var fOc = (int)first.DayOfWeek == dayOfWeek ? first : first.AddDays(dayOfWeek - (int)first.DayOfWeek);
            if (fOc.Month != dt.Month) fOc = fOc.AddDays(7);

            return fOc;
        }
        private DateTime GetLast(int dayOfWeek, DateTime dt)
        {
            var first = GetFirst(dayOfWeek, dt);

            while (first.AddDays(7).Month == dt.Month)
            {
                first = first.AddDays(7);
            }

            return first;
        }
        public enum MonthlyWeekSelector
        {
            First,
            Second,
            Third,
            Fourth,
            Last
        }
        public enum RecurrenceType
        {
            Weekly,
            Monthly
        }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static Recurrence Deserialize(string s)
        {
            try
            {
                return JsonConvert.DeserializeObject<Recurrence>(s);
            }
            catch
            {
                return new Recurrence();
            }
        }
    }
}
