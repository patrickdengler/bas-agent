﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using K2A.Core;
namespace K2A.Core.DateAndTime
{
    public class TimezoneAbbreviations
    {
        public static List<TimezoneObject> GetList()
        {
            try
            {
                // todo check ToStringFromArray : patrickd
                return JsonConvert.DeserializeObject<List<TimezoneObject>>(Resources.tz.ToStringFromArray());
            }
            catch
            {
                return new List<TimezoneObject>();
            }
        }

        public static TimezoneObject GetTimeZoneObject(TimeZoneInfo tzInfo)
        {
            string tzid = tzInfo.Id;

            var timezones = GetList();

            var tz = (from t in timezones where t.Name.ToLower() == tzid.ToLower() select t).FirstOrDefault();

            if (tz == null)
                return new TimezoneObject() { Daylight = tzid, Standard = tzid, Display = tzid, Name = tzid };
            else
                return tz;
        }

        public static TimezoneObject GetLocalTimezone()
        {
            var tzInfo = TimeZoneInfo.Local;
            string tzid = tzInfo.Id;

            var timezones = GetList();

            var tz = (from t in timezones where t.Name.ToLower() == tzid.ToLower() select t).FirstOrDefault();

            if (tz == null)
                return new TimezoneObject() { Daylight = tzid, Standard = tzid, Display = tzid, Name = tzid };
            else
                return tz;
        }
        public class TimezoneObject
        {
            public string Name { get; set; }
            public string Display { get; set; }
            public string Daylight { get; set; }
            public string Standard { get; set; }
        }
    }
}
