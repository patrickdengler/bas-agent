﻿using System;
using Newtonsoft.Json;

namespace K2A.Core.DateAndTime
{

    public class DateRange
    {

        public bool UseRelativeDate { get; set; }

        public bool RelativeCurrent { get; set; }

        public int RelativeCount { get; set; }

        public RangeInterval RelativeInterval { get; set; }

        public bool RelativeIncludeCurrent { get; set; }

        public int RelativeStartsOn { get; set; }

        private DateTime _End;
        private DateTime _Start;

        public DateTime Start
        {
            get
            {
                if (!UseRelativeDate)
                    return _Start;
                else
                    return GetRelativeStartDate();
            }
            set
            {
                _Start = value;
            }
        }

        public DateTime End
        {
            get
            {
                if (!UseRelativeDate)
                    return _End;
                else
                    return GetRelativeEndDate();
            }
            set
            {
                _End = value;
            }
        }

        public DateRange(bool createDefault)
        {
            Start = DateTime.Now.AddDays(-1);
            End = DateTime.Now;
            UseRelativeDate = true;
            RelativeCount = 1;
            RelativeCurrent = true;
            RelativeInterval = RangeInterval.Day;
            RelativeIncludeCurrent = true;
            RelativeStartsOn = 1;
        }

        public DateRange()
        {
            if (PreviousDateRange == null)
            {
                PreviousDateRange = new DateRange(true);
            }

            _Start = PreviousDateRange.Start;
            _End = PreviousDateRange.End;

            UseRelativeDate = PreviousDateRange.UseRelativeDate;
            RelativeCount = PreviousDateRange.RelativeCount;
            RelativeCurrent = PreviousDateRange.RelativeCurrent;
            RelativeInterval = PreviousDateRange.RelativeInterval;
            RelativeIncludeCurrent = PreviousDateRange.RelativeIncludeCurrent;
            RelativeStartsOn = PreviousDateRange.RelativeStartsOn;
        }

        public DateRange(DateTime startDt, DateTime endDt)
        {
            _Start = startDt;
            _End = endDt;

            UseRelativeDate = false;
        }

        public static DateRange PreviousDateRange { get; set; }

        public override string ToString()
        {
            return Start + " To " + End;
        }

        public TimeSpan Duration
        {
            get
            {
                if (End != null && Start != null)
                    return (End - Start);
                else
                    return new TimeSpan();
            }
        }

        public string Description()
        {
            if (!UseRelativeDate) return "Literal Date Range";

            if (RelativeCurrent) return "Current " + RelativeInterval.ToString();

            string relString = String.Format("Previous {0} {1}(s)", RelativeCount, RelativeInterval);
            if (RelativeIncludeCurrent)
                relString += " including Current " + RelativeInterval.ToString();

            return relString;
        }

        private DateTime GetRelativeStartDate()
        {
            switch (RelativeInterval)
            {
                case RangeInterval.Minute:
                    if (RelativeCurrent)
                        return DateTime.Now.AddSeconds(0 - DateTime.Now.Second);
                    else
                        return DateTime.Now.AddSeconds(0 - DateTime.Now.Second).AddMinutes(0 - RelativeCount);
                case RangeInterval.Hour:
                    if (RelativeCurrent)
                        return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);
                    else
                        return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0).AddHours(0 - RelativeCount);
                case RangeInterval.Day:
                    if (RelativeCurrent)
                        return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    else
                        return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).AddDays(0 - RelativeCount);
                case RangeInterval.Week:
                    if (RelativeCurrent)
                        return DateTime.Now.StartOfWeek((DayOfWeek)RelativeStartsOn);
                    else
                        return DateTime.Now.StartOfWeek((DayOfWeek)RelativeStartsOn).AddDays(-7 * RelativeCount);
                case RangeInterval.Month:
                    if (RelativeCurrent)
                        return DateTime.Now.StartOfMonth(RelativeStartsOn);
                    else
                        return DateTime.Now.StartOfMonth(RelativeStartsOn).AddMonths(0 - RelativeCount);
                case RangeInterval.Year:
                    if (RelativeCurrent)
                        return new DateTime(DateTime.Now.Year, 1, 1);
                    else
                        return new DateTime(DateTime.Now.Year, 1, 1).AddYears(0 - RelativeCount);
                default:
                    return DateTime.Now;
            }
        }

        private DateTime GetRelativeEndDate()
        {
            if (RelativeCurrent | RelativeIncludeCurrent)
                return DateTime.Now;
            else
            {
                switch (RelativeInterval)
                {
                    case RangeInterval.Minute:
                        return DateTime.Now.AddSeconds(-1);
                    case RangeInterval.Hour:
                        return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0).AddSeconds(-1);
                    case RangeInterval.Day:
                        return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).AddSeconds(-1);
                    case RangeInterval.Week:
                        return DateTime.Now.StartOfWeek((DayOfWeek)RelativeStartsOn).AddSeconds(-1);
                    case RangeInterval.Month:
                        return DateTime.Now.StartOfMonth(RelativeStartsOn).AddSeconds(-1);
                    case RangeInterval.Year:
                        return new DateTime(DateTime.Now.Year, 1, 1).AddSeconds(-1);
                    default:
                        return DateTime.Now;
                }
            }
        }

        public DateRange Clone()
        {
            return JsonConvert.DeserializeObject<DateRange>(JsonConvert.SerializeObject(this));
        }

        public enum RangeInterval
        {
            Minute,
            Hour,
            Day,
            Week,
            Month,
            Year
        };
    }
}