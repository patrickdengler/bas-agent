﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K2A.Core
{
    public class Encryption
    {
        private static System.Text.Encoding textEncoding = System.Text.Encoding.UTF8;

        public static string Encrypt(string plainData, string passphrase)
        {
            
            return Convert.ToBase64String(Encrypt(textEncoding.GetBytes(plainData), passphrase));
        }

        public static string Decrypt(string encData, string passphrase)
        {
            return textEncoding.GetString(Decrypt(Convert.FromBase64String(encData), passphrase));
        }

        public static byte[] Encrypt(byte[] plainData, string passphrase)
        {
            byte[] Result = null;

            try
            {
                System.Security.Cryptography.RijndaelManaged Enc = new System.Security.Cryptography.RijndaelManaged();
                Enc.KeySize = 256;
                Enc.Key = Encryption_Key(passphrase);
                Enc.IV = Encryption_IV(passphrase);

                MemoryStream memoryStream = new MemoryStream();
                System.Security.Cryptography.CryptoStream cryptoStream = null;
                cryptoStream = new System.Security.Cryptography.CryptoStream(memoryStream, Enc.CreateEncryptor(), System.Security.Cryptography.CryptoStreamMode.Write);
                cryptoStream.Write(plainData, 0, plainData.Length);
                cryptoStream.FlushFinalBlock();
                Result = memoryStream.ToArray();
                cryptoStream.Close();
                memoryStream.Close();
                cryptoStream.Dispose();
                memoryStream.Dispose();
            }
            catch
            {
                Result = null;
            }

            return Result;
        }

        public static byte[] Decrypt(byte[] EncData, string passphrase)
        {
            byte[] Result = null;

            try
            {
                System.Security.Cryptography.RijndaelManaged Enc = new System.Security.Cryptography.RijndaelManaged();
                Enc.KeySize = 256;
                Enc.Key = Encryption_Key(passphrase);
                Enc.IV = Encryption_IV(passphrase);

                MemoryStream memoryStream = new MemoryStream(EncData);
                System.Security.Cryptography.CryptoStream cryptoStream = null;
                cryptoStream = new System.Security.Cryptography.CryptoStream(memoryStream, Enc.CreateDecryptor(), System.Security.Cryptography.CryptoStreamMode.Read);

                byte[] TempDecryptArr = null;
                TempDecryptArr = new byte[EncData.Length + 1];
                int decryptedByteCount = 0;
                decryptedByteCount = cryptoStream.Read(TempDecryptArr, 0, EncData.Length);

                cryptoStream.Close();
                memoryStream.Close();
                cryptoStream.Dispose();
                memoryStream.Dispose();

                Result = new byte[decryptedByteCount];
                Array.Copy(TempDecryptArr, Result, decryptedByteCount);
            }
            catch
            {
                Result = null;
            }

            return Result;
        }

        private static byte[] Encryption_Key(string passphrase)
        {
            //Generate a byte array of required length as the encryption key.
            //A SHA256 hash of the passphrase has just the required length. It is used twice in a manner of self-salting.
            System.Security.Cryptography.SHA256Managed SHA256 = new System.Security.Cryptography.SHA256Managed();
            string L1 = System.Convert.ToBase64String(SHA256.ComputeHash(textEncoding.GetBytes(passphrase)));
            string L2 = passphrase + L1;
            byte[] Result = SHA256.ComputeHash(textEncoding.GetBytes(L2));
            return Result;
        }

        private static byte[] Encryption_IV(string passphrase)
        {
            //Generate a byte array of required length as the iv.
            //A MD5 hash of the passphrase has just the required length. It is used twice in a manner of self-salting.
            System.Security.Cryptography.MD5CryptoServiceProvider MD5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            string L1 = System.Convert.ToBase64String(MD5.ComputeHash(textEncoding.GetBytes(passphrase)));
            string L2 = passphrase + L1;
            byte[] Result = MD5.ComputeHash(textEncoding.GetBytes(L2));
            return Result;
        }
    }
}
