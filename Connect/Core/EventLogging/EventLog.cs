﻿using System;
using System.Data;
using System.Data.Odbc;
using System.IO;

namespace K2A.Core
{
    public class Logging
    {
        public static void Log(string Message, EntryType entryType)
        {
            if (entryType == EntryType.Error)
                LogFiles.Log.Error(Message);
            else if (entryType == EntryType.Info)
                LogFiles.Log.Info(Message);
            else if (entryType == EntryType.Trace)
            {
                LogFiles.Log.Trace(Message);
                System.Diagnostics.Debug.Print(Message);
            }
                

        }

        public static void Log(string Message, EntryType entryType, params object[] args)
        {
            Log(string.Format(Message, args), entryType);
        }

        public enum EntryType
        {
            Error,
            Info,
            Trace
        }


    }
}