﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Linq;
using System.IO.Compression;
using System.Data;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using K2A.Core.Serialization.ContractResolvers;

namespace K2A.Core
{
    public static class Extensions
    {
        private static Regex ry = new Regex(@"([A-Z][a-z]+|[A-Z]+[A-Z]|[A-Z]|[^A-Za-z]+[^A-Za-z])", RegexOptions.RightToLeft);

        #region "Strings"

        /// <summary>
        /// Create Hash from a string
        /// </summary>
        /// <param name="raw"></param>
        /// <returns>Hashed string</returns>
        public static string HashText(this string text, string salt)
        {
            var hasher = new SHA1CryptoServiceProvider();
            byte[] textWithSaltBytes = Encoding.UTF8.GetBytes(string.Concat(text, salt.ToLower()));
            byte[] hashedBytes = hasher.ComputeHash(textWithSaltBytes);
            hasher.Clear();
            return Convert.ToBase64String(hashedBytes);
        }

        /// <summary>
        /// Execute a command and return the output as a string
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public static string CommandToString(this string filename, string arguments = "")
        {
            string output = "";
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = filename;
                process.StartInfo.Arguments = arguments;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();
                //* Read the output (or the error)
                output = process.StandardOutput.ReadToEnd();
                //Console.WriteLine(output);
                string err = process.StandardError.ReadToEnd();
                Console.WriteLine(err);
                process.WaitForExit();
            }
            catch { }

            return output;
        }
        /// <summary>
        /// Execute a command
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public static void RunCommand(this string filename, string arguments = "")
        {

            try
            {
                Process process = new Process();
                process.StartInfo.FileName = filename;
                process.StartInfo.Arguments = arguments;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();
            }
            catch { }
        }

        /// <summary>
        /// Returns the full path to a file in the local directory
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static string PathToLocalFile(this string FileName)
        {
            return App.StartupPath + FileName;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static string FileToString(this string FileName)
        {
            if (!File.Exists(FileName)) return "";
            try
            {
                using (var fileStream = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (var textReader = new StreamReader(fileStream))
                {
                    return textReader.ReadToEnd();
                }
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="StrToSave"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static bool ToFile(this string StrToSave, string FileName)
        {
            try
            {
                System.IO.File.WriteAllText(FileName, StrToSave);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="maxLen"></param>
        /// <returns></returns>
        public static byte[] ToByteArray(this string s, int maxLen = -1)
        {
            if (maxLen > 0 && s.Length > maxLen)
            {
                s = s.Substring(0, maxLen);
            }
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ba"></param>
        /// <param name="maxLen"></param>
        /// <returns></returns>
        public static string ToStringFromArray(this byte[] ba, int maxLen = -1)
        {
            string s;
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            s = encoding.GetString(ba).Trim();
            if (maxLen > 0 && s.Length > maxLen)
            {
                s = s.Substring(0, maxLen);
            }
            return s;
        }

        public static String[] NameToArray(this string source)
        {

            string result = ry.Replace(source, "_$1");

            result = result.Replace(" ", "_").Replace("-", "_").ToLower();
            while (result.Contains("__"))
            {
                result = result.Replace("__", "_");
            }

            var segments = result.Split('_');

            return segments.Where(d => d.Length > 0).ToArray();
        }

        public static string JsonToAgens(this string json)
        {
            var obj = JObject.Parse(json);

            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            using (JsonTextWriter writer = new JsonTextWriter(sw))
            {
                writer.QuoteChar = '\'';

                JsonSerializer ser = new JsonSerializer();
                ser.Serialize(writer, obj);
            }

            return sb.ToString().Replace("\\'", "''");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="toAppend"></param>
        /// <param name="FileName"></param>
        public static void AppendToFile(this string toAppend, string FileName)
        {
            try
            {
                if (!System.IO.File.Exists(FileName))
                {
                    using (System.IO.StreamWriter sWriter = new System.IO.StreamWriter(FileName))
                    {
                        sWriter.WriteLine(toAppend);
                        sWriter.Close();
                    }
                }
                else
                {
                    using (System.IO.StreamWriter sw = System.IO.File.AppendText(FileName))
                    {
                        sw.WriteLine(toAppend);
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="MaxLen"></param>
        /// <param name="ValidCharacters"></param>
        /// <param name="AdditionalValidCharacters"></param>
        /// <returns></returns>
        public static string Filter(this string Text, int MaxLen = -1, string ValidCharacters = "", string AdditionalValidCharacters = "")
        {
            if (Text == null) return "";

            int i = 0;
            string Result = "";

            if (ValidCharacters.Length == 0)
                ValidCharacters = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ _-";

            if (AdditionalValidCharacters.Length > 0)
                ValidCharacters = ValidCharacters + AdditionalValidCharacters;

            for (i = 0; i < Text.Length; i++)
            {
                if (ValidCharacters.IndexOf(Text.Substring(i, 1)) >= 0)
                {
                    Result = Result + Text.Substring(i, 1);
                }
            }
            Result = Result.Trim();

            if (MaxLen > 0)
            {
                if (Result.Length > MaxLen)
                    Result = Result.Remove(MaxLen - 1, Result.Length - (MaxLen - 1));
            }

            return Result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="StrValue"></param>
        /// <returns></returns>
        public static Boolean isNumeric(this string StrValue)
        {
            if (string.IsNullOrEmpty(StrValue)) return false;
            float output;
            return (float.TryParse(StrValue, out output) && !float.IsNaN(output) && !float.IsInfinity(output) && !StrValue.Contains(","));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Compress(this string s, string key = "m2mbac08")
        {
            try
            {
                CompressedString cmp = new CompressedString(s, CompressedString.InputDataTypeClass.UnCompressed, key);
                return cmp.Compressed;
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Decompress(this string s, string key = "m2mbac08")
        {
            try
            {
                CompressedString cmp = new CompressedString(s, CompressedString.InputDataTypeClass.Compressed, key);
                return cmp.UnCompressed;
            }
            catch
            {
                return "";
            }
        }

        public static string Encrypt(this string s, string passphrase = "k2aesms201800000")
        {
            return Encryption.Encrypt(s, passphrase);
        }

        public static string Decrypt(this string s, string passphrase = "k2aesms201800000")
        {
            return Encryption.Decrypt(s, passphrase);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringList"></param>
        /// <returns></returns>
        public static string FindCommonStringPrefix(this string[] stringList)
        {
            if (stringList.Length > 0)
            {
                string x = string.Join("", stringList.Select(s => s.AsEnumerable())
                                              .Transpose()
                                              .TakeWhile(s => s.All(d => d == s.First()))
                                              .Select(s => s.First()));

                return x;
            }
            else
            {
                return "";
            }
        }

        public static IEnumerable<IEnumerable<T>> Transpose<T>(this IEnumerable<IEnumerable<T>> source)
        {
            var enumerators = source.Select(e => e.GetEnumerator()).ToArray();
            try
            {
                while (enumerators.All(e => e.MoveNext()))
                {
                    yield return enumerators.Select(e => e.Current).ToArray();
                }
            }
            finally
            {
                Array.ForEach(enumerators, e => e.Dispose());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        public static int ToInteger(this string s, int defaultVal = 0)
        {

            int output;
            if (int.TryParse(s, out output))
                return output;
            else
                return defaultVal;
        }
        public static List<int> ToIntegerList(this string s)
        {
            try
            {
                if (!s.IsNullOrEmpty())
                {
                    var ints = s.Split(',');
                    var intList = new List<int>();

                    foreach (var intVal in ints)
                    {
                        if (intVal.ToInteger(-9999999) > -9999999) intList.Add(intVal.ToInteger());
                    }

                    return intList;
                }
                
            }
            catch { }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        public static double ToDouble(this string s, double defaultVal = 0)
        {
            double output;
            if (double.TryParse(s, out output))
                return output;
            else
                return defaultVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        public static float ToFloat(this string s, float defaultVal = 0)
        {
            float output;
            if (float.TryParse(s, out output))
                return output;
            else
                return defaultVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string[] Lines(this string source)
        {
            return source.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string Reverse(this string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }
        public static bool IsNotNullOrEmpty(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }
        public static bool IsValidEmailAddress(this string email)
        {
            bool invalid = false;
            if (String.IsNullOrEmpty(email))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid email format.
            try
            {
                return Regex.IsMatch(email,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static T DeserializeJson<T>(this string s, IContractResolver contractResolver = null)
        {
            var jss = new JsonSerializerSettings();
            if (contractResolver != null)
                jss.ContractResolver = contractResolver;
            return JsonConvert.DeserializeObject<T>(s, jss);
        }

        public static string SerializeToJson(this object o, bool pretty = false, IContractResolver contractResolver = null)
        {
            var stgs = new JsonSerializerSettings() { Formatting = pretty ? Newtonsoft.Json.Formatting.Indented : Newtonsoft.Json.Formatting.None };
            if (contractResolver != null)
                stgs.ContractResolver = contractResolver;
            return JsonConvert.SerializeObject(o, stgs);
        }

        public static string SerializeToAgens(this object o)
        {
            string json = JsonConvert.SerializeObject(o, new JsonSerializerSettings
            {
                ContractResolver = new LowercaseContractResolver(),
                Formatting = Newtonsoft.Json.Formatting.None
            });

            return json.JsonToAgens();
        }




        #endregion "Strings"

        #region Numbers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="max"></param>
        /// <param name="totalPercent"></param>
        /// <returns></returns>
        public static int PercentOf(this int v, int max, int totalPercent = 100)
        {
            if (v == 0) return 0;
            float val = ((float)v * totalPercent) / (float)max;

            return (int)Math.Floor(val);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int Validate(this int v, int min, int max)
        {
            return Math.Min(Math.Max(v, min), max);
        }
        public static double Validate(this double v, double min, double max)
        {
            return Math.Min(Math.Max(v, min), max);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static string SmartRound(this double v)
        {
            return SmartRoundDoubleToDouble(v).ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static double SmartRoundToDouble(this double v)
        {
            return SmartRoundDoubleToDouble(v);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        private static double SmartRoundDoubleToDouble(double v)
        {
            if (v >= 1000 || v <= -1000)
            {
                return System.Math.Round(v, 0);
            }
            else
                if (v >= 100 || v <= -100)
            {
                return System.Math.Round(v, 1);
            }
            else
                    if (v >= 10 || v <= -10)
            {
                return System.Math.Round(v, 2);
            }
            else
            {
                return System.Math.Round(v, 3);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static string SmartRound(this float v)
        {
            return SmartRoundDoubleToDouble(v).ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static string SmartRound(this String v)
        {
            float s;
            if (float.TryParse(v, out s))
                return s.SmartRound();
            else
                return v;
        }

        public static bool IsBetween<T>(this T item, T start, T end)
        {
            return Comparer<T>.Default.Compare(item, start) >= 0
                && Comparer<T>.Default.Compare(item, end) <= 0;
        }

        #endregion Numbers

        #region "DateTime"

        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static DateTime StartOfMonth(this DateTime dt, int Day)
        {
            if (Day <= 0) Day = 1;
            if (dt.Day >= Day)
            {
                return new DateTime(dt.Year, dt.Month, Day);
            }
            else
            {
                return new DateTime(dt.Year, dt.Month, Day).AddMonths(-1);
            }
        }

        private static int lastUtcCheckHour = -1;
        private static double _utcOffset = 0;

        public static double UtcOffset
        {
            get
            {
                UpdateUtc();
                return _utcOffset;
            }
        }
        private static void UpdateUtc()
        {
            if (DateTime.UtcNow.Hour != lastUtcCheckHour)
            {
                _utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).TotalSeconds;
                lastUtcCheckHour = DateTime.UtcNow.Hour;
            }

        }
        public static string ToPgDateTime(this DateTime dt, bool SecondsAsZero = true, bool convertToUtc = false)
        {
            if (convertToUtc)
            {
                UpdateUtc();
                dt = dt.AddSeconds(UtcOffset * -1);
            }

            if (SecondsAsZero)
            {
                return dt.ToString("yyyy-MM-dd HH:mm:00");
            }
            else
            {
                return dt.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        public static DateTime FromUtc(this DateTime dt)
        {
            if (dt.Year < 2010) return dt;
            UpdateUtc();
            return dt.AddSeconds(UtcOffset);
        }

        public static bool EnableKindFix { get; set; }
        public static DateTime RoundUp(this DateTime dt, TimeSpan d)
        {
            var delta = (d.Ticks - (dt.Ticks % d.Ticks)) % d.Ticks;
            return EnableKindFix
                ? new DateTime(dt.Ticks + delta, dt.Kind)
                : new DateTime(dt.Ticks + delta);
        }

        public static DateTime RoundDown(this DateTime dt, TimeSpan d)
        {
            var delta = dt.Ticks % d.Ticks;
            return EnableKindFix
                ? new DateTime(dt.Ticks - delta, dt.Kind)
                : new DateTime(dt.Ticks - delta);
        }

        public static DateTime Round(this DateTime dt, TimeSpan d)
        {
            var delta = dt.Ticks % d.Ticks;
            bool roundUp = delta > d.Ticks / 2;

            return roundUp ? dt.RoundUp(d) : dt.RoundDown(d);
        }


        public static string ToPgDateOnly(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd");
        }

        private static int[] moveByDays = { 6, 7, 8, 9, 10, 4, 5 };

        public static int WeekOfYear(this DateTime date)
        {
            DateTime startOfYear = new DateTime(date.Year, 1, 1);
            DateTime endOfYear = new DateTime(date.Year, 12, 31);
            // ISO 8601 weeks start with Monday 
            // The first week of a year includes the first Thursday 
            // This means that Jan 1st could be in week 51, 52, or 53 of the previous year...
            int numberDays = date.Subtract(startOfYear).Days +
                            moveByDays[(int)startOfYear.DayOfWeek];
            int weekNumber = numberDays / 7;
            switch (weekNumber)
            {
                case 0:
                    // Before start of first week of this year - in last week of previous year
                    weekNumber = WeekOfYear(startOfYear.AddDays(-1));
                    break;
                case 53:
                    // In first week of next year.
                    if (endOfYear.DayOfWeek < DayOfWeek.Thursday)
                    {
                        weekNumber = 1;
                    }
                    break;
            }
            return weekNumber;
        }
        #endregion "DateTime"

        #region "XMLNodes"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="attrib"></param>
        /// <returns></returns>
        public static string Attribute(this XmlNode v, string attrib)
        {
            XmlNode node = v.Attributes[attrib];
            if (node != null)
                return node.Value;
            else
                return null;
        }

        #endregion "XMLNodes"

        #region "Lists"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="chunkSize"></param>
        /// <returns></returns>
        public static List<List<T>> ChunkBy<T>(this List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list1"></param>
        /// <param name="list2"></param>
        /// <returns></returns>
        public static bool IsEqualTo<T>(this List<T> list1, List<T> list2)
        {
            return !list1.Except(list2).ToList().Any() && !list2.Except(list1).ToList().Any();
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey> (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        #endregion

        #region "bytes"

        /// <summary>
        /// Decompress byte[] to string. Not very beneficial unless &gt; 240 bytes. Gzip adds headers for file save capabilities
        /// </summary>
        /// <param name="raw"></param>
        /// <returns></returns>
        public static byte[] Compress(this byte[] raw, bool useGzip = false)
        { //Doesn't appear to be beneficial unless original is > 240 bytes

            using (MemoryStream memory = new MemoryStream())
            {
                if (useGzip)
                {
                    using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, true))
                    {
                        gzip.Write(raw, 0, raw.Length);
                    }
                }
                else
                {
                    using (DeflateStream dstream = new DeflateStream(memory, CompressionLevel.Fastest))
                    {
                        dstream.Write(raw, 0, raw.Length);
                    }
                }
                return memory.ToArray();
            }

        }

        /// <summary>
        /// Decompress byte[] to string. Not very beneficial unless &gt; 240 bytes. Gzip adds headers for file save capabilities
        /// </summary>
        /// <param name="raw"></param>
        /// <returns></returns>
        public static byte[] Decompress(this byte[] gzip, bool useGzip = false)
        {
            int size = Math.Min(gzip.Length, 4096);
            byte[] buffer = new byte[size];
            int count = 0;

            if (useGzip)
            {
                using (GZipStream input = new GZipStream(new MemoryStream(gzip), CompressionMode.Decompress))
                {
                    using (MemoryStream output = new MemoryStream())
                    {
                        do
                        {
                            count = input.Read(buffer, 0, size);
                            if (count > 0)
                            {
                                output.Write(buffer, 0, count);
                            }
                        }
                        while (count > 0);

                        return output.ToArray();

                    }
                }
            }
            else
            {
                using (MemoryStream output = new MemoryStream())
                {
                    using (DeflateStream input = new DeflateStream(new MemoryStream(gzip), CompressionMode.Decompress))
                    {
                        //dstream.CopyTo(output);
                        do
                        {
                            count = input.Read(buffer, 0, size);
                            if (count > 0)
                            {
                                output.Write(buffer, 0, count);
                            }
                        }
                        while (count > 0);

                        return output.ToArray();
                    }
                    //return output.ToArray();
                }
            }

        }

        public static bool ToFile(this byte[] byteArray, string fileName)
        {
            try
            {
                using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region "DataTable"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static byte[] Serialize(this DataTable dt)
        {
            Dictionary<int, Type> columnTypes = new Dictionary<int, Type>();

            MemoryStream stream = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(stream);

            bw.Write(dt.Rows.Count);
            bw.Write(dt.Columns.Count);

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                columnTypes[i] = dt.Columns[i].DataType;
                bw.Write(dt.Columns[i].DataType.FullName);
                bw.Write(dt.Columns[i].ColumnName);
            }

            foreach (DataRow r in dt.Rows)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    bool isDbNull = (r[i] is DBNull);

                    if (columnTypes[i] == typeof(string)) bw.Write(isDbNull ? "" : (string)r[i]);
                    else if (columnTypes[i] == typeof(DateTime))
                        bw.Write(isDbNull ? new DateTime().Ticks : ((DateTime)r[i]).Ticks);
                    else if (columnTypes[i] == typeof(double)) bw.Write(isDbNull ? double.NaN : (double)r[i]);
                    else if (columnTypes[i] == typeof(float)) bw.Write(isDbNull ? float.NaN : (float)r[i]);
                    else if (columnTypes[i] == typeof(int)) bw.Write(isDbNull ? int.MinValue : (int)r[i]);
                    else if (columnTypes[i] == typeof(long)) bw.Write(isDbNull ? int.MinValue : (long)r[i]);
                }

            }

            return stream.ToArray();
        }

        public static DataTable DeserializeDataTable(this byte[] data)
        {
            Dictionary<int, Type> columnTypes = new Dictionary<int, Type>();
            BinaryReader br;
            int rowCount;
            int colCount;

            try
            {
                DataTable dt = new DataTable();
                MemoryStream stream = new MemoryStream(data);
                br = new BinaryReader(stream);

                rowCount = br.ReadInt32();
                colCount = br.ReadInt32();

                for (int i = 0; i < colCount; i++)
                {
                    columnTypes[i] = Type.GetType(br.ReadString());
                    dt.Columns.Add(br.ReadString(), columnTypes[i]);
                }

                for (int i = 0; i < rowCount; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int i2 = 0; i2 < colCount; i2++)
                    {

                        if (columnTypes[i2] == typeof(string)) dr[i2] = br.ReadString();
                        else if (columnTypes[i2] == typeof(DateTime))
                        {
                            long ticks = br.ReadInt64();
                            dr[i2] = new DateTime(ticks);
                        }
                        else if (columnTypes[i2] == typeof(double))
                        {
                            double val = br.ReadDouble();
                            if (double.IsNaN(val))
                                dr[i2] = DBNull.Value;
                            else
                                dr[i2] = val;
                        }
                        else if (columnTypes[i2] == typeof(float)) dr[i2] = br.ReadDouble();
                        else if (columnTypes[i2] == typeof(int)) dr[i2] = br.ReadInt32();
                        else if (columnTypes[i2] == typeof(long)) dr[i2] = br.ReadInt64();

                    }
                    dt.Rows.Add(dr);
                }

                return dt;
            }
            catch
            {

            }

            return null;
        }

        #endregion

        #region "Socket"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="endpoint"></param>
        /// <param name="timeout"></param>
        public static void Connect(this Socket socket, EndPoint endpoint, int timeout)
        {
            var result = socket.BeginConnect(endpoint, null, null);

            bool success = result.AsyncWaitHandle.WaitOne(timeout, true);
            if (success)
            {
                socket.EndConnect(result);
            }
            else
            {
                socket.Close();
                throw new SocketException(10060); // Connection timed out.
            }
        }
        public static void Connect(this Socket socket, string hostname, int port, int timeout)
        {
            var result = socket.BeginConnect(hostname, port, null, null);

            bool success = result.AsyncWaitHandle.WaitOne(timeout, true);
            if (success)
            {
                socket.EndConnect(result);
            }
            else
            {
                socket.Close();
                throw new SocketException(10060); // Connection timed out.
            }
        }
        public static bool AuthenticateAsClient(this SslStream stream, string targetHost, int timeout)
        {
            var result = stream.BeginAuthenticateAsClient(targetHost, CallbackTest, null);

            bool success = result.AsyncWaitHandle.WaitOne(timeout, true);

            if (success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool AuthenticateAsServer(this SslStream stream, X509Certificate cert, int timeout)
        {
            var result = stream.BeginAuthenticateAsServer(cert, false, System.Security.Authentication.SslProtocols.Tls, true, null, null);

            bool success = result.AsyncWaitHandle.WaitOne(timeout, true);

            if (success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private static void CallbackTest(IAsyncResult ar)
        {
            return;
        }

        #endregion


        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                bool invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
    }
}
