﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K2A.Core.Collections
{
    public class FixedSizedQueue<T> : Queue<T>
    {
        //First in, first out Queue.  Keeps the specified number of items only
        private readonly int maxQueueSize;
        private readonly object syncRoot = new object();

        public FixedSizedQueue(int maxQueueSize)
        {
            this.maxQueueSize = maxQueueSize;
        }

        public void Add(T item)
        {
            Enqueue(item);
        }

        public new void Enqueue(T item)
        {
            lock (syncRoot)
            {
                base.Enqueue(item);
                if (Count > maxQueueSize)
                    Dequeue(); // Throw away
            }
        }
    }
}
