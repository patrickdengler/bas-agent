﻿using Ionic.Zip;
using K2A.Connect.ConnectAgent;
using K2A.Connect.mLink.Clients;
using K2A.Connect.Models;
using K2A.Data.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace K2A.Connect.AgentManager
{
    public class UpdateWorker
    {
        Timer workerTimer = null;
        Client redis;

        int currentVersion = 0;

        public UpdateWorker(string serialNum)
        {

            redis = new Client();
            redis.Connect("127.0.0.1", 2);
            redis.DeleteKey("AgentManager:UpdateInProgress");

            workerTimer = new Timer(RunWorker, null, 60000, Timeout.Infinite);

            currentVersion = "curr.ver".PathToLocalFile().FileToString().Trim().ToInteger(0);

            CheckForUpdates();
        }


        void RunWorker(object state)
        {
            workerTimer.Change(60000, Timeout.Infinite);

            if (redis.ReadObject<string>("AgentManager:UpdateInProgress").IsNotNullOrEmpty()) return;

            CheckForUpdates();
        }

        MiniAgentLicenseClient updateClient = new MiniAgentLicenseClient() { KeepAlive = true };

        private void CheckForUpdates()
        {
            if (updateClient.IsConnected() || updateClient.Connect("manager", "licensecheck"))
            {
                try
                {
                    MiniAgentUpdateInformation update = updateClient.GetAvailableUpdates(currentVersion);

                    if (update != null && update.NewVersion > currentVersion)
                    {
                        Logging.Log("Update found: " + update.NewVersion, Logging.EntryType.Info);
                        redis.StoreObject("AgentManager:UpdateInProgress", "1", 600000);
                        ApplyUpdate(update);
                    }
                }
                catch (Exception ex)
                {
                    Logging.Log("Error applying updates: " + ex.Message, Logging.EntryType.Error);
                    redis.StoreObject("StackTrace:ApplyingUpdates", ex.StackTrace);
                    redis.DeleteKey("AgentManager:UpdateInProgress");
                }
            }
        }

        void ApplyUpdate(MiniAgentUpdateInformation update)
        {
            try
            {
                Logging.Log("Downloading update files", Logging.EntryType.Info);
                DownloadUpdateFiles(update.UpdateFiles.Keys.ToList());

                //ApplyCommands(update.Commands);


                //Logging.Log("Applying Updates to Mini Agent", Logging.EntryType.Info);
                var dirInfo = new DirectoryInfo(K2A.App.StartupPath + "updates/");
                foreach (var file in dirInfo.GetFiles())
                {
                    if (file.Name == "applyupdate.sh")
                    {
                        file.CopyTo(K2A.App.StartupPath + file.Name, true);
                        file.Delete();
                    }

                }

                update.NewVersion.ToString().ToFile("pending.ver".PathToLocalFile());


                Logging.Log("Updates for version " + update.NewVersion + " downloaded.", Logging.EntryType.Info);
            }
            catch (Exception ex)
            {
                Logging.Log("Error downloading updates: " + ex.Message, Logging.EntryType.Error);
                redis.DeleteKey("AgentManager:UpdateInProgress");
                Reboot();
                return;
            }

            string result = "sudo".CommandToString("bash /K2A/applyupdate.sh");
            redis.StoreObject("UpdateResult", result);

        }
        void ApplyCommands(string commands)
        {
            if (!string.IsNullOrEmpty(commands))
            {

                string[] cmds = commands.Split(';');
                foreach (var cmd in cmds)
                {
                    var p = Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = cmd });
                    p.WaitForExit();
                }

            }
        }
        private void Reboot()
        {
            Logging.Log("Reboot Command Sent.", Logging.EntryType.Info);
            Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = "reboot" });
        }

        static void DownloadUpdateFiles(List<string> files)
        {
            string updatePath = string.Format("{0}updates{1}", K2A.App.StartupPath, Path.DirectorySeparatorChar);

            //Logging.Log("Downloading Update Files", Logging.EntryType.Info);

            using (var data = new K2A.Data.DataDriver(K2A.Data.DataDriver.ConnectionDestination.UpdateServer))
            {
                foreach (var file in files)
                {
                    string zipFile = K2A.App.StartupPath + "updates" + Path.DirectorySeparatorChar + file;

                    using (var reader = data.ExecuteRead("Select filedata from agent18updates where filename like '" + file + "'"))
                    {
                        if (reader.Read())
                        {
                            byte[] fdata = (byte[])reader.GetObject("filedata");
                            File.WriteAllBytes(zipFile, fdata);
                        }
                        else
                        {
                            Logging.Log("Update file not found: " + file, Logging.EntryType.Error);
                            throw new Exception("Update file not found: " + file);
                        }
                    }

                    using (ZipFile zip1 = ZipFile.Read(zipFile))
                    {
                        foreach (ZipEntry e in zip1)
                        {
                            e.Extract(updatePath, ExtractExistingFileAction.OverwriteSilently);
                        }
                    }

                    System.IO.File.Delete(zipFile);

                }
            }

            if (Directory.Exists("/K2A_Setup")) ExtractWebSetupFiles();

        }

        private static bool ExtractWebSetupFiles()
        {
            try
            {
                if (File.Exists("setup.zip".PathToLocalFile()))
                {
                    Logging.Log("Applying Updates to Agent Setup.", Logging.EntryType.Info);
                    using (ZipFile zip1 = ZipFile.Read("setup.zip".PathToLocalFile()))
                    {
                        zip1.ExtractAll("/K2A_Setup", ExtractExistingFileAction.OverwriteSilently);
                    }

                    File.Delete("setup.zip".PathToLocalFile());
                    return true;
                }
            }
            catch
            {

            }
            return false;
        }
    }
}
