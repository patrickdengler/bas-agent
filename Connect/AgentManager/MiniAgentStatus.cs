﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K2A.Connect.AgentManager
{
    public class MiniAgentStatus
    {
        public int ProcessId { get; set; }
        public int AgentId { get; set; }
        public string SerialNumber { get; set; }

    }
}
