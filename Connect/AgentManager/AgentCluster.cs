﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using K2A.Data.Redis;
using K2A.Connect.ConnectAgent.Workers;
using K2A.Connect.mLink.Clients;

namespace K2A.Connect.AgentManager
{
    public class VMACluster
    {
        K2A.Connect.mLink.Clients.RemoteAccessHost ssh_tunnel;
        K2A.Connect.mLink.Clients.RemoteAccessHost redis_tunnel;

        public List<MiniAgentStatus> MiniAgents = new List<MiniAgentStatus>();

        public Client redis;

        System.Threading.Timer workerTimer = null;

        public VMACluster()
        {
            redis = new Client();

            //redis.Connect("env-01.connect.key2act.work", 3);
            redis.Connect("127.0.0.1", 2);
        }

        void RunWorkers(object state)
        {
            lock (MiniAgents)
            {
                foreach (var m in MiniAgents)
                {
                    if (!IsProcessRunning(m.ProcessId) || !redis.KeyExists($"Agents:{m.AgentId}:Status"))
                    {
                        Console.WriteLine("Restarting agent: " + m.AgentId);
                        LogInfo($"Restarting Agent {m.AgentId}: {m.SerialNumber}");
                        StartMiniAgent(m);
                    }
                }

            }

            workerTimer.Change(30000, Timeout.Infinite);
        }
        public void StartWorkerTimer()
        {
            workerTimer = new Timer(RunWorkers, null, 60000, Timeout.Infinite);
        }
        public void CreateMiniAgentInstance(string sn)
        {
            int agentId = 0;

            string fullSn = $"{sn.Substring(0, 5)}-{sn.Substring(5, 5)}-{sn.Substring(10, 5)}-{sn.Substring(15, 5)}-{sn.Substring(20, 5)}";

            string lic = GetLicense(fullSn);

            if (lic.IsNullOrEmpty() || lic.Length < 10)
            {
                LogError($"Unable to find a valid license: {fullSn}.  Retrying in 300 seconds...");

                var t = Task.Run(() =>
                {
                    System.Threading.Thread.Sleep(300000);
                    CreateMiniAgentInstance(sn);
                });

                return;
            }

            try
            {
                var result = JObject.Parse(lic);

                var svrData = result.GetValue("serverdata").ToString();

                var svrConnectionInfo = JObject.Parse(svrData.Decompress());

                agentId = (int)result["agentid"];

                if (agentId > 0 && ssh_tunnel == null)
                {
                    ssh_tunnel = new mLink.Clients.RemoteAccessHost();
                    redis_tunnel = new mLink.Clients.RemoteAccessHost();

                    try
                    {
                        ssh_tunnel.ConnectAsHost("_s_" + sn, "appservice-01.connect.key2act.io", 57001, "127.0.0.1", 22);
                        //redis_tunnel.ConnectAsHost("_r_" + sn, "brian01.key2act.work", 57000, "127.0.0.1", 6379);
                        redis_tunnel.ConnectAsHost("_r_" + sn, "appservice-01.connect.key2act.io", 57001, "127.0.0.1", 6379);
                    }
                    catch (Exception ex)
                    {
                        LogError("Unable to start remote access: " + ex.Message);
                    }
                }
            }
            catch
            {
                LogError("Unable to read license.  Retrying in 300 seconds...");


                var t = Task.Run(() =>
                {
                    System.Threading.Thread.Sleep(300000);
                    CreateMiniAgentInstance(sn);
                });

                return;

            }

            lock (MiniAgents)
            {
                var m = MiniAgents.Where(a => a.SerialNumber == fullSn).FirstOrDefault();

                if (m != null)
                {
                    LogError("Duplicate serial number: " + fullSn);
                    return;
                }

                var ma = new MiniAgentStatus()
                {
                    SerialNumber = fullSn,
                    AgentId = agentId
                };

                MiniAgents.Add(ma);


                StartMiniAgent(ma);
            }


        }

        private string GetLicense(string fullSn)
        {
            Console.WriteLine("Getting License");

            string lic = "";

            try { lic = redis.ReadObject<string>("Agents:" + fullSn + ":License"); }
            catch { }

            if (lic.IsNullOrEmpty())
            {
                lic = K2A.Connect.mLink.Clients.MiniAgentLicenseClient.DownloadLicense(fullSn);

                if (lic.IsNotNullOrEmpty() && lic != "-1")
                {
                    Console.WriteLine("Storing Agent License to Redis");
                    try { redis.StoreObject("Agents:" + fullSn + ":License", lic, 300, false); }
                    catch { }

                }
                else
                {
                    if (!fullSn.StartsWith("VMA01") && Licensing.SerialNumber().ToUpper() == fullSn.ToUpper())
                    {
                        Logging.Log("Requesting License", Logging.EntryType.Info);
                        mLink.Clients.MiniAgentLicenseClient.RequestLicense();
                    }
                }
            }
            return lic;
        }


        private void StartMiniAgent(MiniAgentStatus miniAgent)
        {
            
            if (miniAgent.ProcessId > 0)
            {
                while (IsProcessRunning(miniAgent.ProcessId))
                {
                    Logging.Log("Killing process " + miniAgent.ProcessId, Logging.EntryType.Info);
                    Process.Start("kill", "-9 " + miniAgent.ProcessId);
                    System.Threading.Thread.Sleep(2000);
                }
            }

            var p = Process.Start(new ProcessStartInfo() { FileName = "MiniAgent.exe".PathToLocalFile(), Arguments = miniAgent.SerialNumber });
            miniAgent.ProcessId = p.Id;
            Logging.Log("Agent Id " + miniAgent.AgentId + " started with ProcessId " + p.Id, Logging.EntryType.Info);
            redis.StoreObject("Agents:" + miniAgent.AgentId + ":Process", miniAgent);
        }

        private bool IsProcessRunning(int pId)
        {
            try
            {
                var p = Process.GetProcessById(pId);
                if (p != null && !p.HasExited) return true;
            }
            catch
            {

            }

            return false;
        }


        public void LogInfo(string message)
        {
            Logging.Log(message, Logging.EntryType.Info);
        }

        public void LogError(string message)
        {
            Logging.Log(message, Logging.EntryType.Error);
        }
    }
}
