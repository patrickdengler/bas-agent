﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using K2A.Data.Redis;
using NLog.Targets;

namespace K2A.Connect.AgentManager
{
    public class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            UpdateWorker updateWorker = null;

            System.Threading.ManualResetEvent waitEvent = new System.Threading.ManualResetEvent(false);
            ErrorHandling.UnhandledExceptionManager.AddHandler("AgentManager");
            ErrorHandling.UnhandledExceptionManager.killProcessOnError = false;

            System.Threading.Thread.Sleep(5000);

            Console.WriteLine("Checking Network...");

            CheckNetworkConnection();

            if (SetHostName(Licensing.SerialNumber().ToUpper().Replace("-", "")))
            {
                Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = "reboot" });
                Thread.Sleep(30000);
                return;
            }

            if (App.IsRunningOnMono() && File.Exists("/K2A/upgrade.sh"))
            {
                K2A.Connect.LogFiles.CreateAgentLogConfiguration("agent_manager", false);
                Logging.Log("Running Upgrade Script", Logging.EntryType.Info);

                var upgradeStatus = "sudo".CommandToString("bash /K2A/upgrade.sh");
                upgradeStatus.ToFile("/K2A/upgradelog.txt");

                Logging.Log("Upgrade complete", Logging.EntryType.Info);

                Thread.Sleep(3600000);
                return;
            }

            if (K2A.App.IsRunningOnMono())
            {
                Console.WriteLine("Starting Redis");

                if (IsK2AMiniAgent())
                {
                    try
                    {
                        "redis.conf".PathToLocalFile().FileToString().ToFile("/redis-4.0.9/redis.conf");
                        
                    }
                    catch { }
                    Process.Start("sudo", "/redis-4.0.9/src/redis-server /redis-4.0.9/redis.conf");
                }
                else
                {
                    try
                    {
                        "redis.conf".PathToLocalFile().FileToString().ToFile("/redis-4.0.9/redis.conf");

                        Process.Start("/redis-4.0.9/src/redis-server", "/redis-4.0.9/redis.conf");
                    }
                    catch { }
                }

                Thread.Sleep(5000);
            }

            KillMiniAgentInstances();

            Console.WriteLine("Starting Agent Manager Service...");

            VMACluster cluster = new VMACluster();

            K2A.Connect.LogFiles.CreateAgentLogConfiguration("agent_manager", false, new ConnectAgent.RedisTarget(cluster.redis, "AgentManager:Logs") { Channel = "agent_mgr" });

            Logging.Log("Starting Agent Manager Service...", Logging.EntryType.Info);

            if (args.Length == 0) //Standard Mini Agent
            {
                args = new string[1] { Licensing.SerialNumber() };
            }

            args[0] = args[0].Replace("-", "").ToUpper().Trim();

            List<string> serials = args[0].Split(',').ToList();

            foreach (var sn in serials)
            {
                if (sn.Trim().Length != 25 || sn.Trim().ToUpper() != sn.Trim().Filter(25, "0123456789ABCDEFVM"))
                {
                    cluster.LogError($"Invalid serial number: {sn.Trim()}.");
                }
                else
                {
                    if (!sn.StartsWith("VMA01") && App.IsRunningOnMono() && updateWorker == null)
                    {
                        updateWorker = new UpdateWorker(sn);
                    }

                    cluster.CreateMiniAgentInstance(sn.Trim());
                }

            }

            cluster.StartWorkerTimer();

            waitEvent.WaitOne();
        }

        static void KillMiniAgentInstances()
        {
            if (!App.IsRunningOnMono()) return;

            string runningProcesses = "pgrep".CommandToString("-f mono.*MiniAgent.*");

            if (IsK2AMiniAgent())
                runningProcesses = "pgrep".CommandToString("-f sudo mono.*MiniAgent.*");

            foreach (var line in runningProcesses.Lines())
            {
                var pId = line.Trim().ToInteger(-445);

                if (pId > 0)
                {
                    try
                    {
                        Process.GetProcessById(pId).Kill();
                    }
                    catch { }
                }
            }
        }

        private static bool Ping()
        {
            try
            {
                var ping = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply reply = ping.Send("4.2.2.2", 1000);

                return reply.Status == System.Net.NetworkInformation.IPStatus.Success;
            }
            catch
            {
                return false;
            }
        }

        private static void CheckNetworkConnection()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Logging.Log("Waiting for network connection...", Logging.EntryType.Info);
            while (!Ping() && sw.ElapsedMilliseconds < 60000)
            {
                Thread.Sleep(50);
            }

        }

        private static bool SetHostName(string sn)
        {
            if (!IsK2AMiniAgent()) return false;

            if (Environment.MachineName.ToLower() != "raspberrypi" && !Environment.MachineName.ToLower().StartsWith("k2a")) return false;

            string hostName = "K2A" + sn.Substring(20, 5);

            if (Environment.MachineName == hostName) return false; //if no name is specified or it already matches, ignore

            StringBuilder sb = new StringBuilder();

            string fileData = "/etc/hosts".FileToString();
            var lines = fileData.Lines();

            foreach (var line in lines)
            {
                if (line.StartsWith("127.0.1.1"))
                {
                    sb.AppendLine("127.0.1.1\t" + hostName);
                }
                else
                {
                    sb.AppendLine(line);
                }
            }

            sb.ToString().ToFile("/etc/hosts");

            hostName.ToFile("/etc/hostname");

            Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = "/etc/init.d/hostname.sh" });

            SetSSID(hostName);

            return true;
        }

        private static void SetSSID(string hostName)
        {
            StringBuilder sb = new StringBuilder();

            string fileData = "/etc/hostapd/hostapd.conf".FileToString();
            var lines = fileData.Lines();

            foreach (var line in lines)
            {
                if (line.StartsWith("ssid="))
                {
                    sb.AppendLine("ssid=" + hostName);
                }
                else
                {
                    sb.AppendLine(line);
                }
            }

            sb.ToString().ToFile("/etc/hostapd/hostapd.conf");
        }

        public static bool IsK2AMiniAgent()
        {
            return App.IsRunningOnMono() && File.Exists("/K2A_Setup/MiniAgentSetup.exe");
        }
    }
}
