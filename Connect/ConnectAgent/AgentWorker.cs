﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using K2A.Connect.Models;
using K2A.Connect.mLink.Clients;
using System.Diagnostics;
using K2A.Connect.ConnectAgent.Workers;
using K2A.Data.Redis;

namespace K2A.Connect.ConnectAgent
{
    public class AgentWorker
    {
        public delegate void ShutdownRequestedHandler(bool shouldRestart);
        public event ShutdownRequestedHandler ShutdownRequested;

        public int AgentID;
        public K2A.Connect.Models.Agent MyAgent;

        private bool _run = true;

        public bool Running
        {
            get
            {
                return _run;
            }
        }

        private Workers.ConnectAgentWorker agentClient;

        #region "Service and Startup"

        public AgentWorker(int agent)
        {
            Logging.Log("Starting Agent Id " + agent, Logging.EntryType.Info);
            AgentID = agent;
        }

        private void StartAgent()
        {
            System.Threading.Thread.Sleep(2000);

            if (MyAgent == null) MyAgent = K2A.Connect.Models.Agent.FromId(AgentID);

            if (MyAgent == null)
            {
                Logging.Log("Unable to find Agent Id " + AgentID, Logging.EntryType.Error);

                _run = false;
                return;
            }

            switch (MyAgent.ConnectionType)
            {
                case Models.Agent.BBConnectionType.Niagara:
                    agentClient = new NiagaraAgentWorker();
                    break;
                case Models.Agent.BBConnectionType.BACnetEthernet:
                case Models.Agent.BBConnectionType.BACnetIP:
                    agentClient = new BACnetAgentWorker();
                    break;
                case Models.Agent.BBConnectionType.JSON:
                    agentClient = new JsonAgentWorker();
                    break;
                case Models.Agent.BBConnectionType.ModbusTcp:
                    agentClient = new ModbusTcpAgentWorker();
                    break;
                //case Models.Agent.BBConnectionType.SmartThings:
                //    agentClient = new SmartThingsAgentWorker();
                //    break;
                //case Models.Agent.BBConnectionType.Telesense:
                //    agentClient = new TelesenseAgentWorker();
                //    break;
                //case Models.Agent.BBConnectionType.Incoming:
                //    agentClient = new IncomingAgentWorker();
                //    break;
                //case Models.Agent.BBConnectionType.Ubiquity:
                //    agentClient = new UbiquityAgentWorker();
                //    break;
                default:
                    Logging.Log("Invalid Connection Type.  Agent unable to start.", Logging.EntryType.Error);
                    _run = false;
                    return;
            }

            agentClient.RestartServiceRequest += agentClient_RestartServiceRequest;
            agentClient.ConnectionRejected += bbAgentClient_ConnectionRejected;
            agentClient.ServiceStopped += agentClient_ServiceStopped;
            agentClient.MyAgent = MyAgent;
            agentClient.Start();
        }

        void agentClient_ServiceStopped()
        {
            agentClient.RestartServiceRequest -= agentClient_RestartServiceRequest;
            agentClient.ConnectionRejected -= bbAgentClient_ConnectionRejected;

            Logging.Log("Service Stop command received.", Logging.EntryType.Info);
            ShutdownRequested?.Invoke(true);
            //agentClient.Dispose();
            //System.Threading.Thread.Sleep(3000);
            //StartAgent();
        }

        void agentClient_RestartServiceRequest(string Sender)
        {
            agentClient.RestartServiceRequest -= agentClient_RestartServiceRequest;
            agentClient.ConnectionRejected -= bbAgentClient_ConnectionRejected;

            agentClient.Stop();

            Logging.Log("Restart Agent Command received.", Logging.EntryType.Info);
            ShutdownRequested?.Invoke(true);

            //agentClient.Dispose();
            //System.Threading.Thread.Sleep(3000);
            //StartAgent();
        }

        public void Start()
        {
            StartAgent();

            while (_run)
            {
                System.Threading.Thread.Sleep(10);
            }
        }

        public void Stop()
        {
            if (agentClient != null) agentClient.Stop();
            _run = false;
        }

        public Workers.ConnectAgentWorker.ActiveAgentStatus GetAgentStatus()
        {
            return agentClient.GetAgentStatus();
        }

        #endregion

        private void bbAgentClient_ConnectionRejected(string message)
        {
            _run = false;
        }
    }
}
