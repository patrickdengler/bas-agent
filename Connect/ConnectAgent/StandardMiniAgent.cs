﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using K2A;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Ionic.Zip;
using K2A.Connect.Models;
using K2A.Connect.mLink.Clients;
using K2A.mLink;
using K2A.Connect.ConnectAgent.Workers;
using K2A.Connect.Models.Auth;
using Newtonsoft.Json.Linq;
using K2A.Data.Redis;
using System.Threading.Tasks;

namespace K2A.Connect.ConnectAgent
{
    public class StandardMiniAgent : BaseAgent
    {
        long txBytes = 0;
        long rxBytes = 0;
        //string hostName = "";
        public StandardMiniAgent(string sn) : base(sn)
        {
            InitializeCPUCounter();

            string fullSn = $"{sn.Substring(0, 5)}-{sn.Substring(5, 5)}-{sn.Substring(10, 5)}-{sn.Substring(15, 5)}-{sn.Substring(20, 5)}";
            //hostName = "K2A" + sn.Substring(20, 5);

            SerialNumber = fullSn;

            Initialize();
        }

        internal override void Initialize()
        {

            string lic = GetLicense();

            if (lic.IsNullOrEmpty() || lic.Length < 10)
            {
                Console.WriteLine($"1 - Unable to find a valid license: {SerialNumber}. Shutting down.");
                Process.GetCurrentProcess().Kill();

                return;
            }
            else
            {
                try
                {
                    var result = JObject.Parse(lic);
                    agentId = (int)result["agentid"];

                    K2A.Connect.LogFiles.CreateAgentLogConfiguration("agent_" + agentId, true, new ConnectAgent.RedisTarget(redis, "Agents:" + agentId + ":Logs"));

                    var svrData = result.GetValue("serverdata").ToString();

                    var serverInfo = Models.ServerInfo.Deserialize(svrData);

                    if (agentId != result["agentid"].ToString().ToInteger(0))
                    {
                        Logging.Log("Invalid serial number and/or license.  Retrying in 60 seconds...", Logging.EntryType.Error);
                        System.Threading.Thread.Sleep(60000);

                        Process.GetCurrentProcess().Kill();

                        return;
                    }

                    ActiveUser.SetConnectionSettings(new ConnectConnectionSettings()
                    {
                        //ConnectionId = serverId,
                        DatabaseName = serverInfo.DatabaseName,
                        DatabasePassword = serverInfo.DatabasePassword,
                        DatabaseUserName = serverInfo.DatabaseUser,
                        ServerAddress = serverInfo.BBServer,
                        DatabaseServerAddress = serverInfo.DatabaseServer,
                        ServerPort = serverInfo.BBServerPort
                    });
                }
                catch
                {
                    Logging.Log("Unable to read license.  Retrying in 60 seconds...", Logging.EntryType.Error);
                    System.Threading.Thread.Sleep(60000);

                    Process.GetCurrentProcess().Kill();

                    return;

                }
            }

            SaveNetInfo();
            StartAgent();

        }

        private void SaveNetInfo()
        {
            string netInfo = "ifconfig".CommandToString("").Replace("\n", "\r\n");
            netInfo.ToFile("/K2A/logs/agent_" + agentId + "/netinfo.txt");
        }

        internal override void miniClient_CheckInResponse(int newVersion, List<string> updateFiles, string licenseFile, DateTime serverDateTime, string commands)
        {
            //bool needReboot = false;

            //if (!string.IsNullOrEmpty(licenseFile))
            //{
            //    if (SetHostName()) needReboot = true;
            //}

            //try
            //{
            //    if (updateFiles.Count > 0 && App.IsRunningOnMono() && !redis.KeyExists("PendingUpdates"))
            //    {
            //        var update = new MiniAgentUpdateInformation() { NewVersion = newVersion, UpdateFiles = updateFiles, Commands = commands };
            //        redis.StoreObject("PendingUpdates", update, 300);
            //    }
            //}
            //catch (Exception ex)
            //{

            //}

            //if (needReboot) Reboot();

            base.miniClient_CheckInResponse(newVersion, updateFiles, licenseFile, serverDateTime, commands);
        }

        internal override int GetTemperature()
        {
            string tempString = "/opt/vc/bin/vcgencmd".CommandToString("measure_temp");
            tempString = tempString.Filter(-1, "0123456789.");
            redis.StoreObject("TempC", tempString, 120);
            return (int)tempString.ToDouble(-445);
        }

        internal override int GetMemory()
        {
            string output = "free".CommandToString("");

            try
            {
                Regex ram_regex = new Regex(@"(\d+)\s+(\d+)");
                string[] lines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                lines[2] = lines[2].Trim();

                Match match = ram_regex.Match(lines[2]);
                if (match.Success && match.Groups.Count > 2)
                {
                    int memUsed = match.Groups[1].Value.ToInteger();
                    int memFree = match.Groups[2].Value.ToInteger();

                    int memPercent = memUsed.PercentOf(memUsed + memFree);
                    return memPercent;

                }
                else
                {
                    return -445;
                }
            }
            catch (Exception)
            {
                return -445;
            }
        }
        internal override int[] GetTxRxBytes()
        {
            string output = "cat".CommandToString("/proc/net/dev");

            long tx = 0;
            long rx = 0;

            try
            {
                Regex ram_regex = new Regex(@"\d+");
                string[] lines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                for (int i = 2; i < lines.Length; i++)
                {
                    lines[i] = lines[i].Trim();
                    if (!string.IsNullOrEmpty(lines[i]))
                    {
                        var match = ram_regex.Matches(lines[i]);
                        if (match != null && match.Count > 15)
                        {
                            rx = rx + ToInt64(match[match.Count - 16].Value, 0);
                            tx = tx + ToInt64(match[match.Count - 8].Value, 0);
                        }
                    }
                }

                int[] result = new int[2] { (int)(tx - txBytes), (int)(rx - rxBytes) };
                txBytes = tx;
                rxBytes = rx;
                return result;
            }
            catch (Exception)
            {
                return new int[2] { -1, -1 };
            }
        }

        public Int64 ToInt64(string s, int defaultVal = 0)
        {
            try
            {
                return Convert.ToInt64(s);
            }
            catch
            {
                return defaultVal;
            }
        }

        private PerformanceCounter cpuCounter;
        private void InitializeCPUCounter()
        {
            try
            {
                cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total", true);

                cpuCounter.NextValue();
            }
            catch { }
        }

        internal override int GetCPU()
        {
            try
            {
                return (int)cpuCounter.NextValue();
            }
            catch
            {
                return -445;
            }
        }


    }
}
