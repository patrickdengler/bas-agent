﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using K2A;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Ionic.Zip;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog.Targets;

namespace K2A.Connect.ConnectAgent
{
    static class Program
    {
        public static System.Threading.ManualResetEvent ShutdownEvent = new System.Threading.ManualResetEvent(false);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {

            System.Threading.ManualResetEvent waitEvent = new System.Threading.ManualResetEvent(false);
            ErrorHandling.UnhandledExceptionManager.AddHandler("MiniAgent");


            Console.WriteLine("Starting Mini Agent Service...");

            if (args.Length == 0)
            {
                Console.WriteLine("No arguments specified");

                var p = Process.Start(new ProcessStartInfo() { FileName = "AgentManager.exe".PathToLocalFile() });

                Thread.Sleep(3600000); //1 hour
                return;
            }
            else
            {
                string sn = args[0].Replace("-", "").ToUpper().Trim();

                if (sn.Trim().Length != 25 || sn.Trim().ToUpper() != sn.Trim().Filter(25, "0123456789ABCDEFVM"))
                {
                    Console.WriteLine($"Invalid serial number: {sn.Trim()}.");
                }
                else
                {
                    if (sn.StartsWith("VMA01")) //used for Docker and secondary agents on the same Mini Agent
                    {
                        var vma = new VirtualMiniAgent(sn.Trim());
                    }
                    else
                    {
                        var ma = new StandardMiniAgent(sn.Trim());
                    }
                }
            }

            //waitEvent.WaitOne();

            Console.WriteLine("Wait Event triggered");
        }



    }
}
