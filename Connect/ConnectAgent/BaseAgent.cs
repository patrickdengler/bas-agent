﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using K2A;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Ionic.Zip;
using K2A.Connect.Models;
using K2A.Connect.mLink.Clients;
using K2A.mLink;
using K2A.Connect.ConnectAgent.Workers;
using K2A.Connect.Models.Auth;
using Newtonsoft.Json.Linq;
using K2A.Data.Redis;
using System.Threading.Tasks;

namespace K2A.Connect.ConnectAgent
{
    public class BaseAgent : IDisposable
    {
        public string SerialNumber = "";
        System.Timers.Timer checkinTimer; //timer to check license, etc
        AgentWorker agentWorker; //Worker performing logging
        Thread agentWorkerThread; //thread for the agent worker
        ManualResetEvent _quitEvent = new ManualResetEvent(false); //used to terminate application before rebooting.
        DateTime lastPPM;
        internal int agentId = 0;
        internal Client redis;
        Stopwatch timeSinceLicenseValidation = new Stopwatch();
        DateTime lastTimeSync = new DateTime(2015, 1, 1);
        private MiniAgentLicenseClient miniClient = new MiniAgentLicenseClient(); //mLink communicator

        public BaseAgent(string sn)
        {
            timeSinceLicenseValidation.Start();
            redis = new Client();

            redis.Connect("127.0.0.1", 2);

            redis.Subscribe("agent_mgr");
            redis.MessageReceived += Redis_MessageReceived;
        }

        private void Redis_MessageReceived(string channel, string message)
        {
            if (channel == "agent_mgr") Logging.Log("Agent Manager: " + message, Logging.EntryType.Error);
        }

        internal virtual void Initialize()
        {



        }
        public void Stop()
        {
            agentWorker.Stop();
            _quitEvent.Set();
        }

        public void Dispose()
        {
            if (checkinTimer != null)
            {
                checkinTimer.Dispose();
                checkinTimer = null;
            }
            if (agentWorker != null)
            {
                agentWorker.Stop();
                agentWorker = null;
            }
        }

        void checkinTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //Syncing Time with database, sending statistics to server
            CheckInWithServer();
            SyncTimeWithDatabase();
        }

        public void StartAgent()
        {
            SyncTimeWithDatabase();

            //Syncing Time with database, sending statistics to server
            checkinTimer = new System.Timers.Timer(60000);
            checkinTimer.Elapsed += checkinTimer_Elapsed;
            checkinTimer.Start();

            int version = "curr.ver".PathToLocalFile().FileToString().ToInteger(0);
            if (version >= 80000) version = version - 80000;
            decimal v = (decimal)version / 100;

            Logging.Log(String.Format("Connect Mini Agent v8.{0:00.00}", v), Logging.EntryType.Info, SerialNumber);
            Logging.Log("Mini Agent Starting. Serial Number: {0}", Logging.EntryType.Info, SerialNumber);

            miniClient.AliveTimeout = 90;
            miniClient.CheckInResponse += miniClient_CheckInResponse;

            lastPPM = DateTime.UtcNow;

            if (agentId == 0)
            {
                Logging.Log($"Agent not found: {SerialNumber}.  Unable to start agent service.", Logging.EntryType.Error);
                return;
            }

            agentWorker = new AgentWorker(agentId);
            agentWorker.ShutdownRequested += AgentWorker_ShutdownRequested;
            agentWorkerThread = new Thread(() => agentWorker.Start());
            agentWorkerThread.Start();

            redis.StoreObject($"Agents:{agentId}:Status", new ConnectAgentWorker.ActiveAgentStatus(), 180);



            Run();
        }

        private void AgentWorker_ShutdownRequested(bool shouldRestart)
        {
            _quitEvent.Set();
        }


        private void Run()
        {
            _quitEvent.WaitOne();

            Console.WriteLine("Stop command received");

            miniClient.KeepAlive = false;
            if (miniClient.IsConnected() || miniClient.Connect(SerialNumber))
            {
                miniClient.CheckIn(SerialNumber, 0, -445, -445, -445, -445, -445, -445, -445, -445, -445);
            }

            agentWorker.Stop();
            miniClient.Disconnect();

            redis.DeleteKey($"Agents:{agentId}:Status");

            Process.GetCurrentProcess().Kill();

            Environment.Exit(0);
        }
        internal virtual void miniClient_CheckInResponse(int newVersion, List<string> updateFiles, string licenseFile, DateTime serverDateTime, string commands)
        {


        }

        void CacheValuesToDatabase()
        {
            using (var data = new Data.DataDriver())
            {
                var trx = data.CreateTransaction();

                var devIds = Agent.DeviceIds(new List<int> { agentId });

                Dictionary<string, int> pathLookup = (from d in devIds select new { devId = d, path = $"Agents:{agentId}:Devices:{d}:cache" }).ToDictionary(t => t.path, t => t.devId);

                var results = redis.ReadMultipleHashes(pathLookup.Keys.ToList());
                redis.DeleteMultipleKeys(pathLookup.Keys.ToList());
                //pt.WriteDebugAndReset("Read redis");

                foreach (var result in results)
                {
                    var devId = pathLookup[result.Key];
                    
                    foreach (var obj in result.Value.Where(v => v.Value.IsNotNullOrEmpty()))
                    {
                        string execString = $"Insert INTO value_cache(agentid, deviceid, objectid, val, datetime) VALUES ({agentId}, {devId}, {obj.Key}, '{obj.Value}', '{DateTime.UtcNow.ToPgDateTime()}') " +
                                    $" ON CONFLICT (agentid, deviceid, objectid) DO UPDATE set val = '{obj.Value}', datetime = '{DateTime.UtcNow.ToPgDateTime()}'";

                        trx.AddToBatch(execString);
                    }

                }

                trx.Commit();

            }

        }
        private void SyncTimeWithDatabase()
        {
            if ((DateTime.UtcNow - lastTimeSync).TotalMinutes < 60) return;

            lastTimeSync = DateTime.UtcNow;

            try
            {
                using (var reader = K2A.Data.DataDriver.Instance.ExecuteRead("select timezone('UTC', now())"))
                {
                    if (reader != null && reader.Read())
                    {
                        var dbDT = reader.GetDateTime(0);
                        int diff = Math.Abs((int)(dbDT - DateTime.UtcNow).TotalMinutes);

                        if (diff >= 3)
                        {
                            string dt = dbDT.FromUtc().ToString("dd MMM yyyy HH:mm:ss");
                            Logging.Log("Server time (UTC): {0}, Local Time: {1}", Logging.EntryType.Info, dbDT.ToString(), dt);
                            Logging.Log("Syncing time with database.", Logging.EntryType.Info);
                            Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = "date -s \"" + dt + "\"" });
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Log("Error with time Synchronization: " + ex.Message, Logging.EntryType.Error);
            }
        }
        internal void CheckInWithServer()
        {
            string lic = GetLicense();

            if ((lic.IsNullOrEmpty() && timeSinceLicenseValidation.Elapsed.TotalDays >= 1) || lic.Length < 10)
            {
                Console.WriteLine("Unable to find a valid license");
                Logging.Log($"Unable to find a valid license: {SerialNumber}.  Shutting down...", Logging.EntryType.Error);

                _quitEvent.Set();
                return;
            }

            ConnectAgentWorker.ActiveAgentStatus agentStatus;

            if (agentWorker != null && agentWorker.Running)
            {
                agentStatus = agentWorker.GetAgentStatus();
                if (agentStatus.PPM > 0) lastPPM = DateTime.UtcNow;

                if (lastPPM.AddHours(1) < DateTime.UtcNow && agentStatus.ObjectCount > 0)
                {
                    Logging.Log("Error recieving values from BAS.  Shutting down.", Logging.EntryType.Info);
                    _quitEvent.Set();
                }
            }
            else
            {
                Logging.Log("Connection to server lost.", Logging.EntryType.Info);
                //_quitEvent.Set();
                return;
            }


            int version = "curr.ver".PathToLocalFile().FileToString().ToInteger(0);

            if (!redis.IsConnected()) Console.WriteLine("Redis not connected!");
            redis.StoreObject($"Agents:{agentId}:Status", agentStatus, 180);


            if (miniClient.IsConnected() || miniClient.Connect(SerialNumber))
            {
                var txrx = GetTxRxBytes();
                miniClient.CheckIn(SerialNumber, version, agentStatus.ObjectCount, agentStatus.PPM, agentStatus.Load, agentStatus.ReadErrors, GetTemperature(), GetCPU(), GetMemory(), txrx[0], txrx[1]);
            }

            var t = Task.Run(() => {
                try
                {
                    CacheValuesToDatabase();
                }
                catch (Exception ex)
                {
                    Logging.Log("Error caching values to PostgreSQL: " + ex.Message, Logging.EntryType.Error);
                }
            });

        }


        internal string GetLicense()
        {

            string lic = "";

            try { lic = redis.ReadObject<string>("Agents:" + SerialNumber + ":License"); } //try to read from cache first
            catch { }

            if (lic.IsNullOrEmpty())
            {
                lic = miniClient.DownloadLicenseFile(SerialNumber); //If not found, download license via mLink Client

                if (lic.IsNotNullOrEmpty() && lic.Length > 10)
                {

                    try
                    {
                        var result = JObject.Parse(lic);
                        if (DateTime.UtcNow > (DateTime)result["expiration"])
                        {
                            Logging.Log("License is expired.  Please contact support@KEY2ACT.com", Logging.EntryType.Error);
                            lic = "-1";
                        }
                        else
                        {
                            timeSinceLicenseValidation.Restart();
                            redis.StoreObject("Agents:" + SerialNumber + ":License", lic, 300, false);
                        }
                    }
                    catch {
                        Logging.Log("Unable to parse license file.", Logging.EntryType.Error);
                        lic = "-1";
                    }

                }
            }
            return lic;
        }

        internal virtual int GetTemperature()
        {
            return -445;
        }
        internal virtual int GetMemory()
        {
            return -445;
        }
        internal virtual int[] GetTxRxBytes()
        {
            return new int[2] { -445, -445 };
        }

        internal virtual int GetCPU()
        {
            return -445;
        }
    }
}
