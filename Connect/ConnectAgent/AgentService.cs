using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using K2A;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Ionic.Zip;
using K2A.Connect.Models;
using K2A.Connect.mLink.Clients;
using K2A.mLink;
using K2A.Connect.ConnectAgent.Workers;

namespace K2A.Connect.ConnectAgent
{
    class AgentService : IDisposable
    {
        ManualResetEvent _quitEvent = new ManualResetEvent(false); //used to terminate application before rebooting.
        //ManualResetEventSlim _downloadReset = new ManualResetEventSlim(false); //Used to wait for async response to license file download
        AgentWorker agentWorker; //Worker performing logging
        Thread agentWorkerThread; //thread for the agent worker
        System.Timers.Timer checkinTimer; //timer to check license, etc
        //bool downloadsInProgress = false; //prevent an additional check in if downloads are being processed
        //bool licRequestSent = false; //used to prevent multiple license requests being sent
        MiniAgentClient miniClient = new MiniAgentClient(); //mLink communicator
        //bool isLicensed = false; //Used to know if it was licensed on initial boot.  If not, reboot after validating license.
        long txBytes = 0;
        long rxBytes = 0;
        //private DateTime lastTimeSync;
        private DateTime lastPPM;

        public void Dispose()
        {
        }

        private void StartAgent()
        {
            lastPPM = DateTime.UtcNow;

            int agentId = ActiveUser.ConnectClient.AgentId;

            if (!ConnectToDatabase()) return;

            //SyncTimeWithDatabase();

            //if (agentId == 0)
            //{
            //    agentId = GetFirstAgentId();
            //}

            if (agentId == 0)
            {
                Logging.Log("Agent not found.  Unable to start agent service.", Logging.EntryType.Error);
                return;
            }

            Logging.Log("Starting Agent Service", Logging.EntryType.Info);

            agentWorker = new AgentWorker(agentId);

            agentWorkerThread = new Thread(new ThreadStart(agentWorker.Start));
            agentWorkerThread.Start();

            Logging.Log(Licensing.SerialNumber(), Logging.EntryType.Info);
            //SaveNetInfo();
        }

        //private void SyncTimeWithDatabase()
        //{
        //    if ((DateTime.UtcNow - lastTimeSync).TotalMinutes < 60) return;

        //    lastTimeSync = DateTime.UtcNow;

        //    if (ActiveUser.ConnectClient.DatabaseServerAddress == "127.0.0.1") return;

        //    using (var reader = K2A.Data.DataDriver.Instance.ExecuteRead("select timezone('UTC', now())"))
        //    {
        //        if (reader != null && reader.Read())
        //        {
        //            var dbDT = reader.GetDateTime(0);
        //            int diff = Math.Abs((int)(dbDT - DateTime.UtcNow).TotalMinutes);

        //            if (diff >= 3)
        //            {
        //                string dt = dbDT.FromUtc().ToString("dd MMM yyyy HH:mm:ss");
        //                Logging.Log("Server time (UTC): {0}, Local Time: {1}", Logging.EntryType.Info, dbDT.ToString(), dt);
        //                Logging.Log("Syncing time with database.", Logging.EntryType.Info);
        //                Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = "date -s \"" + dt + "\"" });
        //            }

        //        }
        //    }
        //}
        private bool ConnectToDatabase()
        {
            int retries = 0;
            System.Threading.Thread.Sleep(3000); //Just wait a few seconds before testing db connection
            while (!K2A.Data.DataDriver.TestDataConnection())
            {
                if (retries == 4)
                {
                    Logging.Log("Unable to connect to database.", Logging.EntryType.Error);
                    return false;
                }
                Logging.Log(string.Format("Unable to connect to database: {0}.  Retrying attempt {1} of 3", K2A.Data.DataDriver.Instance.LastError, retries + 1), Logging.EntryType.Error);
                System.Threading.Thread.Sleep(20000);
                retries++;
            }



            return true;
        }

        //private int GetFirstAgentId()
        //{
        //    Logging.Log("Getting Agent Id from database.", Logging.EntryType.Info);
        //    int retries = 0;
        //    System.Threading.Thread.Sleep(3000); //Just wait a few seconds before testing db connection
        //    while (!K2A.Data.DataDriver.TestDataConnection() && retries < 4)
        //    {
        //        Logging.Log(string.Format("Unable to connect to database: {0}.  Retrying attempt {1} of 3", K2A.Data.DataDriver.Instance.LastError, retries), Logging.EntryType.Error);
        //        System.Threading.Thread.Sleep(20000);
        //        retries++;
        //    }

        //    if (retries == 4)
        //    {
        //        Logging.Log("Unable to connect to database.", Logging.EntryType.Error);
        //        return 0;
        //    }

        //    var agentIds = Connect.Models.Agent.GetAgentIds();
        //    if (agentIds.Count > 0)
        //        return agentIds[0];
        //    else
        //        return 0;
        //}

        private void miniClient_ConnectionLost(Worker sender)
        {
            Logging.Log("Connection to Agent Server lost.", Logging.EntryType.Error);
        }

        private void miniClient_CheckInResponse(int newVersion, List<string> updateFiles, string licenseFile, DateTime serverDateTime, string commands)
        {
            //bool needReboot = false;

            //if (!string.IsNullOrEmpty(licenseFile))
            //{
            //    licenseFile.ToFile("license.lic".PathToLocalFile());
            //    //Licensing.Instance.RefreshLicense();
            //    //Settings.Instance.RefreshSettings();

            //    SetHostName();

            //    needReboot = true;
            //}

            //try
            //{
            //    if (updateFiles.Count > 0)
            //    {
            //        downloadsInProgress = true;
            //        DownloadUpdateFiles(updateFiles);
            //        newVersion.ToString().ToFile("pending.ver".PathToLocalFile());
            //        needReboot = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logging.Log("Error downloading updates: " + ex.Message, Logging.EntryType.Error);
            //    needReboot = false;
            //    downloadsInProgress = false;
            //}

            //if (!string.IsNullOrEmpty(commands))
            //{

            //    string[] cmds = commands.Split(';');
            //    foreach (var cmd in cmds)
            //    {
            //        Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = cmd });
            //    }

            //}

            //if (needReboot) Reboot();
        }

        private bool Ping()
        {
            try
            {
                var ping = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply reply = ping.Send("4.2.2.2", 1000);

                return reply.Status == System.Net.NetworkInformation.IPStatus.Success;
            }
            catch
            {
                return false;
            }
        }

        //private bool RunUpdateCommands()
        //{
        //    bool needRestart = false;

        //    try
        //    {
        //        var dirInfo = new DirectoryInfo(K2A.App.StartupPath);
        //        foreach (var file in dirInfo.GetFiles())
        //        {
        //            string fileName = file.Name;
        //            if (fileName.StartsWith("command") && fileName.EndsWith(".txt"))
        //            {
        //                string cmd = fileName.PathToLocalFile().FileToString();

        //                Logging.Log("Applying Update Command: " + cmd, Logging.EntryType.Info);

        //                var process = Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = cmd });
        //                process.WaitForExit(1000 * 60 * 5);    // Wait up to five minutes.

        //                File.Delete(fileName.PathToLocalFile());
        //                Logging.Log("Update Command applied.", Logging.EntryType.Info);
        //                needRestart = true;
        //            }
        //        }


        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //    return needRestart;
        //}
        //private static void SaveNetInfo()
        //{
        //    string netInfo = "ifconfig".CommandToString("").Replace("\n", "\r\n");
        //    netInfo.ToFile("/K2A/logs/mini_agent/netinfo.txt");
        //}
        public AgentService(int agentId)
        {
            //Settings.Instance.OverrideConnectionId(10);

            ActiveUser.SetConnectionSettings(new ConnectConnectionSettings()
            {
                AgentId = agentId,
                ConnectionId = 96,
                DatabaseName = "connect_demo",
                DatabasePassword = "CVWLS%zpP$",
                DatabaseUserName = "connect_demo",
                ServerAddress = "appservice-01.connect.key2act.io",
                DatabaseServerAddress = "connectdata.cirrus.key2act.com",
                ServerPort = 57580
            });

            //ActiveUser.ConnectClient.AgentId = 11;

            K2A.Connect.LogFiles.CreateAgentLogConfiguration("mini_agent", true);

            //if (ExtractWebSetupFiles())
            //{
            //    Reboot();
            //    System.Threading.Thread.Sleep(10000); //just wait to give the reboot command time to process
            //    return;
            //}
            //if (RunUpdateCommands())
            //{
            //    Reboot();
            //    System.Threading.Thread.Sleep(10000); //just wait to give the reboot command time to process
            //    return;
            //}
            Thread.Sleep(2000);

            //InitializeCPUCounter();


            //Logging.Log("Mini Agent Starting. Serial Number: {0}, License Expires: {1}", Logging.EntryType.Info, Licensing.SerialNumber(), Licensing.Instance.GetDateTime("Connect", "Expiration"));

            if (File.Exists("pending.ver".PathToLocalFile()))
            {
                int v = "pending.ver".PathToLocalFile().FileToString().ToInteger();
                //Logging.Log("Mini Agent Version: 7.{0:0000}", Logging.EntryType.Info, v, Licensing.Instance.GetDateTime("Connect", "Expiration"));
            }

            //    if (ApplyUpdates())
            //{
            //    Reboot();
            //    System.Threading.Thread.Sleep(10000); //just wait to give the reboot command time to process
            //    return;
            //}



            Logging.Log("Settings Loaded", Logging.EntryType.Info);

            //CreateSettingsFile();

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Logging.Log("Waiting for network connection...", Logging.EntryType.Info);
            while (!Ping() && sw.ElapsedMilliseconds < 30000)
            {
                Thread.Sleep(50);
            }

            string[] ipAddresses = "hostname".CommandToString("-I").Replace("\n", "\r\n").Lines();
            Logging.Log("IP Addresses: {0}", Logging.EntryType.Info, String.Join(";", ipAddresses));

            Logging.Log("Checking firewall...", Logging.EntryType.Info);
            var status = K2A.Net.ConnectNetworkStatus.GetStatus(5432, 57000, ActiveUser.ConnectClient.ServerPort);
            if (status.AllStatusOk)
            {
                Logging.Log("Firewall settings verified.", Logging.EntryType.Info);
            }
            else
            {
                if (status.DatabaseNetworkStatus)
                    Logging.Log("Database Port: OK", Logging.EntryType.Info);
                else
                    Logging.Log("Database Port: UNABLE TO REACH PORT", Logging.EntryType.Error);

                if (status.LicensingNetworkStatus)
                    Logging.Log("Licensing Port: OK", Logging.EntryType.Info);
                else
                    Logging.Log("Licensing Port: UNABLE TO REACH PORT", Logging.EntryType.Error);

                if (status.ServerNetworkStatus)
                    Logging.Log("Connect Server Port: OK", Logging.EntryType.Info);
                else
                    Logging.Log("Connect Server Port: UNABLE TO REACH PORT", Logging.EntryType.Error);

                Logging.Log("One or more ports could not be reached.  Verify your DNS settings and make sure the following ports are open: 5432, 57000, {0}.", Logging.EntryType.Error, ActiveUser.ConnectClient.ServerPort);
                Logging.Log("This test uses http://portquiz.net.  If this is blocked by your IT department, this test may provide a false failure.", Logging.EntryType.Error, ActiveUser.ConnectClient.ServerPort);
            }

            //var file = MiniAgentStatusFile.Read();
            //file.IPAddresses = String.Join(";", ipAddresses);
            //file.HostName = Environment.MachineName;
            //file.SerialNumber = Licensing.SerialNumber();
            //file.LastStart = DateTime.Now;
            //file.Update();

            //SaveNetInfo();
            //Logging.Log("Getting txrx", Logging.EntryType.Info);
            GetTxRxBytes();

            miniClient.AliveTimeout = 90;
            miniClient.CheckInResponse += miniClient_CheckInResponse;
            //miniClient.LicenseResponse += miniClient_LicenseResponse;
            miniClient.ConnectionLost += miniClient_ConnectionLost;

            StartAgent();

            var RunThread = new System.Threading.Thread(this.Run) { Name = "Mini Agent Run Thread" };
            RunThread.Start();

        }

        private void Run()
        {
            Console.WriteLine(Licensing.SerialNumber());
            _quitEvent.WaitOne();
        }
        private void CheckInWithServer()
        {
            ConnectAgentWorker.ActiveAgentStatus agentStatus;

            if (agentWorker != null && agentWorker.Running)
            {
                agentStatus = agentWorker.GetAgentStatus();
                if (agentStatus.PPM > 0) lastPPM = DateTime.UtcNow;
                if (lastPPM.AddHours(1) < DateTime.UtcNow)
                {
                    Logging.Log("Error recieving values from BAS.  Rebooting.", Logging.EntryType.Info);
                    Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = "reboot" });
                }
            }
            else
            {
                agentStatus = new ConnectAgentWorker.ActiveAgentStatus();
                //RestartAgent();
                Logging.Log("Connection to server lost.  Reboot Command Sent.", Logging.EntryType.Info);
                Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = "reboot" });
            }


            int version = "curr.ver".PathToLocalFile().FileToString().ToInteger(0);

            //if (!downloadsInProgress)
            //{
            if (miniClient.IsConnected() || miniClient.Connect())
            {
                MiniAgentStatusFile file = MiniAgentStatusFile.Read();
                int[] txrx = GetTxRxBytes();
                file.CheckIn(version, agentStatus.ObjectCount, agentStatus.PPM, agentStatus.Load, agentStatus.ReadErrors, -445, (int)cpuCounter.NextValue(), (int)GetMemory(), txrx[0], txrx[1]);

                miniClient.CheckIn(Licensing.SerialNumber(), file.Version, file.ObjectCount, file.PPM, file.Load, file.ReadErrors, file.TemperatureC, file.CPU, file.Memory, file.Tx, file.Rx);
            }
            //}
        }

        void checkinTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
                CheckInWithServer();


        }

        void miniClient_LicenseResponse(bool foundLicense)
        {
            //if (foundLicense)
            //{
            //    Logging.Log("License file received from server.", Logging.EntryType.Info);
            //    Licensing.Instance.RefreshLicense();
            //}

            //_downloadReset.Set();
        }

        private bool IsLicenseValid()
        {
            return true;
            //Logging.Log(String.Format("Expiration: {0}, MaxAgents: {1}", Licensing.Instance.GetDateTime("Connect", "Expiration"), Licensing.Instance.GetInteger("Connect", "MaxAgents")), Logging.EntryType.Info);
            //return Licensing.Instance.GetDateTime("Connect", "Expiration") > DateTime.UtcNow && Licensing.Instance.GetInteger("Connect", "MaxAgents") > 0;
        }

        //private void SubmitNewLicenseRequest()
        //{
        //    if (miniClient.IsConnected())
        //    {
        //        string ver = "curr.ver".PathToLocalFile().FileToString();
        //        Logging.Log("Agent License Expired or not valid.  Sending License Request.", Logging.EntryType.Error);
        //        string file = Licensing.Instance.CreateRequest("KEY2ACT", "brian@key2act.com", "KEY2ACT", "KEY2ACT", "Mini Agent", ver, "lic.licreq".PathToLocalFile());
        //        if (!string.IsNullOrEmpty(file))
        //        {
        //            //172.15.156.81
        //            var bytes = System.IO.File.ReadAllBytes(file);
        //            miniClient.RequestLicense(bytes);

        //            System.IO.File.Delete(file);
        //            licRequestSent = true;
        //        }
        //    }

        //}

        //private bool CheckLicense()
        //{
        //    try
        //    {
        //        if (!miniClient.IsConnected())
        //        {
        //            if (!miniClient.Connect()) Logging.Log("Unable to connect to Mini Server", Logging.EntryType.Error);
        //        }

        //        if (miniClient.IsConnected()) //don't attempt download if connection to server failed
        //        {
        //            try
        //            {
        //                LicenseCheckClient.DownloadLicense();
        //            }
        //            catch
        //            {
        //                Logging.Log("Timeout getting license from server", Logging.EntryType.Error);
        //            }

        //            //_downloadReset.Reset();
        //            //Logging.Log("Requesting License from server.", Logging.EntryType.Info);
        //            //miniClient.DownloadLicense();
        //            //bool completed = _downloadReset.Wait(2000);
        //            //if (!completed) Logging.Log("Timeout getting license from server", Logging.EntryType.Error);
        //        }

        //        if (!IsLicenseValid()) //License NOT valid
        //        {
        //            if (!licRequestSent) //Make sure it hasn't already been sent
        //            {
        //                SubmitNewLicenseRequest();
        //            }

        //            return false;
        //        }
        //        else //License is valid
        //        {
        //            //Set hostname from license file
        //            if (SetHostName()) Reboot();

        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logging.Log("Error checking license: " + ex.Message, Logging.EntryType.Error);
        //        return false;
        //    }
        //}

        //private void Reboot()
        //{
        //    Logging.Log("Reboot Command Sent.", Logging.EntryType.Info);
        //    Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = "reboot" });
        //    checkinTimer.Stop();
        //    _quitEvent.Set();
        //}

        //private bool SetHostName()
        //{
        //    Logging.Log("HostName: {0}", Logging.EntryType.Info, Environment.MachineName);

        //    string refNum = Licensing.Instance["General", "Reference"];
        //    if (string.IsNullOrEmpty(refNum)) return false;

        //    string hostName = "K2A" + refNum;
        //    if (Environment.MachineName == hostName) return false; //if no name is specified or it already matches, ignore

        //    StringBuilder sb = new StringBuilder();

        //    string fileData = "/etc/hosts".FileToString();
        //    var lines = fileData.Lines();

        //    foreach (var line in lines)
        //    {
        //        if (line.StartsWith("127.0.1.1"))
        //        {
        //            sb.AppendLine("127.0.1.1\t" + hostName);
        //        }
        //        else
        //        {
        //            sb.AppendLine(line);
        //        }
        //    }

        //    sb.ToString().ToFile("/etc/hosts");

        //    hostName.ToFile("/etc/hostname");

        //    Process.Start(new ProcessStartInfo() { FileName = "sudo", Arguments = "/etc/init.d/hostname.sh" });
        //    return true;
        //}

        #region "Performance"
        //private static bool ExtractWebSetupFiles()
        //{
        //    try
        //    {
        //        if (File.Exists("setup.zip".PathToLocalFile()))
        //        {
        //            Logging.Log("Applying Updates to Agent Setup.", Logging.EntryType.Info);
        //            using (ZipFile zip1 = ZipFile.Read("setup.zip".PathToLocalFile()))
        //            {
        //                zip1.ExtractAll("/K2A_Setup", ExtractExistingFileAction.OverwriteSilently);
        //            }

        //            File.Delete("setup.zip".PathToLocalFile());
        //            return true;
        //        }
        //    }
        //    catch
        //    {

        //    }
        //    return false;
        //}
        //private void DownloadUpdateFiles(List<string> files)
        //{
        //    Logging.Log("Downloading Update Files", Logging.EntryType.Info);

        //    string updatePath = string.Format("{0}updates{1}", K2A.App.StartupPath, Path.DirectorySeparatorChar);

        //    foreach (string file in files)
        //    {
        //        string zipFile = string.Format("{0}{1}{2}", updatePath, Path.DirectorySeparatorChar, file);

        //        K2A.Web.DownloadFile("http://m2m.net/BB7/miniupdates/" + file, zipFile);

        //        try
        //        {
        //            using (ZipFile zip1 = ZipFile.Read(zipFile))
        //            {
        //                foreach (ZipEntry e in zip1)
        //                {
        //                    e.Extract(updatePath, ExtractExistingFileAction.OverwriteSilently);
        //                }
        //            }

        //            System.IO.File.Delete(zipFile);
        //        }
        //        catch (Exception ex)
        //        {
        //            Logging.Log("Error unzipping file: " + ex.Message, Logging.EntryType.Error);
        //            ex.StackTrace.ToFile("unziperror.txt".PathToLocalFile());
        //        }
        //    }

        //}

        //static void DownloadUpdateFiles(List<string> files)
        //{
        //    string updatePath = string.Format("{0}updates{1}", K2A.App.StartupPath, Path.DirectorySeparatorChar);

        //    Logging.Log("Downloading Update Files", Logging.EntryType.Info);

        //    using (var data = new K2A.Data.DataDriver(K2A.Data.DataDriver.ConnectionDestination.UpdateServer))
        //    {
        //        foreach (var file in files)
        //        {
        //            string zipFile = K2A.App.StartupPath + "updates" + Path.DirectorySeparatorChar + file;

        //            using (var reader = data.ExecuteRead("Select filedata from agentupdates where filename like '" + file + "'"))
        //            {
        //                reader.Read();
        //                byte[] fdata = (byte[])reader.GetObject("filedata");
        //                File.WriteAllBytes(zipFile, fdata);
        //            }

        //            using (ZipFile zip1 = ZipFile.Read(zipFile))
        //            {
        //                foreach (ZipEntry e in zip1)
        //                {
        //                    e.Extract(updatePath, ExtractExistingFileAction.OverwriteSilently);
        //                }
        //            }

        //            System.IO.File.Delete(zipFile);

        //        }
        //    }

        //    ExtractWebSetupFiles();

        //}


        //private double GetTemp()
        //{
        //    string tempString = "/opt/vc/bin/vcgencmd".CommandToString("measure_temp");
        //    if (tempString.Length > 0)
        //    {
        //        Regex ram_regex = new Regex(@"\=(.*?)\'");

        //        Match match = ram_regex.Match(tempString);
        //        if (match.Success)
        //        {
        //            return match.Groups[1].Value.ToDouble();
        //        }
        //    }

        //    return -1;
        //}

        private int GetMemory()
        {
            string output = "free".CommandToString("");

            try
            {
                Regex ram_regex = new Regex(@"(\d+)\s+(\d+)");
                string[] lines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                lines[2] = lines[2].Trim();

                Match match = ram_regex.Match(lines[2]);
                if (match.Success && match.Groups.Count > 2)
                {
                    int memUsed = match.Groups[1].Value.ToInteger();
                    int memFree = match.Groups[2].Value.ToInteger();

                    int memPercent = memUsed.PercentOf(memUsed + memFree);
                    return memPercent;

                }
                else
                {
                    return -1;
                }
            }
            catch (Exception)
            {
                return -1;
            }
        }

        private int[] GetTxRxBytes()
        {
            string output = "cat".CommandToString("/proc/net/dev");

            long tx = 0;
            long rx = 0;

            try
            {
                Regex ram_regex = new Regex(@"\d+");
                string[] lines = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                for (int i = 2; i < lines.Length; i++)
                {
                    lines[i] = lines[i].Trim();
                    if (!string.IsNullOrEmpty(lines[i]))
                    {
                        var match = ram_regex.Matches(lines[i]);
                        if (match != null && match.Count > 15)
                        {
                            rx = rx + ToInt64(match[match.Count - 16].Value, 0);
                            tx = tx + ToInt64(match[match.Count - 8].Value, 0);
                        }
                    }
                }

                int[] result = new int[2] { (int)(tx - txBytes), (int)(rx - rxBytes) };
                txBytes = tx;
                rxBytes = rx;
                return result;
            }
            catch (Exception)
            {
                return new int[2] { -1, -1 };
            }
        }

        public Int64 ToInt64(string s, int defaultVal = 0)
        {
            try
            {
                return Convert.ToInt64(s);
            }
            catch
            {
                return defaultVal;
            }
        }

        private PerformanceCounter cpuCounter;
        private void InitializeCPUCounter()
        {
            cpuCounter = new PerformanceCounter(
            "Processor",
            "% Processor Time",
            "_Total",
            true
            );

            cpuCounter.NextValue();
        }
        #endregion
    }
}
