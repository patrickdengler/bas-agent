﻿using System;
using System.Collections.Generic;
using System.Linq;
using K2A.Connect.Models;
using System.Threading;
using K2A.mLink;
using K2A.Connect.mLink.Clients;
using K2A.Connect.mLink.Messages;

namespace K2A.Connect.ConnectAgent.Workers
{
    public class SmartThingsAgentWorker : ConnectAgentWorker
    {

        internal override void ReadObjectID(string objObject, int ReqID)
        {
            OnValueRead(ReqID, "SmartThings Value", "");
        }


        internal override void ReadValue(string devObject, string objObject, int ReqID)
        {
            SmartThingsConnectionInfo connectionInfo = SmartThingsConnectionInfo.Deserialize(MyAgent.Connection);
            var devData = K2A.Net.SmartThings.SmartThingsDriver.ReadHub(connectionInfo);

            if (devData != null)
            {
                var stDev = (from d in devData where d.id == devObject select d).FirstOrDefault();
                if (stDev != null)
                {
                    var stObj = (from o in stDev.objects where o.name == objObject select o).FirstOrDefault();

                    if (stObj != null)
                    {
                        OnValueRead(ReqID, stObj.value, stObj.value);
                    }
                }
            }
        }

        internal override void StartDriver()
        {
            if (!MyAgent.DisableLogging)
            {
                lWorker = new LogWorker(MyAgent);
                lWorker.ObjectListCreated += lWorker_ObjectListCreated;
                lWorker.ReadObjectRequest += lWorker_ReadObjectRequest;
            }
        }

        #region "SmartThings Methods"
        private void STScan(string path, bool objScan)
        {
            SmartThingsConnectionInfo connectionInfo = SmartThingsConnectionInfo.Deserialize(MyAgent.Connection);
            var devData = K2A.Net.SmartThings.SmartThingsDriver.ReadHub(connectionInfo);

            if (devData != null)
            {
                if (!objScan)
                {
                    foreach (var dev in devData)
                    {
                        var devRes = new DeviceScanResponse
                        {
                            DevicePath = dev.id,
                            Name = dev.device,
                            DeviceObject = dev.id,
                            Description = dev.device,
                            Model = "SmartThings",
                        };
                        Client.DeviceScanIAm(DevScanClient, devRes);
                    }
                    Client.ScanStateChanged(DevScanClient, AgentClient.ScanStatus.Complete);
                }
                else
                {
                    var stDev = (from d in devData where d.id == path select d).FirstOrDefault();

                    if (stDev != null)
                    {
                        foreach (var obj in stDev.objects)
                        {
                            var ObjRes = new ObjectScanResponse
                            {
                                Name = obj.name,
                                Description = obj.name,
                                ObjectObject = obj.name,
                                ObjectValue = obj.value
                            };
                            Client.SendAsync(ObjScanClient, Client.ClientName, (Int16)ConnectClient.MessageTypeEnum.ObjectScanResponse, ObjRes);
                        }
                        Client.ScanStateChanged(ObjScanClient, AgentClient.ScanStatus.Complete);
                    }
                }
            }


        }
        #endregion

        #region "ClientRequests"

        internal override void DeviceScan(string Sender, mLink.Messages.DeviceScan Message)
        {
            var t2 = new Thread(() => STScan(Message.PathToScan, false));
            t2.IsBackground = true;
            t2.Start();
        }
        internal override void ObjectScan(string Sender, mLink.Messages.ObjectScan Message)
        {
            var t2 = new Thread(() => STScan(Message.PathToScan, true));
            t2.IsBackground = true;
            t2.Start();
        }

        private void lWorker_ObjectListCreated(List<LogWorker.ObjectInfo> objectList)
        {

        }
        private void lWorker_ReadObjectRequest(List<LogWorker.ObjectInfo> objectList)
        {
            var t = new Thread(() => ReadObjectList(objectList)) { IsBackground = true };
            t.Start();

        }
        private void ReadObjectList(List<LogWorker.ObjectInfo> objectList)
        {

            lWorker.PendingValueReads = objectList.Count;

            SmartThingsConnectionInfo connectionInfo = SmartThingsConnectionInfo.Deserialize(MyAgent.Connection);
            var devData = K2A.Net.SmartThings.SmartThingsDriver.ReadHub(connectionInfo);

            if (devData != null)
            {
                foreach (var obj in objectList)
                {
                    var stDev = (from d in devData where d.id == obj.DeviceLog.DeviceObject select d).FirstOrDefault();
                    if (stDev != null)
                    {
                        var stObj = (from o in stDev.objects where o.name == obj.ObjectLog.ObjectString select o).FirstOrDefault();

                        if (stObj != null)
                        {
                            obj.ObjectLog.ObjectValue = stObj.value;
                            lWorker.ValueReadComplete(obj);
                        }
                        else
                        {
                            lWorker.ValueReadComplete(obj, true, "Object Not Found");
                        }
                    }
                }
            }
            else
            {
                foreach (var obj in objectList)
                {
                    lWorker.ValueReadComplete(obj, true, "Device Not Found");
                }
            }

            lWorker.PendingValueReads = 0;
        }
        #endregion
    }
}
