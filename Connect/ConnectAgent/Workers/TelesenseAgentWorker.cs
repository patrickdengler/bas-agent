﻿using System;
using System.Collections.Generic;
using System.Linq;
using K2A.Connect.Models;
using K2A.mLink.Messages;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using K2A.Connect.mLink.Messages;

namespace K2A.Connect.ConnectAgent.Workers
{
    public class TelesenseAgentWorker : ConnectAgentWorker
    {
        TelesenseData telesenseData;
        internal override void ReadObjectID(string objObject, int ReqID)
        {
            OnValueRead(ReqID, "Telesense Value", "");
        }


        internal override void ReadValue(string devObject, string objObject, int ReqID)
        {
            var val = telesenseData.GetValue(devObject, objObject);
            OnValueRead(ReqID, val.Value, val.Value);
        }

        internal override void StartDriver()
        {
            telesenseData = new TelesenseData(MyAgent);

            if (!MyAgent.DisableLogging)
            {
                lWorker = new LogWorker(MyAgent);
                lWorker.ObjectListCreated += lWorker_ObjectListCreated;
                lWorker.ReadObjectRequest += lWorker_ReadObjectRequest;
            }
        }

        #region "ClientRequests"

        internal override void DeviceScan(string Sender, DeviceScan Message)
        {
            var devices = telesenseData.GetDevices();

            if (devices != null && devices.Count > 0)
            {
                foreach (var dev in devices)
                {
                    var devScanResponse = new DeviceScanResponse() { Description = "Telesense_Device", DeviceObject = dev.Uuid, DevicePath = dev.Uuid, IconId = -1, Model = "Telesense", Name = dev.DeviceName };
                    Client.DeviceScanIAm(DevScanClient, devScanResponse);
                }
            }

            Client.ScanStateChanged(DevScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }
        internal override void ObjectScan(string Sender, ObjectScan Message)
        {
            var dev = telesenseData.GetDevice(Message.PathToScan);
            if (dev != null && dev.ObjectNames.Count > 0)
            {
                foreach (var obj in dev.ObjectNames)
                {
                    var ObjRes = new ObjectScanResponse()
                    {
                        Name = obj.Value,
                        Description = "Value",
                        ObjectObject = obj.Key,
                        ObjectValue = dev.GetValue(obj.Key).Value
                    };

                    Client.SendAsync(ObjScanClient, mLink.Clients.ConnectClient.MessageTypeEnum.ObjectScanResponse, ObjRes);
                }
            }


            Client.ScanStateChanged(ObjScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }

        private void lWorker_ObjectListCreated(List<LogWorker.ObjectInfo> objectList)
        {

        }
        private void lWorker_ReadObjectRequest(List<LogWorker.ObjectInfo> objectList)
        {
            var t = new Thread(() => ReadObjectList(objectList)) { IsBackground = true };
            t.Start();

        }

        private void ReadObjectList(List<LogWorker.ObjectInfo> objectList)
        {
            lWorker.PendingValueReads = objectList.Count;

            TelesenseConnectionInfo connectionInfo = TelesenseConnectionInfo.Deserialize(MyAgent.Connection);

            var devs = objectList.GroupBy(o => o.DeviceLog.DeviceObject);

            foreach (var dev in devs)
            {
                var teleDev = telesenseData.GetDevice(dev.Key);

                foreach (LogWorker.ObjectInfo obj in dev)
                {
                    if (teleDev == null)
                    {
                        lWorker.ValueReadComplete(obj, true, "Device Not Found");
                    }
                    else
                    {
                        var val = teleDev.GetValue(obj.ObjectLog.ObjectString);

                        if (!val.HasError)
                        {
                            obj.ObjectLog.ObjectValue = val.Value;
                            lWorker.ValueReadComplete(obj);
                        }
                        else
                            lWorker.ValueReadComplete(obj, true, val.Value);
                    }
                }

            }

            lWorker.PendingValueReads = 0;


        }
        #endregion

        private class TelesenseData
        {
            private List<TelesenseDeviceCache> devices { get; set; }
            private Dictionary<string, TelesenseDeviceCache> deviceLookup;
            public TelesenseData(Agent myAgent)
            {
                Logging.Log("Loading Telesense Devices from Server", Logging.EntryType.Info);
                bool success = false;
                int tries = 0;

                while (!success && tries < 5)
                {
                    if (tries > 0)
                    {
                        System.Threading.Thread.Sleep(5000);
                        Logging.Log("Retrying to load Telesense Devices from Server", Logging.EntryType.Info);
                    }
                    try
                    {
                        tries++;
                        using (var client = new mLink.Clients.LicenseCheckClient())
                        {
                            var devices = client.RequestTelesenseDevices(Settings.Instance.ConnectClient.DatabaseName, myAgent.Id);

                            if (devices != null)
                            {
                                this.devices = new List<TelesenseDeviceCache>();
                                deviceLookup = new Dictionary<string, TelesenseDeviceCache>();
                                foreach (var dev in devices)
                                {
                                    var newDev = new TelesenseDeviceCache() { MyAgent = myAgent, ObjectNames = dev.ObjectNames, Uuid = dev.uuid, DeviceName = dev.Name };

                                    deviceLookup.Add(dev.uuid, newDev);
                                    this.devices.Add(newDev);
                                }
                                success = true;
                            }

                        }
                    }
                    catch
                    {
                        Logging.Log("Error reading Telesense Devices from Server", Logging.EntryType.Error);
                    }
                }

            }

            public List<TelesenseDeviceCache> GetDevices()
            {
                return devices;
            }

            public TelesenseDeviceCache GetDevice(string uuid)
            {
                if (deviceLookup.ContainsKey(uuid))
                {
                    var dev = deviceLookup[uuid];
                    dev.RefreshValues();
                    return dev;
                }

                return null;
            }

            public TelesenseValue GetValue(string uuid, string objName)
            {
                var dev = GetDevice(uuid);
                if (dev == null) return new TelesenseValue() { HasError = true, Value = "UNKNOWN DEVICE" };

                return dev.GetValue(objName);
            }
        }
        private class TelesenseDeviceCache
        {
            public string Uuid { get; set; }
            public string DeviceName { get; set; }
            public Dictionary<string, string> ObjectValues { get; set; }
            public Dictionary<string, string> ObjectNames { get; set; }
            private DateTime LastUpdate { get; set; }
            public Agent MyAgent { get; set; }

            public TelesenseValue GetValue(string objName)
            {
                RefreshValues();

                if (!ObjectValues.ContainsKey(objName))
                {
                    Logging.Log("Unknown object in uuid " + Uuid + ": " + objName, Logging.EntryType.Error);
                    return new TelesenseValue() { HasError = true, Value = "UNKNOWN OBJECT" };
                }
                else
                {
                    if (objName.EndsWith("tc"))
                    {
                        try
                        {
                            var cVal = ObjectValues[objName].ToDouble();
                            var fVal = cVal * 1.8 + 32;
                            if (fVal < -200) fVal = -445;
                            return new TelesenseValue() { Value = fVal.ToString() };
                        }
                        catch
                        {
                        }
                    }

                    return new TelesenseValue() { Value = ObjectValues[objName] };
                }
            }

            internal void RefreshValues()
            {
                if (ObjectValues == null) ObjectValues = new Dictionary<string, string>();
                if (DateTime.UtcNow > LastUpdate.AddMinutes(1))
                {
                    lock (ObjectValues)
                    {
                        try
                        {
                            TelesenseConnectionInfo tConnectionInfo = TelesenseConnectionInfo.Deserialize(MyAgent.Connection);
                            string url = tConnectionInfo.Server + Uuid;
                            string data = K2A.Net.Http.DownloadString(url, null, 15000, true);
                            var values = GetValuesFromJSON(data);

                            if (values != null && values.Count > 0) ObjectValues = values;
                            LastUpdate = DateTime.UtcNow;
                        }
                        catch
                        {
                            Logging.Log("Unable to refresh Telesense values.", Logging.EntryType.Error);
                        }
                    }
                }
            }
            private Dictionary<string, string> GetValuesFromJSON(string data)
            {

                try
                {
                    JObject o = JObject.Parse(data);
                    var objs = (from os in o["state"]["payload"] select (JProperty)os).ToDictionary(p => p.Name, p => p.Value.ToString());
                    return objs;
                }
                catch
                {

                    return null;
                }
            }
        }

        private class TelesenseValue
        {
            public string Value { get; set; }
            public bool HasError { get; set; }
        }


    }
}
