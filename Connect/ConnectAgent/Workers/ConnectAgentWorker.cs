﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using K2A.Connect.Models;
using K2A;
using K2A.Net.BACnet;
using K2A.Net.Niagara;
using System.Threading.Tasks;
using K2A.Connect.mLink.Clients;
using K2A.Connect.mLink.Messages;
using K2A.mLink.Client;
using K2A.mLink;
using K2A.Data.Redis;

namespace K2A.Connect.ConnectAgent.Workers
{
    public class ConnectAgentWorker
    {
        internal K2A.Connect.mLink.Clients.AgentClient Client;

        public delegate void ConnectionRejectedHandler(string message);
        public event ConnectionRejectedHandler ConnectionRejected;

        public delegate void ServiceStoppedHandler();
        public event ServiceStoppedHandler ServiceStopped;

        public delegate void RestartRequestEventHandler(string Sender);
        public event RestartRequestEventHandler RestartServiceRequest;

        public int AgentId { get; set; }

        internal Dictionary<int, WatchTag> requestLookup = new Dictionary<int, WatchTag>();

        private RemoteAccessHost remoteAccessHost;
        private RemoteAccessHost remoteAccessSecondaryHost;

        private ManualResetEvent weatherReset = new ManualResetEvent(false);

        private DateTime lastWeatherUpdate;
        private Dictionary<string, string> weatherData = new Dictionary<string, string>();

        Client redis;

        public bool Status
        {
            get
            {
                return _run;
            }
        }

        public LogWorker lWorker;

        private Agent myAgent;
        public Agent MyAgent
        {
            get
            {
                return myAgent;
            }
            set
            {
                if (value != null) AgentId = value.Id;
                myAgent = value;
            }
        }
        private bool _run = false;

        internal Hashtable DevIDLookup = new Hashtable();

        internal string DevScanClient;

        internal Hashtable ObjIDLookup = new Hashtable();

        internal string ObjScanClient;

        internal virtual void StartDriver()
        {

        }

        protected virtual void OnValueRead(int RequestId, string Value, string displayValue)
        {
            lock (requestLookup)
            {
                if (requestLookup.ContainsKey(RequestId))
                {
                    var req = requestLookup[RequestId];

                    Client.RespondToWatch(req.ClientName, req.RequestID, Value, displayValue);
                    requestLookup.Remove(RequestId);

                    CacheValue(req.CachePath, Value);
                }
            }
        }

        public void CacheValue(string cachePath, string value)
        {
            redis.StoreObject(cachePath, value, 15);

            string hashPath = cachePath.Substring(0, cachePath.IndexOf(":Objects:") + 1);

            string s2 = cachePath.Substring(0, cachePath.IndexOf(":Objects:"));
            string s3 = cachePath.Substring(s2.Length + 9, cachePath.IndexOf(":", s2.Length + 9) - (s2.Length + 9));

            redis.AppendToHash(hashPath + "cache", s3, value);
        }

        internal void OnValueReadMultiple(string clientname, List<WatchMultipleResponse.Response> objects)
        {
            Client.RespondMultiple(clientname, objects);
        }
        
        internal virtual void ReadObjectID(string objObject, int ReqID)
        {

        }

        internal virtual void ReadPriorityArray(string devObject, string objObject, int ReqID)
        {

        }

        internal virtual void ReadValue(string devObject, string objObject, int ReqID)
        {

        }

        internal virtual void WritePriorityValue(string devObject, string objObject, string newValue, string priority)
        {

        }

        internal virtual void WriteValue(string devObject, string objObject, string newValue)
        {

        }

        public ConnectAgentWorker()
        {
            redis = new Client();

            redis.Connect("127.0.0.1", 2);

            Client = new AgentClient();

            Client.ClientName = null;
            Client.ClientType = ConnectClient.ConnectClientType.AgentClient;
            Client.DeviceScan += new AgentClient.DeviceScanEventHandler(Client_DeviceScan);
            Client.ObjectScan += new AgentClient.ObjectScanEventHandler(Client_ObjectScan);
            Client.RestartServiceRequest += new AgentClient.RestartRequestEventHandler(Client_RestartServiceRequest);
            Client.StopServiceRequest += new AgentClient.StopRequestEventHandler(Client_StopServiceRequest);
            Client.WeatherDataResponse += new AgentClient.WeatherDataResponseHandler(Client_WeatherDataResponse);
            Client.WatchRequest += new AgentClient.WatchRequestEventHandler(ProcessWatchRequest);
            Client.WatchMultiple += new AgentClient.WatchMultipleEventHandler(Client_WatchMultiple);
            Client.WriteValue += new AgentClient.WriteValueEventHandler(Client_WriteValue);
            Client.ConnectionRejected += Client_ConnectionRejected;
            Client.DeviceScanAbort += DeviceScanAbort;
            Client.ObjectScanAbort += ObjectScanAbort;

            lastWeatherUpdate = new DateTime(2010, 1, 1);
        }

        internal virtual void ObjectScanAbort()
        {
            
        }

        internal virtual void DeviceScanAbort()
        {
            
        }

        public void Start()
        {
            if (Status)
            {
                return;
            }
            _run = true;

            Client.AliveTimeout = 35;
            Logging.Log("Start command sent to Connect Agent Client", Logging.EntryType.Info);
            var RunThread = new System.Threading.Thread(this.Execute) { Name = "Connect Agent Client Thread" };
            RunThread.Start();
        }

        public virtual void Stop()
        {
            try
            {
                _run = false;
                Client.Disconnect();
                Client.Dispose();

                if (MyAgent == null)
                {
                    return;
                }

                ServerLog.Log(ServerLog.SenderType.Agent, MyAgent.Id, ServerLog.EventLevel.Info, ServerLog.MessageType.AgentStop, MyAgent.RefId + " Agent Stopped");

                lWorker.Stop();
                lWorker.ServerObjectRequest -= LWorker_ServerObjectRequest;
            }
            catch
            {
            }
        }

        private void ConnectToServer()
        {
            string procName = Process.GetCurrentProcess().ProcessName.ToLower();
            if (procName != "agentsvc" && procName != "agentsvc.vshost" && procName != "miniagent" && procName != "miniagent.vshost")
            {
                _run = false;
                Logging.Log("Agents disabled", Logging.EntryType.Error);
                return;
            }

            System.Threading.Thread.Sleep(500);

            if (Client.ConnectionStatus != K2A.mLink.Worker.ConnectionState.Connected)
            {
                while (!(Client.Connect(AgentId)))
                {
                    System.Threading.Thread.Sleep(10000);
                    if (!_run) return;
                }
            }

            if (MyAgent == null) MyAgent = K2A.Connect.Models.Agent.FromId(AgentId);

            if (MyAgent == null)
            {
                Client.ConsoleMessage(String.Format("AgentID {0} not found in database.", AgentId));
                Logging.Log(String.Format("AgentID {0} not found in database. Retrying in 60 seconds.", AgentId), Logging.EntryType.Error);

                Thread.Sleep(60000);
                ConnectToServer();
            }

            Client.ConsoleMessage(string.Format("Agent ({0}) connected to server", MyAgent.RefId));
            Logging.Log(string.Format("Agent ({0}) connected to server.", MyAgent.RefId), Logging.EntryType.Info);

            ServerLog.Log(ServerLog.SenderType.Agent, MyAgent.Id, ServerLog.EventLevel.Info, ServerLog.MessageType.AgentStart, MyAgent.RefId + " Agent Started");

            var Lookup = MyAgent.GetObjectLookup();

            foreach (Agent.ObjectLookup obj in Lookup)
            {
                if (!DevIDLookup.ContainsKey(obj.DeviceID))
                {
                    DevIDLookup.Add(obj.DeviceID, obj.DeviceObject);
                }
                if (!ObjIDLookup.ContainsKey(obj.ObjectID))
                {
                    ObjIDLookup.Add(obj.ObjectID, obj.ObjectObject);
                }
            }


        }

        internal virtual void SendCheckIn()
        {

        }

        public ActiveAgentStatus GetAgentStatus()
        {
            if (lWorker == null) return new ActiveAgentStatus();
            return new ActiveAgentStatus()
            {
                ObjectCount = lWorker.LogObjectCount,
                PPM = lWorker.LastPPM,
                Load = lWorker.LoadAverage,
                ReadErrors = lWorker.LastReadError
            };
        }

        public class ActiveAgentStatus
        {
            public int ObjectCount;
            public int PPM;
            public int Load;
            public int ReadErrors;

            public override string ToString()
            {
                return string.Format("Objects: {0}, PPM: {1}, Load: {2}%, Errors: {3}", ObjectCount, PPM, Load, ReadErrors);
            }
        }

        private void Execute()
        {
            try
            {
                ConnectToServer();
                StartAgent();

                if (lWorker != null)
                {
                    lWorker.SendConsoleMessage += lWorker_SendConsoleMessage;
                    Client.CheckIn(AgentId, lWorker.LogObjectCount, lWorker.LastPPM, lWorker.LastLoad, lWorker.LoadAverage, lWorker.LastReadError);
                }

                if (MyAgent == null) return;

                if (MyAgent.EnableRemoteAccess)
                {
                    Logging.Log("Starting Remote Access Tunnel.", Logging.EntryType.Info);

                    if (MyAgent.RemoteAccessPort > 0)
                    {
                        remoteAccessHost = new mLink.Clients.RemoteAccessHost();
                        remoteAccessHost.ConnectAsHost(AgentId.ToString(), ActiveUser.ConnectClient.ServerAddress, ActiveUser.ConnectClient.ServerPort, MyAgent.RemoteAccessIP, MyAgent.RemoteAccessPort);
                    }


                    if (MyAgent.RemoteAccessSecondaryPort > 0)
                    {
                        remoteAccessSecondaryHost = new mLink.Clients.RemoteAccessHost();
                        remoteAccessSecondaryHost.ConnectAsHost(AgentId + "b", ActiveUser.ConnectClient.ServerAddress, ActiveUser.ConnectClient.ServerPort, MyAgent.RemoteAccessIP, MyAgent.RemoteAccessSecondaryPort);
                    }
                }
            }
            catch (Exception)
            {

                return;
            }



            int secondsSinceCheckIn = 0;

            while (_run)
            {
                System.Threading.Thread.Sleep(1000);

                secondsSinceCheckIn++;

                if (secondsSinceCheckIn >= 10)
                {
                    SendCheckIn();

                    if (lWorker != null)
                        Client.CheckIn(AgentId, lWorker.LogObjectCount, lWorker.LastPPM, lWorker.LastLoad, lWorker.LoadAverage, lWorker.LastReadError);
                    else
                    {
                        Client.CheckIn(AgentId, -1, 0, 0, 0, 0);
                        Client.ConsoleMessage(String.Format("Agent ({0}) {1}", MyAgent.RefId, "lWorker is null"));
                    }

                    secondsSinceCheckIn = 0;
                }


                while (Client.ConnectionStatus != K2A.mLink.Worker.ConnectionState.Connected && _run)
                {
                    if (!_run || Client.ConnectionStatus == K2A.mLink.Worker.ConnectionState.Connected) break;
                    System.Threading.Thread.Sleep(5000);
                }

                if (!_run) return;


            }
        }

        void lWorker_SendConsoleMessage(string message)
        {
            Client.ConsoleMessage(String.Format("Agent ({0}) {1}", MyAgent.RefId, message));
        }

        public void ConnectionClosing()
        {
            if (Status)
            {
                var msg = new AgentStatus()
                {
                    PPM = 0,
                    Load = -1,
                    ObjectCount = 0,
                    AverageLoad = 0,
                    AgentId = AgentId,
                    ReadErrors = 0
                };

                Client.SendAsync("", ConnectClient.MessageTypeEnum.AgentStatus, msg);
            }

        }


        private void StartAgent()
        {
            if (MyAgent == null)
            {
                return;
            }

            Client.CheckIn(AgentId, 0, 0, 0, 0, 0);

            try
            {
                if (MyAgent.DisableLogging)
                {
                    Logging.Log("Logging is currently disabled for this Agent.  To enable logging, please check the Agent properties.", Logging.EntryType.Info);
                }

                StartDriver();
            }
            catch (Exception ex)
            {
                Logging.Log("Unable to start driver: {0}", Logging.EntryType.Error, ex.Message);
            }

            if (lWorker != null)
            {
                lWorker.Start();
                lWorker.ServerObjectRequest += LWorker_ServerObjectRequest;
            }
        }

        private void LWorker_ServerObjectRequest(List<LogWorker.ObjectInfo> objectList)
        {
            foreach (var obj in objectList)
            {
                if (obj.DeviceLog.DeviceObject.StartsWith("weather|"))
                {
                    try
                    {
                        obj.ObjectLog.ObjectValue = GetWeatherValue(obj.DeviceLog.DeviceObject, obj.ObjectLog.ObjectString);
                        lWorker.ValueReadComplete(obj);
                    }
                    catch
                    {
                        lWorker.ValueReadComplete(obj, true, "Unable to read weather value");
                    }
                }
                
            }
        }

        internal virtual void DeviceScan(string Sender, mLink.Messages.DeviceScan Message)
        {

        }
        private void Client_DeviceScan(string Sender, mLink.Messages.DeviceScan Message)
        {
            DevScanClient = Sender;

            if (MyAgent == null)
            {
                return;
            }

            Client.ConsoleMessage(String.Format("Agent {0} initiated device scan.", AgentId));

            DeviceScan(Sender, Message);
        }

        internal virtual void ObjectScan(string Sender, ObjectScan Message)
        {

        }

        private void Client_ObjectScan(string Sender, ObjectScan Message)
        {
            ObjScanClient = Sender;

            if (MyAgent == null)
            {
                return;
            }

            Client.ConsoleMessage(String.Format("Agent {0} initiated object scan", AgentId));


            if (Message.PathToScan.StartsWith("weather|"))
            {
                WeatherObjectScan(Message.PathToScan.Substring(8));
            }
            else
            {
                ObjectScan(Sender, Message);
            }

        }

        internal void ProcessWatchRequest(string FromClient, WatchRequest Message)
        {

            string cachePath = $"Agents:{AgentId}:Devices:{Message.DeviceId}:Objects:{Message.ObjectId}:{Message.ObjProperty.ToString()}";
            var cachedValue = redis.ReadObject<string>(cachePath);

            if (cachedValue.IsNullOrEmpty())
                Client_WatchRequest(FromClient, Message);
            else
                Client.RespondToWatch(FromClient, Message.RequestID, cachedValue, cachedValue);

        }
        internal void Client_WatchRequest(string FromClient, WatchRequest Message)
        {
            if (!DevIDLookup.ContainsKey(Message.DeviceId) || !ObjIDLookup.ContainsKey(Message.ObjectId))
            {
                return;
            }

            var devObject = (string)DevIDLookup[Message.DeviceId];
            var objObject = (string)ObjIDLookup[Message.ObjectId];


            int reqID;

            lock (requestLookup)
            {
                reqID = GetAvailableRequestId();
                string cachePath =  $"Agents:{AgentId}:Devices:{Message.DeviceId}:Objects:{Message.ObjectId}:{Message.ObjProperty.ToString()}";
                requestLookup[reqID] = new WatchTag() { ClientName = FromClient, RequestID = Message.RequestID, CachePath = cachePath };
            }

            if (devObject.StartsWith("weather|"))
            {
                WeatherWatchRequest(reqID, Message, devObject, objObject);
                return;
            }

            switch (Message.ObjProperty)
            {
                case WatchRequest.ObjectProperty.PresentValue:
                case WatchRequest.ObjectProperty.DisplayValue:
                    ReadValue(devObject, objObject, reqID);
                    break;

                case WatchRequest.ObjectProperty.ObjectId:
                    ReadObjectID(objObject, reqID);
                    break;

                case WatchRequest.ObjectProperty.Arrays:
                    ReadPriorityArray(devObject, objObject, reqID);
                    break;
            }
        }


        internal void ObjectScanRespond(string objectName, string val)
        {
            var ObjRes = new ObjectScanResponse()
            {
                Name = objectName,
                Description = objectName,
                ObjectObject = objectName,
                ObjectValue = val
            };

            Client.SendAsync(ObjScanClient, Client.ClientName, (Int16)ConnectClient.MessageTypeEnum.ObjectScanResponse, ObjRes);
        }

        private void Client_RestartServiceRequest(string Sender)
        {
            if (RestartServiceRequest != null) RestartServiceRequest(Sender);
        }

        private void Client_WriteValue(string FromClient, WriteValue Message)
        {
            var dDev = BBDevice.FromId(Message.DeviceID);
            var dObj = BBObject.FromId(Message.ObjectID);

            if (dDev == null || dObj == null || dDev.Object.StartsWith("weather|"))
            {
                return;
            }



            if (Message.Priority == string.Empty)
            {
                Logging.Log(String.Format("Write request from {0} sent to {1}, {2} ({3}).", FromClient, dDev.RefId, dObj.RefId, Message.Value), Logging.EntryType.Info);
                WriteValue(dDev.Object, dObj.ObjectObject, Message.Value);
            }
            else
            {
                Logging.Log(String.Format("Write request from {0} sent to {1}, {2} @ Proiority {3} ({4}).", FromClient, dDev.RefId, dObj.RefId, Message.Priority, Message.Value), Logging.EntryType.Info);
                WritePriorityValue(dDev.Object, dObj.ObjectObject, Message.Value, Message.Priority);
            }
        }

        internal int GetAvailableRequestId()
        {

            var i = 0;

            while (requestLookup.ContainsKey(i))
            {
                i++;
            }

            return i;
        }


        private void Client_StopServiceRequest(string Sender)
        {
            Client.ConsoleMessage(String.Format("Agent {0} is stopping", AgentId));
            Logging.Log("Stop command received from UI", Logging.EntryType.Info);

            Stop();
            Thread.Sleep(3000);

            if (ServiceStopped != null) ServiceStopped();
        }




        internal class WatchTag
        {
            //WatchTag is used to identify the client that requested the value
            public int RequestID { get; set; }

            public string ClientName { get; set; }

            public string CachePath { get; set; }
        }

        private void Client_ConnectionRejected(string message)
        {
            if (!_run) return;
            Logging.Log("Agent connection request rejected: " + message, Logging.EntryType.Error);

            System.Threading.Thread.Sleep(10000);
            Client.Disconnect();

            if (ConnectionRejected != null) ConnectionRejected(message);
        }

        private void Client_WatchMultiple(string FromClient, WatchMultipleRequest Message)
        {
            List<string> keys = new List<string>();

            foreach (var obj in Message.ObjectList)
            {
                obj.AgentId = AgentId;
                keys.Add(obj.CachePath());
            }


            try
            {
                var cachedVals = redis.ReadObjects<string>(keys);
                if (cachedVals != null)
                {
                    foreach (var val in cachedVals)
                    {
                        var msg = Message.ObjectList.Where(m => m.CachePath() == val.Key).FirstOrDefault();
                        Client.RespondToWatch(FromClient, msg.RequestID, val.Value, val.Value);
                        Message.ObjectList.Remove(msg);
                    }
                }

            }
            catch { }

            if (Message.ObjectList.Count > 0) WatchMultiple(FromClient, Message);
        }

        internal virtual void WatchMultiple(string FromClient, WatchMultipleRequest Message)
        {
            foreach (var obj in Message.ObjectList)
            {
                Client_WatchRequest(FromClient, new WatchRequest(obj));
            }
        }


        private string GetWeatherValue(string location, string name)
        {
            if (location.StartsWith("weather|")) location = location.Substring(8);

            UpdateWeatherData(location);

            lock (weatherData)
            { 

                if (weatherData.ContainsKey(name))
                    return weatherData[name];
                else
                    return "Value Not Found";
            }
        }

        private void UpdateWeatherData(string location)
        {
            if (lastWeatherUpdate <= DateTime.UtcNow.AddMinutes(-1))
            {
                weatherReset.Reset();

                var msg = new Message(Client.ClientName, "", (Int16)ConnectClient.MessageTypeEnum.WeatherDataRequest, location);
                Client.SendAsync(msg);
                weatherReset.WaitOne(5000);
            }
        }

        void WeatherObjectScan(string location)
        {
            Task.Factory.StartNew(() =>
            {
                UpdateWeatherData(location);

                lock (weatherData)
                {
                    foreach (var prop in weatherData)
                    {
                        ObjectScanRespond(prop.Key, prop.Value);
                    }

                    Client.ScanStateChanged(ObjScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
                }
            });


        }
        void WeatherWatchRequest(int reqId, WatchRequest message, string devObject, string objObject)
        {
            Task.Factory.StartNew(() =>
            {
                switch (message.ObjProperty)
                {
                    case WatchRequest.ObjectProperty.DisplayValue:
                    case WatchRequest.ObjectProperty.PresentValue:
                        string val = GetWeatherValue(devObject, objObject);
                        OnValueRead(reqId, val, val);
                        break;
                    case WatchRequest.ObjectProperty.ObjectId:
                        OnValueRead(reqId, "Weather Value", "Weather Value");
                        break;
                }
            });


        }

        private void Client_WeatherDataResponse(Dictionary<string, string> weatherDataResponse)
        {
            if (weatherDataResponse != null)
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + ": " + "Locking to update results");
                lock (this.weatherData)
                {
                    weatherData = weatherDataResponse;
                    lastWeatherUpdate = DateTime.UtcNow;
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + ": " + "Setting 'Finished' setter");
                    weatherReset.Set();
                }
            }
        }
    }
}
