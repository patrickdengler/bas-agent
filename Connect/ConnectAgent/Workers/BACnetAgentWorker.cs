﻿using System;
using System.Collections.Generic;
using System.Linq;
using K2A.Net.BACnet;
using K2A.Connect.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;
using K2A.Connect.mLink.Messages;
using K2A.Connect.mLink.Clients;
using static K2A.Connect.mLink.Clients.ConnectClient;
using K2A.Timers;

namespace K2A.Connect.ConnectAgent.Workers
{
    public class BACnetAgentWorker : ConnectAgentWorker
    {
        private TraneScan tDevScan;
        private TraneObjectScan tObjScan;
        private BACnetObjectScan bObjScan;
        private DeviceScanWithNames bDevScan;
        private GatewayObjectScan gObjScan;
        private BACnetLive bLive = new BACnetLive();
        private BACnetLive loggerLive = new BACnetLive();
        public BACnetDriver bDriver;
        private int bDeviceID;
        private int lastObjScanStatus = -1;

        internal override void StartDriver()
        {
            Logging.Log("Starting BACnet Agent.", Logging.EntryType.Info);
            bDriver = BACnetDriver.Instance;
            var connInfo = BACnetConnectionInfo.Deserialize(MyAgent.Connection);

            bDeviceID = bDriver.LoadConnection(connInfo);
            Logging.Log("BACnet Driver loaded.", Logging.EntryType.Info);

            if (bDeviceID >= 0 && !MyAgent.DisableLogging)
            {
                lWorker = new LogWorker(MyAgent);
                lWorker.ObjectListCreated += lWorker_ObjectListCreated;
                lWorker.ReadObjectRequest += lWorker_ReadObjectRequest;
            }

            bLive.onLiveRead += bLive_onLiveRead;
            bLive.onLiveReadMultiple += bLive_onLiveReadMultiple;
            bLive.Start(new List<BACnetObject>());

            loggerLive.onLiveRead += loggerLive_onLiveRead;
            loggerLive.onLiveReadMultiple += loggerLive_onLiveReadMultiple;
            loggerLive.onIdleTimeUpdate += loggerLive_onIdleTimeUpdate;
            loggerLive.Start(new List<BACnetObject>());

            Initialize();

            Logging.Log("BACnet Agent Started.", Logging.EntryType.Info);
        }

        void loggerLive_onIdleTimeUpdate(int idleTime, int loadTime, int pendingCount)
        {
            if (lWorker == null) return;
            lWorker.PendingValueReads = pendingCount;
        }


        void bLive_onLiveReadMultiple(List<BACnetObject> bObjects, string Err)
        {
            try
            {
                lock (requestLookup)
                {
                    var objs = (from i in bObjects
                                where requestLookup.ContainsKey((int)i.UserData)
                                select new { userdata = (int)i.UserData, cachePath = requestLookup[(int)i.UserData].CachePath, response = new WatchMultipleResponse.Response() { RequestID = requestLookup[(int)i.UserData].RequestID, DisplayValue = i.Value, ObjectValue = i.Value } }
                                )
                                .ToList();

                    foreach (var val in objs)
                    {
                        CacheValue(val.cachePath, val.response.ObjectValue);
                    }

                    var grouped = (from o in objs group o.response by requestLookup[o.userdata].ClientName into g select new { clientname = g.Key, objects = g.ToList() }).ToList();

                    foreach (var g in grouped)
                    {
                        OnValueReadMultiple(g.clientname, g.objects);
                    }

                    foreach (var o in bObjects)
                    {
                        requestLookup.Remove((int)o.UserData);
                    }
                }
            }
            catch
            {
            }
        }

        public override void Stop()
        {

            if (bDriver != null)
            {
                bDriver.Stop();
                bDriver.Comm = null;
                bDriver = null;
            }

            base.Stop();
        }

        internal override void ReadObjectID(string objObject, int ReqID)
        {
            var bObj = BACnetObject.FromString(objObject);
            if (bObj.IsProprietary)
            {
                OnValueRead(ReqID, "Proprietary BACnet Object", "");
            }
            else
            {
                var objID = String.Format("{0}-{1}", bObj.ObjectType, bObj.ObjectInstance);
                OnValueRead(ReqID, objID, "");
            }

        }

        internal override void ReadPriorityArray(string devObject, string objObject, int ReqID)
        {
            if (MyAgent == null || devObject == null || objObject == null)
            {
                return;
            }

            if (devObject.StartsWith("|")) return;

            var bDev = BACnetDevice.FromString(devObject);
            var bObj = BACnetObject.FromString(objObject, bDev);

            if (bDev == null || bObj == null || bObj.IsProprietary)
            {
                return;
            }

            bObj.PropId = K2A.Net.BACnet.BacnetPropertyIds.PROP_PRIORITY_ARRAY;
            bObj.UserData = ReqID;
            bLive.AddItem(bObj);
        }

        internal override void WatchMultiple(string FromClient, WatchMultipleRequest Message)
        {
            var pt = new SimpleProcessTimer();
            List<BACnetObject> newObjects = new List<BACnetObject>();


            foreach (var obj in Message.ObjectList)
            {

                var devObject = (string)DevIDLookup[obj.DeviceId];
                var objObject = (string)ObjIDLookup[obj.ObjectId];

                if (devObject.StartsWith("weather|"))
                {
                    ProcessWatchRequest(FromClient, new WatchRequest(obj));
                    continue;
                }

                if (MyAgent != null && devObject != null && objObject != null)
                {
                    int reqID;

                    lock (requestLookup)
                    {
                        reqID = GetAvailableRequestId();
                        requestLookup[reqID] = new WatchTag() { ClientName = FromClient, RequestID = obj.RequestID, CachePath = obj.CachePath() };
                    }

                    if (devObject.StartsWith("|"))
                    {
                        var objs = (from d in BACnetDriver.Instance.GWDatabase.Devices where d.DeviceName == devObject.Substring(1, devObject.Length - 1) select d.Objects).FirstOrDefault();
                        if (objs != null)
                        {
                            var gobj = (from o in objs where o.ObjectName == objObject.Substring(1, objObject.Length - 1) select o).FirstOrDefault();
                            if (gobj != null)
                            {
                                var bDev = new BACnetDevice("", gobj.DeviceInstance, -1, true);
                                var bObj = new BACnetObject(bDev, gobj.ObjectType, gobj.ObjectInstance, gobj.PropID, gobj.PriorityArray, reqID);
                                newObjects.Add(bObj);
                            }
                        }

                    }
                    else
                    {
                        var bDev = BACnetDevice.FromString(devObject);
                        var bObj = BACnetObject.FromString(objObject, bDev);

                        if (bObj != null && bDev != null)  //respond with error? Unable to parse dev/obj
                        {
                            bObj.UserData = reqID;
                            newObjects.Add(bObj);
                        }


                    }
                }


            }

            bLive.AddItems(newObjects);
        }

        internal override void ReadValue(string devObject, string objObject, int ReqID)
        {
            if (MyAgent == null || devObject == null || objObject == null)
            {
                return;
            }

            if (devObject.StartsWith("|")) //Comes from the gateway
            {
                var objs = (from d in BACnetDriver.Instance.GWDatabase.Devices where d.DeviceName == devObject.Substring(1, devObject.Length - 1) select d.Objects).FirstOrDefault();
                if (objs != null)
                {
                    var obj = (from o in objs where o.ObjectName == objObject.Substring(1, objObject.Length - 1) select o).FirstOrDefault();
                    if (obj != null)
                    {
                        var bDev = new BACnetDevice("", obj.DeviceInstance, -1, true);
                        var bObj = new BACnetObject(bDev, obj.ObjectType, obj.ObjectInstance, obj.PropID, obj.PriorityArray, ReqID);
                        bLive.AddItem(bObj);
                    }
                }

            }
            else
            {
                var bDev = BACnetDevice.FromString(devObject);
                var bObj = BACnetObject.FromString(objObject, bDev);

                if (bObj == null || bDev == null) return; //respond with error? Unable to parse dev/obj

                bObj.UserData = ReqID;
                bLive.AddItem(bObj);
            }
        }

        internal override void WritePriorityValue(string devObject, string objObject, string newValue, string priority)
        {
            if (MyAgent == null || devObject == null || objObject == null)
            {
                return;
            }

            if (devObject.StartsWith("|"))
            {
                var objs = (from d in BACnetDriver.Instance.GWDatabase.Devices where d.DeviceName == devObject.Substring(1, devObject.Length - 1) select d.Objects).FirstOrDefault();
                if (objs != null)
                {
                    var obj = (from o in objs where o.ObjectName == objObject.Substring(1, objObject.Length - 1) select o).FirstOrDefault();
                    if (obj != null)
                    {
                        var bDev = new BACnetDevice("", obj.DeviceInstance, -1, true);
                        var bObj = new BACnetObject(bDev, obj.ObjectType, obj.ObjectInstance, obj.PropID, obj.PriorityArray, 0);
                        BACnetCommands.WriteProperty(bDev.DeviceID, bObj.ObjectType, bObj.ObjectInstance, BacnetPropertyIds.PROP_PRESENT_VALUE, priority.ToInteger(8), newValue);
                    }
                }

            }
            else
            {
                var bDev = BACnetDevice.FromString(devObject);
                var bObj = BACnetObject.FromString(objObject, bDev);

                BACnetCommands.WriteProperty(bDev.DeviceID, bObj.ObjectType, bObj.ObjectInstance, BacnetPropertyIds.PROP_PRESENT_VALUE, priority.ToInteger(8), newValue);
            }
        }

        internal override void WriteValue(string devObject, string objObject, string newValue)
        {
            if (MyAgent == null || devObject == null || objObject == null)
            {
                return;
            }

            if (devObject.StartsWith("|"))
            {
                var objs = (from d in BACnetDriver.Instance.GWDatabase.Devices where d.DeviceName == devObject.Substring(1, devObject.Length - 1) select d.Objects).FirstOrDefault();
                if (objs != null)
                {
                    var obj = (from o in objs where o.ObjectName == objObject.Substring(1, objObject.Length - 1) select o).FirstOrDefault();
                    if (obj != null)
                    {
                        var bDev = new BACnetDevice("", obj.DeviceInstance, -1, true);
                        var bObj = new BACnetObject(bDev, obj.ObjectType, obj.ObjectInstance, obj.PropID, obj.PriorityArray, 0);
                        BACnetCommands.WritePropertyAsync(bDev.DeviceID, bObj.ObjectType, bObj.ObjectInstance, BacnetPropertyIds.PROP_PRESENT_VALUE, 8, newValue);
                    }
                }

            }
            else
            {
                var bDev = BACnetDevice.FromString(devObject);
                var bObj = BACnetObject.FromString(objObject);
                bObj.DeviceObject = bDev;

                BACnetCommands.WritePropertyAsync(bDev.DeviceID, bObj.ObjectType, bObj.ObjectInstance, BacnetPropertyIds.PROP_PRESENT_VALUE, 8, newValue);
            }

        }

        #region "BACnet Methods"


        private void bLive_onLiveRead(BACnetObject Itm, string Err)
        {
            try
            {
                if (string.IsNullOrEmpty(Err))
                {
                    OnValueRead((int)Itm.UserData, Itm.Value, Itm.Value);
                }
                else
                {

                    OnValueRead((int)Itm.UserData, Err, Err);
                }
            }
            catch
            {
            }
        }

        void loggerLive_onLiveRead(BACnetObject Itm, string Err)
        {
            if (Itm.UserData is LogWorker.ObjectInfo)
            {
                LogWorker.ObjectInfo obj = (LogWorker.ObjectInfo)Itm.UserData;
                bool hasError = !string.IsNullOrEmpty(Err);
                if (!hasError) obj.ObjectLog.ObjectValue = Itm.Value;

                lWorker.ValueReadComplete(obj, hasError, Err);
                
            }
        }

        private void loggerLive_onLiveReadMultiple(List<BACnetObject> Itm, string Err)
        {
            foreach (var obj in Itm)
            {
                loggerLive_onLiveRead(obj, Err);
            }
        }
        private void bDeviceScan_ProgressChanged(int Percent, string Status)
        {
            Client.ScanProgress(DevScanClient, Percent, Status);
        }

        private void bDeviceScan_ScanComplete(List<IAm> Devices)
        {
            Client.ConsoleMessage(String.Format("Agent {0} completed a device scan", AgentId));
            foreach (IAm dev in Devices)
            {
                var devObj = dev.BACnetDevice.ToString();
                DeviceScanResponse devRes;
                if (dev.DeviceId >= 0)
                {
                    devRes = new DeviceScanResponse
                    {
                        DevicePath = dev.DeviceId.ToString(),
                        Name = dev.DeviceName,
                        DeviceObject = devObj,
                        Description = dev.MAC,
                        Model = dev.ModelName,
                        IconId = dev.VendorId
                    };
                }
                else
                {
                    devRes = new DeviceScanResponse
                    {
                        DevicePath = dev.MAC,
                        Name = dev.DeviceName,
                        DeviceObject = "|" + dev.MAC,
                        Description = "",
                        Model = dev.ModelName,
                        IconId = dev.VendorId
                    };
                }

                Client.DeviceScanIAm(DevScanClient, devRes);
            }
            Client.ScanStateChanged(DevScanClient, AgentClient.ScanStatus.Complete);
        }

        private void bObjectScan_ProgressChanged(int Percent, string Status)
        {
            if (Percent != lastObjScanStatus)
            {
                Client.ScanProgress(ObjScanClient, Percent, Status);
                lastObjScanStatus = Percent;
            }

        }

        private void bObjectScan_ScanAbort(string ErrorMsg)
        {
            Client.ScanStateChanged(ObjScanClient, AgentClient.ScanStatus.Aborted);
        }

        private void bObjectScan_ScanComplete(int DeviceId, iAmObject[] Objects)
        {
            //Debug.Print("Scan Finished");
            Client.ConsoleMessage(String.Format("Agent {0} completed an object scan", AgentId));
            foreach (iAmObject Obj in Objects)
            {
                var ObjObj = Obj.BACnetObject;
                var ObjRes = new ObjectScanResponse
                {
                    Name = Obj.ObjectName,
                    Description = Obj.Description,
                    ObjectObject = ObjObj,
                    ObjectValue = Obj.ObjectValue
                };
                Client.SendAsync(ObjScanClient, MessageTypeEnum.ObjectScanResponse, ObjRes);
            }
            Console.WriteLine("Stopping bLive");
            bObjScan.Stop();
            Console.WriteLine("Sending Scan Status Complete");
            Client.ScanStateChanged(ObjScanClient, AgentClient.ScanStatus.Complete);
        }

        private void tObjScan_ProgressChanged(int Progress)
        {
            Client.ScanProgress(ObjScanClient, Progress, "Scanning Objects");
        }

        private void tObjScan_ScanComplete(iAmObject[] DevList)
        {
            foreach (iAmObject Obj in DevList)
            {
                try
                {
                    var ObjObj = Obj.BACnetObject;
                    var objRes = new ObjectScanResponse
                    {
                        Name = Obj.ObjectName,
                        Description = Obj.Description,
                        ObjectObject = ObjObj,
                        ObjectValue = Obj.ObjectValue
                    };
                    Client.ObjectScanResponse(ObjScanClient, objRes);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                }
            }

            Console.WriteLine("Sending Scan Status Complete");
            Client.ScanStateChanged(ObjScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }

        private void tScan_ProgressChanged(int Progress, string CurrentStep, string Details)
        {
            Client.ScanProgress(DevScanClient, Progress, CurrentStep);
        }

        private void tScan_ScanComplete(TraneSubDevice[] Devices)
        {
            Client.ConsoleMessage(String.Format("Agent {0} completed a device scan", AgentId));
            foreach (TraneSubDevice dev in Devices)
            {
                var devObj = dev.ToString();
                var devRes = new DeviceScanResponse
                {
                    DevicePath = dev.Device.DeviceId.ToString(),
                    Name = dev.DeviceName,
                    DeviceObject = devObj,
                    Description = "MAC= " + dev.Device.MAC,
                    Model = dev.ModelName,
                    IconId = dev.VendorId
                };
                Client.DeviceScanIAm(DevScanClient, devRes);
            }
            Client.ScanStateChanged(DevScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }

        private void gObjScan_ScanComplete(iAmGatewayObject[] bObjects)
        {
            //Debug.Print("Scan Finished");
            foreach (iAmGatewayObject Obj in bObjects)
            {
                var ObjRes = new ObjectScanResponse()
                {
                    Name = Obj.ObjectName,
                    Description = "Virtual Object",
                    ObjectObject = "|" + Obj.ObjectPath,
                    ObjectValue = Obj.ObjectValue
                };
                Client.SendAsync(ObjScanClient, Client.ClientName, (Int16)MessageTypeEnum.ObjectScanResponse, ObjRes);
            }
            bObjScan.Stop();
            Client.ScanStateChanged(ObjScanClient, K2A.Connect.mLink.Clients.AgentClient.ScanStatus.Complete);
        }

        #endregion

        #region "ClientRequests"

        internal override void DeviceScan(string Sender, mLink.Messages.DeviceScan Message)
        {
            if (Message.ScanSubDevices)
            {
                tDevScan.Scan(Message.LoLimit, Message.HiLimit);
            }
            else
            {
                Console.WriteLine("Scan received from " + Message.Header.Source);
                bDevScan.RangeHiLimit = Message.HiLimit;
                bDevScan.RangeLoLimit = Message.LoLimit;
                bDevScan.GetDescription = Message.GetDescription;
                bDevScan.Start();
            }
        }
        internal override void ObjectScanAbort()
        {
            if (bDevScan != null) bDevScan.Stop();
        }
        internal override void DeviceScanAbort()
        {
            if (bObjScan != null) bObjScan.Stop();
        }
        internal override void ObjectScan(string Sender, ObjectScan Message)
        {
            var Device = BBDevice.FromId(Message.PathToScan.ToInteger(0));
            if (Device == null)
            {
                return;
            }

            if (Device.Object.StartsWith("|"))
            {
                if (!gObjScan.Running)
                {
                    gObjScan.Device = Device.Object;
                    gObjScan.Scan();
                }
            }
            else
            {
                var bDevice = BACnetDevice.FromString(Device.Object);

                if (bDevice.isNative)
                {
                    if (!bObjScan.Running)
                    {
                        if (Message.GetDescription) bObjScan.NameOptions = NameValueOptions.Description;
                        if (Message.GetJCIName) bObjScan.NameOptions = NameValueOptions.JCIName;
                        bObjScan.GetValues = Message.GetValues;
                        bObjScan.Scan(bDevice.DeviceID);
                    }
                }
                else
                {
                    tObjScan.Start(bDevice);
                }
            }
        }

        private void lWorker_ObjectListCreated(List<LogWorker.ObjectInfo> objectList)
        {

        }
        private async void lWorker_ReadObjectRequest(List<LogWorker.ObjectInfo> objectList)
        {
            Task.Run(() => ObjectsRequested(objectList));
        }
        private void ObjectsRequested(List<LogWorker.ObjectInfo> objectList)
        {
            //Logging.Log(loggerLive.GetStatusString(), Logging.EntryType.Trace);
            List<BACnetObject> objList = new List<BACnetObject>();
            foreach (var obj in objectList)
            {
                string devObject = obj.DeviceLog.DeviceObject;
                string objObject = obj.ObjectLog.ObjectString;
                if (devObject.StartsWith("|")) //Comes from the gateway
                {
                    var objs = (from d in BACnetDriver.Instance.GWDatabase.Devices where d.DeviceName == devObject.Substring(1, devObject.Length - 1) select d.Objects).FirstOrDefault();
                    if (objs != null)
                    {
                        var gwobj = (from o in objs where o.ObjectName == objObject.Substring(1, objObject.Length - 1) select o).FirstOrDefault();
                        if (gwobj != null)
                        {
                            var bDev = new BACnetDevice("", gwobj.DeviceInstance, -1, true);
                            var bObj = new BACnetObject(bDev, gwobj.ObjectType, gwobj.ObjectInstance, gwobj.PropID, gwobj.PriorityArray, obj);
                            objList.Add(bObj);
                        }
                    }

                }
                else
                {
                    var bDev = BACnetDevice.FromString(obj.DeviceLog.DeviceObject);
                    var bObj = BACnetObject.FromString(obj.ObjectLog.ObjectString, bDev);

                    bObj.UserData = obj;
                    objList.Add(bObj);
                }

            }

            loggerLive.AddItems(objList);
        }
        #endregion

        private void Initialize()
        {
            this.tDevScan = new K2A.Net.BACnet.TraneScan();
            this.tObjScan = new K2A.Net.BACnet.TraneObjectScan();
            this.bObjScan = new K2A.Net.BACnet.BACnetObjectScan();
            this.bDevScan = new K2A.Net.BACnet.DeviceScanWithNames();
            this.gObjScan = new K2A.Net.BACnet.GatewayObjectScan();
            // 
            // tDevScan
            // 
            this.tDevScan.OverallProgress = 0;
            this.tDevScan.StatusDetails = null;
            this.tDevScan.StatusStep = null;
            this.tDevScan.ProgressChanged += new K2A.Net.BACnet.TraneScan.ProgressChangedEventHandler(this.tScan_ProgressChanged);
            this.tDevScan.ScanComplete += new K2A.Net.BACnet.TraneScan.ScanCompleteEventHandler(this.tScan_ScanComplete);
            // 
            // tObjScan
            // 
            this.tObjScan.ProgressChanged += new K2A.Net.BACnet.TraneObjectScan.ProgressChangedEventHandler(this.tObjScan_ProgressChanged);
            this.tObjScan.ScanComplete += new K2A.Net.BACnet.TraneObjectScan.ScanCompleteEventHandler(this.tObjScan_ScanComplete);
            // 
            // bObjScan
            // 
            this.bObjScan.ProgressChanged += new K2A.Net.BACnet.BACnetObjectScan.ProgressChangedEventHandler(this.bObjectScan_ProgressChanged);
            this.bObjScan.ScanComplete += new K2A.Net.BACnet.BACnetObjectScan.ScanCompleteEventHandler(this.bObjectScan_ScanComplete);
            // 
            // bDevScan
            // 
            this.bDevScan.ProgressChanged += new K2A.Net.BACnet.DeviceScanWithNames.ProgressChangedEventHandler(this.bDeviceScan_ProgressChanged);
            this.bDevScan.ScanComplete += new K2A.Net.BACnet.DeviceScanWithNames.ScanCompleteEventHandler(this.bDeviceScan_ScanComplete);
            // 
            // gObjScan
            // 
            this.gObjScan.ScanComplete += new K2A.Net.BACnet.GatewayObjectScan.ScanCompleteEventHandler(this.gObjScan_ScanComplete);

        }
    }
}
