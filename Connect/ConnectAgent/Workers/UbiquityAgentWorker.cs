﻿using System;
using System.Collections.Generic;
using System.Linq;
using K2A.Connect.Models;
using K2A.mLink.Messages;
using System.Threading;
using System.Threading.Tasks;

namespace K2A.Connect.ConnectAgent.Workers
{
    public class UbiquityAgentWorker : ConnectAgentWorker
    {
        private K2A.Net.Ubiquity.UbiquityDriver uDriver;

        private Dictionary<string, List<int>> objHash = new Dictionary<string, List<int>>();
        int timeSinceLastRequest = 0;

        internal override void ReadObjectID(string objObject, int ReqID)
        {
            OnValueRead(ReqID, "Ubiquity Value", "");
        }


        internal override void ReadValue(string devObject, string objObject, int ReqID)
        {
            if (MyAgent == null || devObject == null || objObject == null) return;

            var path = string.Format("{0}|{1}", devObject, objObject);

            lock (objHash)
            {
                timeSinceLastRequest = 0;
                List<int> reqIDs;
                if (objHash.ContainsKey(path))
                {
                    reqIDs = (List<int>)objHash[path];
                }
                else
                {
                    reqIDs = new List<int>();
                    objHash.Add(path, reqIDs);
                }

                if (!reqIDs.Contains(ReqID)) reqIDs.Add(ReqID);
            }
        }

        internal override void StartDriver()
        {
            if (!MyAgent.DisableLogging)
            {
                lWorker = new LogWorker(MyAgent);
                lWorker.ObjectListCreated += lWorker_ObjectListCreated;
                lWorker.ReadObjectRequest += lWorker_ReadObjectRequest;
            }

            uDriver = new Net.Ubiquity.UbiquityDriver(K2A.Connect.Objects.UbiquityConnectionInfo.Deserialize(MyAgent.Connection));

            var t = new Thread(() => RunBatch()) { IsBackground = true };
            t.Start();
        }

        private void RunBatch()  //Dynamic Data
        {
            while (true)
            {
                //Allow time to collect multiple objects before running batch
                while (objHash.Count == 0)
                {
                    Thread.Sleep(5);
                }

                int waitTime = 0;

                while (timeSinceLastRequest < 30 && waitTime < 1000)
                {
                    Thread.Sleep(5);
                    timeSinceLastRequest += 5;
                    waitTime += 5;
                }

                lock (objHash)
                {
                    List<string> objects = objHash.Keys.ToList();

                    try
                    {
                        var values = uDriver.GetValues(objects);

                        foreach (var val in values)
                        {
                            List<int> reqIds = objHash[val.Key];
                            foreach (int reqId in reqIds)
                            {
                                OnValueRead(reqId, val.Value, val.Value);
                            }
                        }
                    }
                    catch
                    {
                        Logging.Log("Error running Ubiquity Batch", Logging.EntryType.Error);
                    }

                    objHash.Clear();
                }

            }
        }

        #region "ClientRequests"

        internal override void DeviceScan(string Sender, Messages.DeviceScan Message)
        {
            var results = uDriver.GetDevices();

            if (results != null)
            {
                foreach (var r in results)
                {
                    var devScanResponse = new DeviceScanResponse() { Description = r.Path, DeviceObject = r.Path, DevicePath = r.Path, IconId = -1, Model = r.Description, Name = r.DisplayName };
                    Client.DeviceScanIAm(DevScanClient, devScanResponse);
                }

            }

            Client.ScanStateChanged(DevScanClient, K2A.mLink.Clients.AgentClient.ScanStatus.Complete);
        }
        internal override void ObjectScan(string Sender, Messages.ObjectScan Message)
        {
            var results = uDriver.GetObjects(Message.PathToScan);
            if (results != null)
            {
                foreach (var r in results)
                {
                    var ObjRes = new ObjectScanResponse
                    {
                        Name = r.DisplayName,
                        Description = r.Description,
                        ObjectObject = r.Path,
                        ObjectValue = r.ObjectValue
                    };
                    Client.SendAsync(ObjScanClient, Client.ClientName, mComm.MessageTypeEnum.ObjectScanResponse, ObjRes);
                }
            }
            Client.ScanStateChanged(ObjScanClient, K2A.mLink.Clients.AgentClient.ScanStatus.Complete);
        }

        private void lWorker_ObjectListCreated(List<LogWorker.ObjectInfo> objectList)
        {

        }
        private void lWorker_ReadObjectRequest(List<LogWorker.ObjectInfo> objectList)
        {
            lWorker.PendingValueReads = objectList.Count;
            List<string> objects = new List<string>();

            //var devs = objectList.GroupBy(o => o.DeviceLog.DeviceObject);

            foreach (var obj in objectList)
            {
                objects.Add(obj.DeviceLog.DeviceObject + "|" + obj.ObjectLog.ObjectString);
            }

            var values = uDriver.GetValues(objects);

            foreach (var obj in objectList)
            {
                if (values.ContainsKey(obj.DeviceLog.DeviceObject + "|" + obj.ObjectLog.ObjectString))
                {
                    obj.ObjectLog.ObjectValue = values[obj.DeviceLog.DeviceObject + "|" + obj.ObjectLog.ObjectString];
                    lWorker.ValueReadComplete(obj, false);
                }
            }

            lWorker.PendingValueReads = 0;
        }

        #endregion
    }
}
