﻿using System;
using System.Collections.Generic;
using System.Linq;
using K2A.Connect.Models;
using K2A.Net.ModbusTCP;
using System.Threading;
using K2A.Connect.mLink.Messages;

namespace K2A.Connect.ConnectAgent.Workers
{
    public class ModbusTcpAgentWorker : ConnectAgentWorker
    {
        ModbusDriver driver;

        Dictionary<string, List<int>> objLookup = new Dictionary<string, List<int>>();
        Dictionary<string, LogWorker.ObjectInfo> logObjects = new Dictionary<string, LogWorker.ObjectInfo>();

        internal override void StartDriver()
        {
            driver = new ModbusDriver();

            if (!driver.Start(ModbusTCPConfiguration.Deserialize(MyAgent.Connection)))
            {
                Logging.Log("Unable to start Agent.", Logging.EntryType.Error);
                return;
            }

            driver.OnException += Driver_OnException;
            driver.OnValueReceived += Driver_OnValueReceived;

            if (!MyAgent.DisableLogging)
            {
                lWorker = new LogWorker(MyAgent);
                lWorker.ObjectListCreated += lWorker_ObjectListCreated;
                lWorker.ReadObjectRequest += lWorker_ReadObjectRequest;
            }
        }

        private void Driver_OnValueReceived(ModbusTCPConfiguration.ModbusTCPDevice modbusDevice, ModbusTCPConfiguration.ModbusTCPObject modbusObject)
        {
            string objId = $"{modbusDevice.Id}.{modbusObject.GetAddressId()}";

            if (objLookup.ContainsKey(objId))
            {
                var reqIds = objLookup[objId];

                foreach (var id in reqIds)
                {
                    if (id == -1)
                    {
                        if (logObjects.ContainsKey(objId))
                        {
                            string objVal = modbusObject.Value.ToString();
                            var obj = logObjects[objId];
                            if (objVal.IsNotNullOrEmpty())
                            {
                                obj.ObjectLog.ObjectValue = modbusObject.Value.ToString();
                                lWorker.ValueReadComplete(obj, false);
                            }
                            else
                            {
                                lWorker.ValueReadComplete(obj, true, modbusObject.LastError);
                            }
                        }
                        
                    }
                    else
                        OnValueRead(id, modbusObject.Value.ToString(), modbusObject.Value.ToString());
                }

                objLookup[objId] = new List<int>();
            }

            lWorker.PendingValueReads = driver.GetPendingReadCount();
        }

        private void Driver_OnException(ModbusTCPConfiguration.ModbusTCPDevice modbusDevice, ModbusTCPConfiguration.ModbusTCPObject modbusObject)
        {
            //lWorker.PendingValueReads = driver.GetPendingReadCount();
            //log error
        }

        internal override void ReadObjectID(string objObject, int ReqID)
        {
            OnValueRead(ReqID, objObject, "");
        }


        internal override void ReadValue(string devObject, string objObject, int ReqID)
        {
            string objId = $"{devObject}.{objObject}";

            lock (objLookup)
            {
                if (objLookup.ContainsKey(objId))
                {
                    objLookup[objId].Add(ReqID);
                }
                else
                {
                    objLookup[objId] = new List<int>() { ReqID };
                }
            }

            try
            {
                driver.ReadValueAsync(devObject.ToInteger(), objObject);
            }
            catch (Exception ex)
            {
                OnValueRead(ReqID, ex.Message, ex.Message);
                objLookup[objId].Remove(ReqID);
            }
        }



        #region "ClientRequests"

        internal override void DeviceScan(string Sender, DeviceScan Message)
        {
            foreach (var dev in driver.Configuration.Devices)
            {
                var devScanResponse = new DeviceScanResponse() { Description = dev.GetDescription(), DeviceObject = dev.Id.ToString(), DevicePath = dev.Id.ToString(), IconId = -1, Model = "ModbusTCP_Device", Name = dev.Name };
                Client.DeviceScanIAm(DevScanClient, devScanResponse);
            }

            Client.ScanStateChanged(DevScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }
        internal override void ObjectScan(string Sender, ObjectScan Message)
        {
            var dev = driver.Configuration.GetDevice(Message.PathToScan.ToInteger());

            if (dev != null && dev.Objects != null && dev.Objects.Count > 0)
            {
                foreach (var obj in dev.Objects)
                {
                    var objRes = new ObjectScanResponse()
                    {
                        Name = obj.Name,
                        Description = obj.GetDescription(),
                        ObjectObject = obj.GetAddressId(),
                        ObjectValue = obj.Value.ToString()
                    };

                    Client.ObjectScanResponse(ObjScanClient, objRes);
                }
            }

            Client.ScanStateChanged(ObjScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }

        private void lWorker_ObjectListCreated(List<LogWorker.ObjectInfo> objectList)
        {

        }
        private void lWorker_ReadObjectRequest(List<LogWorker.ObjectInfo> objectList)
        {
            var t = new Thread(() => ReadObjectList(objectList)) { IsBackground = true };
            t.Start();

        }

        private void ReadObjectList(List<LogWorker.ObjectInfo> objectList)
        {

            foreach (var obj in objectList)
            {
                string objId = $"{obj.DeviceLog.DeviceObject}.{obj.ObjectLog.ObjectString}";

                logObjects[objId] = obj;

                ReadValue(obj.DeviceLog.DeviceObject, obj.ObjectLog.ObjectString, -1);
                //driver.ReadValueAsync(obj.DeviceLog.DeviceObject.ToInteger(), obj.ObjectLog.ObjectString, 59);
            }

            lWorker.PendingValueReads = driver.GetPendingReadCount();
        }
        #endregion
    }
}
