﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using K2A.Net.Niagara;
using System.Threading;
using System.Collections;
using K2A.Connect.Models;
using K2A.Connect.mLink.Messages;

namespace K2A.Connect.ConnectAgent.Workers
{
    public class NiagaraAgentWorker : ConnectAgentWorker
    {
        private bool _alive = false;
        private NiagaraDriver aDriver;
        private Dictionary<string, List<int>> objHash = new Dictionary<string, List<int>>();
        private Hashtable axImageHash = new Hashtable();
        private int maxTasks = 4;
        int timeSinceLastRequest = 0;

        internal override void StartDriver()
        {
            axImageHash = K2A.Images.DeserializeImageHashTable("icons.img".PathToLocalFile(), true);
            if (axImageHash == null) axImageHash = new Hashtable();

            if (!MyAgent.DisableLogging)
            {
                lWorker = new LogWorker(MyAgent);
                lWorker.ObjectListCreated += lWorker_ObjectListCreated;
                lWorker.ReadObjectRequest += lWorker_ReadObjectRequest;
            }

            aDriver = new NiagaraDriver(K2A.Connect.Models.NiagaraConnectionInfo.Deserialize(MyAgent.Connection));
            maxTasks = aDriver.Connection.MaxTasks;
            _alive = true;

            var t = new Thread(() => RunBatch()) ;
            t.Start();
        }


        internal override void ReadObjectID(string objObject, int ReqID)
        {
            OnValueRead(ReqID, objObject, "");
        }

        internal override void ReadPriorityArray(string devObject, string objObject, int ReqID)
        {
            var pArrays = "";
            var pn = aDriver.ReadPath(devObject + objObject);
            if (pn == null)
            {
                return;
            }
            foreach (ObixNode cn in pn.ChildNodes)
            {
                switch (cn.Name)
                {
                    case "in1":
                    case "in2":
                    case "in3":
                    case "in4":
                    case "in5":
                    case "in6":
                    case "in7":
                    case "in8":
                    case "in9":
                    case "in10":
                    case "in11":
                    case "in12":
                    case "in13":
                    case "in14":
                    case "in15":
                    case "in16":
                        pArrays = pArrays + "{" + cn.Value + "} ";
                        break;
                }

                OnValueRead(ReqID, pArrays, "");
            }
        }

        internal override void ReadValue(string devObject, string objObject, int ReqID)
        {
            if (MyAgent == null || devObject == null || objObject == null) return;

            var path = aDriver.GetActualPath(devObject + objObject);
            lock (objHash)
            {
                timeSinceLastRequest = 0;
                List<int> reqIDs;
                if (objHash.ContainsKey(path))
                {
                    reqIDs = (List<int>)objHash[path];
                }
                else
                {
                    reqIDs = new List<int>();
                    objHash.Add(path, reqIDs);
                }

                if (!reqIDs.Contains(ReqID)) reqIDs.Add(ReqID);
            }
        }

        internal override void WritePriorityValue(string devObject, string objObject, string newValue, string priority)
        {
            if (MyAgent == null || devObject == null || objObject == null)
            {
                return;
            }

            var path = devObject + objObject;

            var pn = aDriver.ReadPath(path);
            if (pn == null)
            {
                return;
            }
            switch (pn.ObjectType)
            {
                case NiagaraDriver.ObixPointType.BooleanPoint:
                    newValue = newValue.ToLower();
                    if (newValue == "null")
                    {
                        aDriver.ReleaseBooleanValue(path);
                    }
                    else
                    {
                        aDriver.OverrideBooleanValue(path, (newValue == "true"), 0);
                    }
                    break;

                case NiagaraDriver.ObixPointType.EnumPoint:
                    if (newValue.ToLower() == "null")
                    {
                        aDriver.ReleaseEnumValue(path);
                    }
                    else
                    {
                        aDriver.OverrideEnumValue(path, newValue, 0);
                    }
                    break;

                case NiagaraDriver.ObixPointType.NumericPoint:
                    if (newValue.ToLower() == "null")
                    {
                        aDriver.ReleaseValue(path);
                    }
                    else
                    {
                        aDriver.OverrideValue(path, newValue, 0);
                    }
                    break;

                case NiagaraDriver.ObixPointType.StringPoint:
                    if (newValue.ToLower() == "null")
                    {
                        aDriver.ReleaseStringValue(path);
                    }
                    else
                    {
                        aDriver.OverrideStringValue(path, newValue, 0);
                    }
                    break;
            }
        }

        internal override void WriteValue(string devObject, string objObject, string newValue)
        {
            if (MyAgent == null || devObject == null || objObject == null)
            {
                return;
            }

            var path = devObject + objObject;

            var pn = aDriver.ReadPath(path);
            if (pn == null)
            {
                return;
            }
            switch (pn.ObjectType)
            {
                case NiagaraDriver.ObixPointType.BooleanPoint:
                    newValue = newValue.ToLower();
                    if (newValue == "null")
                    {
                        aDriver.ReleaseBooleanValue(path);
                    }
                    else
                    {
                        aDriver.OverrideBooleanValue(path, (newValue == "true"), 0);
                    }
                    break;

                case NiagaraDriver.ObixPointType.EnumPoint:
                    if (newValue.ToLower() == "null")
                    {
                        aDriver.ReleaseEnumValue(path);
                    }
                    else
                    {
                        aDriver.OverrideEnumValue(path, newValue, 0);
                    }
                    break;

                case NiagaraDriver.ObixPointType.NumericPoint:
                    if (newValue.ToLower() == "null")
                    {
                        aDriver.ReleaseValue(path);
                    }
                    else
                    {
                        aDriver.OverrideValue(path, newValue, 0);
                    }
                    break;

                case NiagaraDriver.ObixPointType.StringPoint:
                    if (newValue.ToLower() == "null")
                    {
                        aDriver.ReleaseStringValue(path);
                    }
                    else
                    {
                        aDriver.OverrideStringValue(path, newValue, 0);
                    }
                    break;
            }
        }

        #region "AxMethods"
        private void AXScan(string ScanPath, bool objScan)
        {
            var pn = aDriver.ReadPath(ScanPath);

            if (objScan)
            {
                ObjectScan(pn);
            }
            else
            {
                DeviceScan(pn);
            }
        }

        private void ObjectScan(ObixNode pn)
        {
            if (pn == null)
            {
                Client.ScanStateChanged(ObjScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
                return;
            }
            var bc = new BatchCollection();
            var hash = new Hashtable();

            foreach (ObixNode cn in pn.ChildNodes)
            {
                if (cn.aIs != string.Empty && cn.Icon.Length > 0)
                {
                    var fullPath = cn.RootPath + cn.href;
                    var devObj = aDriver.GetRelativePath(fullPath);
                    var icon = 193;

                    if (axImageHash != null && axImageHash.ContainsKey(cn.Icon))
                    {
                        icon = (int)axImageHash[cn.Icon];
                    }
                    var objRes = new ObjectScanResponse()
                    {
                        Name = cn.DisplayName,
                        Description = cn.aIs,
                        IconId = icon,
                        ObjectObject = devObj,
                        ObjectValue = cn.Display
                    };

                    var bi = new BatchItem(fullPath, string.Empty, BatchItem.NiagaraCommand.Read);
                    bc.Add(bi);
                    hash.Add(fullPath, objRes);
                }
            }

            var rc = aDriver.ReadBatch(bc);

            foreach (Response resp in rc.Values)
            {
                var objRes = (ObjectScanResponse)hash[resp.URI];
                if (objRes != null)
                {
                    objRes.ObjectValue = objRes.ObjectValue + "|" + resp.CurrentValue;
                    Client.SendAsync(ObjScanClient, Client.ClientName, (Int16)mLink.Clients.ConnectClient.MessageTypeEnum.ObjectScanResponse, objRes);
                }
            }

            Client.ScanStateChanged(ObjScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }

        private void DeviceScan(ObixNode pn)
        {
            if (pn == null)
            {
                Client.ScanStateChanged(DevScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
                return;
            }
            foreach (ObixNode cn in pn.ChildNodes)
            {
                if (cn.aIs != string.Empty && cn.Icon.Length > 0)
                {
                    var devObj = aDriver.GetRelativePath(cn.RootPath + cn.href);
                    var icon = 193;

                    if (axImageHash != null && axImageHash.ContainsKey(cn.Icon))
                    {
                        icon = (int)axImageHash[cn.Icon];
                    }
                    var devRes = new DeviceScanResponse
                    {
                        DevicePath = cn.href,
                        Name = cn.DisplayName,
                        DeviceObject = devObj,
                        Description = cn.Display,
                        Model = cn.aIs,
                        IconId = icon
                    };
                    Client.DeviceScanIAm(DevScanClient, devRes);
                }
            }
            Client.ScanStateChanged(DevScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }
        #endregion

        private async void GetDynamicValues(WatchCollection wc)
        {
            try
            {
                var bp = new BatchPoll(aDriver);
                wc = bp.RunBatchPoll(wc);
                if (wc != null) ProcessMultipleDynamicValues(wc);
            }
            catch
            {
                NiagaraDriver.Log.Error("Error retrieving values");
            }
        }

        private void ProcessMultipleDynamicValues(WatchCollection wc)
        {
            try
            {
                var vals = wc.Values;

                lock (requestLookup)
                {
                    var objs = (from v in vals from r in (List<int>)v.UserData where requestLookup.ContainsKey(r) && (v.CurrentValue != null || v.Display != null) select new { client = requestLookup[r].ClientName, cachePath = requestLookup[r].CachePath, response = new WatchMultipleResponse.Response() { RequestID = requestLookup[r].RequestID, DisplayValue = v.Display ?? v.CurrentValue, ObjectValue = v.CurrentValue ?? v.Display } }).ToList();

                    foreach (var val in objs)
                    {
                        CacheValue(val.cachePath, val.response.ObjectValue);
                    }

                    var grouped = (from o in objs group o.response by o.client into g select new { clientname = g.Key, objects = g.ToList() }).ToList();

                    foreach (var g in grouped)
                    {
                        OnValueReadMultiple(g.clientname, g.objects);
                    }

                    foreach (var o in vals)
                    {
                        foreach (int i in (List<int>)o.UserData)
                        {
                            requestLookup.Remove(i);
                        }

                    }
                }
            }
            catch
            {
            }
        }
        private void RunBatch()  //Dynamic Data
        {
            while (_alive)
            {
                //Allow time to collect multiple objects before running batch
                while (objHash.Count == 0)
                {
                    Thread.Sleep(5);
                }

                int waitTime = 0;

                while (timeSinceLastRequest < 30 && waitTime < 1000)
                {
                    Thread.Sleep(5);
                    timeSinceLastRequest += 5;
                    waitTime += 5;
                }

                var wList = new WatchCollection();
                int completedObjects = 0;
                List<Task> taskList = new List<Task>();

                lock (objHash)
                {
                    foreach (var itm in objHash)
                    {
                        wList.Add(new WatchValue() { URI = itm.Key + "out/value/", TimeStamp = DateTime.UtcNow, UserData = itm.Value });

                        completedObjects++;

                        if (wList.Count >= aDriver.Connection.BatchReadSize || completedObjects == objHash.Count)
                        {
                            var list = wList.Clone();
                            taskList.Add(Task.Run(() => GetDynamicValues(list)));
                            wList.Clear();
                        }
                    }

                    objHash.Clear();
                }

                var taskListChunked = taskList.ChunkBy(maxTasks);

                try
                {
                    foreach (var tlc in taskListChunked)
                    {
                        Task.WaitAll(tlc.ToArray(), 300000);
                    }
                }
                catch
                {

                }

            }
        }

        #region "ClientRequests"

        internal override void DeviceScan(string Sender, DeviceScan Message)
        {
            Task.Run(() => AXScan(Message.PathToScan, false));
        }
        internal override void ObjectScan(string Sender, ObjectScan Message)
        {
            Task.Run(() => AXScan(Message.PathToScan, true));
        }

        private void lWorker_ObjectListCreated(List<LogWorker.ObjectInfo> objectList)
        {

        }
        private void lWorker_ReadObjectRequest(List<LogWorker.ObjectInfo> objectList)
        {
            var t = new Thread(() => ReadLogObjectList(objectList)) { IsBackground = true };
            t.Start();
        }

        private void ReadLogObjectList(List<LogWorker.ObjectInfo> objectList)
        {
            lWorker.PendingValueReads = objectList.Count;


            var wList = new WatchCollection();
            int completedObjects = 0;
            List<Task> taskList = new List<Task>();

            if (!aDriver.Authenticate())
            {
                Logging.Log("Unable to Read Object List due to authentication failure.", Logging.EntryType.Error);
                lWorker.PendingValueReads = 0;
                return;
            }

            foreach (var obj in objectList)
            {
                string path = String.Format("{0}{1}out/value/", obj.DeviceLog.DeviceObject, obj.ObjectLog.ObjectString);
                path = aDriver.GetActualPath(path);

                wList.Add(new WatchValue() { URI = path, TimeStamp = DateTime.UtcNow, UserData = obj });

                completedObjects++;

                if (wList.Count >= aDriver.Connection.BatchReadSize || completedObjects == objectList.Count)
                {
                    var list = wList.Clone();
                    taskList.Add(Task.Run(() => GetLogValues(list)));

                    if (taskList.Count == maxTasks || completedObjects == objectList.Count)
                    {
                        Task.WaitAll(taskList.ToArray(), 300000);
                        taskList.Clear();
                    }


                    wList.Clear();
                }
            }

            lWorker.PendingValueReads = 0;
        }
        private async Task GetLogValues(WatchCollection wList)
        {
            try
            {
                BatchPoll bp = new BatchPoll(aDriver);
                WatchCollection wc = bp.RunBatchPoll(wList);


                foreach (WatchValue wv in wc.Values)
                {
                    var objInfo = (LogWorker.ObjectInfo)wv.UserData;
                    if (string.IsNullOrEmpty(wv.CurrentValue)) wv.CurrentValue = "";
                    if (objInfo.ObjectLog.DataType == "DOUBLE PRECISION" && !wv.CurrentValue.isNumeric())
                    {
                        if (!string.IsNullOrEmpty(wv.CurrentValue))
                            NiagaraDriver.Log.Error(string.Format("Error converting Niagara value ({0}) to DOUBLE PRECISION: {1}", wv.CurrentValue, objInfo.ObjectLog.ObjectName));

                        wv.CurrentValue = "-445";
                    }
                    else
                    {
                        objInfo.ObjectLog.ObjectValue = wv.CurrentValue;
                    }

                    if (wc.ReadError) //error occurred
                        lWorker.ValueReadComplete(objInfo, true, "Connection Error");
                    else
                        lWorker.ValueReadComplete(objInfo, wv.HasError(), wv.Error);

                }
            }
            catch (Exception ex)
            {
                NiagaraDriver.Log.Error("Error getting log values: " + ex.Message);
            }
        }
        #endregion
    }
}
