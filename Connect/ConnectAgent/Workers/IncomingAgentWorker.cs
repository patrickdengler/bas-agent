﻿using System;
using System.Collections.Generic;
using System.Linq;
using K2A.Connect.Models;
using K2A.mLink.Messages;
using System.Threading;
using Newtonsoft.Json;
using System.Text;
using K2A.Connect.mLink.Messages;

namespace K2A.Connect.ConnectAgent.Workers
{
    public class IncomingAgentWorker : ConnectAgentWorker
    {
        private List<IncomingDevice> Devices = new List<IncomingDevice>();
        //private Dictionary<string, IncomingObject> ObjectLookup = new Dictionary<string, IncomingObject>();
        Dictionary<string, BBObjectLog> ObjectLogLookup = new Dictionary<string, BBObjectLog>();
        private K2A.Net.WebService.WebServer webServer;
        private IncomingConnectionInfo connInfo;

        internal override void ReadObjectID(string objObject, int ReqID)
        {
            OnValueRead(ReqID, "Incoming Value", "");
        }

        internal override void ReadValue(string devObject, string objObject, int ReqID)
        {
            IncomingObject foundObj = (from d in Devices from o in d.Objects where d.DevicePath == devObject && o.ObjectPath == objObject select o).FirstOrDefault();

            if (foundObj != null)
                OnValueRead(ReqID, foundObj.ObjectValue, foundObj.ObjectValue);
            else
                OnValueRead(ReqID, "", "");
        }

        internal override void StartDriver()
        {
            LoadDevices();

            if (!MyAgent.DisableLogging)
            {
                lWorker = new LogWorker(MyAgent);
                lWorker.ObjectListCreated += lWorker_ObjectListCreated;
                connInfo = IncomingConnectionInfo.Deserialize(MyAgent.Connection);
                webServer = new Net.WebService.WebServer();
                webServer.ProcessRequest += webServer_ProcessRequest;
                webServer.ConnectionLost += webServer_ConnectionLost;
                webServer.NewConnection +=webServer_NewConnection;
                
                webServer.StartListeningAsync(connInfo.ServerIP, connInfo.ServerPort);
            }
        }


        private void LoadDevices()
        {
            Dictionary<int, List<BBObject>> profileLookup = new Dictionary<int, List<BBObject>>();

            foreach (var dev in MyAgent.DeviceList())
            {
                var d = new IncomingDevice() { DeviceModel = dev.Model, DeviceName = dev.RefId, DeviceDescription = dev.Description, DevicePath = dev.Object, Objects = new List<IncomingObject>() };

                if (!profileLookup.ContainsKey(dev.ProfileId)) profileLookup.Add(dev.ProfileId, dev.ObjectList());

                foreach (var obj in profileLookup[dev.ProfileId])
                {
                    d.Objects.Add(new IncomingObject() { ObjectName = obj.RefId, ObjectDescription = obj.Description, ObjectValue = "", ObjectPath = obj.ObjectObject });
                }

                Devices.Add(d);
            }
        }

        #region "Incoming Methods"

        private void Scan(string path, bool objScan)
        {
            lock (Devices)
            {
                if (!objScan)
                {

                    foreach (var dev in Devices)
                    {
                        var devRes = new DeviceScanResponse
                        {
                            DevicePath = dev.DevicePath,
                            Name = dev.DeviceName,
                            DeviceObject = dev.DevicePath,
                            Description = dev.DeviceDescription,
                            Model = dev.DeviceModel,
                        };
                        Client.DeviceScanIAm(DevScanClient, devRes);
                    }
                    Client.ScanStateChanged(DevScanClient, K2A.mLink.Clients.AgentClient.ScanStatus.Complete);
                }
                else
                {
                    var iDev = (from d in Devices where d.DevicePath.ToUpper() == path.ToUpper() select d).FirstOrDefault();

                    if (iDev != null)
                    {
                        foreach (var obj in iDev.Objects)
                        {
                            var ObjRes = new ObjectScanResponse
                            {
                                Name = obj.ObjectName,
                                Description = obj.ObjectDescription,
                                ObjectObject = obj.ObjectPath,
                                ObjectValue = obj.ObjectValue
                            };
                            Client.SendAsync(ObjScanClient, Client.ClientName, mComm.MessageTypeEnum.ObjectScanResponse, ObjRes);
                        }
                        Client.ScanStateChanged(ObjScanClient, K2A.mLink.Clients.AgentClient.ScanStatus.Complete);
                    }
                }
            }

        }

        private void webServer_ProcessRequest(K2A.Net.WebService.HttpRequest req)
        {
            lWorker.PendingValueReads = 1;

            StringBuilder sb = new StringBuilder();
            bool success = false; 

            byte[] bs;

            if (!req.HttpUrl.Parameters.ContainsKey("apikey") || req.HttpUrl.Parameters["apikey"] != connInfo.APIKey.ToLower() )
            {
                sb.AppendLine("HTTP/1.0 403 Forbidden");
                sb.AppendLine("Connection: close");
                sb.AppendLine("");
                bs = Encoding.ASCII.GetBytes(sb.ToString());
                req.Response = bs;
                return;
            }

            if (req.HttpMethod.ToUpper() != "POST")
            {
                sb.AppendLine("HTTP/1.0 404 Not Found");
                sb.AppendLine("Connection: close");
                sb.AppendLine("");
                bs = Encoding.ASCII.GetBytes(sb.ToString());
                req.Response = bs;
                return;
            }
            

            lock (Devices)
            {
                switch (req.HttpUrl.Command)
                {
                    case "postvalue":
                        success = PostValue(req.Data);
                        break;
                    case "postvalues":
                        success = PostValues(req.Data);
                        break;
                    case "postall":
                        success = PostAll(req.Data);
                        break;
                    case "postdevice":
                        success = PostDevice(req.Data);
                        break;
                    default:
                        success = false;
                        break;

                }
            }

            lWorker.PendingValueReads = 0;

            if (success)
            {
                sb.AppendLine("HTTP/1.0 200 OK");
                //sb.AppendLine("Content-Type: " + contentType);
                sb.AppendLine("Connection: close");
                sb.AppendLine("");
            }
            else
            {
                sb.AppendLine("HTTP/1.0 400 Bad Request");
                //sb.AppendLine("Content-Type: " + contentType);
                sb.AppendLine("Connection: close");
                sb.AppendLine("");

            }


            bs = Encoding.ASCII.GetBytes(sb.ToString());
            req.Response = bs;
        }
        private void webServer_ConnectionLost()
        {
            lWorker.PendingValueReads = 0;
        }
        private void webServer_NewConnection()
        {
            lWorker.PendingValueReads = 1;
        }
        private void ParseIncomingValue(IncomingValue val)
        {
            string hash = String.Format("{0}|{1}", val.DeviceName, val.ObjectName);

            if (ObjectLogLookup.ContainsKey(hash))
            {
                ObjectLogLookup[hash].ObjectValue = val.ObjectValue;
                lWorker.ValueReadComplete(null);
            }
            //else
            //{
                var dev = (from d in Devices where d.DevicePath == val.DeviceName select d).FirstOrDefault();
                if (dev == null)
                {
                    var iDev = new IncomingDevice() { DevicePath = val.DeviceName, DeviceName = val.DeviceName, DeviceDescription = val.DeviceName, Objects = new List<IncomingObject>() };
                    iDev.Objects.Add(new IncomingObject() { ObjectDescription = val.ObjectName, ObjectName = val.ObjectName, ObjectPath = val.ObjectName, ObjectValue = val.ObjectValue });
                    Devices.Add(iDev);
                }
                else
                {
                    var obj = (from o in dev.Objects where o.ObjectPath == val.ObjectName select o).FirstOrDefault();

                    if (obj == null)
                    {
                        dev.Objects.Add(new IncomingObject() { ObjectDescription = val.ObjectName, ObjectName = val.ObjectName, ObjectPath = val.ObjectName, ObjectValue = val.ObjectValue });
                    }
                    else
                    {
                        obj.ObjectValue = val.ObjectValue;
                        
                    }
                }
            //}
        }
        private void ParseIncomingDevice(IncomingDevice incomingDev)
        {
            var cachedDev = (from d in Devices where d.DevicePath == incomingDev.DevicePath select d).FirstOrDefault();

            if (cachedDev == null)
            {
                cachedDev = incomingDev;
                Devices.Add(cachedDev);
            }
            else
            {
                cachedDev.DeviceDescription = incomingDev.DeviceDescription;
                cachedDev.DeviceModel = incomingDev.DeviceModel;
                cachedDev.DeviceName = incomingDev.DeviceName;
            }


            foreach (var obj in incomingDev.Objects)
            {
                string hash = String.Format("{0}|{1}", incomingDev.DevicePath, obj.ObjectName);

                if (ObjectLogLookup.ContainsKey(hash))
                {
                    ObjectLogLookup[hash].ObjectValue = obj.ObjectValue;
                    lWorker.ValueReadComplete(null);
                }

                var fObj = (from o in cachedDev.Objects where o.ObjectPath == obj.ObjectPath select o).FirstOrDefault();

                if (fObj == null)
                {
                    cachedDev.Objects.Add(obj);
                }
                else
                {
                    fObj.ObjectValue = obj.ObjectValue;
                    fObj.ObjectDescription = obj.ObjectDescription;
                }
            }
        }
        private bool PostValue(string data)
        {
            try
            {
                IncomingValue val = JsonConvert.DeserializeObject<IncomingValue>(data);
                if (val == null) return false;
                ParseIncomingValue(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool PostValues(string data)
        {
            try
            {
                List<IncomingValue> vals = JsonConvert.DeserializeObject<List<IncomingValue>>(data);
                if (vals == null) return false;
                foreach (var val in vals)
                {
                    ParseIncomingValue(val);
                }
                
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool PostDevice(string data)
        {
            try
            {
                IncomingDevice dev = JsonConvert.DeserializeObject<IncomingDevice>(data);
                if (dev == null) return false;
                ParseIncomingDevice(dev);

                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool PostAll(string data)
        {
            try
            {
                List<IncomingDevice> devs = JsonConvert.DeserializeObject<List<IncomingDevice>>(data);
                if (devs == null) return false;
                foreach (var dev in devs)
                {
                    ParseIncomingDevice(dev);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region "ClientRequests"

        internal override void DeviceScan(string Sender, DeviceScan Message)
        {
            var t2 = new Thread(() => Scan(Message.PathToScan, false));
            t2.IsBackground = true;
            t2.Start();
        }
        internal override void ObjectScan(string Sender, ObjectScan Message)
        {
            var t2 = new Thread(() => Scan(Message.PathToScan, true));
            t2.IsBackground = true;
            t2.Start();
        }


        private void lWorker_ObjectListCreated(List<LogWorker.ObjectInfo> objectList)
        {

            foreach (var obj in objectList)
            {
                ObjectLogLookup[obj.Hash] = obj.ObjectLog;
            }
            
        }

        #endregion

        #region "Incoming Objects"

        public class IncomingDevice
        {
            public string DeviceName { get; set; }
            public string DeviceModel { get; set; }
            public string DeviceDescription { get; set; }
            public string DevicePath { get; set; }

            public List<IncomingObject> Objects { get; set; }
        }

        public class IncomingObject
        {
            private string _ObjectValue;
            public string ObjectValue
            {
                get
                {
                    //if (ObjectLog != null)
                    //    return ObjectLog.ObjectValue;
                    //else
                        return _ObjectValue;
                }
                set
                {
                    //if (ObjectLog != null)
                    //    ObjectLog.ObjectValue = value;
                    //else
                        _ObjectValue = value;
                }
            }
            //public BBObjectLog ObjectLog { get; set; }
            public string ObjectName { get; set; }
            public string ObjectPath { get; set; }
            public string ObjectDescription { get; set; }
        }

        internal class IncomingValue
        {
            public string DeviceName { get; set; }
            public string ObjectName { get; set; }
            public string ObjectValue { get; set; }
            public string GetHash()
            {
                return string.Format("{0}|{1}", DeviceName, ObjectName);
            }
        }
        #endregion

        #region WebService



        #endregion
    }
}
