﻿using System;
using System.Collections.Generic;
using System.Linq;
using K2A.Connect.Models;
using System.Threading;
using K2A.Connect.mLink.Messages;

namespace K2A.Connect.ConnectAgent.Workers
{
    public class JsonAgentWorker : ConnectAgentWorker
    {
        //Use cache here because json reader is static class
        private DateTime LastJsonRead = new DateTime();
        private Dictionary<string, string> JsonValues = new Dictionary<string, string>();

        internal override void ReadObjectID(string objObject, int ReqID)
        {
            OnValueRead(ReqID, "JSON Value", "");
        }


        internal override void ReadValue(string devObject, string objObject, int ReqID)
        {
            if (LastJsonRead.AddSeconds(10) >= DateTime.UtcNow && JsonValues != null && JsonValues.ContainsKey(objObject))
            {
                //If found in cache less than 10 seconds ago
                OnValueRead(ReqID, (string)JsonValues[objObject], (string)JsonValues[objObject]);
            }
            else
            {
                JsonConnectionInfo jConnectionInfo = JsonConnectionInfo.Deserialize(MyAgent.Connection);
                var results = K2A.Connect.Net.JSON.FlatJson.GetValues(jConnectionInfo.ServerIP);
                lock (JsonValues)
                {
                    if (results != null)
                    {
                        JsonValues = results;
                        LastJsonRead = DateTime.UtcNow;

                        if (JsonValues.ContainsKey(objObject))
                            OnValueRead(ReqID, (string)JsonValues[objObject], (string)JsonValues[objObject]);
                        else
                            OnValueRead(ReqID, "", "");
                    }
                    else
                    {
                        OnValueRead(ReqID, "", "");
                    }
                }
            }
        }

        internal override void StartDriver()
        {
            if (!MyAgent.DisableLogging)
            {
                lWorker = new LogWorker(MyAgent);
                lWorker.ObjectListCreated += lWorker_ObjectListCreated;
                lWorker.ReadObjectRequest += lWorker_ReadObjectRequest;
            }
        }

        #region "ClientRequests"

        internal override void DeviceScan(string Sender, DeviceScan Message)
        {
            var jConnInfo = JsonConnectionInfo.Deserialize(MyAgent.Connection);
            var results = K2A.Connect.Net.JSON.FlatJson.GetValues(jConnInfo.ServerIP);
            if (results != null)
            {
                var devScanResponse = new DeviceScanResponse() { Description = "JSON_Device", DeviceObject = "/", DevicePath = "/", IconId = -1, Model = "JSON", Name = "JSON_Device" };
                Client.DeviceScanIAm(DevScanClient, devScanResponse);
            }

            Client.ScanStateChanged(DevScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }
        internal override void ObjectScan(string Sender, ObjectScan Message)
        {
            var jConnInfo = JsonConnectionInfo.Deserialize(MyAgent.Connection);
            var results = K2A.Connect.Net.JSON.FlatJson.GetValues(jConnInfo.ServerIP);
            if (results != null)
            {
                foreach (var r in results)
                {
                    ObjectScanRespond(r.Key, (string)r.Value);
                }
            }
            Client.ScanStateChanged(ObjScanClient, mLink.Clients.AgentClient.ScanStatus.Complete);
        }

        private void lWorker_ObjectListCreated(List<LogWorker.ObjectInfo> objectList)
        {

        }
        private void lWorker_ReadObjectRequest(List<LogWorker.ObjectInfo> objectList)
        {
            var t = new Thread(() => ReadObjectList(objectList)) { IsBackground = true };
            t.Start();

        }

        private void ReadObjectList(List<LogWorker.ObjectInfo> objectList)
        {
            lWorker.PendingValueReads = objectList.Count;

            if (LastJsonRead.AddSeconds(10) < DateTime.UtcNow || JsonValues != null)
            {
                JsonConnectionInfo jConnectionInfo = JsonConnectionInfo.Deserialize(MyAgent.Connection);
                var results = K2A.Connect.Net.JSON.FlatJson.GetValues(jConnectionInfo.ServerIP);

                lock (JsonValues)
                {
                    if (results != null)
                        JsonValues = results;
                }
            }

            //If found in cache less than 10 seconds ago
            foreach (var obj in objectList)
            {
                if (JsonValues.ContainsKey(obj.ObjectLog.ObjectString))
                {
                    obj.ObjectLog.ObjectValue = JsonValues[obj.ObjectLog.ObjectString];
                    lWorker.ValueReadComplete(obj);
                }
                else
                {
                    lWorker.ValueReadComplete(obj, true, "Object Not Found");
                }
            }

            lWorker.PendingValueReads = 0;
        }
        #endregion
    }
}
