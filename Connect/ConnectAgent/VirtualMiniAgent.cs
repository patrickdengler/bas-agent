using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using K2A;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Ionic.Zip;
using K2A.Connect.Models;
using K2A.Connect.mLink.Clients;
using K2A.mLink;
using K2A.Connect.ConnectAgent.Workers;
using K2A.Connect.Models.Auth;
using Newtonsoft.Json.Linq;
using K2A.Data.Redis;
using System.Threading.Tasks;

namespace K2A.Connect.ConnectAgent
{
    public class VirtualMiniAgent : BaseAgent
    {
        public VirtualMiniAgent(string sn) : base(sn)
        {

            try
            {
                string agentIdHex = sn.Substring(10, 5);
                agentId = int.Parse("0" + agentIdHex, System.Globalization.NumberStyles.HexNumber);
            }
            catch { }

            string fullSn = $"{sn.Substring(0, 5)}-{sn.Substring(5, 5)}-{sn.Substring(10, 5)}-{sn.Substring(15, 5)}-{sn.Substring(20, 5)}";

            SerialNumber = fullSn;

            K2A.Connect.LogFiles.CreateAgentLogConfiguration("agent_" + agentId, true, new ConnectAgent.RedisTarget(redis, "Agents:" + agentId + ":Logs"));

            Initialize();
        }

        internal override void Initialize()
        {

            string lic = GetLicense();

            if (lic.IsNullOrEmpty() || lic.Length < 10)
            {
                Logging.Log($"Unable to find a valid license: {SerialNumber}.  Retrying in 300 seconds...", Logging.EntryType.Error);
                System.Threading.Thread.Sleep(300000);

                Initialize();

                return;
            }

            try
            {
                var result = JObject.Parse(lic);

                var svrData = result.GetValue("serverdata").ToString();

                var serverInfo = Models.ServerInfo.Deserialize(svrData);

                string serverIdHex = SerialNumber.Substring(6, 5);
                int serverId = int.Parse("0" + serverIdHex, System.Globalization.NumberStyles.HexNumber);

                if (agentId != result["agentid"].ToString().ToInteger(0))
                {
                    Logging.Log("Invalid serial number and/or license.  Retrying in 300 seconds...", Logging.EntryType.Error);
                    System.Threading.Thread.Sleep(300000);

                    Initialize();

                    return;
                }

                ActiveUser.SetConnectionSettings(new ConnectConnectionSettings()
                {
                    ConnectionId = serverId,
                    DatabaseName = serverInfo.DatabaseName,
                    DatabasePassword = serverInfo.DatabasePassword,
                    DatabaseUserName = serverInfo.DatabaseUser,
                    ServerAddress = serverInfo.BBServer,
                    DatabaseServerAddress = serverInfo.DatabaseServer,
                    ServerPort = serverInfo.BBServerPort
                });
            }
            catch
            {
                Logging.Log("Unable to read license.  Retrying in 300 seconds...", Logging.EntryType.Error);
                System.Threading.Thread.Sleep(300000);

                Initialize();

                return;

            }

            StartAgent();

        }

    }
}
