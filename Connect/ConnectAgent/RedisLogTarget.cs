﻿using NLog;
using NLog.Config;
using NLog.Targets;

namespace K2A.Connect.ConnectAgent
{
    [Target("Redis")]
    public sealed class RedisTarget : TargetWithLayout
    {
        public RedisTarget(Data.Redis.Client client, string key)
        {
            this.RedisClient = client;
            this.Name = "redis";
            this.Key = key;

        }

        [RequiredParameter]
        public K2A.Data.Redis.Client RedisClient { get; set; }

        [RequiredParameter]
        public string Key { get; set; }

        public string Channel { get; set; }

        protected override void Write(LogEventInfo logEvent)
        {
            string logMessage = this.Layout.Render(logEvent);

            if (RedisClient.IsConnected())
                {
                try
                {
                    RedisClient.AppendToList(Key + ":EventLog", logMessage, 1000);
                    if (logEvent.Level.Name == "Error") RedisClient.AppendToList(Key + ":Errors", logMessage, 1000);

                    if (Channel.IsNotNullOrEmpty() && logEvent.Level.Name.ToUpper() == "ERROR") RedisClient.SendMessage(Channel, logEvent.FormattedMessage);
                }
                catch { }
            }

        }
    }
}