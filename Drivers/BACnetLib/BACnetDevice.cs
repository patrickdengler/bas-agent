﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K2A.Drivers.BACnetLib
{
    public class BACnetDevice
    {
        public string DeviceName = "";
        public int DeviceID = -1;
        public int DeviceArray = -1;
        public bool isNative = true;

        public int TraneDeviceType = -1;

        public BACnetDevice(string DevName, int DevID, int DevArray, bool isNativeBACnet)
        {
            DeviceName = DevName;
            DeviceID = DevID;
            DeviceArray = DevArray;
            isNative = isNativeBACnet;
        }

        public BACnetDevice(string DevName, int DevID, int DevArray, bool isNativeBACnet, int DeviceType)
        {
            DeviceName = DevName;
            DeviceID = DevID;
            DeviceArray = DevArray;
            isNative = isNativeBACnet;
            TraneDeviceType = DeviceType;
        }

        public BACnetDevice()
        {
        }

        public override string ToString()
        {
            string toStr = string.Format("{0},{1},{2},{3}", DeviceID, DeviceArray, (isNative ? 1 : 0), TraneDeviceType);

            return toStr;
        }

        public static BACnetDevice FromString(string FromStr)
        {
            if (string.IsNullOrEmpty(FromStr)) return null;

            string[] DevString = FromStr.Split(',');

            BACnetDevice bDevice = new BACnetDevice("", int.Parse(DevString[0]), int.Parse(DevString[1]), (int.Parse(DevString[2]) == 1 ? true : false), int.Parse(DevString[3]));
            return bDevice;
        }

    }
}
