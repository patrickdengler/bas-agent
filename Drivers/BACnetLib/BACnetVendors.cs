﻿using System;
using System.Collections.Generic;
using System.Linq;
using K2A.Core;
namespace K2A.Drivers.BACnetLib
{
    public static class Vendors
    {
        private static Dictionary<int, string> lookup = new Dictionary<int, string>();

        public static string GetVendorName (int VendorId)
        {
            if (lookup.Count == 0) BuildLookup();

            if (lookup.ContainsKey(VendorId))
                return lookup[VendorId];
            else
                return VendorId.ToString();
        }

        private static void BuildLookup ()
        {
            lookup.Clear();
            string[] vendors = Resources.bacnetvendors.Lines();
            foreach (string vendor in vendors)
            {
                int id = vendor.Split('=')[1].ToInteger(-1);
                string vName = vendor.Split('=')[0];
                lookup.Add(id, vName);
            }
        }
    }
}
