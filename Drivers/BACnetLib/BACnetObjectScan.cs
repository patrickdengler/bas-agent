﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using K2A.Core;

namespace K2A.Drivers.BACnetLib
{
    public class BACnetObjectScan : Component
    {
        private List<iAmObject> FoundObjects = new List<iAmObject>();

        private BACnetLive bLive;

        public NameValueOptions NameOptions = NameValueOptions.ObjectName;

        //public bool GetDescription = false;
        //public bool GetJCIName = false;

        public bool FilterObjects = true;

        public bool GetValues = true;

        public bool OnlyRetrieveProprietary = false;

        private List<int> deviceIds = new List<int>();

        private int objCount = 0;
        private int completedObjects = 0;

        private int currentStep = 0;

        #region Delegates and Events

        public event ProgressChangedEventHandler ProgressChanged;

        public delegate void ProgressChangedEventHandler(int Percent, string Status);

        public event ScanCompleteEventHandler ScanComplete;

        public delegate void ScanCompleteEventHandler(int deviceId, iAmObject[] bObjects);

        public event ScanFinalizedEventHandler ScanFinalized;

        public delegate void ScanFinalizedEventHandler();

        #endregion Delegates and Events

        public BACnetObjectScan()
        {
        }

        public bool Running
        {
            get { return (bLive != null) && bLive.ObjectCount() > 0; }
        }

        public void Scan(int deviceId)
        {
            Scan(new List<int>() { deviceId });
        }

        public void Scan(List<int> deviceIds)
        {
            Stop();

            if (ProgressChanged != null)
            {
                ProgressChanged(0, "Scanning Objects");
            }

            this.deviceIds = deviceIds;

            NativeBACnetScan();
        }

        public void Stop()
        {
            if ((bLive != null))
                bLive.StopLive();

            FoundObjects.Clear();

            objCount = 0;
            completedObjects = 0;

            if (bLive != null)
            {
                bLive.onLiveRead -= OnLiveRead;
                bLive.StopLive();
            }
            bLive = new BACnetLive();
            bLive.onLiveRead += OnLiveRead;
            bLive.Start();
        }

        private void OnLiveRead(BACnetObject itm, string Err)
        {
            completedObjects++;

            if (!string.IsNullOrEmpty(Err))
                itm.Value = Err;

            switch (itm.PropId)
            {
                //If it is from ObjectList get ObjectType and instance
                case BacnetPropertyIds.PROP_OBJECT_LIST:
                    BacnetObjectTypes ObjT;
                    if (itm.Value.Split(':')[0].isNumeric())
                        ObjT = (BacnetObjectTypes)itm.Value.ToInteger();
                    else
                    {
                        ObjT = (BacnetObjectTypes)Enum.Parse(typeof(BacnetObjectTypes), "OBJECT_" + itm.Value.Split(':')[0]);
                    }


                    int ObjI = int.Parse(itm.Value.Split(':')[1]);

                    if (IsValidObject((int)ObjT))
                    {
                        BACnetObject objObject = new BACnetObject(new BACnetDevice("", itm.DeviceInstance, -1, true), ObjT, ObjI, BacnetPropertyIds.PROP_PRESENT_VALUE, -1, "");
                        iAmObject iAm = new iAmObject
                        {
                            DeviceId = itm.DeviceInstance,
                            Description = String.Format("{0}-{1}", ObjT, ObjI),
                            BACnetObject = objObject.ToString()
                        };

                        FoundObjects.Add(iAm);

                        objObject.UserData = iAm;
                        //Add Object Name or Description, Add Present Value
                        if (NameOptions == NameValueOptions.Description)
                            bLive.AddItem(new BACnetObject(objObject.DeviceObject, ObjT, ObjI, BacnetPropertyIds.PROP_DESCRIPTION, -1, iAm));
                        else if (NameOptions == NameValueOptions.JCIName)
                            bLive.AddItem(new BACnetObject(objObject.DeviceObject, ObjT, ObjI, (BacnetPropertyIds)2390, -1, iAm));
                        else
                            bLive.AddItem(new BACnetObject(objObject.DeviceObject, ObjT, ObjI, BacnetPropertyIds.PROP_OBJECT_NAME, -1, iAm));

                        if (GetValues) bLive.AddItem(objObject);
                    }
                    else
                    {
                        //If incompatible ObjectType, increment Current Progress for Name/Value
                        completedObjects = (GetValues) ? completedObjects + 2 : completedObjects + 1;
                    }
                    break;

                //If Name or Value
                default:
                    if (itm.PropId == BacnetPropertyIds.PROP_DESCRIPTION || itm.PropId == BacnetPropertyIds.PROP_OBJECT_NAME || itm.PropId == (BacnetPropertyIds)2390)
                    {
                        ((iAmObject)itm.UserData).ObjectName = itm.Value;
                    }
                    else
                    {
                        ((iAmObject)itm.UserData).ObjectValue = itm.Value;
                    }
                    break;
            }
            //Debug.Print(completedObjects + " of " + objCount + " read. " + bLive.ObjectCount() + " remaining.");
            if (currentStep == 2)
            {
                if (ProgressChanged != null)
                {
                    int progress = completedObjects.PercentOf(objCount, 90) + 10;
                    ProgressChanged(progress, "Scanning Objects");
                }
                if (bLive.ObjectCount() == 0 && ScanComplete != null)
                {
                    Debug.Print(completedObjects + " of " + objCount + "read. " + bLive.ObjectCount() + " remaining.");
                    foreach (int devId in deviceIds)
                    {
                        var objs = (from o in FoundObjects where o.DeviceId == devId select o).ToArray();
                        if (objs != null) ScanComplete(devId, objs);
                    }
                    if (ScanFinalized != null) ScanFinalized();
                }
            }
        }

        private bool IsValidObject(int ObjT)
        {
            if (OnlyRetrieveProprietary)
            {
                return ObjT > 30;
            }
            else
            {
                return ((int)ObjT <= 5 | (int)ObjT == 13 | (int)ObjT == 14 | (int)ObjT == 19) || !FilterObjects;
            }
        }

        private void NativeBACnetScan()
        {
            currentStep = 1;

            int devsCompleted = 0;
            List<BACnetObject> objs = new List<BACnetObject>();

            foreach (var dev in deviceIds)
            {
                if (ProgressChanged != null)
                {
                    int progress = devsCompleted.PercentOf(deviceIds.Count, 10);
                    ProgressChanged(progress, "Getting Object Count");
                }

                var vals = BACnetCommands.ReadPropertyArray(dev, BacnetObjectTypes.OBJECT_DEVICE, dev, BacnetPropertyIds.PROP_OBJECT_LIST, -1);
                if (vals == null) //Unable to read all at once, iterate
                {

                    int oCount = BACnetCommands.ReadProperty(dev, BacnetObjectTypes.OBJECT_DEVICE, dev, BacnetPropertyIds.PROP_OBJECT_LIST, 0).ToInteger();
                    Debug.Print(oCount + " objects found in Device " + dev);
                    if (oCount > 0)
                    {
                        objCount = GetValues ? objCount + (oCount * 3) : objCount + (oCount * 2);

                        for (int i = 1; i <= oCount; i++)
                        {
                            objs.Add(new BACnetObject(dev, BacnetObjectTypes.OBJECT_DEVICE, dev, BacnetPropertyIds.PROP_OBJECT_LIST, i, -1));
                        }
                    }
                    else
                    {
                        BACnetDriver.Log.Error("Unable to retrieve object count from device " + dev);
                    }
                    devsCompleted++;
                    continue;
                }

                var sortedList = SortBacnetObjects(vals);
                Debug.Print(sortedList.Count + " objects found in Device " + dev);

                foreach (var itm in sortedList)
                {
                    if (IsValidObject((int)itm.Type))
                    {
                        objCount = GetValues ? objCount + 2 : objCount + 1;
                        var bObj = new BACnetObject(dev, itm.Type, (int)itm.Instance, BacnetPropertyIds.PROP_PRESENT_VALUE, -1, 2);
                        iAmObject iAm = new iAmObject
                        {
                            DeviceId = dev,
                            Description = String.Format("{0}-{1}", itm.Type, itm.Instance),
                            BACnetObject = bObj.ToString()
                        };

                        FoundObjects.Add(iAm);
                        bObj.UserData = iAm;

                        if (GetValues) objs.Add(bObj);

                        if (NameOptions == NameValueOptions.Description)
                            objs.Add(new BACnetObject(dev, itm.Type, (int)itm.Instance, BacnetPropertyIds.PROP_DESCRIPTION, -1, iAm));
                        else if (NameOptions == NameValueOptions.JCIName)
                            objs.Add(new BACnetObject(dev, itm.Type, (int)itm.Instance, (BacnetPropertyIds)2390, -1, iAm));
                        else
                            objs.Add(new BACnetObject(dev, itm.Type, (int)itm.Instance, BacnetPropertyIds.PROP_OBJECT_NAME, -1, iAm));

                    }
                }

                devsCompleted++;
            }

            Debug.Print(objCount + " in objectCount, " + objs.Count + " in bLive");
            bLive.AddItems(objs);


            if (ProgressChanged != null)
            {
                ProgressChanged(10, "Getting Object Count");
            }

            currentStep = 2;
        }

        private List<BacnetObjectId> SortBacnetObjects(IList<BacnetValue> RawList)
        {

            List<BacnetObjectId> SortedList = new List<BacnetObjectId>();
            foreach (BacnetValue value in RawList)
                SortedList.Add((BacnetObjectId)value.Value);

            SortedList.Sort();

            return SortedList;
        }
    }

    public class iAmObject
    {
        public int DeviceId { get; set; }
        public string ObjectName { get; set; }

        public string Description { get; set; }

        public string ObjectValue { get; set; }

        public string BACnetObject { get; set; }
    }

    public enum NameValueOptions
    {
        ObjectName,
        Description,
        JCIName
    }
}