﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.ComponentModel;

namespace K2A.Drivers.BACnetLib
{
    public class BACnetLive : Component
    {
        //reduced to batch read for simplicity

        public int IdleTimePercent = 100;
        public int LoadTimePercent = 0;
        public int ReadMultipleSize = -1; //-1 indicates not overriden.  Only overriden when reading object names

        private int _ReadQueueSize = 10;
        private bool _running;
        private Thread _LiveThread;
        private Thread _LoadThread;
        private int pendingReadMultipleResponses = 0; //used to keep ObjectCount from going to 0 when firing OnLiveRead event for read multiple

        private int maxObjectsWithoutReorder = 100;

        private BACnetLiveObjectCollection ObjectList = new BACnetLiveObjectCollection();

        public delegate void LiveStoppedEventHandler();

        public delegate void onIdleTimeUpdateEventHandler(int idleTime, int loadTime, int pendingCount);

        public event LiveStoppedEventHandler LiveStopped;

        public delegate void LiveCompletedEventHandler();

        public event LiveCompletedEventHandler LiveCompleted;

        public event onIdleTimeUpdateEventHandler onIdleTimeUpdate;

        public delegate void onLiveReadEventHandler(BACnetObject Itm, string Err);
        public event onLiveReadEventHandler onLiveRead;

        public delegate void onLiveReadMultipleEventHandler(List<BACnetObject> Itm, string Err);
        public event onLiveReadMultipleEventHandler onLiveReadMultiple;

        public bool Running
        {
            get
            {
                return _running;
            }
            private set
            {
                _running = value;
            }
        }

        public BACnetLive(List<BACnetObject> ObjectCollection)
        {
            ObjectList.Add(ObjectCollection);
        }

        public BACnetLive()
        {
        }



        #region "Live Objects - Add/Remove/Count"

        public int PendingCount
        {
            get { lock (ObjectList) return ObjectList.GetPendingCount(); }
        }

        public void AddItem(BACnetObject itm)
        {
            try
            {
                lock (ObjectList) ObjectList.Add(itm);


            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }
        }

        public void AddItems(List<BACnetObject> itms)
        {
            try
            {
                lock (ObjectList)
                {
                    ObjectList.Add(itms);
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }
        }

        public int ObjectCount()
        {
            return ObjectList.ObjectCount + pendingReadMultipleResponses;
        }

        #endregion "Live Objects - Add/Remove/Count"

        public void ReadMultiple(int deviceId, List<BACnetObject> objectList)
        {
            IList<BacnetReadAccessResult> multi_value_list = null;

            var oList = (from o in objectList group o by new { o.ObjectType, o.ObjectInstance } into g select new { ObjectType = g.Key.ObjectType, ObjectInstance = g.Key.ObjectInstance, ObjectList = g.ToList() }).ToList();

            BacnetReadAccessSpecification[] propToRead = new BacnetReadAccessSpecification[oList.Count];

            Dictionary<int, BacnetObjectId> lookup = new Dictionary<int, BacnetObjectId>();
            string error;

            IAm adr = BACnetDriver.Instance.GetDeviceIAm(deviceId);
            if (adr == null)
            {
                error = "Device Timeout";
            }
            else
            {
                foreach (var pList in oList)
                {
                    //Add a New Read Access Specification Object
                    BacnetObjectId objId = new BacnetObjectId(pList.ObjectType, Convert.ToUInt32(pList.ObjectInstance));
                    BacnetPropertyReference[] properties = new BacnetPropertyReference[pList.ObjectList.Count];


                    //foreach (var p in pList.ObjectList)
                    //{
                    for (int i = 0; i < pList.ObjectList.Count; i++)
                    {
                        var p = pList.ObjectList[i];
                        UInt32 pArray = (p.PriorityArray < 0) ? ASN1.BACNET_ARRAY_ALL : Convert.ToUInt32(p.PriorityArray);

                        properties[i] = new BacnetPropertyReference((uint)p.PropId, pArray);
                    }
                    propToRead[lookup.Count] = new BacnetReadAccessSpecification(objId, properties);
                    //}
                    lookup[lookup.Count] = objId;
                }

                error = BACnetDriver.Instance.Comm.ReadPropertyMultipleRequest(adr.Address, propToRead, out multi_value_list);
                
            }

            List<BACnetObject> valueObjects = new List<BACnetObject>();

            if (error == null)
            {
                for (int i = 0; i < multi_value_list.Count; i++)
                {
                    BacnetReadAccessResult results = multi_value_list[i];


                    foreach (var result in results.values)
                    {
                        string IDString = string.Format("{0},{1},{2},{3},{4}", (int)deviceId, (int)multi_value_list[i].objectIdentifier.type, multi_value_list[i].objectIdentifier.instance, (int)result.property.propertyIdentifier, (int)result.property.propertyArrayIndex);
                        if (result.value[0].Tag == BacnetApplicationTags.BACNET_APPLICATION_TAG_ERROR)
                        {
                            BACnetDriver.Log.Error(string.Format("{0} - {1}", result.value[0].ToString(), IDString));
                            if (result.value[0].ToString() == "ERROR_CLASS_SERVICES: ERROR_CODE_OTHER" || result.value[0].ToString().Contains("ABORT")) 
                                BACnetDriver.Instance.DecreaseReadPropertyCapabilities(deviceId);
                            
                            valueObjects.AddRange(UpdateMatchingObjects(IDString, result.value[0].ToString(), result.value[0].ToString(), false));
                        }
                        else
                        {
                            if (result.value.Count == 1)
                                valueObjects.AddRange(UpdateMatchingObjects(IDString, result.value[0].ToString(), "", true));
                            else
                                valueObjects.AddRange(UpdateMatchingObjects(IDString, string.Join(",", result.value), "", true)); 
                            
                        }
                    }

                }
            }
            else
            {
                foreach (var obj in oList)
                {
                    foreach (var p in obj.ObjectList)
                    {
                        valueObjects.AddRange(UpdateMatchingObjects(p.ToIdString(), error, error, true));
                    }

                }
            }

            if (onLiveReadMultiple != null && valueObjects.Count > 0)
            {
                onLiveReadMultiple(valueObjects, null);
            }
            else
            {
                if (onLiveRead != null && valueObjects.Count > 0)
                {
                    pendingReadMultipleResponses = valueObjects.Count;
                    foreach (var obj in valueObjects)
                    {
                        pendingReadMultipleResponses--;
                        onLiveRead(obj, null);
                    }
                }
            }

            ObjectList.ReadMultipleSuccess(deviceId);

            lock (ObjectList)
            {
                if (ObjectList.ObjectCount == 0 && PendingCount == 0 && LiveCompleted != null) LiveCompleted();
            }
        }

        private List<BACnetObject> UpdateMatchingObjects(string IDString, string value, string err, bool isReadMultiple)
        {
            if (!string.IsNullOrEmpty(err)) value = err.ToString();

            lock (ObjectList)
            {
                var updatedObjects = ObjectList.SetValue(IDString, value);

                if (!isReadMultiple && onLiveRead != null)
                {
                    foreach (var obj in updatedObjects)
                    {
                        onLiveRead(obj, err);
                    }
                }

                return updatedObjects;
            }
            
        }

        public void Start(List<BACnetObject> ObjectCollection)
        {
            ObjectList.Add(ObjectCollection);

            Start();
        }

        public void Start()
        {
            _ReadQueueSize = BACnetDriver.Instance.ConnectionInfo.ReadQueueSize;

            Running = true;

            _LiveThread = new Thread(new ThreadStart(this.RunLive)) { Name = "BACnet Live Thread", IsBackground = true };

            _LoadThread = new Thread(new ThreadStart(this.CheckLoad)) { Name = "BACnet Load Thread", IsBackground = true };

            _LiveThread.Start();
            _LoadThread.Start();

        }

        public void StopLive()
        {
            if (_running)
            {
                Running = false;
            }

            if (ObjectList != null) ObjectList.Clear();
        }

        public string GetStatusString()
        {
            return String.Format("{0}, Load: {1}", ObjectList.GetStatusString(), LoadTimePercent);
        }

        private void CheckList()
        {
            //Only take readquesize
            List<BACnetObject> objs = ObjectList.PendingRead.Where(o => BACnetDriver.Instance.ConnectionInfo.DisableReadMultiple || GetReadMultipleSize(o.DeviceInstance) == 0).Take(_ReadQueueSize).ToList();

            if (objs.Count > maxObjectsWithoutReorder)
                objs = objs.OrderBy(o => Guid.NewGuid()).ToList();

            while (objs.Count > 0)
            {
                var obj = objs[0]; //async causes object to be removed from list before request is sent
                var t = Task.Run(() => SendAsyncRequest(obj));
                Debug.Print("ReadProperty sent to " + obj.ToString());

                ObjectList.ObjectRequestSent(objs[0]);
                objs.RemoveAt(0);

                if (Running != true || PendingCount >= _ReadQueueSize)
                    break;
            }


        }

        private int GetReadMultipleSize(int devId)
        {
            if (ReadMultipleSize < 0)
                return BACnetDriver.Instance.GetReadMultipleSize(devId);
            else
                return Math.Min(ReadMultipleSize, BACnetDriver.Instance.GetReadMultipleSize(devId));
        }

        private void CheckListReadMultiple()
        {

            int count = 0;

            var devs = (from o in ObjectList.PendingRead
                        where !ObjectList.HasReadMultipleBeenRequested(o.DeviceInstance) && GetReadMultipleSize(o.DeviceInstance) > 0
                        orderby o.ObjectType, o.ObjectInstance
                        group o by o.DeviceInstance into g
                        select new { DeviceId = g.Key, Objects = g.Take(GetReadMultipleSize(g.Key)).ToList() })
                        .Where(l => l.Objects.Count > 0);

            foreach (var oList in devs)
            {
                count += oList.Objects.Count;
                ObjectList.ReadMultipleRequested(oList.DeviceId, oList.Objects);
                var t = Task.Run(() => ReadMultiple(oList.DeviceId, oList.Objects));
                //ReadMultiple(oList.DeviceId, oList.Objects);

                Thread.Sleep(50);
            }
        }

        private void CheckLoad()
        {
            int _idlePercent = 0;
            while (Running)
            {
                _idlePercent = 0;
                for (int i = 1; i <= 100; i++)
                {
                    Thread.Sleep(50);
                    if (ObjectCount() == 0)
                        _idlePercent += 1;
                }

                IdleTimePercent = _idlePercent;
                LoadTimePercent = 100 - IdleTimePercent;

                if (onIdleTimeUpdate != null)
                {
                    onIdleTimeUpdate(IdleTimePercent, LoadTimePercent, ObjectCount());
                }
            }
        }

        private void RunLive()
        {
            while (Running)
            {
                try
                {
                    int i = 0;
                    while (Running == true && PendingCount >= _ReadQueueSize && i < 1000)
                    {
                        Thread.Sleep(10);
                        i = i + 1;
                    }

                    lock (ObjectList)
                    {
                        if (ObjectList.PendingRead.Count > 0)
                        {
                            if (!BACnetDriver.Instance.ConnectionInfo.DisableReadMultiple) CheckListReadMultiple();

                            CheckList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.Print(ex.Message);
                    Console.WriteLine("bLive Error: " + ex.Message);
                    //EventLog.Log("Error running BACnet Live: " + ex.Message, EventLog.EntryType.Error);
                }

                Thread.Sleep(20);
            }

            if (LiveStopped != null)
            {
                LiveStopped();
            }
        }

        private void SendAsyncRequest(BACnetObject itm)
        {
            //Console.WriteLine("ASYNC SENT");
            string value = BACnetCommands.ReadProperty(itm.DeviceInstance, itm.ObjectType, itm.ObjectInstance, itm.PropId, itm.PriorityArray);

            UpdateMatchingObjects(itm.ToIdString(), value, "", false);

            lock (ObjectList)
            {
                if (ObjectList.ObjectCount == 0 && PendingCount == 0 && LiveCompleted != null) LiveCompleted();
            }
        }

    }
}
