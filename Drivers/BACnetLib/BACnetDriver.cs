﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using K2A.Connect.Models;
using NLog;
using SharpPcap.LibPcap;

namespace K2A.Drivers.BACnetLib
{
    public class BACnetDriver
    {
        public delegate void IamEventEventHandler(IAm e);

        public BACnetConnectionInfo ConnectionInfo { get; set; }

        public event IamEventEventHandler Iam;

        private Dictionary<int, int> ReadMultipleCapabilities = new Dictionary<int, int>();

        private Dictionary<int, IAm> Devices = new Dictionary<int, IAm>();
        private System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

        public BacnetClient Comm;
        public K2A.Connect.Models.GatewayDatabase GWDatabase = new Connect.Models.GatewayDatabase();

        public static Logger Log = LogManager.GetCurrentClassLogger();

        public int GetReadMultipleSize(int deviceId)
        {
            if (ConnectionInfo.DisableReadMultiple) return 0;
            int size = 0;
            if (ReadMultipleCapabilities.ContainsKey(deviceId))
                size = ReadMultipleCapabilities[deviceId];

            if (size == 1) size = 0;

            return Math.Min(Math.Max(size, 0), 100);
        }

        public void DecreaseReadPropertyCapabilities(int deviceId)
        {
            if (ReadMultipleCapabilities.ContainsKey(deviceId))
            {
                ReadMultipleCapabilities[deviceId] -= 2;
                Log.Info(String.Format("Device Id {0} read multiple decreased to {1}", deviceId, ReadMultipleCapabilities[deviceId]));
            }
        }

        public IAm GetDeviceIAm(int deviceId)
        {
            if (Devices.ContainsKey(deviceId)) return Devices[deviceId];

            SendWhoIs(deviceId, deviceId);
            //Comm.WhoIs(deviceId, deviceId);
            System.Diagnostics.Stopwatch pt = new System.Diagnostics.Stopwatch();
            pt.Start();
            while (!Devices.ContainsKey(deviceId) && pt.ElapsedMilliseconds < 3000)
            {
                Thread.Sleep(5);
            }

            if (Devices.ContainsKey(deviceId))
                return Devices[deviceId];
            else
                return null;

        }

        public void Start()
        {

            Log.Info("BACnet Driver Starting...");
            try
            {
                if (ConnectionInfo.isIP)
                {
                    var ips = BACnetConnectionInfo.GetAdapterList();
                    if (!ips.Contains(ConnectionInfo.LocalIP) && ips.Count > 0) ConnectionInfo.LocalIP = ips[0];
                    Log.Info("Listening on " + ConnectionInfo.LocalIP + ":" + ConnectionInfo.UDP_Port);

                    Comm = new BacnetClient(new BacnetIpUdpProtocolTransport(ConnectionInfo.UDP_Port, false, 1472, ConnectionInfo.LocalIP));
                }
                else
                {
                    Comm = new BacnetClient(new BacnetEthernetProtocolTransport(ConnectionInfo.EthernetInterface));
                }
            }
            catch(Exception ex) 
            {
                Log.Error("Unable to start BACnet Driver: " + ex.Message);
                return; 
            }

            try
            {
                //start BACnet
                Comm.ProposedWindowSize = 20;
                Comm.Retries = ConnectionInfo.APDU_Retries;
                Comm.Timeout = ConnectionInfo.APDU_Timeout;
                Comm.MaxSegments = BacnetClient.GetSegmentsCount(65);

                Comm.OnWhoIs += new BacnetClient.WhoIsHandler(OnWhoIs);
                //Comm.OnReadPropertyRequest += new BacnetClient.ReadPropertyRequestHandler(OnReadPropertyRequest);
                //Comm.OnReadPropertyMultipleRequest += new BacnetClient.ReadPropertyMultipleRequestHandler(OnReadPropertyMultipleRequest);

                Comm.OnIam += new BacnetClient.IamHandler(OnIam);
                //comm.OnCOVNotification += new BacnetClient.COVNotificationHandler(OnCOVNotification);
                //comm.OnEventNotify += new BacnetClient.EventNotificationCallbackHandler(OnEventNotify);
                Comm.Start();

                //start search
                if (Comm.Transport.Type == BacnetAddressTypes.IP || Comm.Transport.Type == BacnetAddressTypes.Ethernet
                    || Comm.Transport.Type == BacnetAddressTypes.IPV6)
                {
                    //System.Threading.ThreadPool.QueueUserWorkItem((o) =>
                    //{
                    //    for (int i = 0; i < Comm.Retries; i++)
                    //    {

                    //        SendWhoIs(-1, -1);
                    //        //Comm.WhoIs();
                    //        System.Threading.Thread.Sleep(Comm.Timeout);
                    //    }
                    //}, null);
                }

                if (!string.IsNullOrEmpty(ConnectionInfo.BBMDIP) && ConnectionInfo.isIP)
                {
                    RegisterForeignDevice();
                }
                //special MSTP auto discovery
                //if (comm.Transport is BacnetMstpProtocolTransport)
                //{
                //    ((BacnetMstpProtocolTransport)comm.Transport).FrameRecieved += new BacnetMstpProtocolTransport.FrameRecievedHandler(MSTP_FrameRecieved);
                //}
            }
            catch (Exception ex)
            {
                Log.Error("Unable to start BACnet Driver: " + ex.Message);
                //m_devices.Remove(Comm);
                //MessageBox.Show(this, "Couldn't start Bacnet communication: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Log Error
            }

            try
            {
                if (!String.IsNullOrEmpty(ConnectionInfo.Gateway))
                {
                    Log.Info("Building Gateway");
                    GWDatabase = K2A.Connect.Models.GatewayDatabase.FromString(ConnectionInfo.Gateway);
                    Log.Info("Gateway Built");
                }
            }
            catch
            {
                //EventLog.Log("Unable to load gateway database", EventLog.EntryType.Error);
                Log.Error("Unable to load gateway database");
            }

            Log.Info("BACnet Driver Started.");
        }

        public void Stop()
        {
            if (Comm != null) Comm.Stop();
        }

        public void SendWhoIs(int Low_Limit, int High_Limit)
        {
            //BACnetDriver.Instance.Comm.WhoIs(Low_Limit, High_Limit);
            if (!string.IsNullOrEmpty(ConnectionInfo.BBMDIP))
            {
                RegisterForeignDevice();
            }

            if (ConnectionInfo.isIP && !string.IsNullOrEmpty(ConnectionInfo.BBMDIP)) Comm.RemoteWhoIs(ConnectionInfo.BBMDIP, ConnectionInfo.BBMD_UDP_Port, Low_Limit, High_Limit);
            Comm.WhoIs(Low_Limit, High_Limit);
        }

        public void SendWhoHas(int Low_Limit, int High_Limit, BacnetObjectId objId, string objName)
        {
            //BACnetDriver.Instance.Comm.WhoIs(Low_Limit, High_Limit);
            if (!string.IsNullOrEmpty(ConnectionInfo.BBMDIP))
            {
                RegisterForeignDevice();
            }

            if (ConnectionInfo.isIP && !string.IsNullOrEmpty(ConnectionInfo.BBMDIP)) Comm.RemoteWhoHas(objId, objName, ConnectionInfo.BBMDIP, ConnectionInfo.BBMD_UDP_Port, Low_Limit, High_Limit);
            Comm.WhoHas(objId, objName, Low_Limit, High_Limit);
        }

        public int LoadConnection(K2A.Connect.Models.BACnetConnectionInfo Connection)
        {
            Log.Info("Loading BACnet connection settings");
            ConnectionInfo = Connection;
            string GatewayFile = Connection.Gateway;
            if (GatewayFile == null) GatewayFile = "";
            Start();
            return 0;
        }

        private void RegisterForeignDevice()
        {
            if (!ConnectionInfo.isIP) return;
            if (sw.IsRunning && sw.ElapsedMilliseconds < 25000) return;
            
            try
            {
                IPAddress[] IPs = Dns.GetHostAddresses(ConnectionInfo.BBMDIP);

                IPAddress IP;

                if (Comm.Transport is BacnetIpUdpProtocolTransport)
                    IP = IPs.First<IPAddress>(o => o.AddressFamily == AddressFamily.InterNetwork);
                else
                    IP = IPs.First<IPAddress>(o => o.AddressFamily == AddressFamily.InterNetworkV6);

                Comm.RegisterAsForeignDevice(IP.ToString(), 30, ConnectionInfo.BBMD_UDP_Port);
                Thread.Sleep(50);
                Comm.RemoteWhoIs(IP.ToString(), ConnectionInfo.BBMD_UDP_Port);
                //SendWhois.Enabled = true;
            }
            catch { }
            sw.Restart();
        }

        // Only the see Yabe on the net
        void OnWhoIs(BacnetClient sender, BacnetAddress adr, int low_limit, int high_limit)
        {
            return; //Do nothing since no objects are implemented
            uint myId = (uint)ConnectionInfo.DeviceID;

            if (low_limit != -1 && myId < low_limit) return;
            else if (high_limit != -1 && myId > high_limit) return;
            sender.Iam(myId, new BacnetSegmentations());
        }

        void OnIam(BacnetClient sender, BacnetAddress adr, uint device_id, uint max_apdu, BacnetSegmentations segmentation, ushort vendor_id)
        {
            BACnetDriver.Log.Trace("I AM received from device " + device_id);
            IAm nIAm = new IAm()
            {
                DeviceId = (int)device_id,
                VendorId = (int)vendor_id,
                NetworkNumber = (int)adr.net,
                Address = adr,
                MaxAPDULengthAccepted = (int)max_apdu,
                SegmentationSupported = segmentation
            };

            lock (Devices)
            {
                Devices[(int)device_id] = nIAm;
                if (!ReadMultipleCapabilities.ContainsKey(nIAm.DeviceId)) ReadMultipleCapabilities[nIAm.DeviceId] = (nIAm.SegmentationSupported == BacnetSegmentations.SEGMENTATION_BOTH) ? nIAm.MaxAPDULengthAccepted / 24 : nIAm.MaxAPDULengthAccepted / 48;
                //if ((int)device_id == 1) BACnetCommands.ReadProperty(1, BacnetObjectTypes.OBJECT_DEVICE, 3, BacnetPropertyIds.PROP_OBJECT_NAME, -1);
                //reqId++;
                //IList<BacnetValue> value_list = null;
                //if (!Comm.ReadPropertyRequest(adr, new BacnetObjectId(BacnetObjectTypes.OBJECT_DEVICE, device_id), BacnetPropertyIds.PROP_OBJECT_LIST, out value_list, Convert.ToByte(reqId)))
                //{
                //    //BACnetDriver.Log.Info("Didn't get response from 'Object List'");
                //    value_list = null;
                //}
            }

            if (Iam != null)
            {
                Iam(nIAm);
            }

        }

        public static BACnetDriver Instance
        {
            // An instance of BACnetDriver wont be created until the very first
            // call to the sealed class.
            get { return BACnetDriverCreator.CreatorInstance; }
        }
        /// Sealed class to avoid any heritage from this helper class
        private sealed class BACnetDriverCreator
        {
            // Retrieve a single instance of a BACnetDriver
            private static readonly BACnetDriver _instance = new BACnetDriver();

            /// Return an instance of the class
            public static BACnetDriver CreatorInstance
            {
                get { return _instance; }
            }
        }

        //static public string GetTDB(string Pass)
        //{
        //    CompressedString cmp = new CompressedString(K2Acommon.Properties.Resources.tdb, CompressedString.InputDataTypeClass.Compressed, "m2m" + Pass + "m2m", "m2m08", "tdb");
        //    return cmp.UnCompressed;
        //}

        public static List<string> GetEthernetInterfaces()
        {
            List<string> interfaces = new List<string>();

            var devices = LibPcapLiveDeviceList.Instance.Where(dev => dev.Interface != null);

            foreach (var device in devices)
            {
                if (device.Opened == false) // Don't re-open and lists an already open interface
                {
                    device.Open();
                    if (device.LinkType == PacketDotNet.LinkLayers.Ethernet
                        && device.Addresses.Count > 0 && device.Interface.MacAddress != null)
                        interfaces.Add(device.Interface.FriendlyName);
                    device.Close();
                }
            }

            return interfaces;
        }
    }
}
