﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K2A.Drivers.BACnetLib
{
    public class IAm
    {
        public int DeviceId { get; set; }

        public string DeviceName { get; set; }

        public int VendorId { get; set; }

        public BacnetAddress Address { get; set; }

        public string VirtualPath { get; set; }

        public string MAC
        {
            get
            {
                if (Address != null && Address.adr != null && Address.adr.Length > 0)
                {
                    return Address.ToString();
                }
                return VirtualPath ?? "";
            }
        }

        public string ModelName { get; set; }

        public int NetworkNumber { get; set; }

        public BacnetSegmentations SegmentationSupported { get; set; }

        public int MaxAPDULengthAccepted { get; set; }

        public IAm()
        {
        }

        public BACnetDevice BACnetDevice
        {
            get { return new BACnetDevice(DeviceName, DeviceId, -1, true); }
        }

        public string GetDeviceKey()
        {
            return String.Format("{0},{1},{2},{3}", DeviceId, NetworkNumber, VendorId, MAC);
        }
    }
}
