﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
using Newtonsoft.Json;
using K2A.Drivers.BACnetLib;
using K2A.Core;

namespace K2A.Connect.Models
{
    public class GatewayDatabase
    {
        public List<GatewayDevice> Devices = new List<GatewayDevice>();
        //public List<GatewayObject> Objects = new List<GatewayObject>();
        public DateTime LastSaved = new DateTime(2000, 1, 1);
        public int ProfileCount;
        public string GatewayType { get; set; }

        public int GetObjectCount()
        {
            return (from d in Devices select d.Objects).Sum(d => d.Count());
        }
        public static GatewayDatabase FromString(string gwDatabase)
        {
            if (string.IsNullOrEmpty(gwDatabase)) return new GatewayDatabase();
            try
            {
                CompressedString cmp = new CompressedString(gwDatabase, CompressedString.InputDataTypeClass.Compressed, "gateway", "bb7gw", "m");
                var gwdb = JsonConvert.DeserializeObject<GatewayDatabase>(cmp.UnCompressed);
                BuildProfiles(gwdb);
                return gwdb;
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error Reading Gateway Database: " + ex.Message);
                //EventLog.Log("Error Reading Gateway Database: " + ex.Message, EventLog.EntryType.Error);
                return null;
            }
        }

        private static int BuildProfiles(GatewayDatabase db)
        {
            Dictionary<string, int> profileIds = new Dictionary<string, int>();

            foreach (var d in db.Devices)
            {
                string hash = d.GetHashString();
                if (!profileIds.ContainsKey(hash))
                {
                    profileIds.Add(hash, profileIds.Count + 1);
                }

                d.Profile = String.Format("gw_{0}", profileIds[hash]);
            }
            db.ProfileCount = profileIds.Count;
            return profileIds.Count;
        }

        public static GatewayDatabase FromFile(string FileName)
        {
            try
            {
                if (System.IO.File.Exists(FileName))
                {
                    return (FromString((FileName).FileToString()));
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error Reading Gateway Database: " + ex.Message);
                //EventLog.Log("Error Reading Gateway Database: " + ex.Message, EventLog.EntryType.Error);
                return null;
            }
        }

        public bool SaveToFile(string FileName)
        {
            foreach (var dev in Devices)
            {
                dev.DeviceName = dev.DeviceName.Replace("'", "");
                dev.Description = dev.DeviceName.Replace("'", "");
            }


            LastSaved = DateTime.UtcNow;
            try
            {
                string json = JsonConvert.SerializeObject(this);
                CompressedString cmp = new CompressedString(json, CompressedString.InputDataTypeClass.UnCompressed, "gateway", "bb7gw", "m");
                (cmp.Compressed).ToFile(FileName);
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error Saving Gateway Database: " + ex.Message);
                //EventLog.Log("Error Saving Gateway Database: " + ex.Message, EventLog.EntryType.Error);
                return false;
            }
            return true;
        }

        public string SaveToString()
        {
            LastSaved = DateTime.UtcNow;
            try
            {
                CompressedString cmp = new CompressedString(JsonConvert.SerializeObject(this), CompressedString.InputDataTypeClass.UnCompressed, "gateway", "bb7gw", "m");
                return cmp.Compressed;
               
            }
            catch
            {
                return "";
            }
        }
    }

    public class GatewayDevice
    {
        public string DeviceName { get; set; }

        public string Description { get; set; }

        public string ModelName { get; set; }

        public int VendorID { get; set; }

        public string VendorName { get; set; }

        public string Profile { get; set; }

        public List<GatewayObject> Objects { get; set; }

        public string DeviceReference { get; set; } //used to store JCI path

        //public List<string> GetObjectNames()
        //{
        //    return (from o in Objects select o.ObjectName).ToList();
        //}
        public string GetHashString()
        {
            return string.Join(".", (from o in Objects orderby o.ObjectName select o.ObjectName));
        }
    }

    public class GatewayObject
    {
        public GatewayObject(string ObjName, string description, int Device, BacnetObjectTypes ObjType, int ObjInstance, BacnetPropertyIds PropID, int pArray, int Interval, object UserData, string msInfoString)
        {
            ObjectName = ObjName;
            Description = description;
            DeviceInstance = Device;
            ObjectType = ObjType;
            ObjectInstance = ObjInstance;
            this.PropID = PropID;
            PriorityArray = pArray;
            this.UserData = UserData;
            Value = "";
            this.Interval = Interval;
            TimeStamp = DateTime.UtcNow.AddYears(-5);
        }

        #region "Properties"

        public string ObjectName { get; set; }

        public string Description { get; set; }

        public DateTime TimeStamp { get; set; }

        public int DeviceInstance { get; set; }

        public BacnetObjectTypes ObjectType { get; set; }

        public int ObjectInstance { get; set; }

        public BacnetPropertyIds PropID { get; set; }

        public int PriorityArray { get; set; }

        public object UserData { get; set; }

        public string Value { get; set; }

        public int Interval { get; set; }

        
        #endregion "Properties"
    }
}
