﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Net.Sockets;

namespace K2A.Drivers.BACnetLib
{
    /// <summary>
    /// This can be both client and server
    /// </summary>
    public class BacnetClient : IDisposable
    {
        private K2A.Drivers.BACnetLib.IBacnetTransport m_client;
        private ushort m_vendor_id = 445;
        private int m_timeout;
        private int m_transmit_timeout = 30000;     //long transmit timeout due to MSTP
        private int m_retries;                      // understand here the number of tentatives
        private byte m_invoke_id = 0;
        private K2A.Drivers.BACnetLib.BacnetMaxSegments m_max_segments = K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0;
        private byte m_last_sequence_number = 0;
        private byte m_proposed_window_size = 10;
        private bool m_default_segmentation_handling;
        private Dictionary<int, List<byte[]>> m_segments = new Dictionary<int, List<byte[]>>();       //only used when 'DefaultSegmentationHandling' = true
        private LastSegmentACK m_last_segment_ack = new LastSegmentACK();
        private bool m_force_window_size = false;
        private uint m_writepriority = 0;

        public static byte DEFAULT_HOP_COUNT = 0xFF;

        public K2A.Drivers.BACnetLib.IBacnetTransport Transport { get { return m_client; } }
        public int Timeout { get { return m_timeout; } set { m_timeout = value; } }
        public int TransmitTimeout { get { return m_transmit_timeout; } set { m_transmit_timeout = value; } }
        public int Retries { get { return m_retries; } set { m_retries = Math.Max(1, value); } } // used as the number of tentatives
        public uint WritePriority { get { return m_writepriority; } set { if (value < 17) m_writepriority = value; } }
        public K2A.Drivers.BACnetLib.BacnetMaxSegments MaxSegments { get { return m_max_segments; } set { m_max_segments = value; } }
        public byte ProposedWindowSize { get { return m_proposed_window_size; } set { m_proposed_window_size = value; } }
        public bool ForceWindowSize { get { return m_force_window_size; } set { m_force_window_size = value; } }
        public bool DefaultSegmentationHandling { get { return m_default_segmentation_handling; } set { m_default_segmentation_handling = value; } }

        // These members allows to access undecoded buffer by the application
        // layer, when the basic undecoding process is not really able to do the job
        // in particular with application_specific_encoding values
        public byte[] raw_buffer;
        public int raw_offset, raw_length;

        private class LastSegmentACK
        {
            public byte invoke_id;
            public byte sequence_number;
            public byte window_size;
            public K2A.Drivers.BACnetLib.BacnetAddress adr;
            private System.Threading.ManualResetEvent m_wait = new System.Threading.ManualResetEvent(false);
            private object m_lockObject = new object();
            public void Set(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, byte sequence_number, byte window_size)
            {
                lock (m_lockObject)
                {
                    this.adr = adr;
                    this.invoke_id = invoke_id;
                    this.sequence_number = sequence_number;
                    this.window_size = window_size;
                    m_wait.Set();
                }
            }
            public bool Wait(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, int timeout)
            {
                System.Threading.Monitor.Enter(m_lockObject);
                while (!adr.Equals(this.adr) || this.invoke_id != invoke_id)
                {
                    m_wait.Reset();
                    System.Threading.Monitor.Exit(m_lockObject);
                    if (!m_wait.WaitOne(timeout)) return false;
                    System.Threading.Monitor.Enter(m_lockObject);
                }
                System.Threading.Monitor.Exit(m_lockObject);
                this.adr = null;
                return true;
            }
        }

        public BacnetClient(int port = 0xBAC0, int timeout = 1000, int retries = 3) :
            this(new K2A.Drivers.BACnetLib.BacnetIpUdpProtocolTransport(port), timeout, retries)
        {
        }

        public BacnetClient(K2A.Drivers.BACnetLib.IBacnetTransport transport, int timeout = 1000, int retries = 3)
        {
            m_client = transport;
            m_timeout = timeout;
            Retries = retries;
            DefaultSegmentationHandling = true;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is K2A.Drivers.BACnetLib.BacnetClient)) return false;
            K2A.Drivers.BACnetLib.BacnetClient a = (K2A.Drivers.BACnetLib.BacnetClient)obj;
            return m_client.Equals(a.m_client);
        }

        public override int GetHashCode()
        {
            return m_client.GetHashCode();
        }

        public override string ToString()
        {
            return m_client.ToString();
        }

        public K2A.Drivers.BACnetLib.EncodeBuffer GetEncodeBuffer(int start_offset)
        {
            return new K2A.Drivers.BACnetLib.EncodeBuffer(new byte[m_client.MaxBufferLength], start_offset);
        }

        public void Start()
        {
            m_client.Start();
            m_client.MessageRecieved += new MessageRecievedHandler(OnRecieve);
            BACnetDriver.Log.Info("Started communication");
        }

        public void Stop()
        {
            m_client.Stop();
            m_client.Dispose();
        }

        public delegate void ConfirmedServiceRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments, K2A.Drivers.BACnetLib.BacnetMaxAdpu max_adpu, byte invoke_id, byte[] buffer, int offset, int length);
        public event ConfirmedServiceRequestHandler OnConfirmedServiceRequest;

        public delegate void ReadPropertyRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyReference property, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event ReadPropertyRequestHandler OnReadPropertyRequest;
        public delegate void ReadPropertyMultipleRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, IList<K2A.Drivers.BACnetLib.BacnetReadAccessSpecification> properties, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event ReadPropertyMultipleRequestHandler OnReadPropertyMultipleRequest;
        public delegate void WritePropertyRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyValue value, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event WritePropertyRequestHandler OnWritePropertyRequest;
        public delegate void WritePropertyMultipleRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, K2A.Drivers.BACnetLib.BacnetObjectId object_id, ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> values, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event WritePropertyMultipleRequestHandler OnWritePropertyMultipleRequest;
        public delegate void AtomicWriteFileRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, bool is_stream, K2A.Drivers.BACnetLib.BacnetObjectId object_id, int position, uint block_count, byte[][] blocks, int[] counts, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event AtomicWriteFileRequestHandler OnAtomicWriteFileRequest;
        public delegate void AtomicReadFileRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, bool is_stream, K2A.Drivers.BACnetLib.BacnetObjectId object_id, int position, uint count, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event AtomicReadFileRequestHandler OnAtomicReadFileRequest;
        public delegate void SubscribeCOVRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, uint subscriberProcessIdentifier, K2A.Drivers.BACnetLib.BacnetObjectId monitoredObjectIdentifier, bool cancellationRequest, bool issueConfirmedNotifications, uint lifetime, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event SubscribeCOVRequestHandler OnSubscribeCOV;
        public delegate void EventNotificationCallbackHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetEventNotificationData EventData);
        public event EventNotificationCallbackHandler OnEventNotify;
        public delegate void SubscribeCOVPropertyRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, uint subscriberProcessIdentifier, K2A.Drivers.BACnetLib.BacnetObjectId monitoredObjectIdentifier, K2A.Drivers.BACnetLib.BacnetPropertyReference monitoredProperty, bool cancellationRequest, bool issueConfirmedNotifications, uint lifetime, float covIncrement, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event SubscribeCOVPropertyRequestHandler OnSubscribeCOVProperty;
        public delegate void DeviceCommunicationControlRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, uint time_duration, uint enable_disable, string password, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event DeviceCommunicationControlRequestHandler OnDeviceCommunicationControl;
        public delegate void ReinitializedRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, K2A.Drivers.BACnetLib.BacnetReinitializedStates state, string password, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event ReinitializedRequestHandler OnReinitializedDevice;
        public delegate void ReadRangeHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, K2A.Drivers.BACnetLib.BacnetObjectId objectId, K2A.Drivers.BACnetLib.BacnetPropertyReference property, K2A.Drivers.BACnetLib.BacnetReadRangeRequestTypes requestType, uint position, DateTime time, int count, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event ReadRangeHandler OnReadRange;
        public delegate void CreateObjectRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, K2A.Drivers.BACnetLib.BacnetObjectId object_id, ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> values, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event CreateObjectRequestHandler OnCreateObjectRequest;
        public delegate void DeleteObjectRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event DeleteObjectRequestHandler OnDeleteObjectRequest;

        protected void ProcessConfirmedServiceRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments, K2A.Drivers.BACnetLib.BacnetMaxAdpu max_adpu, byte invoke_id, byte[] buffer, int offset, int length)
        {
            try
            {
                BACnetDriver.Log.Trace("ConfirmedServiceRequest");

                raw_buffer = buffer;
                raw_length = length;
                raw_offset = offset;

                if (OnConfirmedServiceRequest != null)
                    OnConfirmedServiceRequest(this, adr, type, service, max_segments, max_adpu, invoke_id, buffer, offset, length);

                //don't send segmented messages, if client don't want it
                if ((type & K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED) == 0)
                    max_segments = K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0;

                if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_PROPERTY && OnReadPropertyRequest != null)
                {
                    K2A.Drivers.BACnetLib.BacnetObjectId object_id;
                    K2A.Drivers.BACnetLib.BacnetPropertyReference property;
                    int Ths_Reject_Reason;

                    if ((Ths_Reject_Reason = K2A.Drivers.BACnetLib.Services.DecodeReadProperty(buffer, offset, length, out object_id, out property)) >= 0)
                    {
                        OnReadPropertyRequest(this, adr, invoke_id, object_id, property, max_segments);
                    }
                    else
                    {
                        if (Ths_Reject_Reason == -1)
                        {
                            SendConfirmedServiceReject(adr, invoke_id, K2A.Drivers.BACnetLib.BacnetRejectReasons.REJECT_REASON_MISSING_REQUIRED_PARAMETER);
                        }
                        else if (Ths_Reject_Reason == -2)
                        {
                            SendConfirmedServiceReject(adr, invoke_id, K2A.Drivers.BACnetLib.BacnetRejectReasons.REJECT_REASON_INVALID_TAG);
                        }
                        else if (Ths_Reject_Reason == -3)
                        {
                            SendConfirmedServiceReject(adr, invoke_id, K2A.Drivers.BACnetLib.BacnetRejectReasons.REJECT_REASON_TOO_MANY_ARGUMENTS);
                        }
                        BACnetDriver.Log.Error("Couldn't decode DecodeReadProperty");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_WRITE_PROPERTY && OnWritePropertyRequest != null)
                {
                    K2A.Drivers.BACnetLib.BacnetObjectId object_id;
                    K2A.Drivers.BACnetLib.BacnetPropertyValue value;
                    if (K2A.Drivers.BACnetLib.Services.DecodeWriteProperty(buffer, offset, length, out object_id, out value) >= 0)
                        OnWritePropertyRequest(this, adr, invoke_id, object_id, value, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        //SendConfirmedServiceReject(adr, invoke_id, BacnetRejectReasons.REJECT_REASON_OTHER); 
                        BACnetDriver.Log.Error("Couldn't decode DecodeWriteProperty");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_PROP_MULTIPLE && OnReadPropertyMultipleRequest != null)
                {
                    IList<K2A.Drivers.BACnetLib.BacnetReadAccessSpecification> properties;
                    if (K2A.Drivers.BACnetLib.Services.DecodeReadPropertyMultiple(buffer, offset, length, out properties) >= 0)
                        OnReadPropertyMultipleRequest(this, adr, invoke_id, properties, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode DecodeReadPropertyMultiple");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_WRITE_PROP_MULTIPLE && OnWritePropertyMultipleRequest != null)
                {
                    K2A.Drivers.BACnetLib.BacnetObjectId object_id;
                    ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> values;
                    if (K2A.Drivers.BACnetLib.Services.DecodeWritePropertyMultiple(buffer, offset, length, out object_id, out values) >= 0)
                        OnWritePropertyMultipleRequest(this, adr, invoke_id, object_id, values, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode DecodeWritePropertyMultiple");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_COV_NOTIFICATION && OnCOVNotification != null)
                {
                    uint subscriberProcessIdentifier;
                    K2A.Drivers.BACnetLib.BacnetObjectId initiatingDeviceIdentifier;
                    K2A.Drivers.BACnetLib.BacnetObjectId monitoredObjectIdentifier;
                    uint timeRemaining;
                    ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> values;
                    if (K2A.Drivers.BACnetLib.Services.DecodeCOVNotifyUnconfirmed(buffer, offset, length, out subscriberProcessIdentifier, out initiatingDeviceIdentifier, out monitoredObjectIdentifier, out timeRemaining, out values) >= 0)
                        OnCOVNotification(this, adr, invoke_id, subscriberProcessIdentifier, initiatingDeviceIdentifier, monitoredObjectIdentifier, timeRemaining, true, values, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode COVNotify");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_ATOMIC_WRITE_FILE && OnAtomicWriteFileRequest != null)
                {
                    bool is_stream;
                    K2A.Drivers.BACnetLib.BacnetObjectId object_id;
                    int position;
                    uint block_count;
                    byte[][] blocks;
                    int[] counts;
                    if (K2A.Drivers.BACnetLib.Services.DecodeAtomicWriteFile(buffer, offset, length, out is_stream, out object_id, out position, out block_count, out blocks, out counts) >= 0)
                        OnAtomicWriteFileRequest(this, adr, invoke_id, is_stream, object_id, position, block_count, blocks, counts, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode AtomicWriteFile");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_ATOMIC_READ_FILE && OnAtomicReadFileRequest != null)
                {
                    bool is_stream;
                    K2A.Drivers.BACnetLib.BacnetObjectId object_id;
                    int position;
                    uint count;
                    if (K2A.Drivers.BACnetLib.Services.DecodeAtomicReadFile(buffer, offset, length, out is_stream, out object_id, out position, out count) >= 0)
                        OnAtomicReadFileRequest(this, adr, invoke_id, is_stream, object_id, position, count, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode AtomicReadFile");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_SUBSCRIBE_COV && OnSubscribeCOV != null)
                {
                    uint subscriberProcessIdentifier;
                    K2A.Drivers.BACnetLib.BacnetObjectId monitoredObjectIdentifier;
                    bool cancellationRequest;
                    bool issueConfirmedNotifications;
                    uint lifetime;
                    if (K2A.Drivers.BACnetLib.Services.DecodeSubscribeCOV(buffer, offset, length, out subscriberProcessIdentifier, out monitoredObjectIdentifier, out cancellationRequest, out issueConfirmedNotifications, out lifetime) >= 0)
                        OnSubscribeCOV(this, adr, invoke_id, subscriberProcessIdentifier, monitoredObjectIdentifier, cancellationRequest, issueConfirmedNotifications, lifetime, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode SubscribeCOV");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_SUBSCRIBE_COV_PROPERTY && OnSubscribeCOVProperty != null)
                {
                    uint subscriberProcessIdentifier;
                    K2A.Drivers.BACnetLib.BacnetObjectId monitoredObjectIdentifier;
                    K2A.Drivers.BACnetLib.BacnetPropertyReference monitoredProperty;
                    bool cancellationRequest;
                    bool issueConfirmedNotifications;
                    uint lifetime;
                    float covIncrement;
                    if (K2A.Drivers.BACnetLib.Services.DecodeSubscribeProperty(buffer, offset, length, out subscriberProcessIdentifier, out monitoredObjectIdentifier, out monitoredProperty, out cancellationRequest, out issueConfirmedNotifications, out lifetime, out covIncrement) >= 0)
                        OnSubscribeCOVProperty(this, adr, invoke_id, subscriberProcessIdentifier, monitoredObjectIdentifier, monitoredProperty, cancellationRequest, issueConfirmedNotifications, lifetime, covIncrement, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode SubscribeCOVProperty");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_DEVICE_COMMUNICATION_CONTROL && OnDeviceCommunicationControl != null)
                {
                    uint timeDuration;
                    uint enable_disable;
                    string password;
                    if (K2A.Drivers.BACnetLib.Services.DecodeDeviceCommunicationControl(buffer, offset, length, out timeDuration, out enable_disable, out password) >= 0)
                        OnDeviceCommunicationControl(this, adr, invoke_id, timeDuration, enable_disable, password, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode DeviceCommunicationControl");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_REINITIALIZE_DEVICE && OnReinitializedDevice != null)
                {
                    K2A.Drivers.BACnetLib.BacnetReinitializedStates state;
                    string password;
                    if (K2A.Drivers.BACnetLib.Services.DecodeReinitializeDevice(buffer, offset, length, out state, out password) >= 0)
                        OnReinitializedDevice(this, adr, invoke_id, state, password, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode ReinitializeDevice");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_EVENT_NOTIFICATION && OnEventNotify != null) // F. Chaxel
                {
                    K2A.Drivers.BACnetLib.BacnetEventNotificationData EventData;
                    if (K2A.Drivers.BACnetLib.Services.DecodeEventNotifyData(buffer, offset, length, out EventData) >= 0)
                    {
                        OnEventNotify(this, adr, EventData);
                    }
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode Event/Alarm Notification");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_RANGE && OnReadRange != null)
                {
                    K2A.Drivers.BACnetLib.BacnetObjectId objectId;
                    K2A.Drivers.BACnetLib.BacnetPropertyReference property;
                    K2A.Drivers.BACnetLib.BacnetReadRangeRequestTypes requestType;
                    uint position;
                    DateTime time;
                    int count;
                    if (K2A.Drivers.BACnetLib.Services.DecodeReadRange(buffer, offset, length, out objectId, out property, out requestType, out position, out time, out count) >= 0)
                    {
                        OnReadRange(this, adr, invoke_id, objectId, property, requestType, position, time, count, max_segments);
                    }
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode ReadRange");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_CREATE_OBJECT && OnCreateObjectRequest != null)
                {
                    K2A.Drivers.BACnetLib.BacnetObjectId object_id;
                    ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> values;
                    if (K2A.Drivers.BACnetLib.Services.DecodeCreateObject(buffer, offset, length, out object_id, out values) >= 0)
                        OnCreateObjectRequest(this, adr, invoke_id, object_id, values, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode CreateObject");
                    }
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_DELETE_OBJECT && OnDeleteObjectRequest != null)
                {

                    K2A.Drivers.BACnetLib.BacnetObjectId object_id;
                    if (K2A.Drivers.BACnetLib.Services.DecodeDeleteObject(buffer, offset, length, out object_id) >= 0)
                        OnDeleteObjectRequest(this, adr, invoke_id, object_id, max_segments);
                    else
                    {
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                        BACnetDriver.Log.Error("Couldn't decode DecodeDeleteObject");
                    }
                }

                else
                {
                    ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_REJECT_UNRECOGNIZED_SERVICE);
                    BACnetDriver.Log.Trace("Confirmed service not handled: " + service.ToString());
                }
            }
            catch (Exception ex)
            {
                ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_DEVICE, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_OTHER);
                BACnetDriver.Log.Error("Error in ProcessConfirmedServiceRequest: " + ex.Message);
            }
        }

        public delegate void UnconfirmedServiceRequestHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices service, byte[] buffer, int offset, int length);
        public event UnconfirmedServiceRequestHandler OnUnconfirmedServiceRequest;
        public delegate void WhoHasHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, int low_limit, int high_limit, K2A.Drivers.BACnetLib.BacnetObjectId ObjId, string ObjName);
        public event WhoHasHandler OnWhoHas;
        public delegate void IamHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, UInt32 device_id, UInt32 max_apdu, K2A.Drivers.BACnetLib.BacnetSegmentations segmentation, UInt16 vendor_id);
        public event IamHandler OnIam;
        public delegate void IHaveHandler(K2A.Drivers.BACnetLib.BacnetClient sender,UInt32 device_id, BacnetObjectId objectId, string objectName);
        public event IHaveHandler OnIHave;

        public delegate void WhoIsHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, int low_limit, int high_limit);
        public event WhoIsHandler OnWhoIs;
        public delegate void TimeSynchronizeHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, DateTime dateTime, bool utc);
        public event TimeSynchronizeHandler OnTimeSynchronize;

        //used by both 'confirmed' and 'unconfirmed' notify
        public delegate void COVNotificationHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, uint subscriberProcessIdentifier, K2A.Drivers.BACnetLib.BacnetObjectId initiatingDeviceIdentifier, K2A.Drivers.BACnetLib.BacnetObjectId monitoredObjectIdentifier, uint timeRemaining, bool need_confirm, ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> values, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments);
        public event COVNotificationHandler OnCOVNotification;

        protected void ProcessUnconfirmedServiceRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices service, byte[] buffer, int offset, int length)
        {
            try
            {
                //BACnetDriver.Log.Trace("UnconfirmedServiceRequest");
                if (OnUnconfirmedServiceRequest != null) OnUnconfirmedServiceRequest(this, adr, type, service, buffer, offset, length);
                if (service == K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_I_AM && OnIam != null)
                {
                    uint device_id;
                    uint max_adpu;
                    K2A.Drivers.BACnetLib.BacnetSegmentations segmentation;
                    ushort vendor_id;
                    if (K2A.Drivers.BACnetLib.Services.DecodeIamBroadcast(buffer, offset, out device_id, out max_adpu, out segmentation, out vendor_id) >= 0)
                        OnIam(this, adr, device_id, max_adpu, segmentation, vendor_id);
                    else
                        BACnetDriver.Log.Error("Couldn't decode IamBroadcast");
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_I_HAVE)
                {
                    uint device_id;
                    K2A.Drivers.BACnetLib.BacnetObjectId ObjId;
                    string objName;
                    if (K2A.Drivers.BACnetLib.Services.DecodeIHaveBroadcast(buffer, offset, out device_id, out ObjId, out objName) >= 0)
                        OnIHave?.Invoke(this, device_id, ObjId, objName);
                    else
                        BACnetDriver.Log.Error("Couldn't decode IHaveBroadcast");
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_WHO_IS && OnWhoIs != null)
                {
                    int low_limit;
                    int high_limit;
                    if (K2A.Drivers.BACnetLib.Services.DecodeWhoIsBroadcast(buffer, offset, length, out low_limit, out high_limit) >= 0)
                        OnWhoIs(this, adr, low_limit, high_limit);
                    else
                        BACnetDriver.Log.Error("Couldn't decode WhoIsBroadcast");
                }
                // added by thamersalek
                else if (service == K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_WHO_HAS)
                {
                    if (OnWhoHas != null)
                    {
                        int low_limit;
                        int high_limit;
                        K2A.Drivers.BACnetLib.BacnetObjectId ObjId;
                        string ObjName;

                        if (K2A.Drivers.BACnetLib.Services.DecodeWhoHasBroadcast(buffer, offset, length, out low_limit, out high_limit, out ObjId, out ObjName) >= 0)
                            OnWhoHas(this, adr, low_limit, high_limit, ObjId, ObjName);
                        else
                            BACnetDriver.Log.Error("Couldn't decode WhoHasBroadcast");
                    }

                }
                else if (service == K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_COV_NOTIFICATION && OnCOVNotification != null)
                {
                    uint subscriberProcessIdentifier;
                    K2A.Drivers.BACnetLib.BacnetObjectId initiatingDeviceIdentifier;
                    K2A.Drivers.BACnetLib.BacnetObjectId monitoredObjectIdentifier;
                    uint timeRemaining;
                    ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> values;
                    if (K2A.Drivers.BACnetLib.Services.DecodeCOVNotifyUnconfirmed(buffer, offset, length, out subscriberProcessIdentifier, out initiatingDeviceIdentifier, out monitoredObjectIdentifier, out timeRemaining, out values) >= 0)
                        OnCOVNotification(this, adr, 0, subscriberProcessIdentifier, initiatingDeviceIdentifier, monitoredObjectIdentifier, timeRemaining, false, values, K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0);
                    else
                        BACnetDriver.Log.Error("Couldn't decode COVNotifyUnconfirmed");
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_TIME_SYNCHRONIZATION && OnTimeSynchronize != null)
                {
                    DateTime dateTime;
                    if (K2A.Drivers.BACnetLib.Services.DecodeTimeSync(buffer, offset, length, out dateTime) >= 0)
                        OnTimeSynchronize(this, adr, dateTime, false);
                    else
                        BACnetDriver.Log.Error("Couldn't decode TimeSynchronize");
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_UTC_TIME_SYNCHRONIZATION && OnTimeSynchronize != null)
                {
                    DateTime dateTime;
                    if (K2A.Drivers.BACnetLib.Services.DecodeTimeSync(buffer, offset, length, out dateTime) >= 0)
                        OnTimeSynchronize(this, adr, dateTime, true);
                    else
                        BACnetDriver.Log.Error("Couldn't decode TimeSynchronize");
                }
                else if (service == K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_EVENT_NOTIFICATION && OnEventNotify != null) // F. Chaxel
                {
                    K2A.Drivers.BACnetLib.BacnetEventNotificationData EventData;
                    if (K2A.Drivers.BACnetLib.Services.DecodeEventNotifyData(buffer, offset, length, out EventData) >= 0)
                    {
                        OnEventNotify(this, adr, EventData);
                    }
                    else
                        BACnetDriver.Log.Error("Couldn't decode Event/Alarm Notification");
                }
                else
                {
                    BACnetDriver.Log.Trace("Unconfirmed service not handled: " + service.ToString());
                    // SendUnConfirmedServiceReject(adr); ? exists ?
                }
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error in ProcessUnconfirmedServiceRequest: " + ex.Message);
            }
        }

        public delegate void SimpleAckHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, byte[] data, int data_offset, int data_length);
        public event SimpleAckHandler OnSimpleAck;

        protected void ProcessSimpleAck(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, byte[] buffer, int offset, int length)
        {
            try
            {
                BACnetDriver.Log.Trace("SimpleAck");
                if (OnSimpleAck != null) OnSimpleAck(this, adr, type, service, invoke_id, buffer, offset, length);
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error in ProcessSimpleAck: " + ex.Message);
            }
        }

        public delegate void ComplexAckHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, byte[] buffer, int offset, int length);
        public event ComplexAckHandler OnComplexAck;

        protected void ProcessComplexAck(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, byte[] buffer, int offset, int length)
        {
            try
            {
                //BACnetDriver.Log.Trace("ComplexAck");
                if (OnComplexAck != null) OnComplexAck(this, adr, type, service, invoke_id, buffer, offset, length);
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error in ProcessComplexAck: " + ex.Message);
            }
        }

        public delegate void ErrorHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses error_class, K2A.Drivers.BACnetLib.BacnetErrorCodes error_code, byte[] buffer, int offset, int length);
        public event ErrorHandler OnError;

        protected void ProcessError(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, byte[] buffer, int offset, int length)
        {
            try
            {
                BACnetDriver.Log.Trace("Error");

                K2A.Drivers.BACnetLib.BacnetErrorClasses error_class;
                K2A.Drivers.BACnetLib.BacnetErrorCodes error_code;
                if (K2A.Drivers.BACnetLib.Services.DecodeError(buffer, offset, length, out error_class, out error_code) < 0)
                    BACnetDriver.Log.Error("Couldn't decode Error");

                if (OnError != null) OnError(this, adr, type, service, invoke_id, error_class, error_code, buffer, offset, length);
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error in ProcessError: " + ex.Message);
            }
        }

        public delegate void AbortHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, byte invoke_id, byte reason, byte[] buffer, int offset, int length);
        public event AbortHandler OnAbort;

        protected void ProcessAbort(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, byte invoke_id, byte reason, byte[] buffer, int offset, int length)
        {
            try
            {
                BACnetDriver.Log.Trace("Abort");
                if (OnAbort != null) OnAbort(this, adr, type, invoke_id, reason, buffer, offset, length);
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error in ProcessAbort: " + ex.Message);
            }
        }

        public delegate void SegmentAckHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, byte original_invoke_id, byte sequence_number, byte actual_window_size, byte[] buffer, int offset, int length);
        public event SegmentAckHandler OnSegmentAck;

        protected void ProcessSegmentAck(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, byte original_invoke_id, byte sequence_number, byte actual_window_size, byte[] buffer, int offset, int length)
        {
            try
            {
                BACnetDriver.Log.Trace("SegmentAck");
                if (OnSegmentAck != null) OnSegmentAck(this, adr, type, original_invoke_id, sequence_number, actual_window_size, buffer, offset, length);
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error in ProcessSegmentAck: " + ex.Message);
            }
        }

        public delegate void SegmentHandler(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments, K2A.Drivers.BACnetLib.BacnetMaxAdpu max_adpu, byte sequence_number, bool first, bool more_follows, byte[] buffer, int offset, int length);
        public event SegmentHandler OnSegment;

        private void ProcessSegment(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments, K2A.Drivers.BACnetLib.BacnetMaxAdpu max_adpu, bool server, byte sequence_number, byte proposed_window_number, byte[] buffer, int offset, int length)
        {
            bool first = false;
            if (sequence_number == 0 && m_last_sequence_number == 0)
            {
                first = true;
            }
            else
            {
                //send negative ack
                if (sequence_number != (m_last_sequence_number + 1))
                {
                    SegmentAckResponse(adr, true, server, invoke_id, m_last_sequence_number, proposed_window_number);
                    BACnetDriver.Log.Trace("Segment sequence out of order");
                    return;
                }
            }
            m_last_sequence_number = sequence_number;

            bool more_follows = (type & K2A.Drivers.BACnetLib.BacnetPduTypes.MORE_FOLLOWS) == K2A.Drivers.BACnetLib.BacnetPduTypes.MORE_FOLLOWS;
            if (!more_follows) m_last_sequence_number = 0;  //reset last sequence_number

            //send ACK
            if ((sequence_number % proposed_window_number) == 0 || !more_follows)
            {
                if (m_force_window_size) proposed_window_number = m_proposed_window_size;
                SegmentAckResponse(adr, false, server, invoke_id, sequence_number, proposed_window_number);
            }

            //Send on
            if (OnSegment != null)
                OnSegment(this, adr, type, service, invoke_id, max_segments, max_adpu, sequence_number, first, more_follows, buffer, offset, length);

            //default segment assembly. We run this seperately from the above handler, to make sure that it comes after!
            if (m_default_segmentation_handling)
                PerformDefaultSegmentHandling(this, adr, type, service, invoke_id, max_segments, max_adpu, sequence_number, first, more_follows, buffer, offset, length);
        }

        private byte[] AssembleSegments(int invoke_id)
        {
            int count = 0;
            foreach (byte[] arr in m_segments[invoke_id])
                count += arr.Length;
            byte[] ret = new byte[count];
            count = 0;
            foreach (byte[] arr in m_segments[invoke_id])
            {
                Array.Copy(arr, 0, ret, count, arr.Length);
                count += arr.Length;
            }
            return ret;
        }

        /// <summary>
        /// This is a simple handling that stores all segments in memory and assembles them when done
        /// </summary>
        private void PerformDefaultSegmentHandling(K2A.Drivers.BACnetLib.BacnetClient sender, K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments, K2A.Drivers.BACnetLib.BacnetMaxAdpu max_adpu, byte sequence_number, bool first, bool more_follows, byte[] buffer, int offset, int length)
        {
            if (first)
            {
                //clear any leftover segments
                m_segments[invoke_id] = new List<byte[]>();

                //copy buffer + encode new adpu header
                type &= ~K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_MESSAGE;
                int adpu_header_len = 3;
                if ((type & K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_MASK) == K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST) adpu_header_len = 4;
                byte[] copy = new byte[length + adpu_header_len];
                Array.Copy(buffer, offset, copy, adpu_header_len, length);
                if ((type & K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_MASK) == K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST)
                    K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(new K2A.Drivers.BACnetLib.EncodeBuffer(copy, 0), type, service, max_segments, max_adpu, invoke_id, 0, 0);
                else
                    K2A.Drivers.BACnetLib.APDU.EncodeComplexAck(new K2A.Drivers.BACnetLib.EncodeBuffer(copy, 0), type, service, invoke_id, 0, 0);
                m_segments[invoke_id].Add(copy);       //doesn't include BVLC or NPDU
            }
            else
            {
                //copy only content part
                byte[] copy = new byte[length];
                Array.Copy(buffer, offset, copy, 0, copy.Length);
                m_segments[invoke_id].Add(copy);
            }

            //process when finished
            if (!more_follows)
            {
                //assemble whole part
                byte[] apdu_buffer = AssembleSegments(invoke_id);
                m_segments[invoke_id] = new List<byte[]>();

                //process
                ProcessApdu(adr, type, apdu_buffer, 0, apdu_buffer.Length);
            }
        }

        private void ProcessApdu(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetPduTypes type, byte[] buffer, int offset, int length)
        {
            switch (type & K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_MASK)
            {
                case K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST:
                    {
                        K2A.Drivers.BACnetLib.BacnetUnconfirmedServices service;
                        int apdu_header_len = K2A.Drivers.BACnetLib.APDU.DecodeUnconfirmedServiceRequest(buffer, offset, out type, out service);
                        offset += apdu_header_len;
                        length -= apdu_header_len;
                        ProcessUnconfirmedServiceRequest(adr, type, service, buffer, offset, length);
                    }
                    break;
                case K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_SIMPLE_ACK:
                    {
                        K2A.Drivers.BACnetLib.BacnetConfirmedServices service;
                        byte invoke_id;
                        int apdu_header_len = K2A.Drivers.BACnetLib.APDU.DecodeSimpleAck(buffer, offset, out type, out service, out invoke_id);
                        offset += apdu_header_len;
                        length -= apdu_header_len;
                        ProcessSimpleAck(adr, type, service, invoke_id, buffer, offset, length);
                    }
                    break;
                case K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_COMPLEX_ACK:
                    {
                        K2A.Drivers.BACnetLib.BacnetConfirmedServices service;
                        byte invoke_id;
                        byte sequence_number;
                        byte proposed_window_number;
                        int apdu_header_len = K2A.Drivers.BACnetLib.APDU.DecodeComplexAck(buffer, offset, out type, out service, out invoke_id, out sequence_number, out proposed_window_number);
                        offset += apdu_header_len;
                        length -= apdu_header_len;
                        if ((type & K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_MESSAGE) == 0) //don't process segmented messages here
                        {
                            ProcessComplexAck(adr, type, service, invoke_id, buffer, offset, length);
                        }
                        else
                        {
                            ProcessSegment(adr, type, service, invoke_id, K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0, K2A.Drivers.BACnetLib.BacnetMaxAdpu.MAX_APDU50, false, sequence_number, proposed_window_number, buffer, offset, length);
                        }
                    }
                    break;
                case K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_SEGMENT_ACK:
                    {
                        byte original_invoke_id;
                        byte sequence_number;
                        byte actual_window_size;
                        int apdu_header_len = K2A.Drivers.BACnetLib.APDU.DecodeSegmentAck(buffer, offset, out type, out original_invoke_id, out sequence_number, out actual_window_size);
                        offset += apdu_header_len;
                        length -= apdu_header_len;
                        m_last_segment_ack.Set(adr, original_invoke_id, sequence_number, actual_window_size);
                        ProcessSegmentAck(adr, type, original_invoke_id, sequence_number, actual_window_size, buffer, offset, length);
                    }
                    break;
                case K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_ERROR:
                    {
                        K2A.Drivers.BACnetLib.BacnetConfirmedServices service;
                        byte invoke_id;
                        int apdu_header_len = K2A.Drivers.BACnetLib.APDU.DecodeError(buffer, offset, out type, out service, out invoke_id);
                        offset += apdu_header_len;
                        length -= apdu_header_len;
                        ProcessError(adr, type, service, invoke_id, buffer, offset, length);
                    }
                    break;
                case K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_REJECT:
                case K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_ABORT:
                    {
                        byte invoke_id;
                        byte reason;
                        int apdu_header_len = K2A.Drivers.BACnetLib.APDU.DecodeAbort(buffer, offset, out type, out invoke_id, out reason);
                        offset += apdu_header_len;
                        length -= apdu_header_len;
                        ProcessAbort(adr, type, invoke_id, reason, buffer, offset, length);
                    }
                    break;
                case K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST:
                    {
                        K2A.Drivers.BACnetLib.BacnetConfirmedServices service;
                        K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments;
                        K2A.Drivers.BACnetLib.BacnetMaxAdpu max_adpu;
                        byte invoke_id;
                        byte sequence_number;
                        byte proposed_window_number;
                        int apdu_header_len = K2A.Drivers.BACnetLib.APDU.DecodeConfirmedServiceRequest(buffer, offset, out type, out service, out max_segments, out max_adpu, out invoke_id, out sequence_number, out proposed_window_number);
                        offset += apdu_header_len;
                        length -= apdu_header_len;
                        if ((type & K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_MESSAGE) == 0) //don't process segmented messages here
                        {
                            ProcessConfirmedServiceRequest(adr, type, service, max_segments, max_adpu, invoke_id, buffer, offset, length);
                        }
                        else
                        {
                            ProcessSegment(adr, type, service, invoke_id, max_segments, max_adpu, true, sequence_number, proposed_window_number, buffer, offset, length);
                        }
                    }
                    break;
                default:
                    BACnetDriver.Log.Error("Something else arrived: " + type);
                    break;
            }
        }

        private void OnRecieve(K2A.Drivers.BACnetLib.IBacnetTransport sender, byte[] buffer, int offset, int msg_length, K2A.Drivers.BACnetLib.BacnetAddress remote_address)
        {
            try
            {
                //finish recieve
                if (m_client == null) return;   //we're disposed 

                //parse
                if (msg_length > 0)
                {
                    K2A.Drivers.BACnetLib.BacnetNpduControls npdu_function;
                    K2A.Drivers.BACnetLib.BacnetAddress destination, source;
                    byte hop_count;
                    K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes nmt;
                    ushort vendor_id;
                    int npdu_len = K2A.Drivers.BACnetLib.NPDU.Decode(buffer, offset, out npdu_function, out destination, out source, out hop_count, out nmt, out vendor_id);

                    // Modif FC
                    remote_address.RoutedSource = source;

                    if ((npdu_function & K2A.Drivers.BACnetLib.BacnetNpduControls.NetworkLayerMessage) == K2A.Drivers.BACnetLib.BacnetNpduControls.NetworkLayerMessage)
                    {
                        BACnetDriver.Log.Info("Network Layer message received");
                        return; // Network Layer message discarded
                    }

                    if (npdu_len > 0)
                    {
                        offset += npdu_len;
                        msg_length -= npdu_len;
                        K2A.Drivers.BACnetLib.BacnetPduTypes apdu_type;
                        if (msg_length > 0)
                        {
                            apdu_type = K2A.Drivers.BACnetLib.APDU.GetDecodedType(buffer, offset);
                            //APDU
                            ProcessApdu(remote_address, apdu_type, buffer, offset, msg_length);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error in OnRecieve: " + ex.Message);
            }
        }

        // Modif FC
        public void RegisterAsForeignDevice(String BBMD_IP, short TTL, int Port = 0xbac0)
        {
            bool sent = false;

            try
            {
                System.Net.IPEndPoint ep = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(BBMD_IP), Port);

                // dynamic avoid reference to BacnetIpUdpProtocolTransport or BacnetIpV6UdpProtocolTransport classes
                dynamic m_client_dyn = m_client;
                sent = m_client_dyn.SendRegisterAsForeignDevice(ep, TTL);

                if (sent == false)
                    BACnetDriver.Log.Error("The given address do not match with the IP version");
                else
                    BACnetDriver.Log.Trace("Sending Register as a Foreign Device ... ");
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error on RegisterAsForeignDevice (Wrong Transport, not IP ?)" + ex.Message);
            }
        }

        public void RemoteWhoHas(BacnetObjectId objId, string objName, String BBMD_IP, int Port = 0xbac0, int low_limit = -1, int high_limit = -1)
        {

            try
            {
                if (string.IsNullOrEmpty(BBMD_IP)) BBMD_IP = BACnetDriver.Instance.ConnectionInfo.LocalIP;
                System.Net.IPEndPoint ep = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(BBMD_IP), Port);

                K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
                K2A.Drivers.BACnetLib.BacnetAddress broadcast = m_client.GetBroadcastAddress();
                K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, broadcast, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
                K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_WHO_HAS);
                K2A.Drivers.BACnetLib.Services.EncodeWhoHasBroadcast(b, low_limit, high_limit, objId, objName);

                bool sent = false;

                // dynamic avoid reference to BacnetIpUdpProtocolTransport or BacnetIpV6UdpProtocolTransport classes
                dynamic m_client_dyn = m_client;
                sent = m_client_dyn.SendRemoteWhohas(b.buffer, ep, b.offset);
                
                if (sent == false)
                    BACnetDriver.Log.Error("The given address do not match with the IP version");
                else
                    BACnetDriver.Log.Trace("Sending Remote WhoHas ... ");
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error on Sending WhoHas to remote BBMD (Wrong Transport, not IP ?)" + ex.Message);
            }

        }
        public void RemoteWhoIs(String BBMD_IP, int Port = 0xbac0, int low_limit = -1, int high_limit = -1)
        {

            try
            {
                if (string.IsNullOrEmpty(BBMD_IP)) BBMD_IP = BACnetDriver.Instance.ConnectionInfo.LocalIP;
                System.Net.IPEndPoint ep = new System.Net.IPEndPoint(System.Net.IPAddress.Parse(BBMD_IP), Port);

                K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
                K2A.Drivers.BACnetLib.BacnetAddress broadcast = m_client.GetBroadcastAddress();
                K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, broadcast, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
                K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_WHO_IS);
                K2A.Drivers.BACnetLib.Services.EncodeWhoIsBroadcast(b, low_limit, high_limit);

                bool sent = false;

                // dynamic avoid reference to BacnetIpUdpProtocolTransport or BacnetIpV6UdpProtocolTransport classes
                dynamic m_client_dyn = m_client;
                BACnetDriver.Log.Trace($"Sending Remote WhoIs ({low_limit} - {high_limit})");
                sent = m_client_dyn.SendRemoteWhois(b.buffer, ep, b.offset);

                if (sent == false)
                    BACnetDriver.Log.Error("The given address do not match with the IP version");
                else
                    BACnetDriver.Log.Trace("Sending Remote Whois ... ");
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error on Sending Whois to remote BBMD (Wrong Transport, not IP ?)" + ex.Message);
            }

        }
        public void WhoIs(int low_limit = -1, int high_limit = -1)
        {

            BACnetDriver.Log.Trace($"Sending WhoIs ({low_limit} - {high_limit})");

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);

            // _receiver could be an unicast @ : for direct acces 
            // usefull on BIP for a known IP:Port, unknown device Id

            BacnetAddress receiver = m_client.GetBroadcastAddress();

            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, receiver, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_WHO_IS);
            K2A.Drivers.BACnetLib.Services.EncodeWhoIsBroadcast(b, low_limit, high_limit);

            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, receiver, false, 0);
        }

        public void WhoHas(BacnetObjectId objId, string objName, int low_limit = -1, int high_limit = -1)
        {

            BACnetDriver.Log.Trace("Sending WhoHas ... ");

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);

            // _receiver could be an unicast @ : for direct acces 
            // usefull on BIP for a known IP:Port, unknown device Id

            BacnetAddress receiver = m_client.GetBroadcastAddress();

            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, receiver, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_WHO_HAS);
            K2A.Drivers.BACnetLib.Services.EncodeWhoHasBroadcast(b, low_limit, high_limit, objId, objName);

            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, receiver, false, 0);
        }

        private BacnetAddress GetBroadcastReceiver()
        {
            var ep = new System.Net.IPEndPoint(System.Net.IPAddress.Any, BACnetDriver.Instance.ConnectionInfo.UDP_Port);
            K2A.Drivers.BACnetLib.BacnetAddress broadcast;
            BacnetIpUdpProtocolTransport.Convert(ep, out broadcast);
            broadcast.net = 0xFFFF;
            return broadcast;
        }


        public void Iam(uint device_id, K2A.Drivers.BACnetLib.BacnetSegmentations segmentation)
        {
            BACnetDriver.Log.Trace("Sending Iam ... ");

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.BacnetAddress broadcast = m_client.GetBroadcastAddress();
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, broadcast, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_I_AM);
            K2A.Drivers.BACnetLib.Services.EncodeIamBroadcast(b, device_id, (uint)GetMaxApdu(), segmentation, m_vendor_id);

            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, broadcast, false, 0);
        }

        public void IHave(K2A.Drivers.BACnetLib.BacnetObjectId device_id, K2A.Drivers.BACnetLib.BacnetObjectId ObjId, string ObjName)
        {
            BACnetDriver.Log.Trace("Sending IHave ... ");

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.BacnetAddress broadcast = m_client.GetBroadcastAddress();
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, broadcast, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_I_HAVE);
            K2A.Drivers.BACnetLib.Services.EncodeIhaveBroadcast(b, device_id, ObjId, ObjName);

            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, broadcast, false, 0);

        }

        public void SendUnconfirmedEventNotification(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetEventNotificationData eventData)
        {
            BACnetDriver.Log.Trace("Sending Event Notification ... ");

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, adr, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_EVENT_NOTIFICATION);
            K2A.Drivers.BACnetLib.Services.EncodeEventNotifyUnconfirmed(b, eventData);
            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, adr, false, 0);
        }

        public void SendConfirmedServiceReject(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, K2A.Drivers.BACnetLib.BacnetRejectReasons reason)
        {
            BACnetDriver.Log.Trace("Sending Service reject ... ");

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);

            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, adr, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeError(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_REJECT, (K2A.Drivers.BACnetLib.BacnetConfirmedServices)reason, invoke_id);
            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, adr, false, 0);
        }

        public void SynchronizeTime(K2A.Drivers.BACnetLib.BacnetAddress adr, DateTime dateTime, bool utc)
        {
            BACnetDriver.Log.Trace("Sending Time Synchronize ... ");

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, adr, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            if (!utc)
                K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_TIME_SYNCHRONIZATION);
            else
                K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_UTC_TIME_SYNCHRONIZATION);
            K2A.Drivers.BACnetLib.Services.EncodeTimeSync(b, dateTime);

            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, adr, false, 0);
        }

        public int GetMaxApdu()
        {
            int max_apdu;
            switch (m_client.MaxAdpuLength)
            {
                case K2A.Drivers.BACnetLib.BacnetMaxAdpu.MAX_APDU1476:
                    max_apdu = 1476;
                    break;
                case K2A.Drivers.BACnetLib.BacnetMaxAdpu.MAX_APDU1024:
                    max_apdu = 1024;
                    break;
                case K2A.Drivers.BACnetLib.BacnetMaxAdpu.MAX_APDU480:
                    max_apdu = 480;
                    break;
                case K2A.Drivers.BACnetLib.BacnetMaxAdpu.MAX_APDU206:
                    max_apdu = 206;
                    break;
                case K2A.Drivers.BACnetLib.BacnetMaxAdpu.MAX_APDU128:
                    max_apdu = 128;
                    break;
                case K2A.Drivers.BACnetLib.BacnetMaxAdpu.MAX_APDU50:
                    max_apdu = 50;
                    break;
                default:
                    throw new NotImplementedException();
            }

            //max udp payload IRL seems to differ from the expectations in BACnet
            //so we have to adjust it. (In order to fulfill the standard)
            const int max_npdu_header_length = 4;       //usually it's '2', but it can also be more than '4'. Beware!
            return Math.Min(max_apdu, m_client.MaxBufferLength - m_client.HeaderLength - max_npdu_header_length);
        }

        public int GetFileBufferMaxSize()
        {
            //6 should be the max_apdu_header_length for Confirmed (with segmentation)
            //12 should be the max_atomic_write_file
            return GetMaxApdu() - 18;
        }

        public bool WriteFileRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, ref int position, int count, byte[] file_buffer, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginWriteFileRequest(adr, object_id, position, count, file_buffer, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndWriteFileRequest(result, out position, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            return false;
        }

        public IAsyncResult BeginWriteFileRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, int position, int count, byte[] file_buffer, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending AtomicWriteFileRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_ATOMIC_WRITE_FILE, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeAtomicWriteFile(b, true, object_id, position, 1, new byte[][] { file_buffer }, new int[] { count });

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndWriteFileRequest(IAsyncResult result, out int position, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
                //decode
                bool is_stream;
                if (K2A.Drivers.BACnetLib.Services.DecodeAtomicWriteFileAcknowledge(res.Result, 0, res.Result.Length, out is_stream, out position) < 0)
                    ex = new Exception("Decode");
            }
            else
            {
                position = -1;
            }

            res.Dispose();
        }

        public IAsyncResult BeginReadFileRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, int position, uint count, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending AtomicReadFileRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            //encode
            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_ATOMIC_READ_FILE, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeAtomicReadFile(b, true, object_id, position, count);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndReadFileRequest(IAsyncResult result, out uint count, out int position, out bool end_of_file, out byte[] file_buffer, out int file_buffer_offset, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
                //decode
                bool is_stream;
                if (K2A.Drivers.BACnetLib.Services.DecodeAtomicReadFileAcknowledge(res.Result, 0, res.Result.Length, out end_of_file, out is_stream, out position, out count, out file_buffer, out file_buffer_offset) < 0)
                    ex = new Exception("Decode");
            }
            else
            {
                count = 0;
                end_of_file = true;
                position = -1;
                file_buffer_offset = -1;
                file_buffer = new byte[0];
            }

            res.Dispose();
        }

        public bool ReadFileRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, ref int position, ref uint count, out bool end_of_file, out byte[] file_buffer, out int file_buffer_offset, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginReadFileRequest(adr, object_id, position, count, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndReadFileRequest(result, out count, out position, out end_of_file, out file_buffer, out file_buffer_offset, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            position = -1;
            count = 0;
            file_buffer = null;
            end_of_file = true;
            file_buffer_offset = -1;
            return false;
        }

        // Fc
        public IAsyncResult BeginReadRangeRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, uint idxBegin, uint Quantity, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending ReadRangeRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            //encode
            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_RANGE, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeReadRange(b, object_id, (uint)K2A.Drivers.BACnetLib.BacnetPropertyIds.PROP_LOG_BUFFER, K2A.Drivers.BACnetLib.ASN1.BACNET_ARRAY_ALL, K2A.Drivers.BACnetLib.BacnetReadRangeRequestTypes.RR_BY_POSITION, idxBegin, DateTime.Now, (int)Quantity);
            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;

        }

        // Fc
        public void EndReadRangeRequest(IAsyncResult result, out byte[] trendbuffer, out uint ItemCount, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ItemCount = 0;
            trendbuffer = null;

            ex = res.Error;
            if (ex == null && !res.WaitForDone(40 * 1000))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
                ItemCount = K2A.Drivers.BACnetLib.Services.DecodeReadRangeAcknowledge(res.Result, 0, res.Result.Length, out trendbuffer);
                if (ItemCount == 0)
                    ex = new Exception("Decode");
            }

            res.Dispose();
        }

        // Fc
        public bool ReadRangeRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, uint idxBegin, ref uint Quantity, out byte[] Range, byte invoke_id = 0)
        {
            Range = null;
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginReadRangeRequest(adr, object_id, idxBegin, Quantity, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndReadRangeRequest(result, out Range, out Quantity, out ex); // quantity read could be less than demanded
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            return false;
        }

        public bool SubscribeCOVRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, uint subscribe_id, bool cancel, bool issue_confirmed_notifications, uint lifetime, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginSubscribeCOVRequest(adr, object_id, subscribe_id, cancel, issue_confirmed_notifications, lifetime, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndSubscribeCOVRequest(result, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            return false;
        }

        public IAsyncResult BeginSubscribeCOVRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, uint subscribe_id, bool cancel, bool issue_confirmed_notifications, uint lifetime, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending SubscribeCOVRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_SUBSCRIBE_COV, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeSubscribeCOV(b, subscribe_id, object_id, cancel, issue_confirmed_notifications, lifetime);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndSubscribeCOVRequest(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {

            }
            else
            {

            }

            res.Dispose();
        }

        public bool SubscribePropertyRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyReference monitored_property, uint subscribe_id, bool cancel, bool issue_confirmed_notifications, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginSubscribePropertyRequest(adr, object_id, monitored_property, subscribe_id, cancel, issue_confirmed_notifications, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndSubscribePropertyRequest(result, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            return false;
        }

        public IAsyncResult BeginSubscribePropertyRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyReference monitored_property, uint subscribe_id, bool cancel, bool issue_confirmed_notifications, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending SubscribePropertyRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_SUBSCRIBE_COV_PROPERTY, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeSubscribeProperty(b, subscribe_id, object_id, cancel, issue_confirmed_notifications, 0, monitored_property, false, 0f);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndSubscribePropertyRequest(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {

            }
            else
            {

            }

            res.Dispose();
        }

        public string ReadPropertyRequest(BacnetAddress adr, BacnetObjectId object_id, BacnetPropertyIds property_id, out IList<BacnetValue> value_list, byte invoke_id = 0, uint array_index = ASN1.BACNET_ARRAY_ALL)
        {
            //returns null string unless there is an error
            using (BacnetAsyncResult result = (BacnetAsyncResult)BeginReadPropertyRequest(adr, object_id, property_id, true, invoke_id, array_index))
            {

                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndReadPropertyRequest(result, out value_list, out ex);
                        if (ex != null)
                        {
                            return ex.Message;
                        }
                        else
                            return null;
                    }
                    //if (r < (m_retries - 1))
                    result.Resend();
                }
            }
            value_list = null;
            BACnetDriver.Log.Error(string.Format("Timeout Error: {0},{1},{2},{3}", adr.ToString(true), object_id.ToString(), property_id.ToString(), array_index));
            return "TIMEOUT ERROR";
        }

        public IAsyncResult BeginReadPropertyRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyIds property_id, bool wait_for_transmit, byte invoke_id = 0, uint array_index = K2A.Drivers.BACnetLib.ASN1.BACNET_ARRAY_ALL)
        {
            BACnetDriver.Log.Trace("Sending ReadPropertyRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_PROPERTY, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeReadProperty(b, object_id, (uint)property_id, array_index);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndReadPropertyRequest(IAsyncResult result, out IList<K2A.Drivers.BACnetLib.BacnetValue> value_list, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
                //decode
                K2A.Drivers.BACnetLib.BacnetObjectId response_object_id;
                K2A.Drivers.BACnetLib.BacnetPropertyReference response_property;
                if (K2A.Drivers.BACnetLib.Services.DecodeReadPropertyAcknowledge(res.Result, 0, res.Result.Length, out response_object_id, out response_property, out value_list) < 0)
                    ex = new Exception("Decode");
            }
            else
            {
                value_list = null;
            }

            res.Dispose();
        }

        public bool WritePropertyRequest(BacnetAddress adr, BacnetObjectId object_id, BacnetPropertyIds property_id, uint priority, IEnumerable<BacnetValue> value_list, byte invoke_id = 0)
        {
            using (BacnetAsyncResult result = (BacnetAsyncResult)BeginWritePropertyRequest(adr, object_id, property_id, priority, value_list, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndWritePropertyRequest(result, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            value_list = null;
            return false;
        }

        public bool WritePropertyMultipleRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> value_list, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginWritePropertyMultipleRequest(adr, object_id, value_list, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndWritePropertyRequest(result, out ex); // Share the same with single write
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            value_list = null;
            return false;
        }

        public IAsyncResult BeginWritePropertyRequest(BacnetAddress adr, BacnetObjectId object_id, BacnetPropertyIds property_id, uint priority, IEnumerable<BacnetValue> value_list, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending WritePropertyRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            NPDU.Encode(b, BacnetNpduControls.PriorityNormalMessage | BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            APDU.EncodeConfirmedServiceRequest(b, BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, BacnetConfirmedServices.SERVICE_CONFIRMED_WRITE_PROPERTY, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            Services.EncodeWriteProperty(b, object_id, (uint)property_id, ASN1.BACNET_ARRAY_ALL, priority, value_list);

            //send
            BacnetAsyncResult ret = new BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public IAsyncResult BeginWritePropertyMultipleRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> value_list, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending WritePropertyMultipleRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            //BacnetNpduControls.PriorityNormalMessage 
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);

            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_WRITE_PROP_MULTIPLE, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeWritePropertyMultiple(b, object_id, value_list);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndWritePropertyRequest(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
            }
            else
            {
            }

            res.Dispose();
        }

        // By Chritopher Günter : Write multiple properties on multiple objects
        public bool WritePropertyMultipleRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, ICollection<K2A.Drivers.BACnetLib.BacnetReadAccessResult> value_list, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginWritePropertyMultipleRequest(adr, value_list, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndWritePropertyRequest(result, out ex); // Share the same with single write
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            value_list = null;
            return false;
        }

        public IAsyncResult BeginWritePropertyMultipleRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, ICollection<K2A.Drivers.BACnetLib.BacnetReadAccessResult> value_list, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending WritePropertyMultipleRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            //BacnetNpduControls.PriorityNormalMessage 
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);

            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_WRITE_PROP_MULTIPLE, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeWriteObjectMultiple(b, value_list);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public bool ReadPropertyMultipleRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, IList<K2A.Drivers.BACnetLib.BacnetPropertyReference> property_id_and_array_index, out IList<K2A.Drivers.BACnetLib.BacnetReadAccessResult> values, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginReadPropertyMultipleRequest(adr, object_id, property_id_and_array_index, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndReadPropertyMultipleRequest(result, out values, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            values = null;
            return false;
        }

        public IAsyncResult BeginReadPropertyMultipleRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, IList<K2A.Drivers.BACnetLib.BacnetPropertyReference> property_id_and_array_index, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending ReadPropertyMultipleRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_PROP_MULTIPLE, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeReadPropertyMultiple(b, object_id, property_id_and_array_index);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        // Another way to read multiple properties on multiples objects, if supported by devices
        public string ReadPropertyMultipleRequest(BacnetAddress adr, IList<BacnetReadAccessSpecification> properties, out IList<BacnetReadAccessResult> values, byte invoke_id = 0)
        {
            using (BacnetAsyncResult result = (BacnetAsyncResult)BeginReadPropertyMultipleRequest(adr, properties, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        //Debug.Print("Invoke Id " + result.m_wait_invoke_id + " completed");
                        Exception ex;
                        EndReadPropertyMultipleRequest(result, out values, out ex);
                        if (ex != null) return ex.Message;
                        else return null;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            values = null;
            BACnetDriver.Log.Error(string.Format("Timeout Error, Read Multiple: {0}", adr.ToString(true)));
            return "TIMEOUT ERROR";
        }

        public IAsyncResult BeginReadPropertyMultipleRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, IList<K2A.Drivers.BACnetLib.BacnetReadAccessSpecification> properties, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending ReadPropertyMultipleRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_PROP_MULTIPLE, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeReadPropertyMultiple(b, properties);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndReadPropertyMultipleRequest(IAsyncResult result, out IList<K2A.Drivers.BACnetLib.BacnetReadAccessResult> values, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
                //decode
                if (K2A.Drivers.BACnetLib.Services.DecodeReadPropertyMultipleAcknowledge(res.Result, 0, res.Result.Length, out values) < 0)
                    ex = new Exception("Decode");
            }
            else
            {
                values = null;
            }

            res.Dispose();
        }

        //*********************************************************************************
        // By Christopher Günter
        public bool CreateObjectRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> value_list, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginCreateObjectRequest(adr, object_id, value_list, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndCreateObjectRequest(result, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            value_list = null;
            return false;
        }
        //***********************************************************************

        public IAsyncResult BeginCreateObjectRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, ICollection<K2A.Drivers.BACnetLib.BacnetPropertyValue> value_list, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending CreateObjectRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);

            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);


            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_CREATE_OBJECT, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeCreateProperty(b, object_id, value_list);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }
        //***********************************************************************************************************
        public void EndCreateObjectRequest(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
            }
            else
            {
            }

            res.Dispose();
        }

        //***************************************************************************************************
        public bool DeleteObjectRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginDeleteObjectRequest(adr, object_id, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndDeleteObjectRequest(result, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }

            return false;
        }

        //******************************************************
        public IAsyncResult BeginDeleteObjectRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending DeleteObjectRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);

            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            //NPDU.Encode(b, BacnetNpduControls.PriorityNormalMessage | BacnetNpduControls.ExpectingReply , adr.RoutedSource, null, DEFAULT_HOP_COUNT, BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);

            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_DELETE_OBJECT, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);

            K2A.Drivers.BACnetLib.ASN1.encode_application_object_id(b, object_id.type, object_id.instance);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        //************************************************************
        public void EndDeleteObjectRequest(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
            }
            else
            {
            }

            res.Dispose();
        }
        //*************************************************************

        public bool AddListElementRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyReference reference, IList<K2A.Drivers.BACnetLib.BacnetValue> value_list, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginAddListElementRequest(adr, object_id, reference, value_list, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {

                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndAddListElementRequest(result, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            //values = null;
            return false;
        }
        //**********************************************************************
        public bool RemoveListElementRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyReference reference, IList<K2A.Drivers.BACnetLib.BacnetValue> value_list, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginRemoveListElementRequest(adr, object_id, reference, value_list, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {

                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndAddListElementRequest(result, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            //values = null;
            return false;
        }
        //***********************************************************************
        public IAsyncResult BeginRemoveListElementRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyReference reference, IList<K2A.Drivers.BACnetLib.BacnetValue> value_list, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending RemoveListElementRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_REMOVE_LIST_ELEMENT, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeAddListElement(b, object_id, (uint)reference.propertyIdentifier, (uint)reference.propertyArrayIndex, value_list);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        //******************************************************************************
        public IAsyncResult BeginAddListElementRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyReference reference, IList<K2A.Drivers.BACnetLib.BacnetValue> value_list, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending AddListElementRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_ADD_LIST_ELEMENT, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeAddListElement(b, object_id, (uint)reference.propertyIdentifier, (uint)reference.propertyArrayIndex, value_list);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }
        //*****************************************************************************
        public void EndAddListElementRequest(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
            }
            else
            {
            }

            res.Dispose();
        }

        // Fc
        // Read or Write without APDU Data encoding nor Decoding (just Request type, Object id and Property id)
        // Data is given by the caller starting with the Tag 3 (or maybe another one), and ending with it
        // return buffer start also with the Tag 3
        public bool RawEncodedDecodedPropertyConfirmedRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyIds property_id, K2A.Drivers.BACnetLib.BacnetConfirmedServices service_id, ref byte[] InOutBuffer, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginRawEncodedDecodedPropertyConfirmedRequest(adr, object_id, property_id, service_id, InOutBuffer, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndRawEncodedDecodedPropertyConfirmedRequest(result, service_id, out InOutBuffer, out ex);
                        if (ex != null) throw ex;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            InOutBuffer = null;
            return false;
        }
        // Fc
        public IAsyncResult BeginRawEncodedDecodedPropertyConfirmedRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyIds property_id, K2A.Drivers.BACnetLib.BacnetConfirmedServices service_id, byte[] InOutBuffer, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending RawEncodedRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), service_id, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);

            K2A.Drivers.BACnetLib.ASN1.encode_context_object_id(b, 0, object_id.type, object_id.instance);
            K2A.Drivers.BACnetLib.ASN1.encode_context_enumerated(b, 1, (byte)property_id);

            // No content encoding to do
            if (InOutBuffer != null)
                b.Add(InOutBuffer, InOutBuffer.Length);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }
        // Fc
        public void EndRawEncodedDecodedPropertyConfirmedRequest(IAsyncResult result, K2A.Drivers.BACnetLib.BacnetConfirmedServices service_id, out byte[] InOutBuffer, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            InOutBuffer = null;

            if (ex == null)
            {
                if (service_id == K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_PROPERTY)
                {
                    //decode
                    K2A.Drivers.BACnetLib.BacnetObjectId response_object_id;
                    K2A.Drivers.BACnetLib.BacnetPropertyReference response_property;
                    int offset = 0; int len = 0;
                    byte[] buffer = res.Result; byte tag_number; uint len_value_type;

                    ex = new Exception("Decode");

                    if (!K2A.Drivers.BACnetLib.ASN1.decode_is_context_tag(buffer, offset, 0))
                        return;
                    len = 1;
                    len += K2A.Drivers.BACnetLib.ASN1.decode_object_id(buffer, offset + len, out response_object_id.type, out response_object_id.instance);
                    /* Tag 1: Property ID */
                    len += K2A.Drivers.BACnetLib.ASN1.decode_tag_number_and_value(buffer, offset + len, out tag_number, out len_value_type);
                    if (tag_number != 1)
                        return;
                    len += K2A.Drivers.BACnetLib.ASN1.decode_enumerated(buffer, offset + len, len_value_type, out response_property.propertyIdentifier);

                    InOutBuffer = new byte[buffer.Length - len];
                    Array.Copy(buffer, len, InOutBuffer, 0, InOutBuffer.Length);

                    ex = null;
                }
            }

            res.Dispose();
        }

        public bool DeviceCommunicationControlRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, uint timeDuration, uint enable_disable, string password, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginDeviceCommunicationControlRequest(adr, timeDuration, enable_disable, password, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndDeviceCommunicationControlRequest(result, out ex);
                        if (ex != null) return false;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            return false;
        }

        public IAsyncResult BeginDeviceCommunicationControlRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, uint timeDuration, uint enable_disable, string password, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending DeviceCommunicationControlRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_DEVICE_COMMUNICATION_CONTROL, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeDeviceCommunicationControl(b, timeDuration, enable_disable, password);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndDeviceCommunicationControlRequest(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
            }
            else
            {
            }

            res.Dispose();
        }

        // FChaxel
        public bool GetAlarmSummaryOrEventRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, bool GetEvent, ref IList<K2A.Drivers.BACnetLib.BacnetGetEventInformationData> Alarms, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginGetAlarmSummaryOrEventRequest(adr, GetEvent, Alarms, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        bool MoreEvent;

                        EndGetAlarmSummaryOrEventRequest(result, GetEvent, ref Alarms, out MoreEvent, out ex);
                        if (ex != null) return false;
                        else
                        {
                            if (MoreEvent == true) // never true if GetAlarmSummary is used
                                return GetAlarmSummaryOrEventRequest(adr, GetEvent, ref Alarms);
                            return true;
                        };
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            return false;
        }

        public IAsyncResult BeginGetAlarmSummaryOrEventRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, bool GetEvent, IList<K2A.Drivers.BACnetLib.BacnetGetEventInformationData> Alarms, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending Alarm summary request... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);

            if (GetEvent == false)
                K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_GET_ALARM_SUMMARY, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            else
                K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST | (m_max_segments != K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0 ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_RESPONSE_ACCEPTED : 0), K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_GET_EVENT_INFORMATION, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);

            // Get Next, never true if GetAlarmSummary is usee
            if (Alarms.Count != 0)
                K2A.Drivers.BACnetLib.ASN1.encode_context_object_id(b, 0, Alarms[Alarms.Count - 1].objectIdentifier.type, Alarms[Alarms.Count - 1].objectIdentifier.instance);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndGetAlarmSummaryOrEventRequest(IAsyncResult result, bool GetEvent, ref IList<K2A.Drivers.BACnetLib.BacnetGetEventInformationData> Alarms, out bool MoreEvent, out Exception ex)
        {
            MoreEvent = false;
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
                if (K2A.Drivers.BACnetLib.Services.DecodeAlarmSummaryOrEvent(res.Result, 0, res.Result.Length, GetEvent, ref Alarms, out MoreEvent) < 0)
                    ex = new Exception("Decode");
            }
            else
            {
                ex = new Exception("Service not available");
            }

            res.Dispose();
        }


        // FChaxel
        public bool AlarmAcknowledgement(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId objid, K2A.Drivers.BACnetLib.BacnetEventNotificationData.BacnetEventStates eventState, String AckText, K2A.Drivers.BACnetLib.BacnetGenericTime evTimeStamp, K2A.Drivers.BACnetLib.BacnetGenericTime ackTimeStamp, byte invoke_id = 0)
        {

            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginAlarmAcknowledgement(adr, objid, eventState, AckText, evTimeStamp, ackTimeStamp, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndAlarmAcknowledgement(result, out ex);
                        if (ex != null) return false;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            return false;
        }

        public IAsyncResult BeginAlarmAcknowledgement(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetObjectId objid, K2A.Drivers.BACnetLib.BacnetEventNotificationData.BacnetEventStates eventState, String AckText, K2A.Drivers.BACnetLib.BacnetGenericTime evTimeStamp, K2A.Drivers.BACnetLib.BacnetGenericTime ackTimeStamp, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending AlarmAcknowledgement ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_ACKNOWLEDGE_ALARM, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeAlarmAcknowledge(b, 57, objid, (uint)eventState, AckText, evTimeStamp, ackTimeStamp);
            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndAlarmAcknowledgement(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (!res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
            }
            else
            {
            }
        }

        public bool ReinitializeRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetReinitializedStates state, string password, byte invoke_id = 0)
        {
            using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginReinitializeRequest(adr, state, password, true, invoke_id))
            {
                for (int r = 0; r < m_retries; r++)
                {
                    if (result.WaitForDone(m_timeout))
                    {
                        Exception ex;
                        EndReinitializeRequest(result, out ex);
                        if (ex != null) return false;
                        else return true;
                    }
                    if (r < (m_retries - 1))
                        result.Resend();
                }
            }
            return false;
        }

        public IAsyncResult BeginReinitializeRequest(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetReinitializedStates state, string password, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending ReinitializeRequest ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_REINITIALIZE_DEVICE, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeReinitializeDevice(b, state, password);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndReinitializeRequest(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (ex == null && !res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
            }
            else
            {
            }

            res.Dispose();
        }

        public IAsyncResult BeginConfirmedNotify(K2A.Drivers.BACnetLib.BacnetAddress adr, uint subscriberProcessIdentifier, uint initiatingDeviceIdentifier, K2A.Drivers.BACnetLib.BacnetObjectId monitoredObjectIdentifier, uint timeRemaining, IList<K2A.Drivers.BACnetLib.BacnetPropertyValue> values, bool wait_for_transmit, byte invoke_id = 0)
        {
            BACnetDriver.Log.Trace("Sending Notify (confirmed) ... ");
            if (invoke_id == 0) invoke_id = unchecked(m_invoke_id++);

            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage | K2A.Drivers.BACnetLib.BacnetNpduControls.ExpectingReply, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeConfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_CONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_COV_NOTIFICATION, m_max_segments, m_client.MaxAdpuLength, invoke_id, 0, 0);
            K2A.Drivers.BACnetLib.Services.EncodeCOVNotifyConfirmed(b, subscriberProcessIdentifier, initiatingDeviceIdentifier, monitoredObjectIdentifier, timeRemaining, values);

            //send
            K2A.Drivers.BACnetLib.BacnetAsyncResult ret = new K2A.Drivers.BACnetLib.BacnetAsyncResult(this, adr, invoke_id, b.buffer, b.offset - m_client.HeaderLength, wait_for_transmit, m_transmit_timeout);
            ret.Resend();

            return ret;
        }

        public void EndConfirmedNotify(IAsyncResult result, out Exception ex)
        {
            K2A.Drivers.BACnetLib.BacnetAsyncResult res = (K2A.Drivers.BACnetLib.BacnetAsyncResult)result;
            ex = res.Error;
            if (!res.WaitForDone(m_timeout))
                ex = new Exception("Wait Timeout");

            if (ex == null)
            {
            }
            else
            {
            }
        }

        public bool Notify(K2A.Drivers.BACnetLib.BacnetAddress adr, uint subscriberProcessIdentifier, uint initiatingDeviceIdentifier, K2A.Drivers.BACnetLib.BacnetObjectId monitoredObjectIdentifier, uint timeRemaining, bool issueConfirmedNotifications, IList<K2A.Drivers.BACnetLib.BacnetPropertyValue> values)
        {
            if (!issueConfirmedNotifications)
            {
                BACnetDriver.Log.Trace("Sending Notify (unconfirmed) ... ");
                K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
                K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
                K2A.Drivers.BACnetLib.APDU.EncodeUnconfirmedServiceRequest(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_UNCONFIRMED_SERVICE_REQUEST, K2A.Drivers.BACnetLib.BacnetUnconfirmedServices.SERVICE_UNCONFIRMED_COV_NOTIFICATION);
                K2A.Drivers.BACnetLib.Services.EncodeCOVNotifyUnconfirmed(b, subscriberProcessIdentifier, initiatingDeviceIdentifier, monitoredObjectIdentifier, timeRemaining, values);
                // Modif F. Chaxel

                int sendbytes = m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, adr, false, 0);

                if (sendbytes == b.offset)
                    return true;
                else
                    return false;
            }
            else
            {
                using (K2A.Drivers.BACnetLib.BacnetAsyncResult result = (K2A.Drivers.BACnetLib.BacnetAsyncResult)BeginConfirmedNotify(adr, subscriberProcessIdentifier, initiatingDeviceIdentifier, monitoredObjectIdentifier, timeRemaining, values, true))
                {
                    for (int r = 0; r < m_retries; r++)
                    {
                        if (result.WaitForDone(m_timeout))
                        {
                            Exception ex;
                            EndConfirmedNotify(result, out ex);
                            if (ex != null) throw ex;
                            else return true;
                        }
                        if (r < (m_retries - 1))
                            result.Resend();
                    }
                }
                return false;
            }
        }

        public class Segmentation
        {
            public K2A.Drivers.BACnetLib.EncodeBuffer buffer;
            public byte sequence_number;
            public byte window_size;
            public byte max_segments;
        }

        public static byte GetSegmentsCount(K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments)
        {
            switch (max_segments)
            {
                case K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0:
                    return 0;
                case K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG2:
                    return 2;
                case K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG4:
                    return 4;
                case K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG8:
                    return 8;
                case K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG16:
                    return 16;
                case K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG32:
                    return 32;
                case K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG64:
                    return 64;
                case K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG65:
                    return 0xFF;
                default:
                    throw new Exception("Not an option");
            }
        }

        public static K2A.Drivers.BACnetLib.BacnetMaxSegments GetSegmentsCount(byte max_segments)
        {
            if (max_segments == 0)
                return K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0;
            else if (max_segments <= 2)
                return K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG2;
            else if (max_segments <= 4)
                return K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG4;
            else if (max_segments <= 8)
                return K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG8;
            else if (max_segments <= 16)
                return K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG16;
            else if (max_segments <= 32)
                return K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG32;
            else if (max_segments <= 64)
                return K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG64;
            else
                return K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG65;
        }

        public Segmentation GetSegmentBuffer(K2A.Drivers.BACnetLib.BacnetMaxSegments max_segments)
        {
            if (max_segments == K2A.Drivers.BACnetLib.BacnetMaxSegments.MAX_SEG0) return null;
            Segmentation ret = new Segmentation();
            ret.buffer = GetEncodeBuffer(m_client.HeaderLength);
            ret.max_segments = GetSegmentsCount(max_segments);
            ret.window_size = m_proposed_window_size;
            return ret;
        }

        private K2A.Drivers.BACnetLib.EncodeBuffer EncodeSegmentHeader(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, bool more_follows)
        {
            K2A.Drivers.BACnetLib.EncodeBuffer buffer;
            bool is_segmented = false;
            if (segmentation == null)
                buffer = GetEncodeBuffer(m_client.HeaderLength);
            else
            {
                buffer = segmentation.buffer;
                is_segmented = segmentation.sequence_number > 0 | more_follows;
            }
            buffer.Reset(m_client.HeaderLength);

            //encode
            K2A.Drivers.BACnetLib.NPDU.Encode(buffer, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);

            //set segments limits
            buffer.max_offset = buffer.offset + GetMaxApdu();
            int apdu_header = K2A.Drivers.BACnetLib.APDU.EncodeComplexAck(buffer, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_COMPLEX_ACK | (is_segmented ? K2A.Drivers.BACnetLib.BacnetPduTypes.SEGMENTED_MESSAGE | K2A.Drivers.BACnetLib.BacnetPduTypes.SERVER : 0) | (more_follows ? K2A.Drivers.BACnetLib.BacnetPduTypes.MORE_FOLLOWS : 0), service, invoke_id, segmentation != null ? segmentation.sequence_number : (byte)0, segmentation != null ? segmentation.window_size : (byte)0);
            buffer.min_limit = (GetMaxApdu() - apdu_header) * (segmentation != null ? segmentation.sequence_number : 0);

            return buffer;
        }

        private bool EncodeSegment(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, out K2A.Drivers.BACnetLib.EncodeBuffer buffer, Action<K2A.Drivers.BACnetLib.EncodeBuffer> apdu_content_encode)
        {
            //encode (regular)
            buffer = EncodeSegmentHeader(adr, invoke_id, segmentation, service, false);
            apdu_content_encode(buffer);

            bool more_follows = (buffer.result & K2A.Drivers.BACnetLib.EncodeResult.NotEnoughBuffer) > 0;
            if (segmentation != null && more_follows)
            {
                //reencode in segmented
                EncodeSegmentHeader(adr, invoke_id, segmentation, service, true);
                apdu_content_encode(buffer);
                return true;
            }
            else if (more_follows)
                return true;
            else
            {
                return segmentation != null ? segmentation.sequence_number > 0 : false;
            }
        }

        // Handle the segmentation of several too hugh response (if it's accepted by the client)
        // used by ReadRange, ReadProperty, ReadPropertyMultiple & ReadFile responses
        private void HandleSegmentationResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, Action<K2A.Drivers.BACnetLib.BacnetClient.Segmentation> transmit)
        {

            //send first
            transmit(segmentation);

            if (segmentation == null || segmentation.buffer.result == K2A.Drivers.BACnetLib.EncodeResult.Good) return;

            //start new thread to handle the segment sequence (if required)
            System.Threading.ThreadPool.QueueUserWorkItem((o) =>
            {
                byte old_max_info_frames = Transport.MaxInfoFrames;
                Transport.MaxInfoFrames = segmentation.window_size;      //increase max_info_frames, to increase throughput. This might be against 'standard'
                while (true)
                {
                    bool more_follows = (segmentation.buffer.result & K2A.Drivers.BACnetLib.EncodeResult.NotEnoughBuffer) > 0;

                    //wait for segmentACK
                    if ((segmentation.sequence_number - 1) % segmentation.window_size == 0 || !more_follows)
                    {
                        if (!WaitForAllTransmits(TransmitTimeout))
                        {
                            BACnetDriver.Log.Error("Transmit timeout");
                            break;
                        }
                        byte current_number = segmentation.sequence_number;
                        if (!WaitForSegmentAck(adr, invoke_id, segmentation, this.Timeout))
                        {
                            BACnetDriver.Log.Error("Didn't get segmentACK");
                            break;
                        }
                        if (segmentation.sequence_number != current_number)
                        {
                            BACnetDriver.Log.Trace("Oh, a retransmit");
                            more_follows = true;
                        }
                    }
                    else
                    {
                        //a negative segmentACK perhaps
                        byte current_number = segmentation.sequence_number;
                        WaitForSegmentAck(adr, invoke_id, segmentation, 0);      //don't wait
                        if (segmentation.sequence_number != current_number)
                        {
                            BACnetDriver.Log.Trace("Oh, a retransmit");
                            more_follows = true;
                        }
                    }

                    if (more_follows)
                        //lock (m_lockObject) transmit(segmentation);
                        transmit(segmentation);
                    else
                        break;
                }
                Transport.MaxInfoFrames = old_max_info_frames;
            });
        }
        private void SendComplexAck(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, Action<K2A.Drivers.BACnetLib.EncodeBuffer> apdu_content_encode)
        {
            BACnetDriver.Log.Trace("Sending " + System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(service.ToString().ToLower()) + " ... ");

            //encode
            K2A.Drivers.BACnetLib.EncodeBuffer buffer;
            if (EncodeSegment(adr, invoke_id, segmentation, service, out buffer, apdu_content_encode))
            {
                //client doesn't support segments
                if (segmentation == null)
                {
                    BACnetDriver.Log.Info("Segmenation denied");
                    ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_APDU_TOO_LONG);
                    buffer.result = K2A.Drivers.BACnetLib.EncodeResult.Good;     //don't continue the segmentation
                    return;
                }

                //first segment? validate max segments
                if (segmentation.sequence_number == 0)  //only validate first segment
                {
                    if (segmentation.max_segments != 0xFF && segmentation.buffer.offset > (segmentation.max_segments * (GetMaxApdu() - 5)))      //5 is adpu header
                    {
                        BACnetDriver.Log.Info("Too much segmenation");
                        ErrorResponse(adr, service, invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses.ERROR_CLASS_SERVICES, K2A.Drivers.BACnetLib.BacnetErrorCodes.ERROR_CODE_ABORT_APDU_TOO_LONG);
                        buffer.result = K2A.Drivers.BACnetLib.EncodeResult.Good;     //don't continue the segmentation
                        return;
                    }
                    else
                        BACnetDriver.Log.Trace("Segmentation required");
                }

                //increment before ack can do so (race condition)
                unchecked { segmentation.sequence_number++; };
            }

            //send
            m_client.Send(buffer.buffer, m_client.HeaderLength, buffer.GetLength() - m_client.HeaderLength, adr, false, 0);
        }

        public void ReadPropertyResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyReference property, IEnumerable<K2A.Drivers.BACnetLib.BacnetValue> value)
        {
            // response could be segmented
            HandleSegmentationResponse(adr, invoke_id, segmentation, (o) =>
            {
                SendComplexAck(adr, invoke_id, segmentation, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_PROPERTY, (b) =>
                {
                    K2A.Drivers.BACnetLib.Services.EncodeReadPropertyAcknowledge(b, object_id, property.propertyIdentifier, property.propertyArrayIndex, value);
                });
            });
        }

        public void CreateObjectResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, K2A.Drivers.BACnetLib.BacnetObjectId object_id)
        {
            SendComplexAck(adr, invoke_id, segmentation, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_CREATE_OBJECT, (b) =>
            {
                K2A.Drivers.BACnetLib.Services.EncodeCreateObjectAcknowledge(b, object_id);
            });

        }
        public void ReadPropertyMultipleResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, IList<K2A.Drivers.BACnetLib.BacnetReadAccessResult> values)
        {
            // response could be segmented
            HandleSegmentationResponse(adr, invoke_id, segmentation, (o) =>
            {
                SendComplexAck(adr, invoke_id, segmentation, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_PROP_MULTIPLE, (b) =>
                {
                    K2A.Drivers.BACnetLib.Services.EncodeReadPropertyMultipleAcknowledge(b, values);
                });
            });
        }

        public void ReadRangeResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, K2A.Drivers.BACnetLib.BacnetObjectId object_id, K2A.Drivers.BACnetLib.BacnetPropertyReference property, K2A.Drivers.BACnetLib.BacnetResultFlags status, uint item_count, byte[] application_data, K2A.Drivers.BACnetLib.BacnetReadRangeRequestTypes request_type, uint first_sequence_no)
        {
            // response could be segmented
            HandleSegmentationResponse(adr, invoke_id, segmentation, (o) =>
            {
                SendComplexAck(adr, invoke_id, segmentation, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_RANGE, (b) =>
                {
                    K2A.Drivers.BACnetLib.Services.EncodeReadRangeAcknowledge(b, object_id, property.propertyIdentifier, property.propertyArrayIndex, K2A.Drivers.BACnetLib.BacnetBitString.ConvertFromInt((uint)status), item_count, application_data, request_type, first_sequence_no);
                });
            });
        }

        public void ReadFileResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, int position, uint count, bool end_of_file, byte[] file_buffer)
        {
            // response could be segmented
            HandleSegmentationResponse(adr, invoke_id, segmentation, (o) =>
            {
                SendComplexAck(adr, invoke_id, segmentation, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_READ_PROPERTY, (b) =>
                {
                    K2A.Drivers.BACnetLib.Services.EncodeAtomicReadFileAcknowledge(b, true, end_of_file, position, 1, new byte[][] { file_buffer }, new int[] { (int)count });
                });
            });
        }

        public void WriteFileResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, int position)
        {
            SendComplexAck(adr, invoke_id, segmentation, K2A.Drivers.BACnetLib.BacnetConfirmedServices.SERVICE_CONFIRMED_ATOMIC_WRITE_FILE, (b) =>
            {
                K2A.Drivers.BACnetLib.Services.EncodeAtomicWriteFileAcknowledge(b, true, position);
            });
        }

        public void ErrorResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id, K2A.Drivers.BACnetLib.BacnetErrorClasses error_class, K2A.Drivers.BACnetLib.BacnetErrorCodes error_code)
        {
            BACnetDriver.Log.Trace("Sending ErrorResponse ... ");
            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeError(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_ERROR, service, invoke_id);
            K2A.Drivers.BACnetLib.Services.EncodeError(b, error_class, error_code);
            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, adr, false, 0);
        }

        public void SimpleAckResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, K2A.Drivers.BACnetLib.BacnetConfirmedServices service, byte invoke_id)
        {
            BACnetDriver.Log.Trace("Sending SimpleAckResponse ... ");
            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeSimpleAck(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_SIMPLE_ACK, service, invoke_id);
            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, adr, false, 0);
        }

        public void SegmentAckResponse(K2A.Drivers.BACnetLib.BacnetAddress adr, bool negative, bool server, byte original_invoke_id, byte sequence_number, byte actual_window_size)
        {
            BACnetDriver.Log.Trace("Sending SimpleAckResponse ... ");
            K2A.Drivers.BACnetLib.EncodeBuffer b = GetEncodeBuffer(m_client.HeaderLength);
            K2A.Drivers.BACnetLib.NPDU.Encode(b, K2A.Drivers.BACnetLib.BacnetNpduControls.PriorityNormalMessage, adr.RoutedSource, null, DEFAULT_HOP_COUNT, K2A.Drivers.BACnetLib.BacnetNetworkMessageTypes.NETWORK_MESSAGE_WHO_IS_ROUTER_TO_NETWORK, 0);
            K2A.Drivers.BACnetLib.APDU.EncodeSegmentAck(b, K2A.Drivers.BACnetLib.BacnetPduTypes.PDU_TYPE_SEGMENT_ACK | (negative ? K2A.Drivers.BACnetLib.BacnetPduTypes.NEGATIVE_ACK : 0) | (server ? K2A.Drivers.BACnetLib.BacnetPduTypes.SERVER : 0), original_invoke_id, sequence_number, actual_window_size);
            m_client.Send(b.buffer, m_client.HeaderLength, b.offset - m_client.HeaderLength, adr, false, 0);
        }

        public bool WaitForAllTransmits(int timeout)
        {
            return m_client.WaitForAllTransmits(timeout);
        }

        public bool WaitForSegmentAck(K2A.Drivers.BACnetLib.BacnetAddress adr, byte invoke_id, Segmentation segmentation, int timeout)
        {
            bool signaled = m_last_segment_ack.Wait(adr, invoke_id, timeout);
            if (signaled)
            {
                segmentation.sequence_number = (byte)((m_last_segment_ack.sequence_number + 1) % 256);
                segmentation.window_size = m_last_segment_ack.window_size;
            }
            return signaled;
        }

        public void Dispose()
        {
            m_client.Dispose();
            m_client = null;
        }
    }
}
