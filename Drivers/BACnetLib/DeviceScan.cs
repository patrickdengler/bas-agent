﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using K2A.Core;

namespace K2A.Drivers.BACnetLib
{
    public class DeviceScan
    {
        private System.Timers.Timer tmrTimeOut = new System.Timers.Timer();
        public int RangeHiLimit = 4194303;
        public int RangeLoLimit = 0;

        private int current_high;
        private int current_low;
        private int scanStep; //0 = idle, 1 = first pass, 2 = second pass, 3 = scan gaps
        private int gapCount;
        private Dictionary<string, IAm> devices;
        private List<ScanRange> missingRanges { get; set; }

        #region Delegates and Events

        public event ProgressChangedEventHandler ProgressChanged;

        public delegate void ProgressChangedEventHandler(int Percent, string Status);

        public event ScanCompleteEventHandler ScanComplete;

        public delegate void ScanCompleteEventHandler(List<IAm> Devices);

        #endregion Delegates and Events

        public bool Busy
        {
            get
            {
                return scanStep > 0;
            }
        }

        public void Stop()
        {

            tmrTimeOut.Stop();

            if (devices == null) devices = new Dictionary<string, IAm>();
            devices.Clear();

            BACnetDriver.Instance.Iam -= BACnetDriver_Iam;
            tmrTimeOut.Elapsed -= tmrTimeout_Tick;
            scanStep = 0;

        }

        public void Start()
        {
            if (Busy)
            {
                throw new Exception("Scan already in progress");
            }

            BACnetDriver.Instance.Iam += BACnetDriver_Iam;
            tmrTimeOut = new Timer();
            tmrTimeOut.Interval = BACnetDriver.Instance.ConnectionInfo.WhoIsTimeout * 1000;
            tmrTimeOut.Elapsed += tmrTimeout_Tick;
            scanStep = 1;
            current_high = 0;
            current_low = 0;
            devices = new Dictionary<string, IAm>();
            Console.WriteLine("Starting Step 1");
            ProgressChanged?.Invoke(5, $"Scanning {RangeLoLimit} - {RangeHiLimit}");

            current_high = RangeHiLimit;
            current_low = RangeLoLimit;
            SendWhoIs(RangeLoLimit, RangeHiLimit);
            tmrTimeOut.Start();
        }
        void SendWhoIs(int loLimit, int hiLimit)
        {
            BACnetDriver.Instance.SendWhoIs(loLimit, hiLimit);
        }

        private void BACnetDriver_Iam(IAm e)
        {
            

            if (!Busy || scanStep > 3 || e.DeviceId < current_low || e.DeviceId > current_high) return;

            lock (devices)
            {
                if (!devices.ContainsKey(e.GetDeviceKey()))
                {
                    BACnetDriver.Log.Trace(e.GetDeviceKey());
                    Console.WriteLine("Found: " + e.DeviceId);
                    devices[e.GetDeviceKey()] = e;
                    tmrTimeOut.Stop();
                    tmrTimeOut.Start();
                }
            }
        }
        void tmrTimeout_Tick(object sender, ElapsedEventArgs e)
        {


            switch (scanStep)
            {
                case 1:
                    Console.WriteLine("Starting Step 2");
                    ProgressChanged?.Invoke(20, $"Scanning {RangeLoLimit} - {RangeHiLimit}");
                    scanStep++;
                    current_high = RangeHiLimit;
                    current_low = RangeLoLimit;
                    SendWhoIs(RangeLoLimit, RangeHiLimit);
                    break;

                case 2:
                    Console.WriteLine("Starting Step 3");
                    scanStep++;
                    tmrTimeOut.Stop();
                    BuildGaps();
                    ScanNextGap();
                    break;
                case 3:
                    ScanNextGap();
                    break;
            }
        }
        void ScanNextGap()
        {
            tmrTimeOut.Stop();
            tmrTimeOut.Interval = 1000;

            if (missingRanges.Count > 0)
            {
                current_high = missingRanges[0].RangeHigh;
                current_low = missingRanges[0].RangeLow;

                int percentComplete = 25 + (gapCount - missingRanges.Count).PercentOf(gapCount, 75);
                ProgressChanged?.Invoke(percentComplete, $"Scanning {current_low} - {current_high}");
                Console.WriteLine("Step 3 = " + current_low + " - " + current_high + "= " + percentComplete + "%");

                missingRanges.RemoveAt(0);

                SendWhoIs(current_low, current_high);
                tmrTimeOut.Start();
            }
            else
            {
                FinishScan();
            }

        }

        void BuildGaps()
        {
            current_low = RangeLoLimit;
            current_high = 0;
            missingRanges = new List<ScanRange>();

            lock (devices)
            {
                while (true)
                {

                    while (devices.Values.Any(d => d.DeviceId == current_low))
                    {
                        current_low += 1;
                    }
                    current_high = current_low;

                    while (!devices.Values.Where(d => d.DeviceId == current_high + 1).Any() && current_high < RangeHiLimit)
                    {
                        current_high += 1;
                    }

                    if (current_low > RangeHiLimit)
                    {
                        break;
                    }
                    else
                    {
                        missingRanges.Add(new ScanRange() { RangeLow = current_low, RangeHigh = current_high });
                        current_low = current_high + 1;
                    }
                }
            }

            gapCount = missingRanges.Count;
        }
        void FinishScan()
        {
            Console.WriteLine("Finished");
            
            ScanComplete?.Invoke(devices.Values.ToList());
            Stop();
        }

        internal class ScanRange
        {
            public int RangeLow { get; set; }
            public int RangeHigh { get; set; }
        }
    }
}
