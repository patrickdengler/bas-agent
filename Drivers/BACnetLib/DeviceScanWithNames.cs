﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using K2A.Core;

namespace K2A.Drivers.BACnetLib
{
    public class DeviceScanWithNames
    {
        public bool GetDescription { get; set; }

        private DeviceScan devScan;
        private BACnetLive bLive;
        private bool scanRun;
        private int objCount;
        private int objRemaining;
        public int RangeHiLimit
        {
            get
            {
                return devScan.RangeHiLimit;
            }
            set
            {
                devScan.RangeHiLimit = value;
            }
        }
        public int RangeLoLimit
        {
            get
            {
                return devScan.RangeLoLimit;
            }
            set
            {
                devScan.RangeLoLimit = value;
            }
        }

        private List<IAm> devices;

        #region Delegates and Events

        public event ProgressChangedEventHandler ProgressChanged;

        public delegate void ProgressChangedEventHandler(int Percent, string Status);

        public event ScanCompleteEventHandler ScanComplete;

        public delegate void ScanCompleteEventHandler(List<IAm> Devices);

        #endregion Delegates and Events


        public DeviceScanWithNames()
        {
            devScan = new DeviceScan();
            bLive = new BACnetLive();
            devices = new List<IAm>();

            devScan.ScanComplete += DevScan_ScanComplete;
            devScan.ProgressChanged += DevScan_ProgressChanged;
            bLive.LiveCompleted += BLive_LiveCompleted;
            bLive.onLiveRead += OnLiveRead;

        }

        private void BLive_LiveCompleted()
        {
            ScanComplete?.Invoke(devices);
            scanRun = false;

            bLive.StopLive();
            devScan.Stop();
        }

        private void DevScan_ProgressChanged(int Percent, string Status)
        {
            ProgressChanged?.Invoke(Percent.PercentOf(100, 50), Status);
        }

        private void DevScan_ScanComplete(List<IAm> foundDevices)
        {
            devices = foundDevices;
            foreach (var dev in devices)
            {
                if (devices.Where(d => d.DeviceId == dev.DeviceId).Count() == 1)
                {
                    BACnetDevice bDev = new BACnetDevice("", dev.DeviceId, 0, true);

                    BACnetObject modelObj = new BACnetObject(bDev, BacnetObjectTypes.OBJECT_DEVICE, dev.DeviceId, BacnetPropertyIds.PROP_MODEL_NAME, -1, dev);
                    BACnetObject nameObj;

                    if (GetDescription)
                    {
                        nameObj = new BACnetObject(bDev, BacnetObjectTypes.OBJECT_DEVICE, dev.DeviceId, BacnetPropertyIds.PROP_DESCRIPTION, -1, dev);
                    }
                    else
                    {
                        nameObj = new BACnetObject(bDev, BacnetObjectTypes.OBJECT_DEVICE, dev.DeviceId, BacnetPropertyIds.PROP_OBJECT_NAME, -1, dev);
                    }

                    bLive.AddItems(new List<BACnetObject>() { modelObj, nameObj });
                }
            }

            if (foundDevices.Count == 0)
            {
                objCount = 0;
                objRemaining = 0;

                ScanComplete?.Invoke(devices);
                scanRun = false;

                bLive.StopLive();
                devScan.Stop();
            }
            else
            {
                objCount = bLive.ObjectCount();
                objRemaining = objCount;
            }


            bLive.Start();
        }

        public void Start()
        {
            scanRun = true;
            devScan.Start();
        }
        public void Stop()
        {
            if (devScan != null) devScan.Stop();
            if (bLive != null) bLive.StopLive();

            scanRun = false;
        }

        private void OnLiveRead(BACnetObject itm, string err)
        {
            if (scanRun == false)
            {
                return;
            }

            IAm dev = (IAm)itm.UserData;

            switch (itm.PropId)
            {
                case BacnetPropertyIds.PROP_DESCRIPTION:
                case BacnetPropertyIds.PROP_OBJECT_NAME:
                    dev.DeviceName = itm.Value;
                    break;

                case BacnetPropertyIds.PROP_MODEL_NAME:
                    dev.ModelName = itm.Value;
                    break;
            }
            objRemaining--;

            Console.WriteLine($"{objRemaining} of {objCount} remaining");
            ProgressChanged?.Invoke((objCount - objRemaining).PercentOf(objCount, 50) + 50, "Retrieving Names");
        }
    }
}
