﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K2A.Drivers.BACnetLib
{
    public class BACnetObject
    {
        private BACnetDevice _Device;
        private DateTime _LastRead;
        private DateTime _LastRequest;
        private int _ObjInstance;
        private BacnetObjectTypes _ObjType;
        private int _PriorityArray;
        private BacnetPropertyIds _PropID;
        private object _UserData;
        private string _Value;
        private bool Serializing = false;

        public BACnetObject(BACnetDevice Device, BacnetObjectTypes ObjectType, int ObjectIns, BacnetPropertyIds PropID, int pArray, object UserData)
        {
            _Device = Device;
            _ObjType = ObjectType;
            _ObjInstance = ObjectIns;
            _PropID = PropID;
            _PriorityArray = pArray;
            _UserData = UserData;
            _Value = "";
            _LastRead = DateTime.UtcNow.AddYears(-5);
        }
        public string ToIdString()
        {
            return string.Format("{0},{1},{2},{3},{4}", DeviceInstance, Convert.ToInt32(_ObjType), ObjectInstance, Convert.ToInt32(_PropID), PriorityArray);
        }
        public BACnetObject(int DeviceID, BacnetObjectTypes ObjectType, int ObjectIns, BacnetPropertyIds PropID, int pArray, object UserData)
        {
            bool isNative = ((int)ObjectType <= 30);
            _Device = new BACnetDevice("", DeviceID, -1, isNative);
            _ObjType = ObjectType;
            _ObjInstance = ObjectIns;
            _PropID = PropID;
            _PriorityArray = pArray;
            _UserData = UserData;
            _Value = "";
            _LastRead = DateTime.UtcNow.AddYears(-5);
        }

        protected BACnetObject()
        {
            Serializing = true;
        }


        public static BACnetObject FromString(string strData, BACnetDevice dev)
        {
            if (string.IsNullOrEmpty(strData)) return null;

            string[] dta = strData.Split(',');

            BACnetObject bObject = new BACnetObject(dev, (BacnetObjectTypes)Convert.ToInt32(dta[0]), Convert.ToInt32(dta[1]), (BacnetPropertyIds)Convert.ToInt32(dta[2]), Convert.ToInt32(dta[3]), "");
            return bObject;
        }

        public static BACnetObject FromString(string strData)
        {
            if (string.IsNullOrEmpty(strData)) return null;

            return BACnetObject.FromString(strData, new BACnetDevice("", -1, -1, true));
        }

        public override string ToString()
        {
            string strData = string.Format("{0},{1},{2},{3}", Convert.ToInt32(_ObjType), ObjectInstance, Convert.ToInt32(_PropID), _PriorityArray);
            return strData;
        }

        #region Properties

        public int DeviceInstance
        {
            get { return _Device.DeviceID; }
        }

        public BACnetDevice DeviceObject
        {
            get { return _Device; }
            set { _Device = value; }
        }

        public bool IsProprietary
        {
            get { return (int)_ObjType > 27; }
        }

        public DateTime LastRequest
        {
            get { return _LastRequest; }
            set
            {
                _LastRequest = value;
                _sw.Reset();
                _sw.Start();
            }
        }

        private System.Diagnostics.Stopwatch _sw = new System.Diagnostics.Stopwatch();

        public int ObjectInstance
        {
            get
            {
                if (IsProprietary && _Device.DeviceArray >= 0)
                {
                    return _Device.DeviceArray;
                }
                else
                {
                    return _ObjInstance;
                }
            }
            set
            {
                if (Serializing)
                    _ObjInstance = value;
            }
        }

        public BacnetObjectTypes ObjectType
        {
            get { return _ObjType; }
            set
            {
                if (Serializing)
                    _ObjType = value;
            }
        }

        public int PriorityArray
        {
            get { return _PriorityArray; }
            set
            {
                if (Serializing)
                    _PriorityArray = value;
            }
        }

        public BacnetPropertyIds PropId
        {
            get { return _PropID; }
            set
            {
                //if (Serializing)
                _PropID = value;
            }
        }

        public DateTime TimeStamp
        {
            get { return _LastRead; }
            set { _LastRead = value; }
        }

        public object UserData
        {
            get { return _UserData; }
            set { _UserData = value; }
        }

        public string Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                _sw.Stop();
            }
        }

        public long LastReadExecuteTime
        {
            get
            {

                return _sw.ElapsedMilliseconds;
            }
        }

        #endregion Properties
    }
}
