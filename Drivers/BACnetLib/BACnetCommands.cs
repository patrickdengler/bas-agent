﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using K2A.Core;
namespace K2A.Drivers.BACnetLib
{
    public class BACnetCommands
    {
        public static string ReadProperty(int device_id, BacnetObjectTypes objType, int objId, BacnetPropertyIds propId, int array)
        {

            IList<BacnetValue> value_list = null;
            try
            {
                IAm adr = BACnetDriver.Instance.GetDeviceIAm(device_id);
                if (adr == null)
                {
                    return "Device Timeout";
                }

                value_list = null;
                UInt32 pArray = (array < 0) ? ASN1.BACNET_ARRAY_ALL : Convert.ToUInt32(array);

                BacnetObjectId objectId = new BacnetObjectId(objType, Convert.ToUInt32(objId));
                string error = BACnetDriver.Instance.Comm.ReadPropertyRequest(adr.Address, objectId, propId, out value_list, 0, pArray);

                if (!string.IsNullOrEmpty(error))
                {
                    return error;
                }
                else
                {
                    if (value_list != null && value_list.Count > 0)
                    {
                        if (value_list[0].Tag == BacnetApplicationTags.BACNET_APPLICATION_TAG_ERROR) BACnetDriver.Log.Error(string.Format("{0} - {1},{2},{3},{4}", value_list[0].ToString(), adr.DeviceId, objectId.ToString(), propId, array));
                        return value_list[0].ToString();
                    }
                    else
                    {
                        return "No Values";
                    }
                }
            }
            catch (Exception ex)
            {
                //Log Error
                return ex.Message;
            }
        }

        public static IList<BacnetValue> ReadPropertyArray(int device_id, BacnetObjectTypes objType, int objId, BacnetPropertyIds propId, int array)
        {

            IList<BacnetValue> value_list = null;
            try
            {
                IAm adr = BACnetDriver.Instance.GetDeviceIAm(device_id);
                if (adr == null)
                {
                    return null;
                }

                value_list = null;
                UInt32 pArray = (array < 0) ? ASN1.BACNET_ARRAY_ALL : Convert.ToUInt32(array);

                BacnetObjectId objectId = new BacnetObjectId(objType, Convert.ToUInt32(objId));
                string error = BACnetDriver.Instance.Comm.ReadPropertyRequest(adr.Address, objectId, propId, out value_list, 0, pArray);

                if (!string.IsNullOrEmpty(error))
                {
                    return null;
                }
                else
                {
                    return value_list;
                }
            }
            catch (Exception ex)
            {
                //Log Error
                return null;
            }
        }

        public static void WritePropertyAsync(int device_id, BacnetObjectTypes objType, int objId, BacnetPropertyIds propId, int array, string value)
        {
            System.Threading.ThreadPool.QueueUserWorkItem((o) =>
{
    WriteProperty(device_id, objType, objId, propId, array, value);
});
        }

        public static void WriteProperty(int device_id, BacnetObjectTypes objType, int objId, BacnetPropertyIds propId, int array, string value)
        {
            var b_value = new BacnetValue[1];

            if (string.IsNullOrEmpty(value) || value.ToLower() == "null")
                b_value[0] = new BacnetValue(BacnetApplicationTags.BACNET_APPLICATION_TAG_NULL, null);
            else if (objType == BacnetObjectTypes.OBJECT_BINARY_INPUT || objType == BacnetObjectTypes.OBJECT_BINARY_OUTPUT || objType == BacnetObjectTypes.OBJECT_BINARY_VALUE)
            {
                if (value.ToLower() == "false") value = "0";
                if (value.ToLower() == "true") value = "1";
                b_value[0] = new BacnetValue(BacnetApplicationTags.BACNET_APPLICATION_TAG_ENUMERATED, value.ToInteger());
            }
            //else if (objType == BacnetObjectTypes.OBJECT_MULTI_STATE_INPUT || objType == BacnetObjectTypes.OBJECT_MULTI_STATE_OUTPUT || objType == BacnetObjectTypes.OBJECT_MULTI_STATE_VALUE)
            //{
            //    if (value.ToLower() == "false") value = "0";
            //    if (value.ToLower() == "true") value = "1";
            //    b_value[0] = new BacnetValue(BacnetApplicationTags.BACNET_APPLICATION_TAG_ENUMERATED, value.ToInteger());
            //}
            else if (value.isNumeric())
                b_value[0] = new BacnetValue((float)value.ToDouble());
            else
                b_value[0] = new BacnetValue(value);


            try
            {
                IAm adr = BACnetDriver.Instance.GetDeviceIAm(device_id);
                if (adr == null)
                {
                    return;
                }

                if (!BACnetDriver.Instance.Comm.WritePropertyRequest(adr.Address, new BacnetObjectId(objType, Convert.ToUInt32(objId)), propId, Convert.ToUInt32(array), b_value))
                {
                    BACnetDriver.Log.Log(NLog.LogLevel.Error, "Unable to write property.");
                }
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Log(NLog.LogLevel.Error, "Unable to write property: " + ex.Message);
            }


        }
    }
}
