﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace K2A.Drivers.BACnetLib
{
    public class BACnetLiveObjectCollection
    {
        private Dictionary<string, List<BACnetObject>> lookup = new Dictionary<string, List<BACnetObject>>();

        public List<BACnetObject> PendingRead = new List<BACnetObject>();
        public Dictionary<string, BACnetObject> PendingResponse = new Dictionary<string, BACnetObject>();
        private Dictionary<int, List<BACnetObject>> pendingReadMultiple = new Dictionary<int, List<BACnetObject>>(); //hash by deviceid
        public int ObjectCount = 0;

        #region Properties

        public List<BACnetObject> this[string id]
        {
            get
            {
                if (lookup.ContainsKey(id))
                {
                    return lookup[id];
                }
                else
                {
                    return new List<BACnetObject>();
                }
            }
        }

        #endregion

        #region Public Methods

        public BACnetObject Add(BACnetObject bObj)
        {
            string idString = bObj.ToIdString();
            if (lookup.ContainsKey(idString))
            {
                lookup[idString].Add(bObj);
                ObjectCount++;
                //SetObjectCount();
                return null;
            }
            else
            {
                List<BACnetObject> newList = new List<BACnetObject>() { bObj };
                lookup[idString] = newList;
                PendingRead.Add(bObj);
                ObjectCount++;
                //SetObjectCount();
                return bObj;
            }

        }

        public List<BACnetObject> Add(List<BACnetObject> bObjs)
        {
            List<BACnetObject> newObjects = new List<BACnetObject>();

            if (bObjs != null)
            {
                foreach (var bObj in bObjs)
                {
                    var nObj = Add(bObj);
                    if (nObj != null) newObjects.Add(nObj);
                }

            }

            return newObjects;
        }

        public void Remove(string idString)
        {
            if (lookup.ContainsKey(idString))
            {
                PendingRead = PendingRead.Where(o => o.ToIdString() != idString).ToList();
                PendingResponse.Remove(idString);
                ObjectCount -= lookup[idString].Count;
                lookup.Remove(idString);
                //SetObjectCount();
            }

        }

        public void SetObjectCount()
        {
            try
            {
                ObjectCount = (from itm in lookup from v in itm.Value select v).Count();
            }
            catch
            {
                ObjectCount = 0;
            }
        }

        public int GetPendingCount()
        {
            //returns object count of objects that have been requested but not responded
            lock (pendingReadMultiple)
            {
                int pendingReadMultipleCount = (from d in pendingReadMultiple from v in d.Value select v.DeviceInstance).Count();
                return pendingReadMultipleCount + PendingResponse.Count;
            }
            
        }

        //public List<BACnetObject> GetAllObjects()
        //{
        //    try
        //    {
        //        return (from itm in lookup from v in itm.Value select v).ToList();
        //    }
        //    catch
        //    {
        //        return new List<BACnetObject>();
        //    }
        //}

        public void ObjectRequestSent(BACnetObject obj)
        {
            obj.LastRequest = DateTime.UtcNow;
            if (PendingRead.Contains(obj))
            {
                PendingRead.Remove(obj);
                PendingResponse.Add(obj.ToIdString(), obj);
            }
            else
            {
            }

        }

        public void ReadMultipleRequested(int deviceId, List<BACnetObject> objects)
        {
            lock (pendingReadMultiple) {pendingReadMultiple[deviceId] = objects;}

            foreach (var obj in objects)
            {
                ObjectRequestSent(obj);
            }
        }

        public bool HasReadMultipleBeenRequested(int deviceId)
        {
            return pendingReadMultiple.ContainsKey(deviceId);
        }

        public List<BACnetObject> ReadMultipleRequestError(int deviceId, string error)
        {
            List<BACnetObject> updatedObjects = new List<BACnetObject>();

            lock (pendingReadMultiple)
            {
                if (pendingReadMultiple.ContainsKey(deviceId))
                {
                    foreach (var obj in pendingReadMultiple[deviceId])
                    {
                        updatedObjects.AddRange(SetValue(obj.ToIdString(), error));
                    }
                    pendingReadMultiple.Remove(deviceId);
                }
            }


            return updatedObjects;
        }
        public void ReadMultipleSuccess(int deviceId)
        {
            lock (pendingReadMultiple) { pendingReadMultiple.Remove(deviceId); }
        }
        public List<BACnetObject> SetValue(string idString, string value)
        {
            List<BACnetObject> updatedObjects = new List<BACnetObject>();

            PendingResponse.Remove(idString);

            if (lookup.ContainsKey(idString))
            {
                var objs = lookup[idString];

                for (int i = 0; i < objs.Count; i++)
                {
                    BACnetObject MatchingObject = objs[i];

                    MatchingObject.TimeStamp = DateTime.UtcNow;
                    MatchingObject.Value = value;
                    updatedObjects.Add(MatchingObject);
                }
                ObjectCount -= lookup[idString].Count;
                lookup.Remove(idString);
            }
            //SetObjectCount();
            return updatedObjects;
        }

        public void Clear()
        {
            lookup.Clear();
            ObjectCount = 0;
            PendingRead.Clear();
            PendingResponse.Clear();
        }

        public string GetStatusString()
        {
            return string.Format("Total Objects: {0}, Pending Read: {1}, Pending Response: {2}", lookup.Count, PendingRead.Count, PendingResponse.Count);
        }

        #endregion
    }
}
