using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Threading.Tasks;
using K2A.Core;

namespace K2A.Drivers.BACnetLib
{
    /// <summary>
    /// This is the standard BACNet udp transport
    /// </summary>
    public class BacnetIpUdpProtocolTransport : K2A.Drivers.BACnetLib.IBacnetTransport, IDisposable
    {
        private System.Net.Sockets.UdpClient m_exclusive_conn;
        private int m_port;
        private bool running;

        private K2A.Drivers.BACnetLib.BVLC bvlc;

        public K2A.Drivers.BACnetLib.BVLC Bvlc { get { return bvlc; } }

        private bool m_dont_fragment;
        private int m_max_payload;
        private string m_local_endpoint;

        public K2A.Drivers.BACnetLib.BacnetAddressTypes Type { get { return K2A.Drivers.BACnetLib.BacnetAddressTypes.IP; } }
        public event MessageRecievedHandler MessageRecieved;
        public int SharedPort { get { return m_port; } }
        public int ExclusivePort { get { return ((IPEndPoint)m_exclusive_conn.Client.LocalEndPoint).Port; } }

        public int HeaderLength { get { return K2A.Drivers.BACnetLib.BVLC.BVLC_HEADER_LENGTH; } }
        public K2A.Drivers.BACnetLib.BacnetMaxAdpu MaxAdpuLength { get { return K2A.Drivers.BACnetLib.BVLC.BVLC_MAX_APDU; } }
        public byte MaxInfoFrames { get { return 0xff; } set { /* ignore */ } }     //the udp doesn't have max info frames
        public int MaxBufferLength { get { return m_max_payload; } }

        public BacnetIpUdpProtocolTransport(int port, bool dont_fragment = false, int max_payload = 1472, string local_endpoint_ip = "")
        {
            m_port = port;
            m_max_payload = max_payload;
            m_dont_fragment = dont_fragment;
            m_local_endpoint = local_endpoint_ip;

        }

        public override bool Equals(object obj)
        {
            if (!(obj is K2A.Drivers.BACnetLib.BacnetIpUdpProtocolTransport)) return false;
            K2A.Drivers.BACnetLib.BacnetIpUdpProtocolTransport a = (K2A.Drivers.BACnetLib.BacnetIpUdpProtocolTransport)obj;
            return a.m_port == m_port;
        }

        public override int GetHashCode()
        {
            return m_port.GetHashCode();
        }

        public override string ToString()
        {
            return "Udp:" + m_port;
        }

        private void Open()
        {
            System.Net.EndPoint ep = new IPEndPoint(IPAddress.Any, m_port);
            //if (!string.IsNullOrEmpty(m_local_endpoint)) ep = new IPEndPoint(IPAddress.Parse(m_local_endpoint), m_port);
            m_exclusive_conn = new System.Net.Sockets.UdpClient();
            m_exclusive_conn.ExclusiveAddressUse = true;
            m_exclusive_conn.Client.Bind((IPEndPoint)ep);
            m_exclusive_conn.DontFragment = m_dont_fragment;
            m_exclusive_conn.EnableBroadcast = true;

            bvlc = new K2A.Drivers.BACnetLib.BVLC(this);
            running = true;
        }

        private void Close()
        {
            running = false;
            m_exclusive_conn.Close();
            return;

            if (m_exclusive_conn != null)
            {
                IPEndPoint ep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), m_port);
                m_exclusive_conn.Send(new byte[1] { 0 }, 1, ep);

                var t = Task.Run(() => System.Threading.Thread.Sleep(2000));
                t.Wait();

                m_exclusive_conn.Client.Blocking = false;
                m_exclusive_conn.Client.Shutdown(System.Net.Sockets.SocketShutdown.Both);
                m_exclusive_conn.Client.Close();

                m_exclusive_conn.Close();



                //m_exclusive_conn.Client.Close();
                //m_exclusive_conn.Close();
                GC.Collect();
                //m_exclusive_conn.Close();
            }
        }
        public void Stop()
        {
            try
            {
                Close();
            }
            catch { }
            finally
            {
                var t = Task.Run(() => System.Threading.Thread.Sleep(2000));
                t.Wait();
            }
        }
        public void Start()
        {
            try
            {
                Open();
            }
            catch (Exception ex)
            {
                BACnetDriver.Log.Error("Error opening BACnet connection: " + ex.Message);
            }

            if (m_exclusive_conn != null)
                m_exclusive_conn.BeginReceive(OnReceiveData, m_exclusive_conn);

        }

        private void OnReceiveData(IAsyncResult asyncResult)
        {
            System.Net.Sockets.UdpClient conn = (System.Net.Sockets.UdpClient)asyncResult.AsyncState;
            try
            {
                IPEndPoint ep = new IPEndPoint(IPAddress.Any, 0);
                byte[] local_buffer;
                int rx = 0;

                try
                {
                    if (!running)
                    {
                        return;
                    }
                    local_buffer = conn.EndReceive(asyncResult, ref ep);
                    rx = local_buffer.Length;
                }
                catch (Exception) // ICMP port unreachable
                {
                    //restart data receive
                    conn.BeginReceive(OnReceiveData, conn);
                    return;
                }

                if (rx == 0)    // Empty frame : port scanner maybe
                {
                    //restart data receive
                    conn.BeginReceive(OnReceiveData, conn);
                    return;
                }

                try
                {
                    //verify message
                    K2A.Drivers.BACnetLib.BacnetAddress remote_address;
                    Convert((IPEndPoint)ep, out remote_address);
                    K2A.Drivers.BACnetLib.BacnetBvlcFunctions function;
                    int msg_length;
                    if (rx < K2A.Drivers.BACnetLib.BVLC.BVLC_HEADER_LENGTH)
                    {
                        BACnetDriver.Log.Info("Some garbage data got in");
                    }
                    else
                    {
                        // Basic Header lenght
                        int HEADER_LENGTH = bvlc.Decode(local_buffer, 0, out function, out msg_length, ep);

                        if (HEADER_LENGTH == -1)
                        {
                            Trace.WriteLine("Unknow BVLC Header");
                            return;
                        }

                        // response to BVLC_REGISTER_FOREIGN_DEVICE (could be BVLC_DISTRIBUTE_BROADCAST_TO_NETWORK ... but we are not a BBMD, don't care)
                        if (function == K2A.Drivers.BACnetLib.BacnetBvlcFunctions.BVLC_RESULT)
                        {
                            Trace.WriteLine("Receive Register as Foreign Device Response");
                        }

                        // a BVLC_FORWARDED_NPDU frame by a BBMD, change the remote_address to the original one (stored in the BVLC header) 
                        // we don't care about the BBMD address
                        if (function == K2A.Drivers.BACnetLib.BacnetBvlcFunctions.BVLC_FORWARDED_NPDU)
                        {
                            long ip = ((long)local_buffer[7] << 24) + ((long)local_buffer[6] << 16) + ((long)local_buffer[5] << 8) + (long)local_buffer[4];
                            int port = (local_buffer[8] << 8) + local_buffer[9];    // 0xbac0 maybe
                            ep = new IPEndPoint(ip, port);

                            Convert((IPEndPoint)ep, out remote_address);

                        }

                        if ((function == K2A.Drivers.BACnetLib.BacnetBvlcFunctions.BVLC_ORIGINAL_UNICAST_NPDU) || (function == K2A.Drivers.BACnetLib.BacnetBvlcFunctions.BVLC_ORIGINAL_BROADCAST_NPDU) || (function == K2A.Drivers.BACnetLib.BacnetBvlcFunctions.BVLC_FORWARDED_NPDU))
                            //send to upper layers
                            if ((MessageRecieved != null) && (rx > HEADER_LENGTH)) MessageRecieved(this, local_buffer, HEADER_LENGTH, rx - HEADER_LENGTH, remote_address);
                    }
                }
                catch (Exception ex)
                {
                    BACnetDriver.Log.Error("Exception in udp recieve: " + ex.Message);
                }
                finally
                {
                    //restart data receive
                    conn.BeginReceive(OnReceiveData, conn);
                }
            }
            catch (Exception ex)
            {
                //restart data receive
                if (conn.Client != null)
                {
                    BACnetDriver.Log.Error("Exception in Ip OnRecieveData: " + ex.Message);
                    conn.BeginReceive(OnReceiveData, conn);
                }
            }
        }

        public bool SendRegisterAsForeignDevice(IPEndPoint BBMD, short TTL)
        {
            if (BBMD.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                bvlc.SendRegisterAsForeignDevice(BBMD, TTL);
                return true;
            }
            return false;
        }
        public bool SendRemoteWhois(byte[] buffer, IPEndPoint BBMD, int msg_length)
        {
            if (BBMD.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                bvlc.SendRemoteWhois(buffer, BBMD, msg_length);
                return true;
            }
            return false;
        }

        public bool SendRemoteWhohas(byte[] buffer, IPEndPoint BBMD, int msg_length)
        {
            if (BBMD.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                bvlc.SendRemoteWhohas(buffer, BBMD, msg_length);
                return true;
            }
            return false;
        }

        public bool WaitForAllTransmits(int timeout)
        {
            //we got no sending queue in udp, so just return true
            return true;
        }

        public static string ConvertToHex(byte[] buffer, int length)
        {
            string ret = "";

            for (int i = 0; i < length; i++)
                ret += buffer[i].ToString("X2");

            return ret;
        }

        // Modif FC : used for BBMD communication
        public int Send(byte[] buffer, int data_length, IPEndPoint ep)
        {
            if (!running) return 0;
            try
            {
                return m_exclusive_conn.Send(buffer, data_length, ep);
                System.Threading.ThreadPool.QueueUserWorkItem((o) => m_exclusive_conn.Send(buffer, data_length, ep), null);
                return data_length;
            }
            catch
            {
                return 0;
            }
        }

        public int Send(byte[] buffer, int offset, int data_length, K2A.Drivers.BACnetLib.BacnetAddress address, bool wait_for_transmission, int timeout)
        {
            if (!running || m_exclusive_conn == null) return 0;

            //add header
            int full_length = data_length + HeaderLength;
            bvlc.Encode(buffer, offset - K2A.Drivers.BACnetLib.BVLC.BVLC_HEADER_LENGTH, address.net == 0xFFFF ? K2A.Drivers.BACnetLib.BacnetBvlcFunctions.BVLC_ORIGINAL_BROADCAST_NPDU : K2A.Drivers.BACnetLib.BacnetBvlcFunctions.BVLC_ORIGINAL_UNICAST_NPDU, full_length);

            //create end point
            IPEndPoint ep;
            Convert(address, out ep);

            try
            {
                //send
                return m_exclusive_conn.Send(buffer, full_length, ep);    //broadcasts are transported from our local unicast socket also
            }
            catch
            {
                return 0;
            }
        }

        public static void Convert(IPEndPoint ep, out K2A.Drivers.BACnetLib.BacnetAddress addr)
        {
            byte[] tmp1 = ep.Address.GetAddressBytes();
            byte[] tmp2 = BitConverter.GetBytes((ushort)ep.Port);
            Array.Reverse(tmp2);
            Array.Resize<byte>(ref tmp1, tmp1.Length + tmp2.Length);
            Array.Copy(tmp2, 0, tmp1, tmp1.Length - tmp2.Length, tmp2.Length);
            addr = new K2A.Drivers.BACnetLib.BacnetAddress(K2A.Drivers.BACnetLib.BacnetAddressTypes.IP, 0, tmp1);
        }

        public static void Convert(K2A.Drivers.BACnetLib.BacnetAddress addr, out IPEndPoint ep)
        {
            long ip_address = BitConverter.ToUInt32(addr.adr, 0);
            ushort port = (ushort)((addr.adr[4] << 8) | (addr.adr[5] << 0));
            ep = new IPEndPoint(ip_address, (int)port);
        }

        // A lot of problems on Mono (Raspberry) to get the correct broadcast @
        // so this method is overridable (this allows the implementation of operating system specific code)
        // Marc solution http://stackoverflow.com/questions/8119414/how-to-query-the-subnet-masks-using-mono-on-linux for instance
        //
        protected virtual K2A.Drivers.BACnetLib.BacnetAddress _GetBroadcastAddress()
        {
            // general broadcast
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse("255.255.255.255"), m_port);
            BACnetDriver.Log.Info("Getting broadcast address");
            //if (App.IsRunningOnMono())
            //{
            //    ep = GetMonoMask();
            //}
            //else
            //{
            foreach (System.Net.NetworkInformation.NetworkInterface adapter in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
                foreach (System.Net.NetworkInformation.UnicastIPAddressInformation ip in adapter.GetIPProperties().UnicastAddresses)
                    if (m_local_endpoint == ip.Address.ToString())
                    {
                        try
                        {
                            string[] strCurrentIP = ip.Address.ToString().Split('.');

                            string ipMask;
                            if (App.IsRunningOnMono())
                            {
                                ipMask = IPInfoTools.GetIPv4Mask(adapter.Id);
                            }
                            else
                            {
                                ipMask = ip.IPv4Mask.ToString();
                            }

                            string[] strIPNetMask = ipMask.Split('.');
                            StringBuilder BroadcastStr = new StringBuilder();
                            for (int i = 0; i < 4; i++)
                            {
                                BroadcastStr.Append(((byte)(int.Parse(strCurrentIP[i]) | ~int.Parse(strIPNetMask[i]))).ToString());
                                if (i != 3) BroadcastStr.Append('.');
                            }
                            ep = new IPEndPoint(IPAddress.Parse(BroadcastStr.ToString()), m_port);
                        }
                        catch
                        {
                            BACnetDriver.Log.Error("Unable to get subnet mask.");
                        }  //On mono IPv4Mask feature not implemented
                    }
            //}
            BACnetDriver.Log.Info($"Broadcast address: {ep.ToString()}");

            K2A.Drivers.BACnetLib.BacnetAddress broadcast;
            Convert(ep, out broadcast);
            broadcast.net = 0xFFFF;
            return broadcast;
        }


        private IPEndPoint GetMonoMask()
        {
            BACnetDriver.Log.Info("Getting mini agent broadcast address for " + m_local_endpoint);
            string ifconfig = "ifconfig".CommandToString();

            Regex regex = new Regex(@"\s+inet addr:([\d\.]+).*Mask:([\d\.]+)");
            var matches = regex.Matches(ifconfig);

            foreach (Match match in matches)
            {
                BACnetDriver.Log.Info("Adapter found: " + match.Groups[1].Value + ", Mask: " + match.Groups[2].Value);
                if (m_local_endpoint == match.Groups[1].Value)
                {
                    string[] strCurrentIP = match.Groups[1].Value.Split('.');
                    string[] strIPNetMask = match.Groups[2].Value.Split('.');
                    StringBuilder BroadcastStr = new StringBuilder();
                    for (int i = 0; i < 4; i++)
                    {
                        BroadcastStr.Append(((byte)(int.Parse(strCurrentIP[i]) | ~int.Parse(strIPNetMask[i]))).ToString());
                        if (i != 3) BroadcastStr.Append('.');
                    }
                    BACnetDriver.Log.Info("Using broadcast " + BroadcastStr.ToString());
                    return new IPEndPoint(IPAddress.Parse(BroadcastStr.ToString()), m_port);
                }
            }

            return new IPEndPoint(IPAddress.Parse("255.255.255.255"), m_port);
        }

        K2A.Drivers.BACnetLib.BacnetAddress BroadcastAddress = null;
        public K2A.Drivers.BACnetLib.BacnetAddress GetBroadcastAddress()
        {
            if (BroadcastAddress == null) BroadcastAddress = _GetBroadcastAddress();
            return BroadcastAddress;
        }

        // Give 0.0.0.0:xxxx if the socket is open with IPAddress.Any
        // Today only used by _GetBroadcastAddress method & the bvlc layer class in BBMD mode
        // Some more complex solutions could avoid this, that's why this property is virtual
        public virtual IPEndPoint LocalEndPoint
        {
            get
            {
                return (IPEndPoint)m_exclusive_conn.Client.LocalEndPoint;
            }
        }

        public void Dispose()
        {
            try
            {
                running = false;
                if (m_exclusive_conn != null) m_exclusive_conn.Close();
                m_exclusive_conn = null;
            }
            catch { }
        }
    }
}
