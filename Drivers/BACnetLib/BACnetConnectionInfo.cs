﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using K2A.Core;
namespace K2A.Connect.Models
{
    public class BACnetConnectionInfo
    {
        private string _LocalIP;
        public int APDU_Retries = 3;
        public int APDU_SegmentTimeout = 6000;
        public int APDU_Timeout = 6000;
        public int BBMD_UDP_Port = 47808;
        public string BBMDIP = "";
        public int DeviceID = 70;
        public bool embeddedGateway = false;
        public bool isIP = true;
        public int MaxForeignDevices = 0;
        public int NetworkNumber = 10;
        public string EthernetInterface = "";
        public int UDP_Port = 47808;
        public string Gateway = "";
        public string LocalIP
        {
            get
            {
                if (string.IsNullOrEmpty(_LocalIP))
                {
                    var ips = GetAdapterList();
                    if (ips.Count > 0)
                        _LocalIP = ips[0];
                    else
                        _LocalIP = "192.168.1.1";
                }
                return _LocalIP;
            }
            set
            {
                _LocalIP = value;
            }
        }
        public string BDTTable = "";
        public int ReadQueueSize = 10;
        public int WhoIsTimeout = 5;
        public int GatewayCacheTime = 60;
        public bool DisableReadMultiple = false;
        public int ListenOnIPAddress = 0;

        public static BACnetConnectionInfo Deserialize(string data)
        {
            if (string.IsNullOrEmpty(data)) return new BACnetConnectionInfo();
            try
            {
                if (data.StartsWith("con"))
                {
                    var cmp = new CompressedString(data, CompressedString.InputDataTypeClass.Compressed, "m2mcon", "con", "m");
                    data = cmp.UnCompressed;
                    return JsonConvert.DeserializeObject<BACnetConnectionInfo>(data);
                }
            }
            catch
            {
                //EventLog.Log("Error reading Connection Info.", EventLog.EntryType.Error);
                K2A.Drivers.BACnetLib.BACnetDriver.Log.Error("Error reading Connection Info.");
            }
            return new BACnetConnectionInfo();
        }

        public static List<string> GetAdapterList()
        {
            List<string> adapters = new List<string>();

            try
            {
                string strHostName = Dns.GetHostName();
                // Then using host name, get the IP address list..
                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                IPAddress[] addr = ipEntry.AddressList;

                for (int i = 0; i < addr.Length; i++)
                {
                    Console.WriteLine("IP Address {0}: {1} ", i, addr[i].ToString());
                    if (addr[i].AddressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6)
                    {
                        if (!adapters.Contains(addr[i].ToString())) adapters.Add(addr[i].ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                //EventLog.Log("Error retrieving NIC Adapters: " + ex.Message, EventLog.EntryType.Error);
                K2A.Drivers.BACnetLib.BACnetDriver.Log.Error("Error retrieving NIC Adapters: " + ex.Message);
            }
            return adapters;
        }

        public string Serialize()
        {
            try
            {
                string data = JsonConvert.SerializeObject(this);
                var cmp = new CompressedString(data, CompressedString.InputDataTypeClass.UnCompressed, "m2mcon", "con", "m");
                return cmp.Compressed;
            }
            catch
            {
                //EventLog.Log("Error serializing Connection Info.", EventLog.EntryType.Error);
                K2A.Drivers.BACnetLib.BACnetDriver.Log.Error("Error serializing Connection Info.");
            }
            return "";
        }
    }
}