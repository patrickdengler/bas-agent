﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using K2A.Core;

namespace K2A.Drivers.ModbusLib
{
    public partial class ModbusTCPConfiguration
    {
        public List<ModbusTCPDevice> Devices = new List<ModbusTCPDevice>();

        private Dictionary<int, ModbusTCPDevice> lookup = new Dictionary<int, ModbusTCPDevice>();

        private int GetNextDeviceId()
        {
            if (Devices == null || Devices.Count == 0) return 1;

            int nxtId = Devices.Max(d => d.Id) + 1;

            return nxtId;
        }
        public ModbusTCPDevice GetDevice(int devId)
        {
            if (lookup == null) return null;

            lock (lookup)
            {
                if (lookup.ContainsKey(devId))
                    return lookup[devId];
                else
                    return null;
            }
        }

        public ModbusTCPObject GetObject(int devId, string objAddressId)
        {
            var dev = GetDevice(devId);

            if (dev == null || lookup == null) return null;

            return dev.GetObject(objAddressId);
        }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this).Compress();
        }


        public static ModbusTCPConfiguration Deserialize(string data)
        {
            ModbusTCPConfiguration config;

            try
            {
                config = JsonConvert.DeserializeObject<ModbusTCPConfiguration>(data.Decompress());
            }
            catch
            {
                config = new ModbusTCPConfiguration();
            }

            if (config == null) config = new ModbusTCPConfiguration();

            config.InitializeLookup();

            return config;
        }

        public void InitializeLookup()
        {
            lock (lookup)
            {
                lookup = new Dictionary<int, ModbusTCPDevice>();
                foreach (var dev in Devices)
                {
                    lookup.Add(dev.Id, dev);
                    dev.InitializeLookup();
                }
            }

        }

        public int AddDevice(ModbusTCPDevice device)
        {
            device.Id = GetNextDeviceId();
            Devices.Add(device);
            return device.Id;
        }

    }
}
