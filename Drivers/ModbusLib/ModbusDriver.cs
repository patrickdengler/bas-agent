﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Timers;
using System.Threading;

namespace K2A.Drivers.ModbusLib
{
    public class ModbusDriver
    {
        public ModbusTCPConfiguration Configuration;
        System.Timers.Timer tmrDisconnet;
        bool running;

        Dictionary<int, Worker> workerLookup = new Dictionary<int, Worker>();

        public delegate void ExceptionData(ModbusTCPConfiguration.ModbusTCPDevice modbusDevice, ModbusTCPConfiguration.ModbusTCPObject modbusObject);
        public event ExceptionData OnException;

        public delegate void ValueReceivedEventHandler(ModbusTCPConfiguration.ModbusTCPDevice modbusDevice, ModbusTCPConfiguration.ModbusTCPObject modbusObject);
        public event ValueReceivedEventHandler OnValueReceived;
        public bool Start(ModbusTCPConfiguration config)
        {
            try
            {
                Configuration = config;

                foreach (var dev in Configuration.Devices)
                {
                    var worker = new Worker(dev);
                    worker.OnException += ModbusDevice_OnException;
                    worker.OnValueReceived += ModbusDevice_OnValueReceived;

                    workerLookup.Add(dev.Id, worker);
                }

                tmrDisconnet = new System.Timers.Timer(15000);
                tmrDisconnet.Elapsed += (sender, e) => tmrDisconnect_Elapsed();
                tmrDisconnet.Start();

                running = true;
            }
            catch (SystemException error)
            {
                Console.WriteLine("Error: " + error.Message);
                running = false;
            }

            return running;
        }
        private void tmrDisconnect_Elapsed()
        {
            //Close all idle connections
            foreach (var drv in workerLookup.Values.Where(d => d.IdleTime() >= 10))
            {
                drv.Disconnect();
            }

            
        }

        public int GetPendingReadCount()
        {
            //return 0;
            return workerLookup.Select(w => w.Value.GetPendingReadCount()).Sum();
        }

        public void ReadValueAsync(int devId, string objectId, int maxDataAge = 15)
        {
            if (!workerLookup.ContainsKey(devId))
            {
                throw new Exception($"Invalid Device Id: {devId}");
            }

            var worker = workerLookup[devId];

            var obj = worker.TCPDevice.GetObject(objectId);

            if (obj == null)
            {
                throw new Exception($"Invalid Object: {objectId}");
            }

            if (obj.GetDataAge() <= maxDataAge)
            {
                ModbusDevice_OnValueReceived(worker.TCPDevice, obj);
            }
            else
            {
                worker.ReadAsync(obj);
            }
            
        }

        public void Stop()
        {
            foreach (var worker in workerLookup.Values.Where(d => d.Connected))
            {
                worker.Disconnect();
            }
        }

        void ModbusDevice_OnException(ModbusTCPConfiguration.ModbusTCPDevice dev, ModbusTCPConfiguration.ModbusTCPObject obj, ushort id, byte unit, byte function, byte exception)
        {
            OnException?.Invoke(dev, obj);
            Console.WriteLine($"Error: {Convert.ToString(exception)}");
        }



        private void ModbusDevice_OnValueReceived(ModbusTCPConfiguration.ModbusTCPDevice dev, ModbusTCPConfiguration.ModbusTCPObject obj)
        {
            OnValueReceived?.Invoke(dev, obj);
            //Console.WriteLine($"Value received: id= {obj.Id}, {obj.Name}= {obj.Value}");
        }
    }
}
