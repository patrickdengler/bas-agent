﻿using System;
using System.Linq;
using System.Collections;
using K2A.Core;

namespace K2A.Drivers.ModbusLib
{
    public partial class ModbusTCPConfiguration
    {
        public class ModbusTCPObject
        {
            public int GetDataAge()
            {
                int age = (int)(DateTime.UtcNow - LastRead).TotalSeconds;
                return age;
            }

            public double GetDataRequestTimeout()
            {
                return (LastReadRequest - LastRead).TotalSeconds;
            }
            public string GetAddressId()
            {
                return $"{StartAddress}.{Offset}";
            }

            public string GetDescription()
            {
                return $"{TableType.ToString()}-{StartAddress}";
            }

            public bool ZeroBaseAddress { get; set; } = true;
            public bool BigEndian { get; set; } = true;
            public ModbusValueType ValueType { get; set; }
            public ModbusTableType TableType { get; set; } = ModbusTableType.HoldingRegister;
            public string Name { get; set; }
            public double Factor { get; set; }
            public DateTime LastRead { get; set; }
            public DateTime LastReadRequest { get; set; }
            public string LastError { get; set; }

            object val;
            public object Value
            {
                get
                {
                    if (Factor != 0 && Factor != 1)
                    {
                        try
                        {
                            return (Convert.ToDouble(val)) * Factor;
                        }
                        catch
                        {
                            return val;
                        }
                    }

                    return val;
                }
                set
                {
                    val = value;
                }
            }
            public ushort Id { get; set; }
            public byte Unit { get; set; }

            public int Offset { get; set; }

            public ushort StartAddress { get; set; }
            public ushort NumInputs { get; set; }

            public void UpdateValue(byte[] data)
            {
                LastError = "";
                try
                {
                    if (BigEndian) data = data.Reverse().ToArray();

                    switch (ValueType)
                    {
                        case ModbusValueType.Binary:
                            Value = BitConverter.ToBoolean(data, Offset);
                            break;
                        case ModbusValueType.Signed16BitInteger:
                            Value = BitConverter.ToInt16(data, Offset);
                            break;
                        case ModbusValueType.Signed32BitInteger:
                            Value = BitConverter.ToInt32(data, Offset);
                            break;
                        case ModbusValueType.Unsigned16BitInteger:
                            Value = BitConverter.ToUInt16(data, Offset);
                            break;
                        case ModbusValueType.Unsigned32BitInteger:
                            Value = BitConverter.ToUInt32(data, Offset);
                            break;
                        case ModbusValueType.String:
                            Value = BitConverter.ToString(data, Offset);
                            break;
                        case ModbusValueType.SignedByte:
                            Value = Convert.ToSByte(data[Offset]);
                            break;
                        case ModbusValueType.UnsignedByte:
                            Value = data[Offset];
                            break;
                        case ModbusValueType.Bit:
                            var bits = new BitArray(data);
                            Value = bits[Offset];
                            break;

                    }

                    LastRead = DateTime.UtcNow;
                }
                catch (Exception ex)
                {
                    Logging.Log("Read Error: " + ex.Message, Logging.EntryType.Error);
                    LastError = ex.Message;
                }
            }

            public ModbusTCPObject()
            {
                LastRead = DateTime.UtcNow.AddYears(-1);
            }
            public ModbusTCPObject(string name, ModbusTableType tableType, ushort address, ModbusValueType valueType)
            {
                Name = name;
                TableType = tableType;
                StartAddress = address;
                ValueType = valueType;
                LastRead = DateTime.UtcNow.AddYears(-1);
            }
            public enum ModbusTableType
            {
                Coil = 1,
                DiscreteInputs = 2,
                HoldingRegister = 3,
                InputRegister = 4
            }
            public enum ModbusValueType
            {
                Binary,
                Unsigned16BitInteger,
                Signed16BitInteger,
                Unsigned32BitInteger,
                Signed32BitInteger,
                UnsignedByte,
                SignedByte,
                String,
                Bit
            }
        }

    }
}
