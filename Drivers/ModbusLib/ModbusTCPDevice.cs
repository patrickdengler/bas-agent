﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace K2A.Drivers.ModbusLib
{
    public partial class ModbusTCPConfiguration
    {
        public class ModbusTCPDevice
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string GetDescription()
            {
                return $"{Host}:{Port}";
            }
            private Dictionary<string, ModbusTCPObject> lookup = new Dictionary<string, ModbusTCPObject>();

            public List<ModbusTCPObject> Objects = new List<ModbusTCPObject>();
            public string Host { get; set; }
            public int Port { get; set; }

            public string SerializeValuesToJson()
            {
                var values = (from o in Objects select new { Id = o.GetAddressId(), Name = o.Name, Description = o.GetDescription(), Value = o.Value }).ToList();

                return JsonConvert.SerializeObject(values);
            }
            internal void InitializeLookup()
            {
                lock (lookup)
                {
                    lookup = Objects.ToDictionary(o => o.GetAddressId(), o => o);
                }
            }

            public ModbusTCPObject GetObject(string addressId)
            {
                if (lookup == null) return null;

                lock (lookup)
                {
                    if (lookup.ContainsKey(addressId))
                        return lookup[addressId];
                    else
                        return null;
                }
            }
        }

    }
}
