﻿using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Collections.Concurrent;
using K2A.Core.Timers;
using System.Linq;
using K2A.Core;

namespace K2A.Drivers.ModbusLib
{
    /// <summary>
    /// Modbus TCP common driver class. This class implements a modbus TCP master driver.
    /// It supports the following commands:
    /// 
    /// Read coils
    /// Read discrete inputs
    /// Write single coil
    /// Write multiple cooils
    /// Read holding register
    /// Read input register
    /// Write single register
    /// Write multiple register
    /// 
    /// All commands can be sent in synchronous or asynchronous mode. If a value is accessed
    /// in synchronous mode the program will stop and wait for slave to response. If the 
    /// slave didn't answer within a specified time a timeout exception is called.
    /// The class uses multi threading for both synchronous and asynchronous access. For
    /// the communication two lines are created. This is necessary because the synchronous
    /// thread has to wait for a previous command to finish.
    /// 
    /// </summary>
    public class Worker
    {
        // ------------------------------------------------------------------------
        // Constants for access
        private const byte fctReadCoil = 1;
        private const byte fctReadDiscreteInputs = 2;
        private const byte fctReadHoldingRegister = 3;
        private const byte fctReadInputRegister = 4;
        private const byte fctWriteSingleCoil = 5;
        private const byte fctWriteSingleRegister = 6;
        private const byte fctWriteMultipleCoils = 15;
        private const byte fctWriteMultipleRegister = 16;
        private const byte fctReadWriteMultipleRegister = 23;

        /// <summary>Constant for exception illegal function.</summary>
        public const byte excIllegalFunction = 1;
        /// <summary>Constant for exception illegal data address.</summary>
        public const byte excIllegalDataAdr = 2;
        /// <summary>Constant for exception illegal data value.</summary>
        public const byte excIllegalDataVal = 3;
        /// <summary>Constant for exception slave device failure.</summary>
        public const byte excSlaveDeviceFailure = 4;
        /// <summary>Constant for exception acknowledge.</summary>
        public const byte excAck = 5;
        /// <summary>Constant for exception slave is busy/booting up.</summary>
        public const byte excSlaveIsBusy = 6;
        /// <summary>Constant for exception gate path unavailable.</summary>
        public const byte excGatePathUnavailable = 10;
        /// <summary>Constant for exception not connected.</summary>
        public const byte excExceptionNotConnected = 253;
        /// <summary>Constant for exception connection lost.</summary>
        public const byte excExceptionConnectionLost = 254;
        /// <summary>Constant for exception response timeout.</summary>
        public const byte excExceptionTimeout = 255;
        /// <summary>Constant for exception wrong offset.</summary>
        private const byte excExceptionOffset = 128;
        /// <summary>Constant for exception send failt.</summary>
        private const byte excSendFault = 100;

        // ------------------------------------------------------------------------
        // Private declarations
        private static bool _connected = false;

        private Socket tcpAsyCl;
        private byte[] tcpAsyClBuffer = new byte[2048];

        private ushort nextId = 1;

        DateTime lastCheckForTimeouts;

        ManualResetEvent sendReset = new ManualResetEvent(true);

        Dictionary<ushort, ModbusTCPConfiguration.ModbusTCPObject> requests = new Dictionary<ushort, ModbusTCPConfiguration.ModbusTCPObject>();

        SimpleProcessTimer timeSinceUse = new SimpleProcessTimer();

        public ModbusTCPConfiguration.ModbusTCPDevice TCPDevice { get; set; }

        // ------------------------------------------------------------------------
        /// <summary>Response data event. This event is called when new data arrives</summary>
        public delegate void ResponseData(ModbusTCPConfiguration.ModbusTCPDevice modbusDevice, ushort id, byte unit, byte function, byte[] data);
        /// <summary>Response data event. This event is called when new data arrives</summary>
        public event ResponseData OnResponseData;
        /// <summary>Exception data event. This event is called when the data is incorrect</summary>
        public delegate void ExceptionData(ModbusTCPConfiguration.ModbusTCPDevice modbusDevice, ModbusTCPConfiguration.ModbusTCPObject modbusObject, ushort id, byte unit, byte function, byte exception);
        /// <summary>Exception data event. This event is called when the data is incorrect</summary>
        public event ExceptionData OnException;

        public delegate void ConnectionLostEventHandler(Worker worker);
        public event ConnectionLostEventHandler OnConnectionLost;

        public delegate void ValueReceivedEventHandler(ModbusTCPConfiguration.ModbusTCPDevice modbusDevice, ModbusTCPConfiguration.ModbusTCPObject modbusObject);
        public event ValueReceivedEventHandler OnValueReceived;

        // ------------------------------------------------------------------------
        /// <summary>Shows if a connection is active.</summary>
        public bool Connected
        {
            get { return _connected && tcpAsyCl != null && tcpAsyCl.Connected; }
        }

        public Worker(ModbusTCPConfiguration.ModbusTCPDevice tcpDevice)
        {
            TCPDevice = tcpDevice;
        }

        public bool Connect(bool logError = true)
        {
            if (TCPDevice == null) return false;

            if (string.IsNullOrEmpty(TCPDevice.Host))
            {
                Logging.Log("Invalid connection settings", Logging.EntryType.Error);
                return false;
            }
            try
            {
                // ----------------------------------------------------------------
                // Connect asynchronous client
                tcpAsyCl = new Socket(IPAddress.Parse(TCPDevice.Host).AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                tcpAsyCl.Connect(new IPEndPoint(IPAddress.Parse(TCPDevice.Host), TCPDevice.Port));
                tcpAsyCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, 1);

                tcpAsyCl.BeginReceive(tcpAsyClBuffer, 0, tcpAsyClBuffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), tcpAsyCl);

                _connected = true;
                return Connected;
            }
            catch (System.IO.IOException error)
            {
                _connected = false;
                Logging.Log("Unable to start Modbus TCP driver: " + error.Message, Logging.EntryType.Error);
                //throw (error);
            }

            return false;
        }

        // ------------------------------------------------------------------------
        /// <summary>Stop connection to slave.</summary>
        public void Disconnect()
        {
            if (tcpAsyCl != null)
            {
                if (tcpAsyCl.Connected)
                {
                    try { tcpAsyCl.Shutdown(SocketShutdown.Both); }
                    catch { }
                    tcpAsyCl.Close();
                }
                tcpAsyCl = null;
            }

            timeSinceUse.Stop();
            timeSinceUse.Clear();
        }

        // ------------------------------------------------------------------------
        /// <summary>Destroy master instance.</summary>
        ~Worker()
        {
            Dispose();
        }

        // ------------------------------------------------------------------------
        /// <summary>Destroy master instance</summary>
        public void Dispose()
        {
            Disconnect();
        }

        internal void CheckForTimeouts()
        {

            lock (requests)
            {
                if (lastCheckForTimeouts.AddSeconds(1) < DateTime.UtcNow)
                {
                    var timedOut = (from r in requests where r.Value.GetDataRequestTimeout() > 5 select r).ToList();

                    foreach (var r in timedOut)
                    {
                        CallException(r.Key, r.Value.Unit, Convert.ToByte((int)r.Value.TableType), excExceptionTimeout);
                        requests.Remove(r.Key);
                    }
                    lastCheckForTimeouts = DateTime.UtcNow;
                }
            }

        }

        internal int GetPendingReadCount()
        {
            CheckForTimeouts();

            lock (requests)
            {
                return requests.Count;
            }
        }

        internal void CallException(ushort id, byte unit, byte function, byte exception)
        {
            if (tcpAsyCl == null) return;
            if (exception == excExceptionConnectionLost)
            {
                tcpAsyCl = null;
                OnConnectionLost?.Invoke(this);
            }



            lock (requests)
            {
                if (requests.ContainsKey(id))
                {
                    var obj = requests[id];
                    if (OnException != null) OnException(TCPDevice, obj, id, unit, function, exception);
                    requests.Remove(id);
                }

            }
        }

        internal static UInt16 SwapUInt16(UInt16 inValue)
        {
            return (UInt16)(((inValue & 0xff00) >> 8) |
                     ((inValue & 0x00ff) << 8));
        }


        public int IdleTime()
        {
            CheckForTimeouts();
            return Connected ? (int)timeSinceUse.Elapsed.TotalSeconds : -1;
        }

        public ushort ReadAsync(ModbusTCPConfiguration.ModbusTCPObject obj)
        {
            if (obj == null) return 0;

            if (!Connected && !Connect())
            {
                Logging.Log($"Unable to connect to {TCPDevice.GetDescription()}", Logging.EntryType.Error);
                return 0;
            }

            lock (requests)
            {
                ushort id = nextId;

                obj.Id = id;
                requests[id] = obj;
                obj.LastReadRequest = DateTime.UtcNow;
                WriteAsyncData(CreateReadHeader(id, obj.Unit, obj.ZeroBaseAddress ? (ushort)(obj.StartAddress - 1) : obj.StartAddress, obj.NumInputs, Convert.ToByte((int)obj.TableType)), id);

                nextId++;
                if (nextId >= 10000) nextId = 1;

                return id;
            }
        }


        //// ------------------------------------------------------------------------
        ///// <summary>Write single coil in slave asynchronous. The result is given in the response function.</summary>
        ///// <param name="id">Unique id that marks the transaction. In asynchonous mode this id is given to the callback function.</param>
        ///// <param name="unit">Unit identifier (previously slave address). In asynchonous mode this unit is given to the callback function.</param>
        ///// <param name="startAddress">Address from where the data read begins.</param>
        ///// <param name="OnOff">Specifys if the coil should be switched on or off.</param>
        public void WriteSingleCoils(ushort id, byte unit, ushort startAddress, bool OnOff)
        {
            byte[] data;
            data = CreateWriteHeader(id, unit, startAddress, 1, 1, fctWriteSingleCoil);
            if (OnOff == true) data[10] = 255;
            else data[10] = 0;
            WriteAsyncData(data, id);
        }

        // ------------------------------------------------------------------------
        /// <summary>Write multiple coils in slave asynchronous. The result is given in the response function.</summary>
        /// <param name="id">Unique id that marks the transaction. In asynchonous mode this id is given to the callback function.</param>
        /// <param name="unit">Unit identifier (previously slave address). In asynchonous mode this unit is given to the callback function.</param>
        /// <param name="startAddress">Address from where the data read begins.</param>
        /// <param name="numBits">Specifys number of bits.</param>
        /// <param name="values">Contains the bit information in byte format.</param>
        public void WriteMultipleCoils(ushort id, byte unit, ushort startAddress, ushort numBits, byte[] values)
        {
            byte numBytes = Convert.ToByte(values.Length);
            byte[] data;
            data = CreateWriteHeader(id, unit, startAddress, numBits, (byte)(numBytes + 2), fctWriteMultipleCoils);
            Array.Copy(values, 0, data, 13, numBytes);
            WriteAsyncData(data, id);
        }

        // ------------------------------------------------------------------------
        /// <summary>Write single register in slave asynchronous. The result is given in the response function.</summary>
        /// <param name="id">Unique id that marks the transaction. In asynchonous mode this id is given to the callback function.</param>
        /// <param name="unit">Unit identifier (previously slave address). In asynchonous mode this unit is given to the callback function.</param>
        /// <param name="startAddress">Address to where the data is written.</param>
        /// <param name="values">Contains the register information.</param>
        public void WriteSingleRegister(ushort id, byte unit, ushort startAddress, byte[] values)
        {
            byte[] data;
            data = CreateWriteHeader(id, unit, startAddress, 1, 1, fctWriteSingleRegister);
            data[10] = values[0];
            data[11] = values[1];
            WriteAsyncData(data, id);
        }

        // ------------------------------------------------------------------------
        /// <summary>Write multiple registers in slave asynchronous. The result is given in the response function.</summary>
        /// <param name="id">Unique id that marks the transaction. In asynchonous mode this id is given to the callback function.</param>
        /// <param name="unit">Unit identifier (previously slave address). In asynchonous mode this unit is given to the callback function.</param>
        /// <param name="startAddress">Address to where the data is written.</param>
        /// <param name="values">Contains the register information.</param>
        public void WriteMultipleRegister(ushort id, byte unit, ushort startAddress, byte[] values)
        {
            ushort numBytes = Convert.ToUInt16(values.Length);
            if (numBytes % 2 > 0) numBytes++;
            byte[] data;

            data = CreateWriteHeader(id, unit, startAddress, Convert.ToUInt16(numBytes / 2), Convert.ToUInt16(numBytes + 2), fctWriteMultipleRegister);
            Array.Copy(values, 0, data, 13, values.Length);
            WriteAsyncData(data, id);
        }

        // ------------------------------------------------------------------------
        /// <summary>Read/Write multiple registers in slave asynchronous. The result is given in the response function.</summary>
        /// <param name="id">Unique id that marks the transaction. In asynchonous mode this id is given to the callback function.</param>
        /// <param name="unit">Unit identifier (previously slave address). In asynchonous mode this unit is given to the callback function.</param>
        /// <param name="startReadAddress">Address from where the data read begins.</param>
        /// <param name="numInputs">Length of data.</param>
        /// <param name="startWriteAddress">Address to where the data is written.</param>
        /// <param name="values">Contains the register information.</param>
        public void ReadWriteMultipleRegister(ushort id, byte unit, ushort startReadAddress, ushort numInputs, ushort startWriteAddress, byte[] values)
        {
            ushort numBytes = Convert.ToUInt16(values.Length);
            if (numBytes % 2 > 0) numBytes++;
            byte[] data;

            data = CreateReadWriteHeader(id, unit, startReadAddress, numInputs, startWriteAddress, Convert.ToUInt16(numBytes / 2));
            Array.Copy(values, 0, data, 17, values.Length);
            WriteAsyncData(data, id);
        }

        // ------------------------------------------------------------------------
        // Create modbus header for read action
        private byte[] CreateReadHeader(ushort id, byte unit, ushort startAddress, ushort length, byte function)
        {
            byte[] data = new byte[12];

            byte[] _id = BitConverter.GetBytes((short)id);
            data[0] = _id[1];			    // Slave id high byte
            data[1] = _id[0];				// Slave id low byte
            data[5] = 6;					// Message size
            data[6] = unit;					// Slave address
            data[7] = function;				// Function code
            byte[] _adr = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startAddress));
            data[8] = _adr[0];				// Start address
            data[9] = _adr[1];				// Start address
            byte[] _length = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)length));
            data[10] = _length[0];			// Number of data to read
            data[11] = _length[1];			// Number of data to read
            return data;
        }

        // ------------------------------------------------------------------------
        // Create modbus header for write action
        private byte[] CreateWriteHeader(ushort id, byte unit, ushort startAddress, ushort numData, ushort numBytes, byte function)
        {
            byte[] data = new byte[numBytes + 11];

            byte[] _id = BitConverter.GetBytes((short)id);
            data[0] = _id[1];				// Slave id high byte
            data[1] = _id[0];				// Slave id low byte
            byte[] _size = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)(5 + numBytes)));
            data[4] = _size[0];				// Complete message size in bytes
            data[5] = _size[1];				// Complete message size in bytes
            data[6] = unit;					// Slave address
            data[7] = function;				// Function code
            byte[] _adr = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startAddress));
            data[8] = _adr[0];				// Start address
            data[9] = _adr[1];				// Start address
            if (function >= fctWriteMultipleCoils)
            {
                byte[] _cnt = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)numData));
                data[10] = _cnt[0];			// Number of bytes
                data[11] = _cnt[1];			// Number of bytes
                data[12] = (byte)(numBytes - 2);
            }
            return data;
        }

        // ------------------------------------------------------------------------
        // Create modbus header for read/write action
        private byte[] CreateReadWriteHeader(ushort id, byte unit, ushort startReadAddress, ushort numRead, ushort startWriteAddress, ushort numWrite)
        {
            byte[] data = new byte[numWrite * 2 + 17];

            byte[] _id = BitConverter.GetBytes((short)id);
            data[0] = _id[1];						// Slave id high byte
            data[1] = _id[0];						// Slave id low byte
            byte[] _size = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)(11 + numWrite * 2)));
            data[4] = _size[0];						// Complete message size in bytes
            data[5] = _size[1];						// Complete message size in bytes
            data[6] = unit;							// Slave address
            data[7] = fctReadWriteMultipleRegister;	// Function code
            byte[] _adr_read = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startReadAddress));
            data[8] = _adr_read[0];					// Start read address
            data[9] = _adr_read[1];					// Start read address
            byte[] _cnt_read = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)numRead));
            data[10] = _cnt_read[0];				// Number of bytes to read
            data[11] = _cnt_read[1];				// Number of bytes to read
            byte[] _adr_write = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startWriteAddress));
            data[12] = _adr_write[0];				// Start write address
            data[13] = _adr_write[1];				// Start write address
            byte[] _cnt_write = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)numWrite));
            data[14] = _cnt_write[0];				// Number of bytes to write
            data[15] = _cnt_write[1];				// Number of bytes to write
            data[16] = (byte)(numWrite * 2);

            return data;
        }

        // ------------------------------------------------------------------------
        // Write asynchronous data
        private void WriteAsyncData(byte[] write_data, ushort id)
        {
            timeSinceUse.Reset();

            try
            {
                EnqueueDataForWrite(tcpAsyCl, write_data);
            }
            catch
            {
                CallException(id, write_data[6], write_data[7], excSendFault);
            }
            //return;

            //if ((tcpAsyCl != null) && (tcpAsyCl.Connected))
            //{
            //    try
            //    {
            //        tcpAsyCl.BeginSend(write_data, 0, write_data.Length, SocketFlags.None, new AsyncCallback(OnSend), null);
            //        tcpAsyCl.BeginReceive(tcpAsyClBuffer, 0, tcpAsyClBuffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), tcpAsyCl);
            //    }
            //    catch (SystemException)
            //    {
            //        CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
            //    }
            //}
            //else CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
        }

        // ------------------------------------------------------------------------
        // Write asynchronous data acknowledge
        //private void OnSend(System.IAsyncResult result)
        //{
        //    if (result.IsCompleted == false) CallException(0xFFFF, 0xFF, 0xFF, excSendFault);   
        //}

        // ------------------------------------------------------------------------
        // Write asynchronous data response
        private void OnReceive(System.IAsyncResult result)
        {
            timeSinceUse.Reset();

            if (tcpAsyCl == null || !Connected) return;

            int byteCount;

            try
            {
                byteCount = tcpAsyCl.EndReceive(result);
            }
            catch
            {
                return;
            }
            //Console.WriteLine(byteCount + " bytes received.");
            //tcpAsyCl.BeginReceive(tcpAsyClBuffer, 0, tcpAsyClBuffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), tcpAsyCl);

            //return;

            if (result.IsCompleted == false) CallException(0xFF, 0xFF, 0xFF, excExceptionConnectionLost);

            ushort id = SwapUInt16(BitConverter.ToUInt16(tcpAsyClBuffer, 0));
            byte unit = tcpAsyClBuffer[6];
            byte function = tcpAsyClBuffer[7];
            byte[] data;

            // ------------------------------------------------------------
            // Write response data
            if ((function >= fctWriteSingleCoil) && (function != fctReadWriteMultipleRegister))
            {
                data = new byte[2];
                Array.Copy(tcpAsyClBuffer, 10, data, 0, 2);
            }
            // ------------------------------------------------------------
            // Read response data
            else
            {
                data = new byte[tcpAsyClBuffer[8]];
                Array.Copy(tcpAsyClBuffer, 9, data, 0, tcpAsyClBuffer[8]);
            }
            // ------------------------------------------------------------
            // Response data is slave exception
            if (function > excExceptionOffset)
            {
                function -= excExceptionOffset;
                CallException(id, unit, function, tcpAsyClBuffer[8]);
            }
            // ------------------------------------------------------------
            // Response data is regular data
            else if (OnResponseData != null) OnResponseData(TCPDevice, id, unit, function, data);


            lock (requests)
            {
                if (requests.ContainsKey(id))
                {
                    var obj = requests[id];
                    obj.UpdateValue(data);
                    requests.Remove(id);
                    OnValueReceived?.Invoke(TCPDevice, obj);
                    
                }

            }
            //Console.WriteLine("Received id " + id);

            try
            {
                tcpAsyCl.BeginReceive(tcpAsyClBuffer, 0, tcpAsyClBuffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), tcpAsyCl);
                sendReset.Set();
            }
            catch { }


        }

        ConcurrentQueue<byte[]> writePendingData = new ConcurrentQueue<byte[]>();
        bool sendingData = false;

        void EnqueueDataForWrite(Socket socket, byte[] buffer)
        {
            if (buffer == null)
                return;

            writePendingData.Enqueue(buffer);

            lock (writePendingData)
            {
                if (sendingData)
                {
                    return;
                }
                else
                {
                    sendingData = true;
                }
            }

            Write(socket);
        }

        void Write(Socket socket)
        {
            byte[] buffer = null;

            if (!Connected) Connect();

            if (!Connected) return;

            try
            {
                if (writePendingData.Count > 0 && writePendingData.TryDequeue(out buffer))
                {
                    sendReset.WaitOne(3000);
                    socket.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, WriteCallback, socket);
                    sendReset.Reset();
                }
                else
                {
                    lock (writePendingData)
                    {
                        sendingData = false;
                    }
                }
            }
            catch (Exception ex)
            {
                // handle exception then
                lock (writePendingData)
                {
                    sendingData = false;
                }
            }
        }

        private void WriteCallback(IAsyncResult ar)
        {
            Socket socket = (Socket)ar.AsyncState;

            try
            {
                socket.EndSend(ar);
            }
            catch (Exception ex)
            {
                // handle exception                
            }

            Write(socket);
        }
    }
}
